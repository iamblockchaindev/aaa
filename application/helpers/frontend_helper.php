<?php

	if(!function_exists('getSessionData'))
	{
	 	function getSessionData()
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$CI->session->userdata('user_id')));

			$data = $CI->db->get('pw_users');

			if($data->num_rows() > 0)
			{
				return $data->row();		
			}
			else
			{
				return false;
			}
		}
	}

	function get_pwt() {

		$CI=&get_instance();
        return $CI->db->get('pwt_set_price')->row_array();
	}

	if(!function_exists('getCountryList'))
	{
	 	function getCountryList()
		{
			$CI =& get_instance();

			$data = $CI->db->get('pw_countries');

			if($data->num_rows() > 0)
			{
				return $data->result();		
			}
			else
			{
				return false;
			}
		}
	}

	if(!function_exists('getCountryNameByID'))
	{
	 	function getCountryNameByID($country_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$country_id));
			$data = $CI->db->get('pw_countries');
			$result =  $data->row();		

			return $result->name;
		}

	}

	if(!function_exists('getStateListByCountryID'))
	{
	 	function getStateListByCountryID($country_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('country_id'=>$country_id));
			$data = $CI->db->get('pw_states');
			return $data->result();		
		}

	}

	if(!function_exists('getStateNameByID'))
	{
	 	function getStateNameByID($state_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$state_id));
			$data = $CI->db->get('pw_states');
			$result =  $data->row();		

			return $result->name;
		}

	}

	if(!function_exists('getUserNameByID'))
	{
	 	function getUserNameByID($user_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$user_id));
			$data = $CI->db->get('pw_users');
			$result =  $data->row();		

			return $result->full_name;
		}

	}

	if(!function_exists('getMemberNameByID'))
	{
	 	function getMemberNameByID($user_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$user_id));
			$data = $CI->db->get('pw_users');
			$result =  $data->row();		

			return $result->username;
		}

	}

	if(!function_exists('getPackageDetailsByID'))
	{
	 	function getPackageDetailsByID($id)
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$id));
			$data = $CI->db->get('pw_package');
			
			if($data->num_rows() > 0)
			{
				return $data->row();
			}
			else
			{
				return false;
			}

		}
	}

	if(!function_exists('getPackageList'))
	{
	 	function getPackageList()
		{
			$CI =& get_instance();

			$CI->db->where(array('status'=>'1'));
			$data = $CI->db->get('pw_package');
			
			if($data->num_rows() > 0)
			{
				return $data->result();
			}
			else
			{
				return false;
			}

		}
	}

	if(!function_exists('getSponsorNameByUserID'))
	{
	 	function getSponsorNameByUserID($user_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$user_id));
			$data = $CI->db->get('pw_users');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return $result->sponsor;
			}
			else
			{
				return '';
			}

		}
	}

	if(!function_exists('getMyNextMembershipToUpgrade'))
	{
	 	function getMyNextMembershipToUpgrade()
		{
			$CI =& get_instance();

			$CI->db->where(array('user_id'=>$CI->session->userdata('user_id')));
			$CI->db->order_by("id", "desc");
			$data = $CI->db->get('pw_non_working_user');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				if($result->package == 30)
				{
					$next_package = 40;
				}
				else if($result->package == 40)
				{
					$next_package = 100;
				}
				else if($result->package == 100)
				{
					$next_package = 1000;
				}
				else if($result->package == 1000)
				{
					$next_package = 5000;
				}
				else if($result->package == 5000)
				{
					$next_package = 10000;
				}
				else if($result->package == 10000)
				{
					$next_package = '';
				}

				return $next_package;
			}
			else
			{
				return '';
			}
		}
	}

	function getMyNextMembershipToUpgrade()
		{
			$CI =& get_instance();

			$CI->db->where(array('user_id'=>$CI->session->userdata('user_id')));
			$CI->db->order_by("id", "desc");
			$CI->db->limit("1");
			$data = $CI->db->get('pw_non_working_user');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				if($result->package == 30)
				{
					$next_package = 40;
				}
				else if($result->package == 40)
				{
					$next_package = 100;
				}
				else if($result->package == 100)
				{
					$next_package = 1000;
				}
				else if($result->package == 1000)
				{
					$next_package = 5000;
				}
				else if($result->package == 5000)
				{
					$next_package = 10000;
				}
				else if($result->package == 10000)
				{
					$next_package = '';
				}

				return $next_package;
			}
			else
			{
				return '';
			}
		}


	if(!function_exists('getMyWithdrawableAmountByPackage'))
	{
	 	function getMyWithdrawableAmountByPackage($package)
		{
			$CI =& get_instance();

			$CI->db->where(array('user_id'=>$CI->session->userdata('user_id'), 'package'=>$package));
			$data = $CI->db->get('pw_non_working_user');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return number_format($result->withdrawable_amount, '2');
			}
			else
			{
				return 0.00;
			}
		}
	}

	if(!function_exists('getMyWithdrawlByPackage'))
	{
	 	function getMyWithdrawlByPackage($package)
		{
			$CI =& get_instance();

			$CI->db->where(array('user_id'=>$CI->session->userdata('user_id'), 'package'=>$package));
			$data = $CI->db->get('pw_non_working_user');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return number_format($result->withdrawal, '2');
			}
			else
			{
				return 0.00;
			}
		}
	}

	if(!function_exists('getMyBalanceByPackage'))
	{
	 	function getMyBalanceByPackage($package)
		{
			$CI =& get_instance();

			$CI->db->where(array('user_id'=>$CI->session->userdata('user_id'), 'package'=>$package));
			$data = $CI->db->get('pw_non_working_user');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return number_format($result->balance, '2');
			}
			else
			{
				return 0.00;
			}
		}
	}

	if(!function_exists('isAccountDetailsUpdated'))
	{
	 	function isAccountDetailsUpdated()
		{
			$CI =& get_instance();

			$CI->db->where(array('user_id'=>$CI->session->userdata('user_id')));
			$data = $CI->db->get('pw_users_bank_details');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				if((!empty($result->eth_address)) || !empty($result->btc_address))
				{
					return 1;
				}
				else
				{
					return 2;
				}
				
			}
			else
			{
				return 2;
			}
		}
	}

	if(!function_exists('getFirstLevelDownlineUsersCountByUserID'))
	{
	 	function getFirstLevelDownlineUsersCountByUserID($user_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('sponsor_user_id'=>$user_id));
			$data = $CI->db->get('pw_working_tree');
			
			return $data->num_rows();			
		}
	}

	if(!function_exists('getLatestRankByUserID'))
	{
	 	function getLatestRankByUserID($user_id)
		{
			$CI =& get_instance();

			$CI->db->order_by('id', 'desc');
			$CI->db->where(array('user_id'=>$user_id));
			$data = $CI->db->get('pw_non_working_user');
			
			if($data->num_rows() > 0)
			{
				$row = $data->row();
				return $row->package;
			}
			else
			{
				return '';
			}
		}
	}

	if(!function_exists('getotp'))
	{
	 	function getotp($digits)
		{
			$otp = rand(pow(10, $digits-1), pow(10, $digits)-1);

			return $otp;
		}
	}

	function otp_send_eligiblity($otp_send_datetime)
	{
		$time = strtotime($otp_send_datetime);
		$time = $time + (1 * 60);
		$datetime = date("Y-m-d H:i:s", $time);

		$current_datetime = date("Y-m-d H:i:s");

		if($datetime < $current_datetime)
		{
			return 2; // send another otp
		}
		else
		{
			return 1; //
		}
	}	

	function timeIntervalinMin()
	{
		$CI =& get_instance();
		
		if($CI->session->userdata('timer'))
		{
			$now = time();
			//Calculate how many seconds have passed.
			$timeSince = $now - $CI->session->userdata('timer');		
			return $timeSince;
		}
		else
		{
			return 0;
		}
	}	

	if(!function_exists('getMyWorkingLevelIncome'))
	{
	 	function getMyWorkingLevelIncome()
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$CI->session->userdata('user_id')));
			$data = $CI->db->get('pw_users');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();
				return $result->level_income_wallet;
				//return number_format($result->level_income_wallet, '2');
			}
			else
			{
				return 0.00;
			}
		}
	}

	if(!function_exists('getMyWorkingLevelWithdrawal'))
	{
	 	function getMyWorkingLevelWithdrawal()
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$CI->session->userdata('user_id')));
			$data = $CI->db->get('pw_users');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return number_format($result->level_income_withdrawal, '2');
			}
			else
			{
				return 0.00;
			}
		}
	}

	if(!function_exists('getMyWorkingLevelIncomeBalance'))
	{
	 	function getMyWorkingLevelIncomeBalance()
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$CI->session->userdata('user_id')));
			$data = $CI->db->get('pw_users');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return number_format($result->level_income_balance, '2');
			}
			else
			{
				return 0.00;
			}
		}
	}

	if(!function_exists('getMyDirectWalletIncome'))
	{
	 	function getMyDirectWalletIncome()
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$CI->session->userdata('user_id')));
			$data = $CI->db->get('pw_users');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return number_format($result->direct_income_wallet, '2');
			}
			else
			{
				return 0.00;
			}
		}
	}

	function getCryptoBonus()
	{

		$CI =& get_instance();
		$CI->db->select('crypto_bonus, bonus_balance, income_bonus');
		$CI->db->where('id',  $CI->session->userdata('user_id'));
		return $CI->db->get('pw_users')->row_array();		
	}
	

	if(!function_exists('getMyDirectWithdrawal'))
	{
	 	function getMyDirectWithdrawal()
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$CI->session->userdata('user_id')));
			$data = $CI->db->get('pw_users');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return number_format($result->direct_income_withdrawal, '2');
			}
			else
			{
				return 0.00;
			}
		}
	}

	if(!function_exists('getMyDirectWalletbalance'))
	{
	 	function getMyDirectWalletbalance()
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$CI->session->userdata('user_id')));
			$data = $CI->db->get('pw_users');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return number_format($result->direct_income_balance, '2');
			}
			else
			{
				return 0.00;
			}
		}
	}	

	if(!function_exists('getMyCurrentMembershipPackage'))
	{
	 	function getMyCurrentMembershipPackage()
		{
			$CI =& get_instance();

			$CI->db->where(array('user_id'=>$CI->session->userdata('user_id')));
			$CI->db->order_by('id', 'desc');
			$data = $CI->db->get('pw_non_working_user');
			
			if($data->num_rows() > 0)
			{
				$result = $data->row();

				return $result->package;
			}
			else
			{
				return '';
			}
		}
	}


	if(!function_exists('getSponsorFullNameBySponsor'))
	{
	 	function getSponsorFullNameBySponsor($sponsor)
		{
			$CI =& get_instance();

			$CI->db->where(array('username'=>$sponsor));
			$data = $CI->db->get('pw_users');

			if($data->num_rows() > 0)
			{
				$result =  $data->row();

				return ucwords($result->full_name);
			}
			else
			{
				return '';
			}
		}

	}

	if(!function_exists('getTransactionPasswordbyUserID'))
	{
	 	function getTransactionPasswordbyUserID($user_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('user_id'=>$user_id));
			$data = $CI->db->get('pw_transaction_password');

			if($data->num_rows() > 0)
			{
				$result =  $data->row();

				return $result->password_d;
			}
			else
			{
				return '';
			}
		}
	}

	if(!function_exists('getCityListByCountryID'))
	{
	 	function getCityListByCountryID($state_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('state_id'=>$state_id));
			$data = $CI->db->get('pw_cities');
			return $data->result();		
		}

	}

	if(!function_exists('getCityNameByID'))
	{
	 	function getCityNameByID($city_id)
		{
			$CI =& get_instance();

			$CI->db->where(array('id'=>$city_id));
			$data = $CI->db->get('pw_cities');
			$result =  $data->row();		

			return $result->name;
		}

	}
	
	function get_wallet_balance($user) {
		$CI =& get_instance();
		return $CI->db->get_where('pw_users', array('id' => $user))->row()->wallet_amount;
	}
	
	function getWithdrawalLimit($user) {
		$package = getLatestRankByUserID($user);
		if ($package == 30) {
			return 40;
		} else if ($package == 40) {
			return 40+40;
		} else if ($package == 100) {
			return 40+40+100;
		} else if ($package == 1000) {
			return 40+40+100+1000;
		} else if ($package == 5000) {
			return 40+40+100+1000+5000;
		} else {
			return "";
		}
	}

	function getWalletAmountByUserID($user_id)
	{
		$CI =& get_instance();
		
		$query = "select wallet_amount from pw_users where id = '".$user_id."'";
		$data = $CI->db->query($query);

		$row = $data->row();

		if (!empty($row->wallet_amount)) 
		{
			return $row->wallet_amount;
		}
		else
		{
			return 0;
		}
	}

	function is_maintenance() {
		$CI =& get_instance();		
		return	$CI->db->get_where('admin_login', array('id' => 1))->row()->is_maintenance;
	}
