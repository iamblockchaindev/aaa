<?php

use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Charge;
use CoinbaseCommerce\Webhook;

defined('BASEPATH') or exit('No direct script access allowed');

class B793db6c00482d633b29701c7bcb08c extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->model('Agency_model', 'agency');
        $this->load->model('Common_model', 'common');
        $this->load->model('Trading_Model', 'trade');
    }

    // Each an 5 minute
    function check_payment()
    {
        
        $result = $this->common->getWhereFromLast('pwt_user_notification', array('status !=' => '100', 'type' => 'fund', 'is_read' => 0));
        if ($result) {
            if ($result->payment_type == 'USDTERC20' || $result->payment_type == 'USDTERC20') {
            } else {
                $totalEFX = 0;
                $header_note = array();
                $referral_note = array();
                foreach ($result as $response) {
                    $apiClientObj = ApiClient::init('55c5f0f4-f763-4e5e-ac0f-d9a745b8ee6e');
                    $apiClientObj->verifySsl(false);
                    
                    $chargeObj = Charge::retrieve($response['txn_id']);
                    $payment = $chargeObj->payments;
                    if ($response['status'] == 0) {
                        if ($payment[0]['status'] == 'PENDING') {
                            $time = date('Y-m-d H:i:s');
                            $update_data = array(
                                'status' => 2,
                                'message' => "Waiting for confirmations",
                                'response_data' => json_encode($payment[0]),
                                'modified_datetime' => $time,
                            );
                            $this->common->update_data('pwt_user_notification', array('txn_id' => $response['txn_id']), $update_data);
                        }
                    }
                    if ($response['status'] != 100) {
                        if ($payment[0]['status'] == 'CONFIRMED') {
                            $amount = $payment[0]['value']['local']['amount'];
                            $efx = $amount * 0.1; //10% Bonus
                            $totalEFX = $totalEFX + $efx;
                            $time = date('Y-m-d H:i:s');
                            $update_data = array(
                                'status' => 100,
                                'message' => "Confirmed",
                                'response_data' => json_encode($payment[0]),
                                'modified_datetime' => $time,
                                'balance' => getWalletAmountByUserID($response['from_user'])[0] + $amount,
                            );
                            $this->common->update_data('pwt_user_notification', array('txn_id' => $response['txn_id']), $update_data);
                            $set = '';
                            if (date('Y-m-d') < '2021-08-01') {
                                $set = ', pwt_wallet = pwt_wallet + ' . $efx;
                            }
                            /*******Update Wallet**Start*****/
                            $u_query = "update pwt_users SET wallet_amount = wallet_amount +" . $amount . ", package='1', upgrade_time = NOW()" . $set . " where id = '" . $response['from_user'] . "'";
                            $this->db->query($u_query);
                            /*******Update Wallet**End*****/

                            if (date('Y-m-d') < '2021-08-01') {
                                /*******Referral History **Start****/
                                $insert_note = array(
                                    'from_user' => $response['from_user'],
                                    'to_user' => $response['from_user'],
                                    'log' => 'credit',
                                    'type' => 'referral',
                                    'payment_type' => 'pwt',
                                    'amount' => $efx,
                                    'balance' => getWalletAmountByUserID($response['from_user'])[2],
                                    'txn_id' => '#B' . rand(),
                                    'message' => "Deposit Referral",
                                    'status' => 1,
                                    'created_datetime' => date('Y-m-d H:i:s'),
                                    'modified_datetime' => date('Y-m-d H:i:s'),
                                );
                                $this->common->insert_data('pwt_user_notification', $insert_note);

                                /*******Notification **Start****/
                                $referral_note[] = array(
                                    'user_id' => $response['from_user'],
                                    'type' => 'bonus',
                                    'target' => base_url('history'),
                                    'detail' => "Deposit bonus $efx EFX is received",
                                    'date' => date('Y-m-d H:i:s'),
                                );
                                // $this->common->insert_data('pwt_notification', $insert_note);
                                /*******Notification **Start****/

                                /*******Referral History **End****/
                            }
                            /*******Notification **Start****/
                            $header_note[] = array(
                                'user_id' => $response['from_user'],
                                'type' => 'deposit',
                                'target' => base_url('wallet'),
                                'detail' => "Deposit $ $amount is confirmed",
                                'date' => date('Y-m-d H:i:s'),
                            );
                            // $this->common->insert_data('pwt_notification', $insert_note);
                            /*******Notification **Start****/
                        }
                    }
                }
                if ($header_note) {
                    $this->db->insert_batch('pwt_notification', $header_note);
                }
                if ($referral_note) {
                    $this->db->insert_batch('pwt_notification', $referral_note);
                }
                if (date('Y-m-d') < '2021-07-14') {
                    /*******Update EFX Supply **Start*****/
                    if ($totalEFX) {
                        $u_query = "update pwt_set_price SET total_supply = total_supply -" . $totalEFX . ", total_supplied = total_supplied + " . $totalEFX .  " where id = 1";
                        $this->db->query($u_query);
                    }
                    /*******Update EFX Supply**End*****/
                }
            }
        }
    }

    // Once a day
    function remove_payment()
    {
        $results = $this->common->getWhereFromLast('pwt_user_notification', array('status' => '0', 'type' => 'fund', 'is_read' => 0));
        $ids = array();
        foreach ($results as $row) {
            if ($row['end_date'] <= date('Y-m-d')) {
                array_push($ids, $row['id']);
            }
        }
        $ids = implode(',', $ids);
        if ($ids) {
            $query = "UPDATE pwt_user_notification SET is_read = '1' WHERE FIND_IN_SET(id, '$ids')";
            $this->db->query($query);
        }
        // $this->db->where_in('user_id', $userids);
        // $this->db->delete('pw_add_fund_history');

        // $this->daily_winner();
        $this->update_wheel();
    }

    // Once in a week trading commission for Agency/
    function trading_commission()
    {
        $users = $this->db->get('pwt_agency_tree')->result_array();
        $header_note = array();
        foreach ($users as $row) {
            $income = 0;
            // level 1 
            $totalTrade = $this->agency->get_count_bids($row['tree'], 1);
            if ($totalTrade > 0) {
                $commission = $this->get_level_income(1)['commission'];
                $earned = $totalTrade * $commission / 100;
                $income += $earned;
                $insert = array(
                    'user_id' => $row['sponsor_id'],
                    'sponsor_user' => $row['sponsor_user'],
                    'level' => 1,
                    'commission_type' => 0,
                    'amount' => $totalTrade,
                    'earned' => $earned,
                    'created_date' => date('Y-m-d')
                );
                $this->common->insert_data('pwt_agency_commission', $insert);
            }

            // Level 2
            if ($row['tree_count'] >= 3) {
                $totalTrade = $this->agency->get_count_bids($row['tree'], 2);
                if ($totalTrade > 2000) {
                    $commission = $this->get_level_income(2)['commission'];
                    $earned = $totalTrade * $commission / 100;
                    $income += $earned;
                    $insert = array(
                        'user_id' => $row['sponsor_id'],
                        'sponsor_user' => $row['sponsor_user'],
                        'level' => 2,
                        'commission_type' => 0,
                        'amount' => $totalTrade,
                        'earned' => $earned,
                        'created_date' => date('Y-m-d')
                    );
                    $this->common->insert_data('pwt_agency_commission', $insert);

                    // Level 3
                    if ($row['tree_count'] >= 4) {
                        // $totalTrade = $this->agency->get_count_bids($row['tree'], 3, $row['sponsor_id']);
                        if ($totalTrade > 4000) {
                            $commission = $this->get_level_income(3)['commission'];
                            $earned = $totalTrade * $commission / 100;
                            $income += $earned;
                            $insert = array(
                                'user_id' => $row['sponsor_id'],
                                'sponsor_user' => $row['sponsor_user'],
                                'level' => 3,
                                'commission_type' => 0,
                                'amount' => $totalTrade,
                                'earned' => $earned,
                                'created_date' => date('Y-m-d')
                            );
                            $this->common->insert_data('pwt_agency_commission', $insert);

                            // Level 4
                            if ($row['tree_count'] >= 5) {
                                // $totalTrade = $this->agency->get_count_bids($row['tree'], 4, $row['sponsor_id']);
                                if ($totalTrade > 8000) {
                                    $commission = $this->get_level_income(4)['commission'];
                                    $earned = $totalTrade * $commission / 100;
                                    $income += $earned;
                                    $insert = array(
                                        'user_id' => $row['sponsor_id'],
                                        'sponsor_user' => $row['sponsor_user'],
                                        'level' => 4,
                                        'commission_type' => 0,
                                        'amount' => $totalTrade,
                                        'earned' => $earned,
                                        'created_date' => date('Y-m-d')
                                    );
                                    $this->common->insert_data('pwt_agency_commission', $insert);

                                    // Level 5
                                    if ($row['tree_count'] >= 6) {
                                        // $totalTrade = $this->agency->get_count_bids($row['tree'], 5, $row['sponsor_id']);
                                        if ($totalTrade > 16000) {
                                            $commission = $this->get_level_income(5)['commission'];
                                            $earned = $totalTrade * $commission / 100;
                                            $income += $earned;
                                            $insert = array(
                                                'user_id' => $row['sponsor_id'],
                                                'sponsor_user' => $row['sponsor_user'],
                                                'level' => 5,
                                                'commission_type' => 0,
                                                'amount' => $totalTrade,
                                                'earned' => $earned,
                                                'created_date' => date('Y-m-d')
                                            );
                                            $this->common->insert_data('pwt_agency_commission', $insert);

                                            // Level 6
                                            if ($row['tree_count'] >= 7) {
                                                // $totalTrade = $this->agency->get_count_bids($row['tree'], 6, $row['sponsor_id']);
                                                if ($totalTrade > 32000) {
                                                    $commission = $this->get_level_income(6)['commission'];
                                                    $earned = $totalTrade * $commission / 100;
                                                    $income += $earned;
                                                    $insert = array(
                                                        'user_id' => $row['sponsor_id'],
                                                        'sponsor_user' => $row['sponsor_user'],
                                                        'level' => 6,
                                                        'commission_type' => 0,
                                                        'amount' => $totalTrade,
                                                        'earned' => $earned,
                                                        'created_date' => date('Y-m-d')
                                                    );
                                                    $this->common->insert_data('pwt_agency_commission', $insert);

                                                    // Level 7
                                                    if ($row['tree_count'] >= 8) {
                                                        // $totalTrade = $this->agency->get_count_bids($row['tree'], 7, $row['sponsor_id']);
                                                        if ($totalTrade > 64000) {
                                                            $commission = $this->get_level_income(7)['commission'];
                                                            $earned = $totalTrade * $commission / 100;
                                                            $income += $earned;
                                                            $insert = array(
                                                                'user_id' => $row['sponsor_id'],
                                                                'sponsor_user' => $row['sponsor_user'],
                                                                'level' => 7,
                                                                'commission_type' => 0,
                                                                'amount' => $totalTrade,
                                                                'earned' => $earned,
                                                                'created_date' => date('Y-m-d')
                                                            );
                                                            $this->common->insert_data('pwt_agency_commission', $insert);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /*******Update Wallet**Start*****/
            $u_query = "update pwt_users SET unlocked_pwt = unlocked_pwt +" . $income . " where id = '" . $row['sponsor_id'] . "'";
            $this->db->query($u_query);
            /*******Update Wallet**End*****/

            /*******Notification **Start****/
            $header_note[] = array(
                'user_id' => $row['sponsor_id'],
                'type' => 'agency',
                'target' => base_url('agency'),
                'detail' => "Trading commission $income pwt is received",
                'date' => date('Y-m-d H:i:s'),
            );
            // $this->common->insert_data('pwt_notification', $insert_note);
            /*******Notification **Start****/
        }
        if ($header_note) {
            $this->db->insert_batch('pwt_notification', $header_note);
        }
        // $this->weekly_winner();
    }

    function get_level_income($level)
    {
        $income = 0;
        $commission = 0;
        switch ($level) {
            case 1:
                $income = 50;
                $commission = 1;
                break;
            case 2:
                $income = 25;
                $commission = 0.5;
                break;
            case 3:
                $income = 12.5;
                $commission = 0.25;
                break;
            case 4:
                $income = 6.25;
                $commission = 0.13;
                break;
            case 5:
                $income = 3.12;
                $commission = 0.06;
                break;
            case 6:
                $income = 1.56;
                $commission = 0.03;
                break;
            case 7:
                $income = 0.78;
                $commission = 0.02;
                break;
        }
        $data['income'] = $income;
        $data['commission'] = $commission;
        return $data;
    }

    /*******Once a day for daily winner*******/
    function daily_winner()
    {
        $query = "select * from pwt_user_notification where DATE_FORMAT(created_datetime, '%Y-%m-%d') = CURDATE() and message = 'Daily Winner'";
        $data = $this->db->query($query);
        if ($data->num_rows() <= 0) {
            $daily = $this->trade->get_winners('daily');
            $count = 1;
            $totalEFX = 0;
            $insert_note = array();
            $header_note = array();
            foreach ($daily as  $row) {
                /*******Update User Wallet**Start*****/
                $efx = $this->trade_winner('daily', $count);
                $totalEFX = $totalEFX + $efx;
                if (date('Y-m-d') < '2021-08-01') {
                    $query = "UPDATE pwt_users SET pwt_wallet = pwt_wallet + $efx WHERE id =" . $row['user'];
                } else {
                    $query = "UPDATE pwt_users SET unlocked_pwt = unlocked_pwt + $efx WHERE id =" . $row['user'];
                }
                $this->db->query($query);
                /*******Update User Wallet**End*****/
                /*******Direct Referral History **Start****/
                $insert_note[] = array(
                    'from_user' => $row['user'],
                    'to_user' => $row['user'],
                    'log' => 'credit',
                    'type' => 'winner',
                    'payment_type' => 'EFX',
                    'amount' => $efx,
                    'balance' => date('Y-m-d') < '2021-08-01' ? getWalletAmountByUserID($row['user'])[2] : getWalletAmountByUserID($row['user'])[3],
                    'txn_id' => '#B' . rand(),
                    'message' => "Daily Winner",
                    'status' => 1,
                    'created_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s'),
                );
                /*******Direct Referral History **Start****/
                $count++;
                /*******Notification **Start****/
                $header_note[] = array(
                    'user_id' => $row['user'],
                    'type' => 'winner',
                    'target' => base_url('trading-winner'),
                    'detail' => "Daily winner $efx EFX is received",
                    'date' => date('Y-m-d H:i:s'),
                );
                // $this->common->insert_data('pwt_notification', $insert_note);
                /*******Notification **Start****/
            }
            if ($header_note) {
                $this->db->insert_batch('pwt_notification', $header_note);
            }
            if ($insert_note) {
                $this->db->insert_batch('pwt_user_notification', $insert_note);
            }
            /*******Update EFX Supply **Start*****/
            if ($totalEFX) {
                $u_query = "update pwt_set_price SET total_supply = total_supply -" . $totalEFX . ", total_supplied = total_supplied + " . $totalEFX .  " where id = 1";
                $this->db->query($u_query);
            }
            /*******Update EFX Supply**End*****/
        }
        // $this->update_wheel();
    }

    /*******Once a week for weekly winner*******/
    function weekly_winner()
    {
        $query = "select * from pwt_user_notification where DATE_FORMAT(created_datetime, '%Y-%m-%d') = CURDATE() and message = 'Weekly Winner'";
        $data = $this->db->query($query);
        if ($data->num_rows() <= 0) {
            $weekly = $this->trade->get_winners('weekly');
            $count = 1;
            $insert_note = array();
            $header_note = array();
            $totalEFX = 0;
            foreach ($weekly as  $row) {
                /*******Update User Wallet**Start*****/
                $efx = $this->trade_winner('weekly', $count);
                $totalEFX = $totalEFX + $efx;
                if (date('Y-m-d') < '2021-08-01') {
                    $query = "UPDATE pwt_users SET pwt_wallet = pwt_wallet + $efx WHERE id =" . $row['user'];
                } else {
                    $query = "UPDATE pwt_users SET unlocked_pwt = unlocked_pwt + $efx WHERE id =" . $row['user'];
                }
                $this->db->query($query);
                /*******Update User Wallet**End*****/

                /*******Direct Referral History **Start****/
                $insert_note[] = array(
                    'from_user' => $row['user'],
                    'to_user' => $row['user'],
                    'log' => 'credit',
                    'type' => 'winner',
                    'payment_type' => 'EFX',
                    'amount' => $efx,
                    'balance' => date('Y-m-d') < '2021-08-01' ? getWalletAmountByUserID($row['user'])[2] : getWalletAmountByUserID($row['user'])[3],
                    'txn_id' => '#B' . rand(),
                    'message' => "Weekly Winner",
                    'status' => 1,
                    'created_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s'),
                    'end_date' => date('Y-m-d', strtotime(' +7 days'))
                );
                /*******Direct Referral History **Start****/
                $count++;

                /*******Notification **Start****/
                $header_note[] = array(
                    'user_id' => $row['user'],
                    'type' => 'winner',
                    'target' => base_url('trading-winner'),
                    'detail' => "Weekly winner $efx EFX is received",
                    'date' => date('Y-m-d H:i:s'),
                );
                // $this->common->insert_data('pwt_notification', $insert_note);
                /*******Notification **Start****/
            }
            if ($header_note) {
                $this->db->insert_batch('pwt_notification', $header_note);
            }
            if ($insert_note) {
                $this->db->insert_batch('pwt_user_notification', $insert_note);
            }
            /*******Update EFX Supply **Start*****/
            if ($totalEFX) {
                $u_query = "update pwt_set_price SET total_supply = total_supply -" . $totalEFX . ", total_supplied = total_supplied + " . $totalEFX .  " where id = 1";
                $this->db->query($u_query);
            }
            /*******Update EFX Supply**End*****/
        }
    }

    /*******Once a month for monthly winner*******/
    function monthly_winner()
    {
        $query = "select * from pwt_user_notification where DATE_FORMAT(created_datetime, '%Y-%m-%d') = CURDATE() and message = 'Monthly Winner'";
        $data = $this->db->query($query);
        if ($data->num_rows() <= 0) {
            $monthly = $this->trade->get_winners('monthly');
            $count = 1;
            $insert_note = array();
            $header_note = array();
            $totalEFX = 0;
            foreach ($monthly as  $row) {
                $efx = $this->trade_winner('monthly', $count);
                $totalEFX = $totalEFX + $efx;
                /*******Update User Wallet**Start*****/
                if (date('Y-m-d') < '2021-08-01') {
                    $query = "UPDATE pwt_users SET pwt_wallet = pwt_wallet + $efx WHERE id =" . $row['user'];
                } else {
                    $query = "UPDATE pwt_users SET unlocked_pwt = unlocked_pwt + $efx WHERE id =" . $row['user'];
                }
                $this->db->query($query);
                /*******Update User Wallet**End*****/

                /*******Direct Referral History **Start****/
                $insert_note[] = array(
                    'from_user' => $row['user'],
                    'to_user' => $row['user'],
                    'log' => 'credit',
                    'type' => 'winner',
                    'payment_type' => 'EFX',
                    'amount' => $this->trade_winner('monthly', $count),
                    'balance' => date('Y-m-d') < '2021-08-01' ? getWalletAmountByUserID($row['user'])[2] : getWalletAmountByUserID($row['user'])[3],
                    'txn_id' => '#B' . rand(),
                    'message' => "Monthly Winner",
                    'status' => 1,
                    'created_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s'),
                );
                /*******Direct Referral History **Start****/
                $count++;
                /*******Notification **Start****/
                $header_note[] = array(
                    'user_id' => $row['user'],
                    'type' => 'winner',
                    'target' => base_url('trading-winner'),
                    'detail' => "Monthly winner $efx EFX is received",
                    'date' => date('Y-m-d H:i:s'),
                );
                // $this->common->insert_data('pwt_notification', $insert_note);
                /*******Notification **Start****/
            }
            if ($header_note) {
                $this->db->insert_batch('pwt_notification', $header_note);
            }
            if ($insert_note) {
                $this->db->insert_batch('pwt_user_notification', $insert_note);
            }
            /*******Update EFX Supply **Start*****/
            if ($totalEFX) {
                $u_query = "update pwt_set_price SET total_supply = total_supply -" . $totalEFX . ", total_supplied = total_supplied + " . $totalEFX .  " where id = 1";
                $this->db->query($u_query);
            }
            /*******Update EFX Supply**End*****/
        }
        // $this->monthly_unlock();
    }

    function trade_winner($type, $postion)
    {
        $bonus = 0;
        if ($type == 'daily') {
            switch ($postion) {
                case 1:
                    $bonus = 200;
                    break;
                case 2:
                    $bonus = 150;
                    break;
                case 3:
                    $bonus = 100;
                    break;
                case 4:
                    $bonus = 90;
                    break;
                case 5:
                    $bonus = 80;
                    break;
                case 6:
                    $bonus = 70;
                    break;
                case 7:
                    $bonus = 60;
                    break;
                case 8:
                    $bonus = 50;
                    break;
                case 9:
                    $bonus = 40;
                    break;
                case 10:
                    $bonus = 30;
                    break;
            }
        } else if ($type == 'monthly') {
            switch ($postion) {
                case 1:
                    $bonus = 2500;
                    break;
                case 2:
                    $bonus = 2000;
                    break;
                case 3:
                    $bonus = 1500;
                    break;
                case 4:
                    $bonus = 1000;
                    break;
                case 5:
                    $bonus = 750;
                    break;
                case 6:
                    $bonus = 650;
                    break;
                case 7:
                    $bonus = 550;
                    break;
                case 8:
                    $bonus = 450;
                    break;
                case 9:
                    $bonus = 350;
                    break;
                case 10:
                    $bonus = 250;
                    break;
            }
        } else if ($type == 'weekly') {
            switch ($postion) {
                case 1:
                    $bonus = 500;
                    break;
                case 2:
                    $bonus = 350;
                    break;
                case 3:
                    $bonus = 200;
                    break;
                case 4:
                    $bonus = 150;
                    break;
                case 5:
                    $bonus = 100;
                    break;
                case 6:
                    $bonus = 75;
                    break;
                case 7:
                    $bonus = 65;
                    break;
                case 8:
                    $bonus = 55;
                    break;
                case 9:
                    $bonus = 45;
                    break;
                case 10:
                    $bonus = 35;
                    break;
            }
        }
        return $bonus;
    }

    /*******Run only 15 July, 2021*******/
    function unlock_efx()
    {
        if (date('Y-m-d') == '2021-07-15') {
            $users = $this->common->getWhere('pwt_users', array('pwt_wallet >' => 0, 'pwt_wallet <=' => 100));
            foreach ($users as $row) {
                $u_query = "update pwt_users SET unlocked_efx = " . $row['pwt_wallet'] . " where id = '" . $row['id'] . "'";
                $this->db->query($u_query);
            }
        }
    }

    /*******Once a month for monthly winner*******/
    function monthly_unlock()
    {
        ini_set('memory_limit', '-1');
        $users = $this->common->getWhere('pwt_users', array('pwt_wallet >' => 0));
        $update = array();
        $header_note = array();
        $isPaid =  $this->common->getSingle('pwt_unlock_pwt', array('date' => date('Y-m-d'), 'status' => 0));
        if ($isPaid) {
            foreach ($users as $user) {
                $unlockEFX = $user->pwt_wallet * 10 / 100;
                $update[] = array(
                    'id' => $user->id,
                    'unlocked_pwt' => $user->unlocked_pwt + $unlockEFX
                );
                /*******Notification **Start****/
                $header_note[] = array(
                    'user_id' => $user->id,
                    'type' => 'unlock',
                    'target' => base_url('wallet'),
                    'detail' => number_format($unlockEFX, 2) . " EFX is unlocked",
                    'date' => date('Y-m-d H:i:s'),
                );
                /*******Notification **Start****/
            }
        }
        if ($update) {
            $this->db->update_batch('pwt_users', $update, 'id');
            $this->common->update_data('pwt_unlock_pwt', array('date' => date('Y-m-d')), array('status' => 1));
        }
        if ($header_note) {
            $this->db->insert_batch('pwt_notification', $header_note);
        }
    }

    function sendMessage()
    {
        $content = array(
            "en" => 'English Message'
        );

        $fields = array(
            'app_id' => "57b15889-118e-48a3-bd3d-ed9c108ce57f",
            'include_external_user_ids' => array("coface1206@relumyx.com"),
            'channel_for_external_user_ids' => 'push',
            'data' => array("foo" => "bar"),
            'contents' => $content
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        $header_note = array(
            'user_id' => '89',
            'type' => 'Test',
            'target' => "",
            'detail' => "English Message",
            'date' => date('Y-m-d H:i:s'),
        );
        $this->common->insert_data('pwt_notification', $header_note);

        return $response;
    }

    function update_wheel()
    {
        $update = array();
        $users = $this->common->getWhere('pwt_users', array('is_play' => 1));
        foreach ($users as $user) {
            $update[] = array(
                'id' => $user->id,
                'is_play' => 0
            );
        }
        if ($update) {
            $this->db->update_batch('pwt_users', $update, 'id');
        }
    }

    function test()
    {
        /*******Update Wallet**Start*****/
        $u_query = "update pwt_users SET wallet_amount = wallet_amount + 10 where id = 89";
        $this->db->query($u_query);
        /*******Update Wallet**End*****/
    }

    // Each an 10 minute
    function sucess_payment()
    {
        $fund_data = $this->common->getWhereFromLast('pwt_user_notification', array('type' => 'fund', 'status' => 4));
        foreach ($fund_data as $fund) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.nowpayments.io/v1/payment/' . $fund['txn_id'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => array(
                    'x-api-key: RHYY119-F5K44CG-M3TTKE0-0TM3AFE'
                ),
            ));

            $response = curl_exec($curl);
            $encode = json_encode($response);
            $payment = json_decode($response);
            $amount = ($payment->outcome_amount / get_pwt()['current_price']);
            if ($payment->payment_status == 'partially_paid' || $payment->payment_status == 'finished') {
                $time = date('Y-m-d H:i:s');
                $update_data = array(
                    'status' => 100,
                    'message' => $payment->payment_status,
                    'amount' => $amount,
                    'payment_type' => 'pwt',
                    'txn_id' => $payment->payment_id,
                    'response_data' => json_encode($payment),
                    'modified_datetime' => $time,
                    'balance' => getWalletAmountByUserID($fund['from_user'])[3] + $amount,
                );
                $this->common->update_data('pwt_user_notification', array('id' => $fund['id']), $update_data);
                /*******Update history**End*****/

                /*******Notification **Start****/
                $insert_note = array(
                    'user_id' => $fund['from_user'],
                    'type' => 'deposit',
                    'target' => base_url('wallet'),
                    'detail' => "Deposit $ " . number_format($payment->outcome_amount, 2) . " is confirmed",
                    'date' => date('Y-m-d H:i:s'),
                );
                $this->common->insert_data('pwt_notification', $insert_note);
                /*******Notification **Start****/

                /*******Update Wallet**Start*****/
                $set = ', unlocked_pwt = unlocked_pwt + ' . $amount;
                $u_query = "update pwt_users SET package='1', upgrade_time = NOW()" . $set . " where id = '" . $fund['from_user'] . "'";
                $this->db->query($u_query);
            }
            curl_close($curl);
        }
        $this->confirm_payment();
    }

    function confirm_payment()
    {
        $fund_data = $this->common->getWhereFromLast('pwt_user_notification', array('type' => 'fund', 'status != ' => 100, 'is_read' => 0));
        foreach ($fund_data as $fund) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.nowpayments.io/v1/payment/?limit=20&page=0&sortBy=created_at&orderBy=desc',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => array(
                    'x-api-key: RHYY119-F5K44CG-M3TTKE0-0TM3AFE'
                ),
            ));

            $response = curl_exec($curl);
            $encode = json_encode($response);
            $result = json_decode($response);
            curl_close($curl);

            foreach ($result->data as $res) {

                if ($fund['txn_id'] == $res->invoice_id) {
                    if ($res->payment_status == 'confirmed' || $res->payment_status == 'finished' || $res->payment_status == "partially_paid") {
                        $amount = ($res->outcome_amount / get_pwt()['current_price']);
                        /*******Update history**Start*****/
                        $time = date('Y-m-d H:i:s');
                        if ($res->payment_status == 'partially_paid') {
                            goto next;
                        }
                        $calc = ($res->actually_paid * 100) / $res->pay_amount;
                        if ($calc > 95) {
                            next:
                            $update_data = array(
                                'status' => 100,
                                'message' => $res->payment_status,
                                'amount' => $amount,
                                'payment_type' => 'pwt',
                                'txn_id' => $res->payment_id,
                                'response_data' => json_encode($res),
                                'modified_datetime' => $time,
                                'balance' => getWalletAmountByUserID($fund['from_user'])[3] + $amount,
                            );
                            $this->common->update_data('pwt_user_notification', array('txn_id' => $res->invoice_id), $update_data);
                            /*******Update history**End*****/

                            /*******Notification **Start****/
                            $insert_note = array(
                                'user_id' => $fund['from_user'],
                                'type' => 'deposit',
                                'target' => base_url('wallet'),
                                'detail' => "Deposit $ " . number_format($res->outcome_amount, 2) . " is confirmed",
                                'date' => date('Y-m-d H:i:s'),
                            );
                            $this->common->insert_data('pwt_notification', $insert_note);
                            /*******Notification **Start****/

                            /*******Update Wallet**Start*****/
                            $set = ', unlocked_pwt = unlocked_pwt + ' . $amount;
                            $u_query = "update pwt_users SET package='1', upgrade_time = NOW()" . $set . " where id = '" . $this->session->user_id . "'";
                            $this->db->query($u_query);
                        } else {
                            /*******Update history**Start*****/
                            $update_data = array(
                                'status' => 4,
                                'message' => 'In-Progress',
                                'txn_id' => $res->payment_id,
                                'amount' => $res->pay_amount,
                                'response_data' => json_encode($res),
                                'modified_datetime' => $time,
                            );
                            $this->common->update_data('pwt_user_notification', array('txn_id' => $res->invoice_id), $update_data);
                            /*******Update history**End*****/
                        }
                        /*******Update Wallet**End*****/
                    }
                }
            }
        }
    }
}
