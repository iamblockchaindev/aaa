<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Login extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('captcha');
	}
	function index()
	{
		if ($this->session->userdata('user_id')) {
			redirect(base_url());
		}
		$data['title'] = 'Login';
		$this->form_validation->set_rules('username', 'User name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login', $data);
		} else {
			$username = $this->input->post('username', true);
			$password = $this->input->post('password', true);
			$where = array('username' => $username, 'password' => md5($password), 'is_delete' => 1, 'is_status' => 1);
			$login_data = $this->common->getSingle('pw_users', $where);
			if (!empty($login_data)) {
				/******Last Login Update*Start***/
				$u_query = "update pw_users set last_login = '" . date('Y-m-d H:i:s') . "' where id = '" . $login_data->id . "'";
				$this->db->query($u_query);

				/******Last Login Update*End***/
				$session_data = array(
					'user_id'   => $login_data->id,
					'user_name'  => $login_data->username,
					'user_email'  => $login_data->email,
					'is_profile_update' => $login_data->is_profile_update,
					'full_name' => $login_data->full_name,

				);
				$this->session->set_userdata($session_data);

				$ip_address = $this->input->ip_address();
				$message = 'Login attempt success !';
				$browser_data = $this->getBrowser();
				$browser_name = $browser_data['browser_name'];
				$browser_image = $browser_data['image_name'];
				$insert_data = array(
					'user_id' => $login_data->id,
					'message' => $message,
					'browser_name' => $browser_name,
					'browser_image' => $browser_image,
					'ip_address' => $ip_address,
					'status' => '1',
					'created_datetime' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_log_history', $insert_data);

				$insert_data = array(
					'userid' => $login_data->id,
					'detail' => 'Login',
					'date' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_logs', $insert_data);

				/********Mail for activity ***Start****/
				$from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
				$to_email = $this->session->user_email;
				$WEB_URL = base_url();
				$LOGO_URL = base_url() . 'assets/front/images/logo.png';
				$htmlContent = file_get_contents("./mail_html/activity.html");
				$tpl1 = str_replace('{{LOGO_URL}}', $LOGO_URL, $htmlContent);
				$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
				$tpl3 = str_replace('{{MEMBER}}', $this->session->user_name, $tpl2);
				$tpl4 = str_replace('{{ACTIVITY}}', "Login", $tpl3);
				$tpl5 = str_replace('{{DATETIME}}', date('Y-m-d H:i:s'), $tpl4);
				$message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl5);
				// PHPMailer object
				$mail = $this->phpmailer_lib->load();
				// SMTP configuration
				$mail->isSMTP();
				$mail->Host     = SMTP_HOST;
				$mail->SMTPAuth = true;
				$mail->Username = ADMIN_NO_REPLY_EMAIL;
				$mail->Password = MAIL_PWD;
				$mail->SMTPSecure = 'ssl';
				$mail->Port     = SMTP_PORT;

				$mail->setFrom($from_email, 'Pointwish');
				$mail->addReplyTo($from_email, 'Pointwish');
				$mail->addAddress($to_email);
				$mail->Subject = 'Point Wish: Login Activity';
				$mail->isHTML(true);
				$mail->Body = $message_html;

				// if (!$mail->send()) {
				// 	$result['message'] = $mail->ErrorInfo;
				// } else {
				// 	$result['message'] = 'Message has been sent';
				// $mails = $this->common->getSingle('pw_daily_mailcount', array('date' => date('Y-m-d')));
				// 	if($mails) {
				// 		$update_data = array(
				// 			'count' => $mails->count + 1
				// 		);
				// 		$this->common->update_data('pw_daily_mailcount', array('date' => date('Y-m-d')), $update_data);
				// 	} else {
				// 		$insert_data = array(
				// 			'date' => date('Y-m-d'),
				// 			'count' => 1
				// 		);
				// 		$this->common->insert_data('pw_daily_mailcount', $insert_data);
				// 	}
				// }
				/********Mail for signup***End****/


				$remember_me = $this->input->post('remember_me', true);
				if ($remember_me == 1) {
					$cookie_username = array(
						'name'   => 'username',
						'value'  => $username,
						'expire' => time() + 60 * 60,
					);
					$cookie_password = array(
						'name'   => 'password',
						'value'  => $this->input->post('password', true),
						'expire' => time() + 60 * 60,
					);
					$this->input->set_cookie($cookie_username);
					$this->input->set_cookie($cookie_password);
				} else {
					delete_cookie("username");
					delete_cookie("password");
				}
				if ($login_data->is_profile_update == 0) {
					// redirect(base_url('user/account/view_profile'));
					$result['success'] = 3;
					echo json_encode($result);
				} else {
					// redirect(base_url('user/dashboard'));
					$result['success'] = 1;
					echo json_encode($result);
				}
			} else {
				$result['success'] = 2;
				echo json_encode($result);
				// $this->session->set_flashdata('error_session', 'Invalid Username or Password !');
				// redirect(base_url() . 'login');
			}
		}
	}

	public function generateSponsorId()
	{
		$randnum = "PW" . mt_rand(10000000, 99999999);
		$res = $this->common->getSingle('pw_users', array('username' => $randnum));
		if ($res) {
			return $this->generateSponsorId();
		} else {
			return  $randnum;
		}
	}
	function getBrowser()
	{
		$browser_name = '';
		$image_name = '';
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
			$browser_name = 'Internet explorer';
			$image_name = 'ie.png';
		} elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE) {
			$browser_name = 'Mozilla Firefox';
			$image_name = 'mozilla.png';
		} elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE) {
			$browser_name = 'Google Chrome';
			$image_name = 'chrome.png';
		} elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== FALSE) {
			$browser_name = 'Safari';
			$image_name = 'safari.png';
		}
		return array('browser_name' => $browser_name, 'image_name' => $image_name);
	}
	function signup()
	{
		if ($this->session->userdata('user_id')) {
			redirect(base_url());
		}
		$data['title'] = 'Signup';
		$this->form_validation->set_rules('sponsor_id', 'Sponsor', 'required');
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE) {
			redirect(base_url('login'), 'refresh');
		} else {
			$sponsor = $this->input->post('sponsor_id');
			$is_sponser_exist = $this->is_sponser_exist_server($sponsor);
			if ($is_sponser_exist == 2) {
				$result['success'] = 2;
				echo json_encode($result);
			} else if ($is_sponser_exist == 3) {
				$result['success'] = 3;
				echo json_encode($result);
			} else {
				$fname = $this->input->post('fname');
				$lname = $this->input->post('lname');
				$email = $this->input->post('email');
				$password_decrypted = $this->input->post('password');
				$password = MD5($this->input->post('password'));
				$token = md5(uniqid(rand(), true));
				$insert_data = array(
					'token' => $token,
					'full_name' => $fname . ' ' . $lname,
					'password' => $password,
					'password_decrypted' => $password_decrypted,
					'sponsor' => $sponsor,
					'email' => $email,
					'created_time' => date('Y-m-d H:i:s'),
					'modified_time' => date('Y-m-d H:i:s'),
					'is_status' => 1
				);
				$insert_id = $this->common->insert_data('pw_users', $insert_data);
				/*******Update Username **Start****/
				//$username = 'PW'.(31679712+$insert_id);
				$username = $this->generateSponsorId();
				$update_data = array(
					'username' => $username
				);
				$this->common->update_data('pw_users', array('id' => $insert_id), $update_data);

				/********Mail for signup***Start****/
				$from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
				$to_email = $email;				
				$WEB_URL = base_url();
				$ACTIVE_LINK =  base_url() . 'verification/account/' . $token;
				$LOGO_URL = base_url() . 'assets/user_panel/mail/point_wish-logo.png';
				$htmlContent = file_get_contents("./mail_html/welcome.html");
				$tpl1 = str_replace('{{LOGO_URL}}', $LOGO_URL, $htmlContent);
				$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
				$tpl3 = str_replace('{{ACTIVE_LINK}}', $ACTIVE_LINK, $tpl2);
				$tpl4 = str_replace('{{USERNAME}}', $username, $tpl3);
				$tpl5 = str_replace('{{PASSWORD}}', $password_decrypted, $tpl4);
				$message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl5);
				// PHPMailer object
				$mail = $this->phpmailer_lib->load();
				// SMTP configuration
				$mail->isSMTP();
				$mail->Host     = SMTP_HOST;
				$mail->SMTPAuth = true;
				$mail->Username = ADMIN_NO_REPLY_EMAIL;
				$mail->Password = MAIL_PWD;
				$mail->SMTPSecure = 'ssl';
				$mail->Port     = SMTP_PORT;

				$mail->setFrom($from_email, 'Pointwish');
				$mail->addReplyTo($from_email, 'Pointwish');
				$mail->addAddress($to_email);
				$mail->Subject = 'Point Wish: Welcome to pointwish';
				$mail->isHTML(true);
				$mail->Body = $message_html;

				if (!$mail->send()) {
					$result['message'] = $mail->ErrorInfo;
				} else {
					$result['message'] = 'Message has been sent';
					$mails = $this->common->getSingle('pw_daily_mailcount', array('date' => date('Y-m-d')));
					if($mails) {
						$update_data = array(
							'count' => $mails->count + 1
						);
						$this->common->update_data('pw_daily_mailcount', array('date' => date('Y-m-d')), $update_data);
					} else {
						$insert_data = array(
							'date' => date('Y-m-d'),
							'count' => 1
						);
						$this->common->insert_data('pw_daily_mailcount', $insert_data);
					}
				}
				/********Mail for signup***End****/
				$this->session->set_flashdata('username', $username);
				$this->session->set_flashdata('password', $password_decrypted);
				$this->session->set_flashdata('error_message', 'You have successfully signed up !');
				$result['username'] = $username;
				$result['password'] = $password_decrypted;
				$result['success'] = 1;
				echo json_encode($result);
			}
		}
	}
	private function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	private function getPhoneCodeByCountryCode($country_code)
	{
		$country_data = $this->common->getSingle('pw_countries', array('sortname' => $country_code));
		if (!empty($country_data)) {
			return $country_data->phonecode;
		} else {
			return '';
		}
	}

	private function is_sponser_exist_server($sponsor)
	{
		$result = $this->common->getSingle('pw_users', array('username' => $sponsor, 'is_delete' => 1));
		if (!empty($result)) {
			if (!empty($result->package)) {
				$status = 1;
			} else {
				$status = 3;
			}
		} else {
			$status = 2;
		}
		return $status;
	}

	function is_sponser_exist()
	{
		if (!isset($_GET['sponsor_id'])) {
			redirect(base_url());
		}
		$sponsor_id = $this->input->get('sponsor_id');
		$result = $this->common->getSingle('pw_users', array('username' => $sponsor_id, 'is_delete' => 1));
		$member_name = '';
		if (!empty($result)) {
			if (!empty($result->package)) {
				$status = 1;
				$member_name = ucfirst($result->full_name);
			} else {
				$status = 3;
				$member_name = ucfirst($result->full_name);
			}
		} else {
			$status = 2;
		}
		$json_arr = array('status' => $status, 'member_name' => $member_name);
		echo json_encode($json_arr);
		exit;
	}

	function is_email_exist()
	{
		if (!isset($_GET['email'])) {
			redirect(base_url());
		}
		$email = $this->input->get('email');
		$result = $this->common->getSingle('pw_users', array('email' => $email, 'is_delete' => 1));
		if (!empty($result)) {
			echo 'false';
		} else {
			echo 'true';
		}
	}

	function is_mobile_exist()
	{
		if (!isset($_GET['mobile_number'])) {
			redirect(base_url());
		}
		$mobile = $this->input->get('mobile_number');
		$result = $this->common->getSingle('pw_users', array('mobile' => $mobile, 'is_delete' => 1));
		if (!empty($result)) {
			echo 'false';
		} else {
			echo 'true';
		}
	}

	function refresh()
	{
		// Captcha configuration
		$config = array(
			'img_path'      => './uploads/captcha_images/',
			'img_url'       => base_url() . '/uploads/captcha_images/',
			'font_path'     => 'system/fonts/texb.ttf',
			'img_width'     => '160',
			'img_height'    => 50,
			'word_length'   => 8,
			'font_size'     => 18
		);
		$captcha = create_captcha($config);
		// Unset previous captcha and set new captcha word
		$this->session->unset_userdata('captchaCode');
		$this->session->set_userdata('captchaCode', $captcha['word']);
		// Display captcha image
		echo $captcha['image'];
	}

	function password()
	{
		$this->load->view('forgot_password');
	}

	function forgot_password()
	{
		// if ($this->session->userdata('user_id')) {
		// 	redirect(base_url());
		// }
		$data['title'] = 'Forgot Password';
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login', $data);
		} else {
			$username = $this->input->post('username', true);
			$user_data = $this->common->getSingle('pw_users', array('username' => $username, 'is_delete' => 1));
			$email = $user_data->email;
			if (!empty($user_data)) {				
				/********Mail***Start****/
				$from_email = ADMIN_NO_REPLY_EMAIL;
				$to_email = $email;
				$subject = 'Point Wish: Forgot Password';
				$WEB_URL = base_url();
				$LOGO_URL = base_url() . 'assets/front/images/logo.png';
				$RESET_LINK = base_url() . 'login/reset_password/' . $user_data->token;
				$USERNAME = $user_data->full_name;
				$htmlContent = file_get_contents("./mail_html/change_password.html");
				$tpl1 = str_replace('{{LOGO_URL}}', $LOGO_URL, $htmlContent);
				$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
				$tpl3 = str_replace('{{NAME}}', $USERNAME, $tpl2);
				$message_html = str_replace('{{RESET_LINK}}', $RESET_LINK, $tpl3);
				// PHPMailer object
				$mail = $this->phpmailer_lib->load();
				// SMTP configuration
				$mail->isSMTP();
				$mail->Host     = SMTP_HOST;
				$mail->SMTPAuth = true;
				$mail->Username = ADMIN_NO_REPLY_EMAIL;
				$mail->Password = MAIL_PWD;
				$mail->SMTPSecure = 'ssl';
				$mail->Port     = SMTP_PORT;

				$mail->setFrom($from_email, 'Pointwish');
				$mail->addReplyTo($from_email, 'Pointwish');
				$mail->addAddress($to_email);
				$mail->Subject = 'Point Wish: Forgot Password';
				$mail->isHTML(true);
				$mail->Body = $message_html;

				if (!$mail->send()) {
					$result['message'] = $mail->ErrorInfo;
				} else {
					$result['message'] = 'Message has been sent';
					$mails = $this->common->getSingle('pw_daily_mailcount', array('date' => date('Y-m-d')));
					if($mails) {
						$update_data = array(
							'count' => $mails->count + 1
						);
						$this->common->update_data('pw_daily_mailcount', array('date' => date('Y-m-d')), $update_data);
					} else {
						$insert_data = array(
							'date' => date('Y-m-d'),
							'count' => 1
						);
						$this->common->insert_data('pw_daily_mailcount', $insert_data);
					}
				}
				/********Mail***End****/
				$this->session->set_flashdata("success_message", "Please check mail for the password !");
				$result['success'] = 1;
				echo json_encode($result);
				// redirect(base_url() . 'forgot_password#container_id');
			} else {
				$result['success'] = 2;
				echo json_encode($result);
				// $this->session->set_flashdata("error_message", "Please enter your registered username !");
				// redirect(base_url() . 'forgot_password');
			}
		}
	}

	function reset_password()
	{		
		$data['title'] = 'Reset Password';
		$token = $this->uri->segment(3);
		$user_data = $this->common->getSingle('pw_users', array('token' => $token));
		if (empty($user_data)) {
			$result['success'] = 0;
			echo json_encode($result);
		}
		$this->form_validation->set_rules('new_password', 'New Password', 'required');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('forgot_password', $data);
		} else {
			$new_password = $this->input->post('new_password', true);
			$confirm_password = $this->input->post('confirm_password', true);
			$update_data = array(
				'password' => MD5($new_password),
				'password_decrypted' => $new_password
			);
			$this->common->update_data('pw_users', array('token' => $token), $update_data);

			/********Mail***Start****/
			$from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
			$to_email = $user_data->email;			
			$WEB_URL = base_url();
			$LOGO_URL = base_url() . 'assets/front/images/logo.png';
			$htmlContent = file_get_contents("./mail_html/reset_password.html");
			$tpl1 = str_replace('{{LOGO_URL}}', $LOGO_URL, $htmlContent);
			$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
			$tpl3 = str_replace('{{NAME}}', $user_data->full_name, $tpl2);
			$tpl4 = str_replace('{{USERNAME}}', $user_data->username, $tpl3);
			$message_html = str_replace('{{PASSWORD}}', $new_password, $tpl4);
			$config['mailtype'] = 'html';
			// PHPMailer object
			$mail = $this->phpmailer_lib->load();
			// SMTP configuration
			$mail->isSMTP();
			$mail->Host     = SMTP_HOST;
			$mail->SMTPAuth = true;
			$mail->Username = ADMIN_NO_REPLY_EMAIL;
			$mail->Password = MAIL_PWD;
			$mail->SMTPSecure = 'ssl';
			$mail->Port     = SMTP_PORT;

			$mail->setFrom($from_email, 'Pointwish');
			$mail->addReplyTo($from_email, 'Pointwish');
			$mail->addAddress($to_email);
			$mail->Subject = 'Point Wish: Reset Password';
			$mail->isHTML(true);
			$mail->Body = $message_html;

			if (!$mail->send()) {
				$result['message'] = $mail->ErrorInfo;
			} else {
				$result['message'] = 'Message has been sent';
				$mails = $this->common->getSingle('pw_daily_mailcount', array('date' => date('Y-m-d')));
					if($mails) {
						$update_data = array(
							'count' => $mails->count + 1
						);
						$this->common->update_data('pw_daily_mailcount', array('date' => date('Y-m-d')), $update_data);
					} else {
						$insert_data = array(
							'date' => date('Y-m-d'),
							'count' => 1
						);
						$this->common->insert_data('pw_daily_mailcount', $insert_data);
					}
			}
			/********Mail***End****/
			$this->session->set_flashdata("success_message", 'Password has been reset successfully.');
			$result['success'] = 1;
			echo json_encode($result);
		}
	}

	function random_string($length)
	{
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));
		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}
		return $key;
	}
	
	function logout()
	{
		$insert_data = array(
			'userid' => isset($this->session->user_id) ? $this->session->user_id : 0,
			'detail' => 'Logout',
			'date' => date('Y-m-d H:i:s'),
		);
		$this->common->insert_data('pw_logs', $insert_data);
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_name');
		$this->session->unset_userdata('user_email');
		$this->session->unset_userdata('is_profile_update');
		$this->session->unset_userdata('full_name');

		redirect(base_url() . 'login');
	}	

	function getStateListByCountryID()
	{
		$html = '<option value="">Select</option>';

		if ($this->input->post('country_id')) {
			$country_id = $this->input->post('country_id');
			$query = "select * from pw_states where country_id = '" . $country_id . "'";
			$result = $this->db->query($query);

			if ($result->num_rows() > 0) {
				$record = $result->result();

				foreach ($record as $row) {
					$html .= '<option value="' . $row->id . '">' . $row->name . '</option>';
				}
			}

			echo $html;
			exit;
		} else {
			echo $html;
			exit;
		}
	}

	function getCityListByStateID()
	{
		$html = '<option value="">Select</option>';

		if ($this->input->post('state_id')) {
			$state_id = $this->input->post('state_id');
			$query = "select * from pw_cities where state_id = '" . $state_id . "'";
			$result = $this->db->query($query);

			if ($result->num_rows() > 0) {
				$record = $result->result();

				foreach ($record as $row) {
					$html .= '<option value="' . $row->id . '">' . $row->name . '</option>';
				}
			}

			echo $html;
			exit;
		} else {
			echo $html;
			exit;
		}
	}	
}
