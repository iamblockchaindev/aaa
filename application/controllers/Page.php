<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {

    function __construct() 
    {
		parent::__construct();
    }
    
	function about()
	{
		$data['title'] = 'About Us';
		$this->load->view('about', $data);
	}	

	function contact()
	{
		$data['title'] = 'Contact Us';

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('contact', $data);
		}
		else
		{
			$name = $this->input->post('name', true);
			$email = $this->input->post('email', true);
			$message = $this->input->post('message', true);

			/********Mail for signup***Start****/
				$to_email = ADMIN_SUPPORT_EMAIL; //ADMIN_NO_REPLY_EMAIL, ADMIN_SUPPORT_EMAIL
				$from_email = $email;
				$subject = 'Point Wish: User Contact Information';

				$WEB_URL = base_url();
				$LOGO_URL = base_url().'assets/front/images/logo.png';

				$htmlContent = file_get_contents("./mail_html/contact.html");

				$tpl1 = str_replace('{{LOGO_URL}}', $LOGO_URL, $htmlContent);
				$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
				$tpl3 = str_replace('{{NAME}}', 'Admin', $tpl2);
				$tpl4 = str_replace('{{USERNAME}}', $name, $tpl3);
				$tpl5 = str_replace('{{EMAIL}}', $email, $tpl4);
				$tpl6 = str_replace('{{MESSAGE}}', $message, $tpl5);
				$message_html = str_replace('{{INFO_EMAIL}}', $to_email, $tpl6);
			

				//echo $message_html; die;
				
				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->from($from_email, '');
				$this->email->to($to_email);
				$this->email->subject($subject);
				$this->email->message($message_html);

				if($this->email->send())
				{
					//echo 1; die;
					$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
				}
				else
				{
					//echo 2; die;
					$this->session->set_flashdata("email_sent","You have encountered an error");
				}
			/********Mail for signup***End****/			

			$this->session->set_flashdata('error_message', 'Message sent successfully !');

			redirect(base_url().'page/contact');

		}
	}

    function privacy()
	{
		$data['title'] = 'Privacy';
		$this->load->view('privacy', $data);
	}

	function contact_us()
	{
		$data['title'] = 'Contact_us';
		$this->load->view('contact', $data);
	}
	function faq()
	{
		$data['title'] = 'FAQ';
		$this->load->view('faq', $data);
	}	

	function service()
	{
		$data['title'] = 'Service';
		$this->load->view('service', $data);
	}	

	function business_plan()
	{
		$data['title'] = 'Business Plan';
		$this->load->view('business_plan', $data);
	}

	function terms()
	{
		$data['title'] = 'Terms & Condition';
		$this->load->view('terms', $data);
	}
	
	function create_enquiry() {
		$this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('mobileno', 'Mobile No', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('country', 'Country', 'trim|required|integer');
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required|max_length[65536]');
		if( $this->form_validation->run() == false ) {
			$this->session->set_flashdata('error_message', validation_errors());
		} else {
			$this->db->insert('pw_contact_us', array(
				'full_name' => $this->input->post('fullname'),
				'mobile' => $this->input->post('mobileno'),
				'email' => $this->input->post('email'),
				'country_id' => $this->input->post('country'),
				'subject' => $this->input->post('subject'),
			));

			$this->session->set_flashdata('error_message', 'Submitted Successfully');
			redirect(base_url('page/contact_us'));
		}
	}

}
