<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct() 
    {
		parent::__construct();

    }
    
	function index()
	{
		$data['title'] = 'Home';

		$this->load->view('home', $data);
	}
	
	function home1()
	{
		$data['title'] = 'home1';

		$this->load->view('home1', $data);
	}

}
