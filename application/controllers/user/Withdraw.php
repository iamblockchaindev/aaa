<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Withdraw extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_userLoginCheck();
		/****checks it is login or not End****/

		$this->load->model('Withdraw_model', 'withdraw');
		$this->load->model('Genealogy_model', 'genealogy');
		$this->load->model('Dashboard_model', 'dashboard');
	}

	function withdrawal_details()
	{
		$data['title'] = 'Withdrawal History';

		if (!empty($_GET['offset'])) {
			$offset = $_GET['offset'];
		} else {
			$offset = 1;
		}

		$per_page = 10;

		$start_limit = ($per_page * $offset) - $per_page;

		if ($this->input->get('status')) {
			$status = $this->input->get('status');
		} else {
			$status = '';
		}

		$data['data_result'] = $this->withdraw->getAllWithdrawDetails($start_limit, $per_page, $status);
		$total_records = $this->withdraw->getAllWithdrawDetailsCount($status);

		$config['base_url'] = base_url() . 'user/withdraw/withdrawal_details';
		$config['total_rows'] = $total_records;
		$config['per_page'] = $per_page;
		//$config["uri_segment"] = 3;
		$config['query_string_segment'] = 'offset';
		$config['page_query_string'] = true;
		// custom paging configuration
		//$config['num_links'] = 4;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;


		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';


		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="firstlink page-item page-link">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="lastlink page-item page-link">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="nextlink page-item page-link">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="numlink page-item page-link">';
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$data["links"] = $this->pagination->create_links();

		$this->load->view('user/withdraw/withdrawal_details', $data);
	}

	function make_withdrawal($param1 = '', $param2 = '')
	{
		$data['title'] = 'Make Withdrawal';

		$this->form_validation->set_rules('amount', 'Amount', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

		if ($this->form_validation->run() == FALSE) {
			$data['wallet_amount'] = $user_data->unlocked_pwt;
			$data['user_data'] = $user_data;
			$data['data_result'] = $this->withdraw->getAllWithdrawDetails();
			$data['total_records'] = $data['data_result'] == 0 ? 0 : count($data['data_result']);
			$this->load->view('user/withdraw/make_withdrawal', $data);
		} else {
			if (is_maintenance() == 1) {
				goto next;
			}
			$usertree = $this->genealogy->get_user_matrix($this->session->user_id); // withdrawal allow only for RajKumar sir tree
			if ($usertree) {
				$user_id = $this->session->userdata('user_id');
				$amount = $this->input->post('amount', true);
				$withdrawal_in = $this->input->post('withdrawal_in', true);

				/**********Check Wallet condition**Start*******/

				$admin_charge =  $withdrawal_in == 'btc' ? 0 : 0;
				$isTrue = 1;


				// here amount
				$amount = floatval($amount);

				$is_acount = isAccountDetailsUpdated();


				if ($is_acount == 2) {
					$data['status'] = false;
					$data['message'] = "Please update your account details to withdraw amount !";
				} elseif ($amount < 5000) {
					$isTrue = 0;
					$data['status'] = false;
					$data['message'] = "Minimum withdrawal amount  5000$  or greater than!";
				} elseif (($withdrawal_in == 'btc')) {
					$isTrue = 0;
					$data['status'] = false;
					$data['message'] = "Minimum withdrawal amount including " . $admin_charge . "% deduction should be 100 $ !";
				} elseif ($amount > $user_data->unlocked_pwt) {
					$isTrue = 0;
					$data['status'] = false;
					$data['message'] = "Amount including " . $admin_charge . "% deduction can not be greater than your wallet amount !";
				} else {
					if ($isTrue == 1) {
						// echo "true"; die;
						$txn_id = '#W' . rand();
						$insert_data = array(
							'session_id' => $user_id,
							'username' => $user_data->username,
							'total_amount' => $this->input->post('amount'),
							'amount' => $amount,
							'admin_charge' => $admin_charge,
							'withdrawal_in' => $withdrawal_in,
							'status' => 'P',
							'created_datetime' => date('Y-m-d H:i:s'),
							'txn_id' => $txn_id
						);

						$this->common->insert_data('pw_withdrawal', $insert_data);

						$insert_data = array(
							'userid' => $this->session->user_id,
							'detail' => 'Withdrwal ' . $amount,
							'date' => date('Y-m-d H:i:s'),
						);
						$this->common->insert_data('pw_logs', $insert_data);

						/** deduct amount from wallet***Start**/
						$u_query = "update pw_users set unlocked_pwt = unlocked_pwt - " . $amount . " where id = '" . $user_id . "'";
						$this->db->query($u_query);
						/** deduct amount from wallet***End**/

						/********Mail***Start****/
						$from_email = ADMIN_INFO_EMAIL;
						$to_email = $user_data->email;
						$cc_email = "support@pointwish.io";
						$WEB_URL = base_url();
						$htmlContent = file_get_contents("./mail_html/transfer.html");
						$tpl1 = str_replace('{{TXN_ID}}', $txn_id, $htmlContent);
						$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
						$tpl3 = str_replace('{{TXN_TYPE}}', "Withdraw Fund", $tpl2);
						$tpl4 = str_replace('{{DATETIME}}', date('Y-m-d H:i:s'), $tpl3);
						$message_html = str_replace('{{AMOUNT}}', $amount, $tpl4);
						$config['mailtype'] = 'html';
						// PHPMailer object
						$mail = $this->phpmailer_lib->load();
						// SMTP configuration
						$mail->isSMTP();
						$mail->Host     = SMTP_HOST;
						$mail->SMTPAuth = true;
						$mail->Username = ADMIN_NO_REPLY_EMAIL;
						$mail->Password = MAIL_PWD;
						$mail->SMTPSecure = 'ssl';
						$mail->Port     = SMTP_PORT;

						$mail->setFrom($from_email, 'Pointwish');
						$mail->addReplyTo($from_email, 'Pointwish');
						$mail->addAddress($to_email);
						$mail->addAddress($cc_email);
						$mail->Subject = 'Point Wish: Withdrawal';
						$mail->isHTML(true);
						$mail->Body = $message_html;

						if (!$mail->send()) {
							$data['message'] = $mail->ErrorInfo;
						} else {
							$mails = $this->common->getSingle('pw_daily_mailcount', array('date' => date('Y-m-d')));
							if ($mails) {
								$update_data = array(
									'count' => $mails->count + 1
								);
								$this->common->update_data('pw_daily_mailcount', array('date' => date('Y-m-d')), $update_data);
							} else {
								$insert_data = array(
									'date' => date('Y-m-d'),
									'count' => 1
								);
								$this->common->insert_data('pw_daily_mailcount', $insert_data);
							}
						}
						$this->session->set_flashdata('error_message', "You have successfully sent withdraw request !");
						$data['status'] = true;
					}
				}
			} else {
				next:
				$data['status'] = false;
				$data['message'] = "Website is under-maintenance for new upgrades and withdrawal will be started soon.Thank You";
			}
			echo json_encode($data);
		}
	}

	function convert_price()
	{
		if ($_POST) {

			$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->user_id));
			$totalDeposit = $this->dashboard->get_total_deposit();

			// if ($totalDeposit >= 10) {
			$amount = (float)$this->input->post('amount');
			$from = $this->input->post('fromCurrecy');
			$to = $this->input->post('toCurrecy');

			// $user_data = $this->common->getSingle('pw_users', array('id' => $this->session->user_id));
			if (($from == 'PWT' || $from == 'USDT') && ($to == 'PWT' || $to == 'USDT')) {

				if ($from == 'PWT' && ($amount < 10 || $amount > 10000)) {
					$data['success'] = false;
					$data['message'] = "Amount should not be greater than 10000 PWT";
				} else if ($from == 'USDT' && ($amount < 10 || $amount > 5000)) {
					$data['success'] = false;
					$data['message'] = "Amount should not be greater than 5000 USDT";
				} else if ($from == 'PWT' && $amount > $user_data->unlocked_pwt) {
					$data['success'] = false;
					$data['message'] = "Insufficient Funds";
				} else if ($from == 'USDT' && $amount > $user_data->wallet_amount) {
					$data['success'] = false;
					$data['message'] = "Insufficient Funds";
				} else {
					$pwtPrice = get_pwt()['current_price'];
					$conv = 0;

					if ($from == 'PWT') {
						$calc = $amount * $pwtPrice;
					} else if ($from == 'USDT') {
						$calc = $amount / $pwtPrice;
					}
					// $exchange = $this->dashboard->count_total_exchange();
					// $allow = $totalDeposit * 3; //Allow for convert pwt to usdt;
					// if ($from == 'PWT' && ($exchange > $allow || $calc > $allow)) {
					//     $data['success'] = false;
					//     $data['message'] = $allow - $exchange . " is left for exchange";
					//     goto next;
					// }
					$res = $calc; //Others 0.1% charge
					$conv = number_format($res, 4);
					/**********Convert History**Start*******/
					$history = array(
						'user_id' => $this->session->user_id,
						'from_currency' => $from,
						'to_currency' => $to,
						'amount' => $amount,
						'conv_amount' => $res,
						'date' => date('Y-m-d')
					);
					$this->common->insert_data('pwt_exchange_history', $history);
					/**********Convert History**End*******/

					/**********Update Wallet & History**Start*******/
					if ($from == 'PWT') {
						$user_query = "update pw_users set wallet_amount = wallet_amount + " . $res . ", unlocked_pwt = unlocked_pwt - " . $amount . " where id = '" . $this->session->user_id . "'";

						$this->db->query($user_query);
						/*******Update PWT Supply **Start*****/
						$u_query = "update pwt_set_price SET total_supply = total_supply +" . $amount . ", total_supplied = total_supplied - " . $amount .  " where id = 1";
						$this->db->query($u_query);
						/*******Update PWT Supply**End*****/
					} else if ($from == 'USDT') {
						$user_query = "update pw_users set unlocked_pwt = unlocked_pwt + " . $res . ", wallet_amount = wallet_amount - " . $amount . " where id = '" . $this->session->user_id . "'";

						$this->db->query($user_query);
						/*******Update PWT Supply **Start*****/
						$u_query = "update pwt_set_price SET total_supply = total_supply -" . $amount . ", total_supplied = total_supplied + " . $amount .  " where id = 1";
						$this->db->query($u_query);
						/*******Update PWT Supply**End*****/
					}

					/*******Notification **Start****/
					$insert_note = array(
						'from_user' => $this->session->user_id,
						'to_user' => $this->session->user_id,
						'log' => 'debit',
						'type' => 'exchange',
						'payment_type' => $from,
						'amount' => $amount,
						'balance' => $from == 'PWT' ? getWalletAmountByUserID($user_data->id)[3] : getWalletAmountByUserID($user_data->id)[0],
						'txn_id' => '#E' . rand(),
						'message' => "Convert PWT",
						'status' => 1,
						'created_datetime' => date('Y-m-d H:i:s'),
						'modified_datetime' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pwt_user_notification', $insert_note);

					$insert_note = array(
						'from_user' => $this->session->user_id,
						'to_user' => $this->session->user_id,
						'log' => 'credit',
						'type' => 'exchange',
						'payment_type' => $to,
						'amount' => $conv,
						'balance' => $from == 'PWT' ? getWalletAmountByUserID($user_data->id)[0] : getWalletAmountByUserID($user_data->id)[3],
						'txn_id' => '#E' . rand(),
						'message' => "Convert PWT",
						'status' => 1,
						'created_datetime' => date('Y-m-d H:i:s'),
						'modified_datetime' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pwt_user_notification', $insert_note);
					/*******Notification **End****/

					/*******Update PWT Price **Start*****/
					$get = get_pwt();
					$cprice = (float)$get['current_price'];
					$supply = (float)$get['total_supply'];
					$purchase = $amount * 100 / $supply;
					$price =  $cprice * $purchase / 100;
					$current_price = $from == 'PWT' ? $cprice - $price : $cprice + $price;
					$update_data = array(
						'current_price' => $current_price
					);
					$this->common->update_data('pwt_set_price', array('id' => 1), $update_data);

					$insert_history = array(
						'current_price' => get_pwt()['current_price'], // 10%
						'created_time' =>  date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pwt_coin_history', $insert_history);
					/*******Update PWT Price **End*****/

					/**********Update Wallet & History**End*******/

					$data['success'] = true;
					$data['message'] = "Converted Successfully";
				}
			} else {
				$data['success'] = false;
				$data['message'] = "Invalid Parameter";
			}
			// } else {
			//     $data['success'] = false;
			//     $data['message'] = "Please activate the agency !";
			// }
			next:
			echo json_encode($data);
		}
	}
}
