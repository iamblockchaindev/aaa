<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ticket extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_userLoginCheck();
		/****checks it is login or not End****/

		$this->load->model('Ticket_model', 'ticket');
	}

	function index()
	{
		$data['title'] = 'Ticket';

		$data['total_ticket'] = $this->ticket->getMyTotalTicketCount();

		$data['total_pending_ticket'] = $this->ticket->getMyPendingTicketCount();
		$data['total_inprocess_ticket'] = $this->ticket->getMyInProcessTicketCount();
		$data['total_close_ticket'] = $this->ticket->getMyCloseTicketCount();

		$this->load->view('user/ticket/create_ticket', $data);
	}

	function create_ticket()
	{
		$data['title'] = 'Ticket';

		$data['total_ticket'] = $this->ticket->getMyTotalTicketCount();

		$data['total_pending_ticket'] = $this->ticket->getMyPendingTicketCount();
		$data['total_inprocess_ticket'] = $this->ticket->getMyInProcessTicketCount();
		$data['total_close_ticket'] = $this->ticket->getMyCloseTicketCount();
		$this->form_validation->set_rules('first_name', 'First name', 'required');
		$this->form_validation->set_rules('subject', 'Subject', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('user/ticket/create_ticket', $data);
		} else {

			/*******Upload Photo***Start***/
			if (empty($_FILES['attachment']['name'])) {
				$attachment = '';
			} else {
				$config['upload_path'] = './uploads/ticket_attachment/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 1024 * 5; //  here it is 5 MB
				$config['encrypt_name'] = TRUE;

				$this->upload->initialize($config);

				if (!$this->upload->do_upload('attachment')) {
					$msg = $this->upload->display_errors('', '');
				} else {
					$data = $this->upload->data();
					$attachment = $data["file_name"];
					$this->resize_image($attachment);
				}
			}
			/*******Upload Photo***End***/

			$ticket_id = '';
			$insert_data = array(
				'user_id' => $this->session->userdata('user_id'),
				'first_name' => $this->input->post('first_name', true),
				'last_name' => $this->input->post('first_name', true),
				'email' => $this->session->user_email,
				'attachment' => $attachment,
				'subject' => $this->input->post('subject', true),
				'description' => $this->input->post('description', true),
				'created_datetime' => date('Y-m-d H:i:s'),
				'modified_datetime' => date('Y-m-d H:i:s'),
			);

			$insert_id = $this->common->insert_data('pw_tickets', $insert_data);


			/*******Update Ticket ID **Start****/
			$ticket_id = 'TC' . (10000 + $insert_id);

			$insert_data = array(
				'userid' => $this->session->user_id,
				'detail' => 'TC100' . $ticket_id . 'is created',
				'date' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_logs', $insert_data);

			$update_data = array(
				'ticket_id' => $ticket_id
			);
			$this->common->update_data('pw_tickets', array('id' => $insert_id), $update_data);
			/*******Update Ticket ID **End****/

			/*********Insert for conversation ***Start******/
			$insert_data2 = array(
				'user_id' => $this->session->userdata('user_id'),
				'ticket_id' => $ticket_id,
				'description' => $this->input->post('description', true),
				'from' => 'user',
				'created_datetime' => date('Y-m-d H:i:s'),
				'modified_datetime' => date('Y-m-d H:i:s'),
			);

			$insert_id2 = $this->common->insert_data('pw_tickets_chat', $insert_data2);

			/*********Insert for conversation ***End******/

			$this->session->set_flashdata('error_message', 'Ticket generated successfully !');

			redirect(base_url() . 'user/ticket/tickets_list?type=open');
		}
	}

	function tickets_list()
	{
		$type = $this->input->get('type', true);

		if ($type == 'all') {
			$data['title'] = 'All Tickets List';
		} else if ($type == 'open') {
			$data['title'] = 'Open Tickets List';
		} else if ($type == 'process') {
			$data['title'] = 'Pending Tickets List';
		} else if ($type == 'close') {
			$data['title'] = 'Close Tickets List';
		}

		$data['type'] = $type;
		$data['data_result'] = $this->ticket->getTicketListByType();
		$data['total_rec'] = $data['data_result'] == 0 ? 0 : count($data['data_result']);
		$this->load->view('user/ticket/ticket_list', $data);
	}

	function view_ticket()
	{
		$ticket_id = $this->input->get('ticket_id', true);

		$ticket_data = $this->common->getSingle('pw_tickets', array('ticket_id' => $ticket_id));

		if (empty($ticket_data)) {
			redirect(base_url());
		}

		$data['title'] = 'View Ticket';

		$data['data_result'] = $this->common->getSingle('pw_tickets', array('ticket_id' => $ticket_id));

		$this->load->view('user/ticket/view_ticket', $data);
	}


	function view_chat()
	{

		$data['title'] = 'View Conversation';

		// $data['data_result'] = $this->ticket->getMyTicketChatList();

		$this->load->view('user/ticket/view_chat', $data);
	}	

	function submit_ticket_reply()
	{
		if ($_POST['description'] && $_POST['ticket_id']) {
			$ticket_id = $this->input->post('ticket_id', true);
			$description = $this->input->post('description', true);

			$insert_data = array(
				'user_id' => $this->session->userdata('user_id'),
				'ticket_id' => $ticket_id,
				'description' => $description,
				'from' => 'user',
				'created_datetime' => date('Y-m-d H:i:s'),
				'modified_datetime' => date('Y-m-d H:i:s'),
			);

			$this->common->insert_data('pw_tickets_chat', $insert_data);

			$this->session->set_flashdata('error_message', 'Sent reply successfully !');
			// redirect(base_url() . 'user/ticket/view_chat?ticket_id=' . $ticket_id);
			$time = date('h:i A');

			$response_data = array('status' => 1, 'message' => $description, 'time' => $time);
			echo json_encode($response_data);
			exit;
		} else {
			redirect(base_url());
		}
	}

	function getMessageByTicketID()
	{
		if ($_POST['ticket_id']) {
			$ticket_id = $this->input->post('ticket_id', true);

			$query = "select * from pw_tickets_chat where ticket_id = '" . $ticket_id . "'";
			$data = $this->db->query($query);

			$message_html = '';

			if ($data->num_rows() > 0) {
				$result = $data->result();

				foreach ($result as $row) {
					if ($row->from == 'user') {
						$message_html .= '<div class="chat__box__text-box flex items-end float-right mb-4">';
						$message_html .=     '<div class="bg-theme-1 px-4 py-3 text-white rounded-l-md rounded-t-md">';
						$message_html .=        $row->description;
						$message_html .=         '<div class="mt-1 text-xs text-theme-25">';
						$message_html .=             date('h:i A', strtotime($row->created_datetime));
						$message_html .=         '</div>';
						$message_html .=     '</div>';
						$message_html .=     '<div class="w-10 h-10 hidden sm:block flex-none image-fit relative ml-5">';
						$message_html .=         '<img src="' . base_url() . 'assets/user_panel/images/user.png" class="rounded-full" alt="avatar"> <span class="avatar-status bg-teal"></span>';
						$message_html .=     '</div>';
						$message_html .= '</div>';
						$message_html .= '<div class="clear-both"></div>';

					} else if ($row->from == 'admin') {
						$message_html .= '<div class="chat__box__text-box flex items-end float-left mb-4">';
						$message_html .=     '<div class="w-10 h-10 hidden sm:block flex-none relative mr-5">';
						$message_html .=         '<img src="' . base_url() . 'assets/user_panel/images/user.jpg" class="rounded-full" alt="avatar">';
						$message_html .=     '</div>';
						$message_html .=     '<div class="bg-gray-200 dark:bg-dark-5 px-4 py-3 text-gray-700 dark:text-gray-300 rounded-r-md rounded-t-md">';
						$message_html .=        $row->description;
						$message_html .=         '<div class="mt-1 text-xs text-gray-600">';
						$message_html .=             date('h:i A', strtotime($row->created_datetime));
						$message_html .=         '</div>';
						$message_html .=     '</div>';
						
						$message_html .= '</div>';
						$message_html .= '<div class="clear-both"></div>';
					}
				}
			}

			$response_data = array('status' => 1, 'message_html' => $message_html);
			echo json_encode($response_data);
			exit;
		} else {
			redirect(base_url());
		}
	}

	private function resize_image($file_name)
	{
		$this->load->library('image_lib');

		$config['image_library'] = 'gd2';
		$config['source_image'] = './uploads/ticket_attachment/' . $file_name;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = TRUE;
		$config['width']     = 600;
		$config['height']   = 400;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$this->image_lib->clear();
	}
}
