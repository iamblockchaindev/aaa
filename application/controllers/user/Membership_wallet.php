<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Membership_wallet extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_userLoginCheck();
		/****checks it is login or not End****/
	}

	function index()
	{
		$data['title'] = 'Wallet Management';
// 		$this->set_level_withdrawal_amount_n();
		$this->load->view('user/profile/membership_wallet', $data);
	}

	private function set_level_withdrawal_amount()
	{
		// Move amount after 20 days of level complettion date

		$user_id = $this->session->userdata('user_id');

		$query = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_complete_amount != '0' and NOW() >=  `level_complete_date` + INTERVAL 20 DAY";

		$data =  $this->db->query($query);

		if ($data->num_rows() > 0) {
			$result = $data->result();

			foreach ($result as $row) {
				$amount = $row->level_complete_amount;

				/*** Update**`pw_non_working_user`****Start****/
				$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
				$this->db->query($update_query);
				/*** Update**`pw_non_working_user`****End****/

				/*** Update**`pw_non_working_level`****Start****/
				$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
				$this->db->query($update_query);
				/*** Update**`pw_non_working_level`****End****/
			}
		}
	}

	private function set_level_withdrawal_amount_n()
	{
		$days = 1; // Move amount after 20 days of level complettion date
		$current_date = date('Y-m-d');

		$user_id = $this->session->userdata('user_id');

		$package_arr = array('30', '40', '100', '1000', '5000', '10000');

		for ($i = 0; $i < count($package_arr); $i++) {

			$query = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_number = '1' and package = '" . $package_arr[$i] . "'";
			$data =  $this->db->query($query);

			if ($data->num_rows() > 0) {
				$row = $data->row();

				$next_date = date('Y-m-d', strtotime('+' . $days . ' days', strtotime($row->level_complete_date)));

				if ($next_date <= $current_date) {
					$amount = $row->level_complete_amount;

					/*** Update**`pw_non_working_user`****Start****/
					$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
					$this->db->query($update_query);
					/*** Update**`pw_non_working_user`****End****/

					/*** Update**`pw_non_working_level`****Start****/
					$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
					$this->db->query($update_query);
					/*** Update**`pw_non_working_level`****End****/

					/******Level 2 **********Start**********/
					$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($next_date)) . " +" . $days . " day"));
					$query2 = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_number = '2' and package = '" . $package_arr[$i] . "'";
					$data =  $this->db->query($query2);
					if ($data->num_rows() > 0) {
						$row = $data->row();

						if ($next_date <= $current_date) {
							$amount = $row->level_complete_amount;

							/*** Update**`pw_non_working_user`****Start****/
							$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
							$this->db->query($update_query);
							/*** Update**`pw_non_working_user`****End****/

							/*** Update**`pw_non_working_level`****Start****/
							$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
							$this->db->query($update_query);
							/*** Update**`pw_non_working_level`****End****/

							/******Level 3 **********Start**********/
							$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($next_date)) . " +" . $days . " day"));
							$query3 = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_number = '3' and package = '" . $package_arr[$i] . "'";
							$data =  $this->db->query($query3);
							if ($data->num_rows() > 0) {

								$row = $data->row();

								if ($next_date <= $current_date) {
									$amount = $row->level_complete_amount;

									/*** Update**`pw_non_working_user`****Start****/
									$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
									$this->db->query($update_query);
									/*** Update**`pw_non_working_user`****End****/

									/*** Update**`pw_non_working_level`****Start****/
									$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
									$this->db->query($update_query);
									/*** Update**`pw_non_working_level`****End****/

									/******Level 4 **********Start**********/
									$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($next_date)) . " +" . $days . " day"));
									$query4 = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_number = '4' and package = '" . $package_arr[$i] . "'";
									$data =  $this->db->query($query4);
									if ($data->num_rows() > 0) {
										$row = $data->row();

										if ($next_date <= $current_date) {
											$amount = $row->level_complete_amount;

											/*** Update**`pw_non_working_user`****Start****/
											$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
											$this->db->query($update_query);
											/*** Update**`pw_non_working_user`****End****/

											/*** Update**`pw_non_working_level`****Start****/
											$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
											$this->db->query($update_query);
											/*** Update**`pw_non_working_level`****End****/

											/******Level 5 **********Start**********/
											$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($next_date)) . " +" . $days . " day"));
											$query5 = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_number = '5' and package = '" . $package_arr[$i] . "'";
											$data =  $this->db->query($query5);
											if ($data->num_rows() > 0) {
												$row = $data->row();

												if ($next_date <= $current_date) {
													$amount = $row->level_complete_amount;

													/*** Update**`pw_non_working_user`****Start****/
													$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
													$this->db->query($update_query);
													/*** Update**`pw_non_working_user`****End****/

													/*** Update**`pw_non_working_level`****Start****/
													$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
													$this->db->query($update_query);
													/*** Update**`pw_non_working_level`****End****/


													/******Level 6 **********Start**********/
													$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($next_date)) . " +" . $days . " day"));
													$query6 = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_number = '6' and package = '" . $package_arr[$i] . "'";
													$data =  $this->db->query($query6);
													if ($data->num_rows() > 0) {
														$row = $data->row();

														if ($next_date <= $current_date) {
															$amount = $row->level_complete_amount;

															/*** Update**`pw_non_working_user`****Start****/
															$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
															$this->db->query($update_query);
															/*** Update**`pw_non_working_user`****End****/

															/*** Update**`pw_non_working_level`****Start****/
															$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
															$this->db->query($update_query);
															/*** Update**`pw_non_working_level`****End****/


															/******Level 7 **********Start**********/
															$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($next_date)) . " +" . $days . " day"));
															$query7 = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_number = '7' and package = '" . $package_arr[$i] . "'";
															$data =  $this->db->query($query7);
															if ($data->num_rows() > 0) {
																$row = $data->row();

																if ($next_date <= $current_date) {
																	$amount = $row->level_complete_amount;

																	/*** Update**`pw_non_working_user`****Start****/
																	$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
																	$this->db->query($update_query);
																	/*** Update**`pw_non_working_user`****End****/

																	/*** Update**`pw_non_working_level`****Start****/
																	$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
																	$this->db->query($update_query);
																	/*** Update**`pw_non_working_level`****End****/


																	/******Level 8 **********Start**********/
																	$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($next_date)) . " +" . $days . " day"));
																	$query8 = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_number = '8' and package = '" . $package_arr[$i] . "'";
																	$data =  $this->db->query($query8);
																	if ($data->num_rows() > 0) {
																		$row = $data->row();

																		if ($next_date <= $current_date) {
																			$amount = $row->level_complete_amount;

																			/*** Update**`pw_non_working_user`****Start****/
																			$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
																			$this->db->query($update_query);
																			/*** Update**`pw_non_working_user`****End****/

																			/*** Update**`pw_non_working_level`****Start****/
																			$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
																			$this->db->query($update_query);
																			/*** Update**`pw_non_working_level`****End****/

																			/******Level 9 **********Start**********/
																			$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($next_date)) . " +" . $days . " day"));
																			$query9 = "select * from pw_non_working_level where user_id = '" . $user_id . "' and level_number = '9' and package = '" . $package_arr[$i] . "'";
																			$data =  $this->db->query($query9);
																			if ($data->num_rows() > 0) {
																				$row = $data->row();

																				if ($next_date <= $current_date) {
																					$amount = $row->level_complete_amount;

																					/*** Update**`pw_non_working_user`****Start****/
																					$update_query = "update pw_non_working_user set withdrawable_amount = withdrawable_amount + " . $amount . ", balance = balance + " . $amount . " where user_id = '" . $user_id . "' and package = '" . $row->package . "'";
																					$this->db->query($update_query);
																					/*** Update**`pw_non_working_user`****End****/

																					/*** Update**`pw_non_working_level`****Start****/
																					$update_query = "update pw_non_working_level set level_complete_amount = 0 where id = '" . $row->id . "'";
																					$this->db->query($update_query);
																					/*** Update**`pw_non_working_level`****End****/
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} // for loop end
	}

	function transfer_amount()
	{
		$this->form_validation->set_rules('package', 'Package', 'required');
		$this->form_validation->set_rules('amount', 'Amount', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if ($this->form_validation->run() == FALSE) {
			$message = 'Amount can not be empty !';
			$status = 2;

			$json_data = array('status' => $status, 'message' => $message);
			echo json_encode($json_data);
			exit;
		} else {
			$package_arr = array('30', '40', '100', '1000', '5000', '10000');

			$package = $this->input->post('package', true);
			$amount = $this->input->post('amount', true);


			$message = '';
			$status = '';

			if (empty((float)$amount)) {
				$message = 'Please enter valid amount !';
				$status = 3;

				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			}

			$membership_data = $this->common->getSingle('pw_non_working_user', array('user_id' => $this->session->userdata('user_id'), 'package' => $package));

			if (empty($membership_data)) {
				$message = 'Invalid withdrawal !';
				$status = 4;

				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			}

			if ($amount < 8) // minimum 8 dollor
			{
				$message = 'Minimum 8$ can be withdrawn !';
				$status = 7;

				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			}

			if ($membership_data->balance < $amount) {
				$message = 'Maximum ' . number_format($membership_data->balance, '2') . '$ can be withdrawn !';
				$status = 5;

				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			}

			$minimum_direct = $this->minimum_direct_by_package($package);
			$direct_member_count = $this->getMyDirectMemberCount();

			if ($direct_member_count < $minimum_direct) {
				$message = 'Minimum ' . $minimum_direct . ' direct member should be there !';
				$status = 6;

				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			}

			$is_next_upgraded = $this->isNextMembershipUpgraded($package);

			if ($is_next_upgraded == 0) {
				$message = 'Next Membership should be upgraded !';
				$status = 7;

				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			}

			/********Update `user_upgrade_member` Start********/
			$query1 = "update pw_users set wallet_amount = wallet_amount + " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
			$this->db->query($query1);
			/********Update `user_upgrade_member` End********/

			/********Update `pw_non_working_user` Start********/
			$query2 = "update pw_non_working_user set withdrawal = withdrawal + " . $amount . ", balance = balance - " . $amount . " where user_id = '" . $this->session->userdata('user_id') . "' and package = '" . $package . "'";
			$this->db->query($query2);
			/********Update `pw_non_working_user` End********/

			/********insert **`pw_local_main_wallet_withdrawal` Start********/
			$insert_data = array(
				'user_id' => $this->session->userdata('user_id'),
				'package' => $package,
				'amount' => $amount,
				'withdrawal_datetime' => date('Y-m-d H:i:s'),
			);

			$this->common->insert_data('pw_local_main_wallet_withdrawal', $insert_data);

			/********insert **`pw_local_main_wallet_withdrawal` End********/

			/********insert **`pw_user_notification` Start********/

			if ($package ==  '30')  $pkg = 'OPAL';
			if ($package ==  '40')  $pkg = 'JADE';
			if ($package ==  '100')  $pkg = 'RED BERYL';
			if ($package ==  '1000')  $pkg = 'BLUE NILE';
			if ($package ==  '5000')  $pkg = 'ETERNITY';
			if ($package ==  '10000')  $pkg = 'KOH-I-NOOR';

			$insert_data = array(
				'from_user' => $this->session->userdata('user_id'),
				'to_user' => $this->session->userdata('user_id'),
				'log' => 'debit',
				'amount' => $amount,
				'balance' => getWalletAmountByUserID($this->session->userdata('user_id')),
				'txn_id' => '#W' . rand(),
				'message' => $pkg . " Withdraw amount",
				'status' => 1,
				'created_datetime' => date('Y-m-d H:i:s'),
				'modified_datetime' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_user_notification', $insert_data);

			/********insert **`pw_user_notification` End********/

			/********insert **`pw_logs` Start********/

			$insert_data = array(
				'userid' => $this->session->user_id,
				'detail' => 'Withdrwal '.$pkg.' '.$amount . '$',
				'date' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_logs', $insert_data);

			/********insert **`pw_logs` End********/

			$this->session->set_flashdata('error_message', 'Withdrawal done successfully !');

			$status = 1;
			$json_data = array('status' => $status, 'message' => $message);
			echo json_encode($json_data);
			exit;
		}
	}

	private function isNextMembershipUpgraded($package)
	{
		$status = 0;

		$current_package = getMyCurrentMembershipPackage();

		if ($package == '10000') {
			$status == 1;
		} else {
			if ($package < $current_package) {
				$status = 1;
			}
		}

		return $status;
	}

	private function getMyDirectMemberCount()
	{
		$query = "select count(*) as total_count from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
		$data =  $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();
			return $result->total_count;
		} else {
			return 0;
		}
	}

	private function minimum_direct_by_package($package)
	{
		if ($package == 30) {
			$minimum_direct = 3;
		} else if ($package == 40) {
			$minimum_direct = 5;
		} else if ($package == 100) {
			$minimum_direct = 7;
		} else if ($package == 1000) {
			$minimum_direct = 9;
		} else if ($package == 5000) {
			$minimum_direct = 11;
		} else if ($package == 10000) {
			$minimum_direct = 13;
		}

		return $minimum_direct;
	}


	function transfer_amount_other()
	{
		$this->form_validation->set_rules('type', 'type', 'required');
		$this->form_validation->set_rules('amount', 'Amount', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if ($this->form_validation->run() == FALSE) {
			$message = 'Amount can not be empty !';
			$status = 2;

			$json_data = array('status' => $status, 'message' => $message);
			echo json_encode($json_data);
			exit;
		} else {
			$type_arr = array('level', 'direct');

			$type = $this->input->post('type', true);
			$amount = $this->input->post('amount', true);


			$message = '';
			$status = '';

			if (empty((float)$amount)) {
				$message = 'Please enter valid amount !';
				$status = 3;

				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			}

			if ($type == 'level') {
				$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

				if (empty($user_data)) {
					$message = 'Invalid withdrawal !';
					$status = 4;

					$json_data = array('status' => $status, 'message' => $message);
					echo json_encode($json_data);
					exit;
				}

				if ($amount < 8) // minimum 8 dollor
				{
					$message = 'Minimum 8$ can be withdrawn !';
					$status = 6;

					$json_data = array('status' => $status, 'message' => $message);
					echo json_encode($json_data);
					exit;
				}

				if ($user_data->level_income_balance < $amount) {
					$message = 'Maximum ' . number_format($user_data->level_income_balance, '2') . '$ can be withdrawn !';
					$status = 5;

					$json_data = array('status' => $status, 'message' => $message);
					echo json_encode($json_data);
					exit;
				}

				/********Update `user_upgrade_member` Start********/
				$query1 = "update pw_users set wallet_amount = wallet_amount + " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
				$this->db->query($query1);
				/********Update `user_upgrade_member` End********/

				/********Update `pw_non_working_user` Start********/
				$query2 = "update pw_users set level_income_withdrawal = level_income_withdrawal + " . $amount . ", level_income_balance = level_income_balance - " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
				$this->db->query($query2);
				/********Update `pw_non_working_user` End********/

				/********insert **`pw_local_main_wallet_withdrawal` Start********/
				$insert_data = array(
					'user_id' => $this->session->userdata('user_id'),
					'type' => $type,
					'amount' => $amount,
					'withdrawal_datetime' => date('Y-m-d H:i:s'),
				);

				$this->common->insert_data('pw_local_main_wallet_withdrawal', $insert_data);

				/********insert **`pw_local_main_wallet_withdrawal` End********/

				/********insert **`pw_user_notification` Start********/				

				$insert_data = array(
					'from_user' => $this->session->userdata('user_id'),
					'to_user' => $this->session->userdata('user_id'),
					'log' => 'debit',
					'amount' => $amount,
					'balance' => getWalletAmountByUserID($this->session->userdata('user_id')),
					'txn_id' => '#W' . rand(),
					'message' => "Team Bonus amount",
					'status' => 1,
					'created_datetime' => date('Y-m-d H:i:s'),
					'modified_datetime' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_user_notification', $insert_data);

				/********insert **`pw_user_notification` End********/

				/********insert **`pw_logs` Start********/
				$insert_data = array(
					'userid' => $this->session->user_id,
					'detail' => 'Withdrwal '.$type.' '. $amount . '$',
					'date' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_logs', $insert_data);

				/********insert **`pw_logs` End********/

				$this->session->set_flashdata('error_message', 'Withdrawal done successfully !');

				$status = 1;
				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			} else if ($type == 'direct') {
				$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

				if (empty($user_data)) {
					$message = 'Invalid withdrawal !';
					$status = 4;

					$json_data = array('status' => $status, 'message' => $message);
					echo json_encode($json_data);
					exit;
				}

				if ($amount < 8) // minimum 8 dollor
				{
					$message = 'Minimum 8$ can be withdrawn !';
					$status = 6;

					$json_data = array('status' => $status, 'message' => $message);
					echo json_encode($json_data);
					exit;
				}

				if ($user_data->direct_income_balance < $amount) {
					$message = 'Maximum ' . number_format($user_data->direct_income_balance, '2') . '$ can be withdrawn !';
					$status = 5;

					$json_data = array('status' => $status, 'message' => $message);
					echo json_encode($json_data);
					exit;
				}

				/********Update `user_upgrade_member` Start********/
				$query1 = "update pw_users set wallet_amount = wallet_amount + " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
				$this->db->query($query1);
				/********Update `user_upgrade_member` End********/

				/********Update `pw_non_working_user` Start********/
				$query2 = "update pw_users set direct_income_withdrawal = direct_income_withdrawal + " . $amount . ", direct_income_balance = direct_income_balance - " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
				$this->db->query($query2);
				/********Update `pw_non_working_user` End********/

				/********insert **`pw_local_main_wallet_withdrawal` Start********/
				$insert_data = array(
					'user_id' => $this->session->userdata('user_id'),
					'type' => $type,
					'amount' => $amount,
					'withdrawal_datetime' => date('Y-m-d H:i:s'),
				);

				$this->common->insert_data('pw_local_main_wallet_withdrawal', $insert_data);

				/********insert **`pw_local_main_wallet_withdrawal` End********/

				/********insert **`pw_user_notification` Start********/

				$insert_data = array(
					'from_user' => $this->session->userdata('user_id'),
					'to_user' => $this->session->userdata('user_id'),
					'log' => 'debit',
					'amount' => $amount,
					'balance' => getWalletAmountByUserID($this->session->userdata('user_id')),
					'txn_id' => '#W' . rand(),
					'message' => "Fixed Bonus amount",
					'status' => 1,
					'created_datetime' => date('Y-m-d H:i:s'),
					'modified_datetime' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_user_notification', $insert_data);

				/********insert **`pw_logs` Start********/
				$insert_data = array(
					'userid' => $this->session->user_id,
					'detail' => 'Withdrwal Direct Income '. $amount . '$',
					'date' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_logs', $insert_data);

				/********insert **`pw_logs` End********/

				/********insert **`pw_user_notification` End********/

				$this->session->set_flashdata('error_message', 'Withdrawal done successfully !');

				$status = 1;
				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			} else if ($type == 'bonus') {
				$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

				if (empty($user_data)) {
					$message = 'Invalid withdrawal !';
					$status = 4;

					$json_data = array('status' => $status, 'message' => $message);
					echo json_encode($json_data);
					exit;
				}

				// if ($amount < 8) // minimum 8 dollor
				// {
				// 	$message = 'Minimum 8$ can be withdrawn !';
				// 	$status = 6;

				// 	$json_data = array('status' => $status, 'message' => $message);
				// 	echo json_encode($json_data);
				// 	exit;
				// }

				if($user_data->bonus_balance > 0) {
					if ($user_data->bonus_balance < $amount) {
						$message = 'Insufficient Balance !';
						$status = 5;
	
						$json_data = array('status' => $status, 'message' => $message);
						echo json_encode($json_data);
						exit;
					}
				} else {
					if ($user_data->crypto_bonus <= $amount) {
						$message = 'Amount should be less than ' . number_format($user_data->crypto_bonus, '2') . '$ !';
						$status = 5;
	
						$json_data = array('status' => $status, 'message' => $message);
						echo json_encode($json_data);
						exit;
					}
				}

				/********Update `user_upgrade_member` Start********/
				$query1 = "update pw_users set wallet_amount = wallet_amount + " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
				$this->db->query($query1);
				/********Update `user_upgrade_member` End********/

				/********Update `pw_non_working_user` Start********/
				$query2 = "update pw_users set income_bonus = income_bonus + " . $amount . ", bonus_balance = crypto_bonus - " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
				$this->db->query($query2);
				/********Update `pw_non_working_user` End********/

				/********insert **`pw_local_main_wallet_withdrawal` Start********/
				$insert_data = array(
					'user_id' => $this->session->userdata('user_id'),
					'type' => $type,
					'amount' => $amount,
					'withdrawal_datetime' => date('Y-m-d H:i:s'),
				);

				$this->common->insert_data('pw_local_main_wallet_withdrawal', $insert_data);

				/********insert **`pw_local_main_wallet_withdrawal` End********/


				/********insert **`pw_user_notification` Start********/				

				$insert_data = array(
					'from_user' => $this->session->userdata('user_id'),
					'to_user' => $this->session->userdata('user_id'),
					'log' => 'debit',
					'amount' => $amount,
					'balance' => getWalletAmountByUserID($this->session->userdata('user_id')),
					'txn_id' => '#W' . rand(),
					'message' => "Crypto Bonus amount",
					'status' => 1,
					'created_datetime' => date('Y-m-d H:i:s'),
					'modified_datetime' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_user_notification', $insert_data);

				$insert_data = array(
					'userid' => $this->session->user_id,
					'detail' => 'Withdraw Crypto Bonus ' . $amount . '$',
					'date' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_logs', $insert_data);

				/********insert **`pw_user_notification` End********/

				$this->session->set_flashdata('error_message', 'Withdrawal done successfully !');

				$status = 1;
				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			}  else if ($type == 'royalty') {
				$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

				if (empty($user_data)) {
					$message = 'Invalid withdrawal !';
					$status = 4;

					$json_data = array('status' => $status, 'message' => $message);
					echo json_encode($json_data);
					exit;
				}

				// if ($amount < 8) // minimum 8 dollor
				// {
				// 	$message = 'Minimum 8$ can be withdrawn !';
				// 	$status = 6;

				// 	$json_data = array('status' => $status, 'message' => $message);
				// 	echo json_encode($json_data);
				// 	exit;
				// }

				if ($user_data->royalty_income_balance < $amount) {
					$message = 'Maximum ' . number_format($user_data->royalty_income_balance, '2') . '$ can be withdrawn !';
					$status = 5;

					$json_data = array('status' => $status, 'message' => $message);
					echo json_encode($json_data);
					exit;
				}

				/********Update `user_upgrade_member` Start********/
				$query1 = "update pw_users set wallet_amount = wallet_amount + " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
				$this->db->query($query1);
				/********Update `user_upgrade_member` End********/

				/********Update `pw_non_working_user` Start********/
				$query2 = "update pw_users set royalty_income_withdrawal = royalty_income_withdrawal + " . $amount . ", royalty_income_balance = royalty_income_balance - " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
				$this->db->query($query2);
				/********Update `pw_non_working_user` End********/

				/********insert **`pw_local_main_wallet_withdrawal` Start********/
				$insert_data = array(
					'user_id' => $this->session->userdata('user_id'),
					'type' => 'team income',
					'amount' => $amount,
					'withdrawal_datetime' => date('Y-m-d H:i:s'),
				);

				$this->common->insert_data('pw_local_main_wallet_withdrawal', $insert_data);

				/********insert **`pw_local_main_wallet_withdrawal` End********/

				/********insert **`pw_user_notification` Start********/				

				$insert_data = array(
					'from_user' => $this->session->userdata('user_id'),
					'to_user' => $this->session->userdata('user_id'),
					'log' => 'debit',
					'amount' => $amount,
					'balance' => getWalletAmountByUserID($this->session->userdata('user_id')),
					'txn_id' => '#W' . rand(),
					'message' => "Team Income amount",
					'status' => 1,
					'created_datetime' => date('Y-m-d H:i:s'),
					'modified_datetime' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_user_notification', $insert_data);

				/********insert **`pw_user_notification` End********/

				/********insert **`pw_logs` Start********/
				$insert_data = array(
					'userid' => $this->session->user_id,
					'detail' => 'Withdrwal Team Income '. $amount . '$',
					'date' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_logs', $insert_data);

				/********insert **`pw_logs` End********/

				$this->session->set_flashdata('error_message', 'Withdrawal done successfully !');

				$status = 1;
				$json_data = array('status' => $status, 'message' => $message);
				echo json_encode($json_data);
				exit;
			} 
		}
	}
}
