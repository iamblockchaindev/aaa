<?php

use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Charge;
use CoinbaseCommerce\Webhook;

defined('BASEPATH') or exit('No direct script access allowed');

class Payment extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function coinbase(){
		if ($_POST) {
			$response_data = json_encode($_POST);
			$response = json_decode($response_data);

			if($response->amount && $response->token){				
				$apiClientObj = ApiClient::init('84450fe4-3844-4215-a374-788a23120206');
				$apiClientObj->verifySsl(false);
				$chargeData = array('name' => 'Deposit',
						'description' => 'Deposit USD to wallet',
						'local_price' => array(
							'amount' => $response->amount,
							'currency' => 'USD'),
						'meta_data' => array(
							'token' => $response->token,
							'amount' => $response->amount
							),
						'pricing_type' => 'fixed_price'
				);

				$result = Charge::create($chargeData);

				$res['address'] = $result->addresses[$response->type];
				$res['amount']  = $result->pricing[$response->type]['amount'];
				$res['currency']    = $result->pricing[$response->type]['currency'];
				$res['charge_id']   = $result->id;


				$user_data = $this->common->getSingle('pw_users', array('token' => $response->token));
				$create_time = date('Y-m-d H:i:s');
				$insert_data = array(
					'user_id' => $user_data->id,
					'txn_id' => $result->id,
					'amount' => $response->amount,
					'status' => 0,
					'status_message' => "Waiting For payment",
					'response_data' => json_encode($res),
					'created_datetime' => $create_time,
					'modified_datetime' => $create_time,
				);
				$this->common->insert_data('pw_add_fund_history', $insert_data);

				 header('Content-Type: application/json');
				 echo json_encode($res);
				 exit;

			}else{
				header('Content-Type: application/json');
				echo $this->_response("error");
				exit;
			}




		}else{
			header('Content-Type: application/json');
			echo $this->_response("error");
			exit;
		}
	}
	function check(){
		if ($_POST) {
			$response_data = json_encode($_POST);
			$response = json_decode($response_data);
			$token = $response->token;			
			$apiClientObj = ApiClient::init('84450fe4-3844-4215-a374-788a23120206');
			$apiClientObj->verifySsl(false);
			$chargeObj = Charge::retrieve($response->id);
			$payment = $chargeObj->payments;
			$fund_data = $this->common->getSingle('pw_add_fund_history', array('txn_id' => $response->id));
			if($fund_data->status == 0) {
				if ($payment[0]['status'] == 'PENDING') {
					$time = date('Y-m-d H:i:s');
					$update_data = array(
						'status' => 1,
						'status_message' => "Fund Sent , Waiting for Blockchain confirmations",
						'response_data' => json_encode($payment[0]),
						'modified_datetime' => $time,
					);
					$this->common->update_data('pw_add_fund_history', array('txn_id' => $response->id), $update_data);
				}
			}

			if($fund_data->status != 100){
				if ($payment[0]['status'] == 'CONFIRMED') {
					$amount = $payment[0]['value']['local']['amount'];					
					$time = date('Y-m-d H:i:s');
					$update_data = array(
						'status' => 100,
						'status_message' => "Confirmed",
						'response_data' => json_encode($payment[0]),
						'modified_datetime' => $time,
					);
					$this->common->update_data('pw_add_fund_history', array('txn_id' => $response->id), $update_data);

					/*******Update Wallet**Start*****/
					$u_query = "update pw_users SET wallet_amount = wallet_amount +" . $amount . " where token = '" . $token . "'";
					$this->db->query($u_query);
					/*******Update Wallet**End*****/

					/*******Insert Logs**Start*****/

					$insert_logs = array(
						'userid' =>  $fund_data->user_id,
						'detail' => 'Fund ' . $amount . '$',
						'date' => $time,
					);
					$this->common->insert_data('pw_logs', $insert_logs);
					
					// $insert_blogs = array(
					// 	'userid' =>  $fund_data->user_id,
					// 	'detail' => 'Crypto Bonus ' . $bonus . '$',
					// 	'date' => $time,
					// );
					// $this->common->insert_data('pw_logs', $insert_blogs);

					/*******Insert Logs**End*****/

					/*******Insert History**Start*****/
					// $insert_bonus = array(
					// 	'from_user' => $fund_data->user_id,
					// 	'to_user' => $fund_data->user_id,
					// 	'type' => 5,
					// 	'log' => 'credit',
					// 	'amount' => $bonus,
					// 	'balance' => get_wallet_balance($fund_data->user_id) + $bonus,
					// 	'txn_id' => '#CC' . rand(),
					// 	'message' => "Fund Bonus",
					// 	'status' => 1,
					// 	'created_datetime' => $time,
					// 	'modified_datetime' => $time,
					// );
					// $this->common->insert_data('pw_user_notification', $insert_bonus);
					
					$insert_note = array(
						'from_user' => $fund_data->user_id,
						'to_user' => $fund_data->user_id,						
						'log' => 'credit',
						'amount' => $amount,						
						'balance' => getWalletAmountByUserID($fund_data->user_id),
						'txn_id' => $response->id,
						'message' => "Deposit Fund",
						'status' => 1,
						'created_datetime' => $time,
						'modified_datetime' => $time,
					);
					$this->common->insert_data('pw_user_notification', $insert_note);
					/*******Insert History**End*****/

				}
			}

			header('Content-Type: application/json');
			echo $this->_response($fund_data);
			exit;


		}
	}

	function pay()
	{
		if ($_POST) {
			$response_data = json_encode($_POST);
			$json_decode =  json_decode($response_data);

			if (empty($json_decode->txn_id)) {
				redirect(base_url());
			}

			$txn_id = $json_decode->txn_id;
			$amount = $json_decode->amount1;
			$bonus = ($amount * 5) / 100;			
			$status = $json_decode->status;
			$status_text = $json_decode->status_text;

			$token = $json_decode->custom;

			$user_data = $this->common->getSingle('pw_users', array('token' => $token));

			$fund_data = $this->common->getSingle('pw_add_fund_history', array('txn_id' => $txn_id));
			$tx_id = '#F' . rand();
			if (!empty($fund_data)) {
			    $time = date('Y-m-d H:i:s');
				$update_data = array(
					'status' => $status,
					'status_message' => $status_text,
					'response_data' => $response_data,
					'modified_datetime' => $time,					
				);
				$this->common->update_data('pw_add_fund_history', array('txn_id' => $txn_id), $update_data);
				
				if ($status == '2' || $status == '100') {
					/*******Update Wallet**Start*****/				
					$u_query = "update pw_users SET wallet_amount = wallet_amount +" . $amount . ", crypto_bonus = crypto_bonus +" . $bonus." where token = '" . $token . "'";
					$this->db->query($u_query);
					/*******Update Wallet**End*****/
					
					$insert_logs = array(
						'userid' =>  $user_data->id,
						'detail' => 'Fund ' . $amount . '$',
						'date' => $time,
					);
					$this->common->insert_data('pw_logs', $insert_logs);
					
					$insert_blogs = array(
						'userid' =>  $user_data->id,
						'detail' => 'Crypto Bonus ' . $bonus . '$',
						'date' => $time,
					);
					$this->common->insert_data('pw_logs', $insert_blogs);
					
					/****BonusStart */								
					$insert_bonus = array(
						'from_user' => $user_data->id,
						'to_user' => $user_data->id,
						'type' => 5,
						'log' => 'credit',
						'amount' => $bonus,
						'balance' => get_wallet_balance($user_data->id) + $bonus,
						'txn_id' => '#CC' . rand(),
						'message' => "Fund Bonus",
						'status' => 1,
						'created_datetime' => $time,
						'modified_datetime' => $time,
					);
					$this->common->insert_data('pw_user_notification', $insert_bonus);
					
					$insert_note = array(
						'from_user' => $user_data->id,
						'to_user' => $user_data->id,
						'type' => 1,
						'log' => 'debit',
						'amount' => $amount,
						'balance' => get_wallet_balance($user_data->id) + $amount,
						'txn_id' => $txn_id,
						'message' => "Fund Transfer",
						'status' => 1,
						'created_datetime' => $time,
						'modified_datetime' => $time,
					);
					$this->common->insert_data('pw_user_notification', $insert_note);
	
					/****BonusEnd */
	
					/********Mail***Start****/
					$from_email = ADMIN_INFO_EMAIL;
					$to_email = $this->session->user_email;
					$subject = 'Point Wish: Transfer';
					$WEB_URL = base_url();
					$htmlContent = file_get_contents("./mail_html/transfer.html");
					$tpl1 = str_replace('{{TXN_ID}}', $txn_id, $htmlContent);
					$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
					$tpl3 = str_replace('{{TXN_TYPE}}', "Deposit Fund", $tpl2);
					$tpl4 = str_replace('{{DATETIME}}', date('Y-m-d H:i:s'), $tpl3);
					$message_html = str_replace('{{AMOUNT}}', $amount, $tpl4);
					$config['mailtype'] = 'html';
					$this->email->initialize($config);
					$this->email->from($from_email, '');
					$this->email->to($to_email);
					$this->email->subject($subject);
					$this->email->message($message_html);
					if ($this->email->send()) {
						//echo 1; die;
						$this->session->set_flashdata("email_sent", "Congragulation Email Send Successfully.");
					} else {
						//echo 2; die;
						$this->session->set_flashdata("email_sent", "You have encountered an error");
					}
					/********Mail***End****/
				}
			} else {
			    $fail_time = date('Y-m-d H:i:s');
				$insert_data = array(
					'user_id' => $user_data->id,
					'txn_id' => $txn_id,
					'amount' => $amount,
					'status' => $status,
					'status_message' => $status_text,
					'response_data' => $response_data,
					'created_datetime' => $fail_time,
					'modified_datetime' => $fail_time,
				);
				$this->common->insert_data('pw_add_fund_history', $insert_data);
				$insert_fail = array(
					'from_user' =>  $user_data->id,
					'to_user' => $user_data->id,
					'type' => 1,
					'log' => 'credit',
					'amount' => $amount,
					'balance' => get_wallet_balance($user_data->id) + $amount,
					'txn_id' => $txn_id,
					'message' => "Fund Fail",
					'status' => 0,
					'created_datetime' => $fail_time,
					'modified_datetime' => $fail_time,
				);
				$this->common->insert_data('pw_user_notification', $insert_fail);	
				$fali_logs = array(
					'userid' => $user_data->id,
					'detail' => 'Fund Fail' . $amount . '$',
					'date' => $fail_time,
				);
				$this->common->insert_data('pw_logs', $fali_logs);				
			}
			exit;
		}
	}

	function success_url()
	{
		$this->session->set_flashdata('error_message', 'Pyament done successfully !');
		redirect(base_url());
	}

	function cancel_url()
	{
		$this->session->set_flashdata('error_message', 'Pyament cancelled !');
		redirect(base_url());
	}

	function demo()
	{
		$this->load->view('user/fund/demo');
	}
}
