<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fund extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_userLoginCheck();
		/****checks it is login or not End****/

		$this->load->model('Fund_model', 'fund');
		$this->load->model('Genealogy_model', 'genealogy');
	}

	function add_fund($param1 = '')
	{
		$data['title'] = 'Fund';

		$this->form_validation->set_rules('amountf', 'Amount', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$data['user_data'] = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

		$data['data_result'] = $this->fund->getMyAddFund();
		$data['total_rec'] = $data['data_result'] == 0 ? 0 : count($data['data_result']);
		if ($this->form_validation->run() == FALSE) {
			$data['coin_data'] = $this->common->getSingle('pw_admin_coin_setting', array('id' => 1));
			$this->load->view('user/fund/add_fund', $data);
		} else {

			echo 1;
			exit;
		}
	}	

	function transfer_fund()
	{
		$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

		if (empty($user_data)) {
			redirect(base_url() . 'user/dashboard');
		}

		$data['title'] = 'Transfer Fund';

		$this->form_validation->set_rules('member_id', 'Member ID', 'required');
		$this->form_validation->set_rules('amount', 'Amount', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$data['user_data'] = $user_data;
		// $data['data_result'] = $this->fund->getMyTransferAmount();
		// $data['total_records'] = $data['data_result'] == 0 ? 0 : count($data['data_result']);
		$data['wallet_amount'] = $user_data->wallet_amount;

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('user/fund/transfer_fund', $data);
		} else {
			// $usertree = $this->genealogy->get_user_matrix($this->session->user_id); // withdrawal allow only for RajKumar sir tree
			// if ($usertree) {
			$amount = $this->input->post('amount', true);
			if (preg_match('/^\d+$/', $amount)) {
				$member_id = $this->input->post('member_id', true);


				$transaction_password = $this->input->post('transaction_password', true);

				$member_data = $this->common->getSingle('pw_users', array('username' => $member_id, 'is_delete' => 1));

				$pass_data = $this->common->getSingle('pw_transaction_password', array('user_id' => $this->session->userdata('user_id')));

				if (empty($pass_data) || ($pass_data->password != MD5($transaction_password))) {
					$this->session->set_flashdata('error_message', "Please type correct transaction password !");
					redirect(base_url() . 'user/fund/transfer_fund');
				}

				if (empty($member_data)) {
					$this->session->set_flashdata('error_message', "Member ID doesn't exist !");
					redirect(base_url() . 'user/fund/transfer_fund');
				} elseif ($member_id == $this->session->userdata('user_name')) {
					$this->session->set_flashdata('error_message', "Same Member ID doesn't exist !");
					redirect(base_url() . 'user/fund/transfer_fund');
				} elseif ($amount <= 0) {
					$this->session->set_flashdata('error_message', "Invalid amount value entered!");
					redirect(base_url() . 'user/fund/transfer_fund');
				} elseif ($user_data->wallet_amount < $amount) {
					$this->session->set_flashdata('error_message', "Amount can not be greater than your wallet amount !");
					redirect(base_url() . 'user/fund/transfer_fund');
				} else {
					$query1 = "update pw_users set wallet_amount = wallet_amount - " . $amount . " where id = '" . $this->session->userdata('user_id') . "'";
					$this->db->query($query1);

					$query2 = "update pw_users set wallet_amount = wallet_amount + " . $amount . " where username = '" . $member_id . "'";
					$this->db->query($query2);

					/********Insert Data in `transfer_history`**Start*******/
					$txn_id = '#T' . rand();
					$insert_data = array(
						'from_user' => $this->session->userdata('user_id'),
						'to_user' => $member_data->id,
						'transfer_charge' => '0',
						'amount' => $amount,
						'transfer_by' => 'user',
						'created_datetime' => date('Y-m-d H:i:s'),
						'txn_id' => $txn_id
					);

					$this->common->insert_data('pw_transfer_history', $insert_data);

					$insert_data = array(
						'user_id' => $this->session->userdata('user_id'),
						'member_id' => $member_data->id,
						'type' => 2,
						'log' => 'debit',
						'amount' => $amount,
						'status' => 'Success',
						'txn_id' => $txn_id,
						'created_time' => date('Y-m-d H:i:s'),
					);

					$this->common->insert_data('pw_transaction_history', $insert_data);

					$insert_data = array(
						'userid' => $this->session->user_id,
						'detail' => 'Transfer ' . $amount . '$',
						'date' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pw_logs', $insert_data);

					/********Insert Data in `transfer_history`**End*******/

					/********Insert Notification**Start********/

					$insert_data = array(
						'from_user' => $this->session->userdata('user_id'),
						'to_user' => $member_data->id,
						'log' => 'debit',
						'amount' => $amount,
						'balance' => getWalletAmountByUserID($this->session->userdata('user_id')),
						'txn_id' => $txn_id,
						'message' => 'transfer',
						'status' => 1,
						'created_datetime' => date('Y-m-d H:i:s'),
						'modified_datetime' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pw_user_notification', $insert_data);

					$insert_data = array(
						'from_user' => $member_data->id,
						'to_user' => $this->session->userdata('user_id'),
						'log' => 'credit',
						'amount' => $amount,
						'balance' => getWalletAmountByUserID($member_data->id),
						'txn_id' => $txn_id,
						'message' => 'transfer',
						'status' => 1,
						'created_datetime' => date('Y-m-d H:i:s'),
						'modified_datetime' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pw_user_notification', $insert_data);

					/********Insert Notification**End********/

					/********Mail***Start****/
					$from_email = ADMIN_INFO_EMAIL;
					$to_email = $this->session->user_email;
					$subject = 'Point Wish: Transfer';
					$WEB_URL = base_url();
					$htmlContent = file_get_contents("./mail_html/transfer.html");
					$tpl1 = str_replace('{{TXN_ID}}', $txn_id, $htmlContent);
					$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
					$tpl3 = str_replace('{{TXN_TYPE}}', "Transfer Fund", $tpl2);
					$tpl4 = str_replace('{{DATETIME}}', date('Y-m-d H:i:s'), $tpl3);
					$message_html = str_replace('{{AMOUNT}}', $amount, $tpl4);
					$config['mailtype'] = 'html';
					$this->email->initialize($config);
					$this->email->from($from_email, '');
					$this->email->to($to_email);
					$this->email->subject($subject);
					$this->email->message($message_html);
					if ($this->email->send()) {
						//echo 1; die;
						$this->session->set_flashdata("email_sent", "Congragulation Email Send Successfully.");
					} else {
						//echo 2; die;
						$this->session->set_flashdata("email_sent", "You have encountered an error");
					}
					/********Mail***End****/

					$this->session->set_flashdata('error_message', 'Amount transferred successfully !');

					redirect(base_url('user/fund/transfer_fund'), 'refresh');
				}
			} else {
				$this->session->set_flashdata('error_message', "Please type correct transaction amount value !");
				redirect(base_url() . 'user/fund/transfer_fund');
			}
			// } else {
			// 	$this->session->set_flashdata('error_message', "Website is under-maintenance for new upgrades and trasnfer will be started soon.Thank You");
			// 	redirect(base_url() . 'user/fund/transfer_fund');
			// }
		}
	}


	function isMemberAvailabe()
	{
		if (!isset($_POST['member_id'])) {
			redirect(base_url());
		}

		$member_id = $this->input->post('member_id');

		$member_data = $this->common->getSingle('pw_users', array('username' => $member_id, 'is_delete' => 1));

		$member_name = '';

		if (!empty($member_data)) {
			if ($member_data->id == $this->session->userdata('user_id')) {
				$member_name = ucfirst($member_data->full_name);
				$status = 3;
			} else {
				$member_name = ucfirst($member_data->full_name);
				$status = 1;
			}
		} else {
			$status = 2;
		}

		$json_arr = array('status' => $status, 'member_name' => $member_name);

		echo json_encode($json_arr);
		exit;
	}

	function isCorrectPassword()
	{
		if (!isset($_POST['transaction_password'])) {
			redirect(base_url());
		}

		$transaction_password = $this->input->post('transaction_password');

		$tx_data = $this->common->getSingle('pw_transaction_password', array('user_id' => $this->session->userdata('user_id'), 'password' => MD5($transaction_password)));

		if (!empty($tx_data)) {
			$status = 1;
		} else {
			$status = 2;
		}

		$json_arr = array('status' => $status);

		echo json_encode($json_arr);
		exit;
	}

	function get_history()
	{
		echo json_encode($this->fund->getMyTransferAmount());
	}

	// function 
}
