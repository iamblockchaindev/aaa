<?php

require_once APPPATH . '/third_party/googleLib/GoogleAuthenticator.php';

defined('BASEPATH') or exit('No direct script access allowed');

class Account extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_userLoginCheck();
		/****checks it is login or not End****/

		$this->load->model('Document_model', 'document');
	}


	function view_profile()
	{
		$data['title'] = 'My Profile';

		$data['user_data'] = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

		$this->load->view('user/profile/profile', $data);
	}

	function edit_tripdata()
	{
		$data['title'] = 'Edit Profile';

		$this->form_validation->set_rules('username', 'First Name', 'required');
		$this->form_validation->set_rules('passport_num', 'Passport Number', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');


		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error_message', 'Something wrong');
		} else {

			if (empty($_FILES['pas_photo']['name'])) {
				$this->session->set_flashdata('error_message', 'Something wrong');
			} else {
				$config['upload_path'] = './uploads/user_images/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 1024 * 2.5; //  here it is 5 MB
				$config['encrypt_name'] = TRUE;

				$this->upload->initialize($config);

				if (!$this->upload->do_upload('pas_photo')) {
					$msg = $this->upload->display_errors('', '');
				} else {
					$data = $this->upload->data();
					$profile_image1 = $data["file_name"];
				}
			}
			/*******Upload Photo***End***/
			if (empty($_FILES['pas_front']['name'])) {
				$this->session->set_flashdata('error_message', 'Something wrong');
			} else {
				$config['upload_path'] = './uploads/user_images/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 1024 * 2.5; //  here it is 5 MB
				$config['encrypt_name'] = TRUE;

				$this->upload->initialize($config);

				if (!$this->upload->do_upload('pas_front')) {
					$msg = $this->upload->display_errors('', '');
				} else {
					$data = $this->upload->data();
					$profile_image2 = $data["file_name"];
				}
			}
			/*******Upload Photo***End***/
			if (empty($_FILES['pas_back']['name'])) {
				$this->session->set_flashdata('error_message', 'Something wrong');
			} else {
				$config['upload_path'] = './uploads/user_images/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 1024 * 2.5; //  here it is 5 MB
				$config['encrypt_name'] = TRUE;

				$this->upload->initialize($config);

				if (!$this->upload->do_upload('pas_back')) {
					$msg = $this->upload->display_errors('', '');
				} else {
					$data = $this->upload->data();
					$profile_image3 = $data["file_name"];
				}
			}
			/*******Upload Photo***End***/

			$username = $this->input->post('username', true);
			$passport_num = $this->input->post('passport_num', true);
			$last_name = $this->input->post('last_name', true);



			$insert_data = array(
				'user_id' => $this->session->userdata('user_id'),
				'username' => $username,
				'passport_num' => $passport_num,
				'last_name' => $last_name,
				'date' => date('Y-m-d H:i:s'),
				'pass_photo' => $profile_image1,
				'pass_front' => $profile_image2,
				'pass_back' => $profile_image3

			);
			$this->common->insert_data('pw_blueniletrip', $insert_data);

			$this->session->set_flashdata('error_message', 'Document Upload Successfully');

			// redirect(base_url() . 'user/account/bluenile_trip');
		}
	}

	function bluenile_trip()
	{
		$data['title'] = "Bluenile Trip";
		$data['user_document'] = $this->common->getSingle('pw_blueniletrip', array('user_id' => $this->session->userdata('user_id')));
		$this->load->view('user/profile/thai_trip', $data);
	}

	function edit_profile()
	{
		$data['title'] = 'Edit Profile';

		$this->form_validation->set_rules('full_name', 'Name', 'required');
		// $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		// $this->form_validation->set_rules('mobile', 'Mobile', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$data['user_data'] = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('user/profile/profile', $data);
		} else {

			// if($data['user_data']->is_profile_update == 1)
			// {
			// 	$this->session->set_flashdata('error_message', 'You have already updated profile. for updating again, contact to administrator !');

			// 	redirect(base_url().'user/account/view_profile');				
			// }

			/*******Upload Photo***Start***/

			if (empty($_FILES['profile_photo']['name'])) {
				$profile_image = $data['user_data']->profile_pic;
			} else {
				$config['upload_path'] = './uploads/user_images/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 1024 * 2.5; //  here it is 5 MB
				$config['encrypt_name'] = TRUE;

				$this->upload->initialize($config);

				if (!$this->upload->do_upload('profile_photo')) {
					$msg = $this->upload->display_errors('', '');
				} else {
					$data = $this->upload->data();
					$profile_image = $data["file_name"];
				}
			}
			/*******Upload Photo***End***/

			$full_name = $this->input->post('full_name', true);
			$email = $this->input->post('email', true);
			$mobile = $this->input->post('mobile', true);

			$country_code = $this->input->post('country_code', true);
			$country = $this->input->post('country', true);
			$state = $this->input->post('state', true);
			$city = $this->input->post('city', true);
			$BTC_address = $this->input->post('BTC_address', true);

			$update_data = array(
				'full_name' => $full_name,
				'email' => $email,
				'mobile' => $mobile,
				'profile_pic' => $profile_image,
				'country_code' => $country_code,
				'country' => $country,
				'state' => $state,
				'city' => $city,
				'is_profile_update' => 1,
				'modified_time' => date('Y-m-d H:i:s'),
			);

			$this->session->set_userdata('is_profile_update', 1);

			$this->common->update_data('pw_users', array('id' => $this->session->userdata('user_id')), $update_data);

			$insert_data = array(
				'userid' => $this->session->user_id,
				'detail' => 'Profile Update',
				'date' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_logs', $insert_data);

			redirect(base_url() . 'user/account/view_profile');
			// $this->session->set_flashdata('error_message', 'Profile Updated Successfully');
		}
	}

	function change_password()
	{
		$data['title'] = 'Change Password';

		$this->form_validation->set_rules('old_password', 'Old password', 'required');
		$this->form_validation->set_rules('new_password', 'New password', 'required');
		$this->form_validation->set_rules('confirm_password', 'Confirm password', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('user/profile/change_password', $data);
		} else {
			if ($user_data->password != MD5($this->input->post('old_password', true))) {
				$this->session->set_flashdata('error_message', 'Please enter correct old password');
			} else {
				$update_data = array(
					'password' => MD5($this->input->post('new_password', true)),
					'password_decrypted' => $this->input->post('new_password', true),
				);

				$this->common->update_data('pw_users', array('id' => $this->session->userdata('user_id')), $update_data);
				$insert_data = array(
					'userid' => $this->session->user_id,
					'detail' => 'Change Password',
					'date' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_logs', $insert_data);


				$this->session->set_flashdata('error_message', 'Password Changed successfully');
			}

			redirect(base_url() . 'user/account/change_password');
		}
	}


	function account_details()
	{
		$data['title'] = 'Edit Bank Details';

		$data['result_data'] = $this->common->getSingle('pw_users_bank_details', array('user_id' => $this->session->userdata('user_id')));

		$this->load->view('user/profile/edit_account_details', $data);
	}

	function update_account_details()
	{
		if ($_POST) {
			$account_data = $this->common->getSingle('pw_users_bank_details', array('user_id' => $this->session->userdata('user_id')));

			if (!empty($account_data)) {

				if ($account_data->is_user_update == 1) {
					$this->session->set_flashdata('error_message', 'You have already updated account details. for updating again, contact to administrator !');

					redirect(base_url() . 'user/account/account_details');
					die;
				}

				$update_data = array(
					'bank_name' => $this->input->post('bank_name', true),
					'branch_name' => $this->input->post('branch_name', true),
					'branch_name' => $this->input->post('branch_name', true),
					'ifsc_code' => $this->input->post('ifsc_code', true),
					'account_holder_name' => $this->input->post('account_holder_name', true),
					'account_no' => $this->input->post('account_no', true),
					//'btc_address'=>$this->input->post('btc_address', true),
					'is_user_update' => 1,
					'modified_datetime' => date('Y-m-d H:i:s'),
				);

				$this->common->update_data('pw_users_bank_details', array('user_id' => $this->session->userdata('user_id')), $update_data);
			} else {
				$insert_data = array(
					'user_id' => $this->session->userdata('user_id'),
					'bank_name' => $this->input->post('bank_name', true),
					'branch_name' => $this->input->post('branch_name', true),
					'branch_name' => $this->input->post('branch_name', true),
					'ifsc_code' => $this->input->post('ifsc_code', true),
					'account_holder_name' => $this->input->post('account_holder_name', true),
					'account_no' => $this->input->post('account_no', true),
					//'btc_address'=>$this->input->post('btc_address', true),
					'is_user_update' => 1,
					'created_datetime' => date('Y-m-d H:i:s'),
					'modified_datetime' => date('Y-m-d H:i:s'),
				);

				$this->common->insert_data('pw_users_bank_details', $insert_data);
			}

			$this->session->set_flashdata('error_message', 'Updated Successfully !');

			redirect(base_url() . 'user/account/account_details');
		} else {
			redirect(base_url());
		}
	}
	function btcaddress($str)
	{
		if ($str == '') {
			$this->form_validation->set_message('btcaddress', 'Please enter BTC address');
			return false;
		} else {
			if (preg_match('/^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$/', $str)) {
				return true;
			} else {
				$this->form_validation->set_message('btcaddress', 'Please enter correct BTC address');
				return false;
			}
		}
	}

	function btc_address()
	{
		$btcdata = $data['result_data']  = $this->common->getSingle('pw_users_bank_details', array('user_id' => $this->session->userdata('user_id')));
		$this->form_validation->set_rules('btc_address', 'BTC Address', 'callback_btcaddress');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$update_data['btc_address'] = $this->input->post('btc_address', true);
			$update_data['modified_datetime'] = date('Y-m-d H:i:s');
			if ($btcdata) {
				$this->common->update_data('pw_users_bank_details', array('user_id' => $this->session->userdata('user_id')), $update_data);
			} else {
				$update_data['user_id'] = $this->session->userdata('user_id');
				$update_data['created_datetime'] = date('Y-m-d H:i:s');
				$this->common->insert_data('pw_users_bank_details', $update_data);
			}
			$insert_data = array(
				'userid' => $this->session->user_id,
				'detail' => 'Updated BTC Address',
				'date' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_logs', $insert_data);
			$this->session->set_flashdata('error_message', 'Updated Successfully !');
			redirect($_SERVER['HTTP_REFERER']);
		}

		$data['title'] = 'BTC Adress Update';
		$this->load->view('user/profile/set_eth_address', $data);
	}
	function ethaddress($str)
	{
		if ($str == '') {
			$this->form_validation->set_message('ethaddress', 'Please enter ETH address');
			return false;
		} else {
			if (preg_match('/^(0x)?[0-9a-f]{40}$/i', $str)) {
				return true;
			} else {
				$this->form_validation->set_message('ethaddress', 'Please enter correct ETH address');
				return false;
			}
		}
	}
	function eth_address()
	{
		$btcdata = $data['result_data']  = $this->common->getSingle('pw_users_bank_details', array('user_id' => $this->session->userdata('user_id')));
		$this->form_validation->set_rules('eth_address', 'ETH Address', 'required|callback_ethaddress');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$update_data['eth_address'] = $this->input->post('eth_address', true);
			$update_data['modified_datetime'] = date('Y-m-d H:i:s');
			if ($btcdata) {
				$this->common->update_data('pw_users_bank_details', array('user_id' => $this->session->userdata('user_id')), $update_data);
			} else {
				$update_data['user_id'] = $this->session->userdata('user_id');
				$update_data['created_datetime'] = date('Y-m-d H:i:s');
				$this->common->insert_data('pw_users_bank_details', $update_data);
			}
			$insert_data = array(
				'userid' => $this->session->user_id,
				'detail' => 'ETH Address',
				'date' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_logs', $insert_data);
			$this->session->set_flashdata('error_message', 'Updated Successfully !');
			redirect($_SERVER['HTTP_REFERER']);
		}

		$data['title'] = 'PWT Address';
		$this->load->view('user/profile/set_eth_address', $data);
	}
	function set_transaction_password()
	{
		$data['title'] = 'Set Transaction Password';

		// $this->form_validation->set_rules('old_password', 'Old Password', 'required');
		$this->form_validation->set_rules('new_password', 'New password', 'required');
		$this->form_validation->set_rules('confirm_password', 'Confirm password', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$user_data = $this->common->getSingle('pw_transaction_password', array('user_id' => $this->session->userdata('user_id')));
		$data['user_data'] = $user_data;
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('user/profile/set_transaction_password', $data);
		} else {
			$old = $this->input->post('old_password', true);
			if ($old) {
				if ($user_data->password != MD5($old))
					$this->session->set_flashdata('error_message', 'Please enter correct old password');
			} else {
				// $otp = $this->input->post('otp', true);
				// $new_password = $this->input->post('new_password', true);

				// if($user_data->transaction_otp != $otp)
				// {
				// 	$this->session->set_flashdata('error_message1', '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please enter correct otp !</div>');

				// 	redirect(base_url().'user/account/set_transaction_password');
				// 	die;
				// }

				$trans_pass_data = $this->common->getSingle('pw_transaction_password', array('user_id' => $this->session->userdata('user_id')));

				if (!empty($trans_pass_data)) {
					$update_data = array(
						'password' => MD5($this->input->post('new_password', true)),
						'password_d' => $this->input->post('new_password', true),
						'modified_datetime' => date('Y-m-d H:i:s'),
					);

					$this->common->update_data('pw_transaction_password', array('user_id' => $this->session->userdata('user_id')), $update_data);
				} else {
					$insert_data = array(
						'user_id' => $this->session->userdata('user_id'),
						'password' => MD5($this->input->post('new_password', true)),
						'password_d' => $this->input->post('new_password', true),
						'created_datetime' => date('Y-m-d H:i:s'),
						'modified_datetime' => date('Y-m-d H:i:s'),
					);

					$this->common->insert_data('pw_transaction_password', $insert_data);
				}
				$insert_data = array(
					'userid' => $this->session->user_id,
					'detail' => 'Change Transaction Password',
					'date' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_logs', $insert_data);

				$this->session->set_flashdata('error_message', 'Password set successfully');
			}
			redirect(base_url() . 'user/account/set_transaction_password');
		}
	}

	function exchange()
	{
		$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));
		$data['wallet_amount'] = $user_data->wallet_amount;
		$data['unlocked_pwt'] = $user_data->unlocked_pwt;

		
		
		// $totalDeposit = $this->dashboard->get_total_deposit();
		// $user = $this->common->getSingle('pw_users', array('id' => $this->session->user_id));
		// if ( $totalDeposit >= 10) {
		$data['title'] = 'Exchange';
		$data['history'] = $this->common->getWhereFromLast('pwt_exchange_history', array('user_id' => $this->session->user_id));
		// $data['lists'] = $this->common->getWhereFromLast('pwt_coin_history', array(), 10);
		// $data['moblists'] = $this->common->getWhereFromLast('pwt_coin_history', array(), 3);
		$this->load->view('user/withdraw/exchange', $data);
		// } else {
		//     $this->session->set_flashdata('error_message', "Please fill the walet!");
		//     redirect(base_url('affiliate'));
		// }
	}

	function send_transaction_otp()
	{
		if ($_POST) {
			$digits = 4;
			$transaction_otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

			$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

			$query = "update pw_users set transaction_otp = '" . $transaction_otp . "' where id = '" . $this->session->userdata('user_id') . "'";
			$this->db->query($query);

			/*================MAIL SEND========================== */
			// $from_email = 'verification@pointwish.com'; //ADMIN_INFO_EMAIL
			// $to_email = $user_data->email;
			// $subject = 'Point Wish: Transaction Password';
			// $WEB_URL = base_url();
			// $LOGO_URL = base_url().'assets/front/images/logo.png';
			// $htmlContent = file_get_contents("./mail_html/transaction_password.html");
			// $tpl1 = str_replace('{{LOGO_URL}}', $LOGO_URL, $htmlContent);
			// $tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
			// $message_html = str_replace('{{OPT_CODE}}', $transaction_otp, $tpl2);
			// $config['mailtype'] = 'html';
			// $this->email->initialize($config);
			// $this->email->from('verification@pointwish.com', 'noreply');
			// $this->email->to($to_email);
			// $this->email->subject($subject);
			// $this->email->message(' transaction password is '.$transaction_otp);
			//$this->email->message($message_html);
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://mail.privateemail.com',
				'smtp_port' => 465,
				'smtp_user' => 'verification@pointwish.com',
				'smtp_pass' => 'Geet@2511',
				'mailtype'  => 'html',
				'charset'   => 'iso-8859-1'
			);
			$this->email->initialize($config);
			$html1 = "Your Transition OTP is " . $transaction_otp . ". If it is not you trying to login to the system, change your password immediately.";
			$this->email->set_newline("\r\n");
			$this->email->from('verification@pointwish.com', 'noreply');
			$this->email->to($user_data->email);
			//$this->email->cc('sudheershri007@gmail.com');
			$this->email->set_mailtype("html");
			$this->email->subject('Transaction password');
			$this->email->message($html1);
			//$this->load->library('email', $config);

			$this->email->send();
			/* ==============MAIL END============================ */
			/**********SMS API COde**Start********/

			$mobile_number = $user_data->mobile;

			$message = '';
			$message .= "Your Point Wish OTP: " . $transaction_otp;

			/**********SMS API COde**Start********/
			$this->send_sms_notification(urlencode($message), $mobile_number);
			/**********SMS API Code**End********/

			echo 1;
			exit;
		} else {
			redirect(base_url());
		}
	}

	function document_verification()
	{
		$data['title'] = 'Document Verification';
		$data['id_data'] = $this->document->getMyDocumentByType('id');
		$data['address_data'] = $this->document->getMyDocumentByType('address');
		$this->load->view('user/profile/document_verification', $data);
	}

	function upload_document()
	{
		$type = $this->input->get('type', true);
		$config['upload_path'] = './uploads/user_documents/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 1024 * 5; //  here it is 5 MB
		$config['encrypt_name'] = TRUE;
		$this->upload->initialize($config);
		$flag = false;
		if (empty($_FILES['file']['name'])) {
			$ID_proof = '';
		} else {
			if (!$this->upload->do_upload('file')) {
				$this->session->set_flashdata('error_message', 'Something is wrong on file uploading !');
			} else {
				$flag = true;
			}
		}
		if ($flag == true) {
			/*******Upload ID***Start***/
			$data = $this->upload->data();
			$proof = $data["file_name"];
			$this->resize_image($proof);
			$insert_data = array(
				'user_id' => $this->session->userdata('user_id'),
				'document_type' => $type,
				'document_file' => $proof,
				'created_datetime' => date('Y-m-d H:i:s'),
				'modified_datetime' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_users_document', $insert_data);
			$this->session->set_flashdata('success_message', 'Document uploaded successfully !');
			/*******Upload ID***End***/
		}
		redirect(base_url('user/account/document_verification'));
	}


	function mailTest()
	{
		$from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
		$to_email = 'sudheershri007@gmail.com';
		$subject = 'Point Wish: Transaction Password';
		$WEB_URL = base_url();
		$LOGO_URL = base_url() . 'assets/front/images/logo.png';
		$htmlContent = file_get_contents("./mail_html/transaction_password.html");
		$tpl1 = str_replace('{{LOGO_URL}}', $LOGO_URL, $htmlContent);
		$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $tpl1);
		$message_html = str_replace('{{OPT_CODE}}', '1234', $tpl2);
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from('pointwish.com', 'no-reppy');
		$this->email->to($to_email);
		$this->email->subject($subject);
		$this->email->message($message_html);
		$this->email->send();
	}

	function verification()
	{
		$data['title'] = '2FA Verification';

		// $data['user_data'] = $this->common->getSingle('pw_users', array('id'=>$this->session->userdata('user_id')));

		$this->load->view('user/profile/verification', $data);
	}

	function get_auth_status()
	{
		echo json_encode($this->db->get_where('pw_users', array('id' => $this->session->userdata('user_id')))->result_array());
	}

	function set_auth_key()
	{
		$ga = new GoogleAuthenticator();
		$secret = $ga->createSecret();
		$qrCodeUrl = $ga->getQRCodeGoogleUrl($this->session->user_email, $secret, 'Pointwish');
		$update = array(
			'google_auth_code' => $secret
		);
		$check = array(
			'id' => $this->session->user_id
		);
		$result = $this->common->update_data('pw_users', $check, $update);
		if ($result) {
			$data = array(
				'secret' => $secret,
				'qrCodeUrl' => $qrCodeUrl
			);
		}
		echo json_encode($data);
	}

	function chcek_auth()
	{
		$user = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));
		$ga = new GoogleAuthenticator();
		$secret = $user->google_auth_code;
		$code0 = $this->input->post('code');
		$code1 = $this->input->post('code1');
		$code2 = $this->input->post('code2');
		$code3 = $this->input->post('code3');
		$code4 = $this->input->post('code4');
		$code5 = $this->input->post('code5');
		$code = $code0 . '' . $code1 . '' . $code2 . '' . $code3 . '' . $code4 . '' . $code5;
		$checkResult = $ga->verifyCode($secret, $code, 2); // 2 = 2*30sec clock tolerance
		if ($checkResult) {
			$status = $this->input->post('status');
			if ($status == 'true') {
				$update = array(
					'google_auth_status' => 'true',
				);
			} else {
				$update = array(
					'google_auth_status' => 'false',
				);
			}
			$check = array(
				'id' => $this->session->user_id
			);
			$this->common->update_data('pw_users', $check, $update);
			$data['status'] = $this->input->post('status');
			$data['success'] = true;
		} else {
			$data['success'] = false;
		}
		echo json_encode($data);
	}

	function reset_transaction_password()
	{
		$user_data = $this->common->getSingle('pw_transaction_password', array('user_id' => $this->session->userdata('user_id')));
		if ($user_data) {
			$this->db->where('user_id', $this->session->user_id)->delete('pw_transaction_password');
			$this->session->set_flashdata('error_message', 'Password Reset successfully');
		} else {
			$this->session->set_flashdata('error_message', 'No user found');
		}
		$json_data = array('status' => 1);
		echo json_encode($json_data);
		exit;
	}

	private function resize_image($file_name)
	{
		$this->load->library('image_lib');

		$config['image_library'] = 'gd2';
		$config['source_image'] = './uploads/user_documents/' . $file_name;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = TRUE;
		$config['width']     = 550;
		$config['height']   = 350;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$this->image_lib->clear();
	}
}
