<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_history extends MY_Controller {

    function __construct() 
    {
		parent::__construct();

		/****checks it is login or not start****/
		$this->_userLoginCheck();
		/****checks it is login or not End****/

		$this->load->model('Log_model', 'log_history');
    }

	function index()
	{
		$data['title'] = 'Log History';

        if (!empty($_GET['offset'])) 
        {
            $offset = $_GET['offset'];
        } 
        else 
        {
            $offset = 1;
        } 

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;

        $data['data_result'] = $this->log_history->getLogHistory($start_limit, $per_page);
        $total_records = $this->log_history->getLogHistoryCount();

        $config['base_url'] = base_url() . 'user/log_history';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        $config['num_links']=15;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;             
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
         

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';
         
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';
         
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
              
        $data["links"] = $this->pagination->create_links();

		$this->load->view('user/log/log_history', $data);
	}  
}
