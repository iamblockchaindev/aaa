<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Genealogy extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_userLoginCheck();
        /****checks it is login or not End****/

        $this->load->model('Genealogy_model', 'genealogy');
    }

    function level()
    {
        $data['title'] = 'All Levels';
        $this->load->view('user/genealogy/level', $data);
    }

    function all_downline()
    {
        $data['title'] = 'All Downline';
        $this->load->view('user/genealogy/all_downline', $data);
    }

    function users()
    {
        $data['title'] = 'Users';
        // $data['data_result'] = $this->genealogy->getAllMyUsers();
        // $data['total_records'] = $data['data_result'] == 0 ? 0 : count($data['data_result']);
        $this->load->view('user/genealogy/users', $data);
    }

    function get_all_active_users()
    {
        echo json_encode($this->genealogy->getAllMyUsers('active'));
    }

    function get_level_user($level)
    {
        $users = $this->genealogy->getAllDownline($level);
        foreach ($users as $user) {
            $data[] = array(
                'id' => $user['id'],
                'username' => $user['username'],
                'full_name' => $user['full_name'],
                'mobile' => $user['mobile'],
                'created_time' =>  date('d M, Y H:i', strtotime($user['created_time'])),
                'upgrade_time' => date('d M, Y H:i', strtotime($user['upgrade_time'])),
                'sponsor' => $user['sponsor'],
                'name' => $user['name'],
                'downline' => getFirstLevelDownlineUsersCountByUserID($user['id']),
                'package' => getLatestRankByUserID($user['id'])
            );
        }
        echo json_encode($data);
    }

    function get_all_users()
    {
        $users = $this->genealogy->getAllMyUsers();
        foreach ($users as $user) {
            $data[] = array(
                'id' => $user->id,
                'username' => $user->username,
                'full_name' => $user->full_name,
                'mobile' => $user->mobile,
                'created_time' =>  date('d M, Y H:i', strtotime($user->created_time)),              
                'package' => $user->package
            );
        }
        echo json_encode($data);
    }
}
