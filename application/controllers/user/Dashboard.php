<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_userLoginCheck();
		/****checks it is login or not End****/

		$this->load->model('Dashboard_model', 'dashboard');
		$this->load->model('Genealogy_model', 'genealogy');
	}

	public function index()
	{
		$data['title'] = 'Dashboard';

		$data['user_data'] = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id')));

		$data['wallet_amount'] = $this->dashboard->getMyWalletAmount();

		$data['total_earning'] =  str_replace(',', '', getMyWorkingLevelIncome()) +  str_replace(',', '', getMyDirectWalletIncome()) +  str_replace(',', '', getMyWithdrawableAmountByPackage('30')) +  str_replace(',', '', getMyWithdrawableAmountByPackage('40')) +  str_replace(',', '', getMyWithdrawableAmountByPackage('100')) +  str_replace(',', '', getMyWithdrawableAmountByPackage('1000')) +  str_replace(',', '', getMyWithdrawableAmountByPackage('5000')) +  str_replace(',', '', getMyWithdrawableAmountByPackage('10000'));
		$data['total_withdrawal'] = $this->dashboard->getMyTotalWithdrawal();
		//         $data['total_bonus'] = $this->dashboard->getMyTotalCryptoBonus();
		// 		$data['my_team'] = count( $this->genealogy->getAllMyUsers());
		$mydirect = $this->dashboard->get_my_direct();
		$data['mydirect'] = $mydirect == 0 ? 0 : count($mydirect);
		$data['logs'] = $this->dashboard->get_logs();

		$this->load->view('user/dashboard/dashboard', $data);
	}	

	function demo()
	{
		$levelIncome  =  str_replace(',', '', getMyWorkingLevelIncome());
		$directIncome =  str_replace(',', '', getMyDirectWalletIncome());
		echo "Total Earning = " . ($levelIncome + $directIncome);
		echo "</br>";
		echo "working level income =" . getMyWorkingLevelIncome();
		echo "</br>";
		echo "Direct income =" . str_replace(',', '', getMyDirectWalletIncome());
		echo "</br>";
		echo "Package 30 = " . getMyWithdrawableAmountByPackage('30');
		echo "</br>";
		echo "Package 40 " . getMyWithdrawableAmountByPackage('40');
		echo "</br>";
		echo  "Packge 100 = " . getMyWithdrawableAmountByPackage('100');
		echo "</br>";
		echo "Packge 1000 = " . getMyWithdrawableAmountByPackage('1000');
		echo "</br>";
		echo "Packge 5000 = " . getMyWithdrawableAmountByPackage('5000');
		echo "</br>";
		echo "Package 10000 =" . getMyWithdrawableAmountByPackage('10000');
	}
}
