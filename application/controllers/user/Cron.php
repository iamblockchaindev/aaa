<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller {

    function __construct() 
    {
		parent::__construct();

    }

	function create_backup()
	{
        $this->load->dbutil();
        $prefs = array(
            'format' => 'zip',
            'filename' => 'my_db_backup.sql'
        );
        $backup = & $this->dbutil->backup($prefs);
        $db_name = 'backup-on-' . date("Y-m-d-H-i-s") . '.zip';
        $save = 'uploads/database_backup/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);

        $insert_data = array(
            'file_name'=>$db_name,
            'created_datetime'=>date('Y-m-d-H:i:s')
            );
        $this->common->insert_data('pw_database_backup', $insert_data);

        $this->load->helper('download');
        force_download($db_name, $backup);

        echo 1;
        exit;		
	}


    function getTime()
    {
        echo date('Y-m-d H:i:s');
    }
}
