<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notification extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_userLoginCheck();
        /****checks it is login or not End****/

        $this->load->model('Notification_model', 'notification');
    }

    function notification_list()
    {
        $data['title'] = 'Credit & Debit History';        
        $this->load->view('user/notification/notification_list', $data);
    }

    function get_history() {
        echo json_encode($this->notification->getMyNotificationHistory());
    }

    // public function ajax_list()
    // {
    //     $list = $this->notification->get_datatables();
    //     $data = array();
    //     $no = $_POST['start'];
    //     foreach ($list as $res) {
    //         $no++;
    //         $row = array();
    //         $row[] = $no;
    //         $row[] = date('d M Y H:i', strtotime($res->created_datetime));
    //         $row[] = isset($res->log) ? ucwords($res->log) : 'N/A';
    //         $row[] = $res->message . '<br />(' . getMemberNameByID($res->to_user) . ')';
    //         $row[] = isset($res->amount) ? $res->amount . ' $' : $res->amount;
    //         $row[] = isset($res->balance) ? ($res->balance != 'NA' ? $res->balance . ' $' : 'N/A') : '---';
    //         $row[] = isset($res->status) ?  ($res->status == '1' ? 'Success' : 'Fail')  : 'N/A';
    //         $row[] = isset($res->txn_id) ? $res->txn_id : 'N/A'
 
    //         $data[] = $row;
    //     }
 
    //     $output = array(
    //                     "draw" => $_POST['draw'],
    //                     "recordsTotal" => $this->customers->count_all(),
    //                     "recordsFiltered" => $this->customers->count_filtered(),
    //                     "data" => $data,
    //             );
    //     //output to json format
    //     echo json_encode($output);
    // }
    
}
