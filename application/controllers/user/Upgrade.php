<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Upgrade extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Genealogy_model', 'genealogy');
	}

	private function getDirectIncome()
	{
		$income = 15;
		return $income;
	}

	function upgrade_me()
	{
		if ($_POST) {
			$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id'), 'is_delete' => 1));

			if (empty($user_data)) {
				//redirect(base_url());

				echo 4;
				die;
			}

			if (!empty($user_data->package)) {
				$this->session->set_flashdata('error_message', 'You have already upgraded member !');

				echo 3;
				die;
			}

			if ($user_data->wallet_amount < 40) {
				$this->session->set_flashdata('error_message', 'Minimum <strong>40$</strong> should be in your wallet to upgrade !');

				echo 2;
				die;
			}

			$package = 30;

			/********Upgrade Member Logic Start********/
			$this->upgrade_plan($user_data->id, $package);
			/********Upgrade Member Logic End********/

			/********Non Working Start********/
			$this->apply_non_working($user_data->id, $package);
			/********Non Working End********/

			/**********SMS API COde**Start********/
			// $message = "Your A/c(" . $user_data->username . ") is upgraded to OPAL";
			// $this->send_sms_notification(urlencode($message), $user_data->mobile);
			/**********SMS API Code**End********/

			$this->session->set_flashdata('error_message', 'You have been upgraded successfully !');

			echo 1;
			exit;
		} else {
			redirect(base_url() . 'user/dashboard');
		}
	}

	private function upgrade_plan($user_id, $package)
	{
		$member_data = $this->common->getSingle('pw_users', array('id' => $user_id));

		$update_user = "UPDATE `pw_users` SET `package` = '" . $package . "', `upgrade_time` = '" . date('Y-m-d H:i:s') . "', wallet_amount = wallet_amount- " . 40 . " WHERE `id` = '" . $member_data->id . "'";
		$this->db->query($update_user);

		$sponsor_data = $this->common->getSingle('pw_users', array('username' => $member_data->sponsor));

		$insert_data = array(
			'from_user' => $this->session->userdata('user_id'),
			'to_user' => $member_data->id,
			'log' => 'debit',
			'message' => "Upgrade Package",
			'txn_id' => '#PU' . rand(),
			'amount' => 40,
			'balance' => getWalletAmountByUserID($this->session->userdata('user_id')),
			'status' => 1,
			'created_datetime' => date('Y-m-d H:i:s'),
			'modified_datetime' => date('Y-m-d H:i:s'),
		);
		$this->common->insert_data('pw_user_notification', $insert_data);

		/*****insert into `pw_user_matrix`**Start******/
		$insertTree = $this->genealogy->check_user_matrix($sponsor_data->id, $member_data->id);
		/*****insert into `pw_user_matrix`**End******/

		/*****insert into `pw_company_matrix`**Start******/
		// $trees = $this->common->getSingleFromLast('pw_company_matrix', array('package' => 30));
		// $last = $this->common->getSingleFromSecondLast('pw_company_matrix', array('package' => 30));
		// $totalcount = $last->tree_count * 3;
		// if ($trees->tree_count < $totalcount) {
		// 	$update_user = "UPDATE `pw_company_matrix` SET user_tree = '" . $trees->user_tree . ', ' . $this->session->user_id . "', tree_count = tree_count+1 WHERE package = 30 AND `level` = $trees->level";
		// 	$this->db->query($update_user);
		// } else {
		// 	$company_tree = array(
		// 		'package' => 30,
		// 		'level' => $trees->level + 1,
		// 		'user_tree' => $this->session->userdata('user_id'),
		// 		'tree_count' => 1
		// 	);
		// 	$this->common->insert_data('pw_company_matrix', $company_tree);
		// }
		/*****insert into `pw_company_matrix`**End******/

		if (!empty($sponsor_data)) {
			/*********Update wallet amount of User****Start*****/
			$direct_income = $this->getDirectIncome();

			//$this->wallet_amount($sponsor_data->id, $direct_income);
			/*********Update wallet amount of User****End*****/

			$update_sponsor = "UPDATE `pw_users` SET direct_income_wallet = (direct_income_wallet + " . $direct_income . "), direct_income_balance = (direct_income_balance + " . $direct_income . ") WHERE id = '" . $sponsor_data->id . "'";
			$this->db->query($update_sponsor);

			$update_payout = "INSERT INTO `pw_payout` SET `session_id` = '" . $sponsor_data->id . "', `username`  = '" . $member_data->username . "', `amount`  = '" . $direct_income . "', `type`  = 'C', `comment`  = 'Direct Income', `action`  = 'direct', `created_time` = '" . date('Y-m-d H:i:s') . "'";
			$this->db->query($update_payout);

			$insert_direct = array(
				'from_user' => $sponsor_data->id,
				'to_user' => $this->session->userdata('user_id'),
				'log' => 'credit',
				'message' => "Direct Income",
				'txn_id' => '#DB' . rand(),
				'amount' => $direct_income,
				'balance' => 'NA',
				'status' => 1,
				'created_datetime' => date('Y-m-d H:i:s'),
				'modified_datetime' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_user_notification', $insert_direct);

			/*********Insert Data `pw_working_tree`**Start*******/
			$insert_data = array(
				'sponsor_user_id' => $sponsor_data->id,
				'member_user_id' => $member_data->id,
				'created_datetime' => date('Y-m-d H:i:s')
			);
			$this->common->insert_data('pw_working_tree', $insert_data);
			/*********Insert Data `pw_working_tree`**End*******/

			/*****Level Earning Managment for `10` Level****Start*****/
			$this->plan_user_earning($member_data->id, $member_data->username);
			/*****Level Earning Managment for `10` Level****End*****/
		}
	}

	private function wallet_amount($user_id, $amount)
	{
		$wallet_amount = $amount;

		$update_data1 =  "UPDATE `pw_users` SET wallet_amount = (wallet_amount+$wallet_amount) WHERE `id` = '" . $user_id . "'";
		$this->db->query($update_data1);
	}

	private function plan_user_earning($member_id, $member_username)
	{
		// '1st' Level
		$query1 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_id . "'";
		$result = $this->db->query($query1);

		if ($result->num_rows() > 0) {
			$member_data = $result->row();
			// $result = $this->genealogy->get_user_matrix($member_data->sponsor_user_id); // Income allow only for RajKumar sir tree - 1.11
			// if ($result) {
			// 	$this->user_team_earning($member_id, '8759');
			// } else {
			$level_income = $this->getLevelIncome(1);

			$update_data1 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
			$this->db->query($update_data1);

			/*********Update notification****Start*****/
			$insert_direct = array(
				'from_user' => $member_data->sponsor_user_id,
				'to_user' => $this->session->userdata('user_id'),
				'log' => 'credit',
				'message' => "1st Level Income",
				'txn_id' => '#LI' . rand(),
				'amount' => $level_income,
				'balance' => 'NA',
				'status' => 1,
				'created_datetime' => date('Y-m-d H:i:s'),
				'modified_datetime' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_user_notification', $insert_direct);
			/*********Update notification****End*****/

			// '2nd' Level
			$query2 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
			$result = $this->db->query($query2);

			if ($result->num_rows() > 0) {
				$member_data = $result->row();

				$level_income = $this->getLevelIncome(2);

				$update_data2 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
				$this->db->query($update_data2);

				/*********Update notification****Start*****/
				$insert_direct = array(
					'from_user' => $member_data->sponsor_user_id,
					'to_user' => $this->session->userdata('user_id'),
					'log' => 'credit',
					'message' => "2nd Level Income",
					'txn_id' => '#LI' . rand(),
					'amount' => $level_income,
					'balance' => 'NA',
					'status' => 1,
					'created_datetime' => date('Y-m-d H:i:s'),
					'modified_datetime' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_user_notification', $insert_direct);
				/*********Update notification****End*****/

				// '3rd' Level
				$query3 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
				$result = $this->db->query($query3);

				if ($result->num_rows() > 0) {
					$member_data = $result->row();

					$level_income = $this->getLevelIncome(3);

					$update_data3 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
					$this->db->query($update_data3);

					/*********Update notification****Start*****/
					$insert_direct = array(
						'from_user' => $member_data->sponsor_user_id,
						'to_user' => $this->session->userdata('user_id'),
						'log' => 'credit',
						'message' => "3rd Level Income",
						'txn_id' => '#LI' . rand(),
						'amount' => $level_income,
						'balance' => 'NA',
						'status' => 1,
						'created_datetime' => date('Y-m-d H:i:s'),
						'modified_datetime' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pw_user_notification', $insert_direct);
					/*********Update notification****Start*****/

					// '4th' Level
					$query4 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
					$result = $this->db->query($query4);

					if ($result->num_rows() > 0) {
						$member_data = $result->row();

						$level_income = $this->getLevelIncome(4);

						$update_data4 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
						$this->db->query($update_data4);

						/*********Update notification****Start*****/

						$insert_direct = array(
							'from_user' => $member_data->sponsor_user_id,
							'to_user' => $this->session->userdata('user_id'),
							'log' => 'credit',
							'amount' => $level_income,
							'balance' => 'NA',
							'message' => "4th Level Income",
							'txn_id' => '#LI' . rand(),
							'status' => 1,
							'created_datetime' => date('Y-m-d H:i:s'),
							'modified_datetime' => date('Y-m-d H:i:s'),
						);
						$this->common->insert_data('pw_user_notification', $insert_direct);
						/*********Update notification****End*****/

						// '5th' Level
						$query5 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
						$result = $this->db->query($query5);

						if ($result->num_rows() > 0) {
							$member_data = $result->row();

							$level_income = $this->getLevelIncome(5);

							$update_data5 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
							$this->db->query($update_data5);

							/*********Update notification****Start*****/
							$insert_direct = array(
								'from_user' => $member_data->sponsor_user_id,
								'to_user' => $this->session->userdata('user_id'),
								'balance' => 'NA',
								'log' => 'credit',
								'amount' => $level_income,
								'message' => "5th Level Income",
								'txn_id' => '#LI' . rand(),
								'status' => 1,
								'created_datetime' => date('Y-m-d H:i:s'),
								'modified_datetime' => date('Y-m-d H:i:s'),
							);
							$this->common->insert_data('pw_user_notification', $insert_direct);
							/*********Update notification****End*****/

							// '6th' Level
							$query6 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
							$result = $this->db->query($query6);

							if ($result->num_rows() > 0) {
								$member_data = $result->row();

								$level_income = $this->getLevelIncome(6);

								$update_data6 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
								$this->db->query($update_data6);

								/*********Update notification****Start*****/
								$insert_direct = array(
									'from_user' => $member_data->sponsor_user_id,
									'to_user' => $this->session->userdata('user_id'),
									'balance' => 'NA',
									'log' => 'credit',
									'amount' => $level_income,
									'message' => "6th Level Income",
									'txn_id' => '#LI' . rand(),
									'status' => 1,
									'created_datetime' => date('Y-m-d H:i:s'),
									'modified_datetime' => date('Y-m-d H:i:s'),
								);
								$this->common->insert_data('pw_user_notification', $insert_direct);
								/*********Update notification****End*****/

								// '7th' Level
								$query7 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
								$result = $this->db->query($query7);

								if ($result->num_rows() > 0) {
									$member_data = $result->row();

									$level_income = $this->getLevelIncome(7);

									$update_data7 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
									$this->db->query($update_data7);

									//*********Update notification****Start*****/
									$insert_direct = array(
										'from_user' => $member_data->sponsor_user_id,
										'to_user' => $this->session->userdata('user_id'),
										'balance' => 'NA',
										'log' => 'credit',
										'amount' => $level_income,
										'message' => "7th Level Income",
										'txn_id' => '#LI' . rand(),
										'status' => 1,
										'created_datetime' => date('Y-m-d H:i:s'),
										'modified_datetime' => date('Y-m-d H:i:s'),
									);
									$this->common->insert_data('pw_user_notification', $insert_direct);
									/*********Update notification****End*****/

									// '8th' Level
									$query8 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
									$result = $this->db->query($query8);

									if ($result->num_rows() > 0) {
										$member_data = $result->row();

										$level_income = $this->getLevelIncome(8);

										$update_data8 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
										$this->db->query($update_data8);

										/*********Update notification****Start*****/
										$insert_direct = array(
											'from_user' => $member_data->sponsor_user_id,
											'to_user' => $this->session->userdata('user_id'),
											'balance' => 'NA',
											'log' => 'credit',
											'amount' => $level_income,
											'message' => "8th Level Income",
											'txn_id' => '#LI' . rand(),
											'status' => 1,
											'created_datetime' => date('Y-m-d H:i:s'),
											'modified_datetime' => date('Y-m-d H:i:s'),
										);
										$this->common->insert_data('pw_user_notification', $insert_direct);
										/*********Update notification****End*****/

										// '9th' Level
										$query9 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
										$result = $this->db->query($query9);

										if ($result->num_rows() > 0) {
											$member_data = $result->row();

											$level_income = $this->getLevelIncome(9);

											$update_data9 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
											$this->db->query($update_data9);

											/*********Update notification****Start*****/
											$insert_direct = array(
												'from_user' => $member_data->sponsor_user_id,
												'to_user' => $this->session->userdata('user_id'),
												'balance' => 'NA',
												'log' => 'credit',
												'amount' => $level_income,
												'message' => "9th Level Income",
												'txn_id' => '#LI' . rand(),
												'status' => 1,
												'created_datetime' => date('Y-m-d H:i:s'),
												'modified_datetime' => date('Y-m-d H:i:s'),
											);
											$this->common->insert_data('pw_user_notification', $insert_direct);
											/*********Update notification****End*****/

											// '10th' Level
											$query10 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
											$result = $this->db->query($query10);

											if ($result->num_rows() > 0) {
												$member_data = $result->row();

												$level_income = $this->getLevelIncome(10);

												$update_data10 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
												$this->db->query($update_data10);

												/*********Update notification****Start*****/
												$insert_direct = array(
													'from_user' => $member_data->sponsor_user_id,
													'to_user' => $this->session->userdata('user_id'),
													'balance' => 'NA',
													'log' => 'credit',
													'amount' => $level_income,
													'message' => "Team Income",
													'txn_id' => '#LI' . rand(),
													'status' => 1,
													'created_datetime' => date('Y-m-d H:i:s'),
													'modified_datetime' => date('Y-m-d H:i:s'),
												);
												$this->common->insert_data('pw_user_notification', $insert_direct);
												/*********Update notification****End*****/

												// '11th' Level
												$query11 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
												$result = $this->db->query($query11);

												if ($result->num_rows() > 0) {
													$member_data = $result->row();

													$level_income = $this->getLevelIncome(11);

													$update_data11 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
													$this->db->query($update_data11);

													/*********Update notification****Start*****/
													$insert_direct = array(
														'from_user' => $member_data->sponsor_user_id,
														'to_user' => $this->session->userdata('user_id'),
														'balance' => 'NA',
														'log' => 'credit',
														'amount' => $level_income,
														'message' => "Team Income",
														'txn_id' => '#LI' . rand(),
														'status' => 1,
														'created_datetime' => date('Y-m-d H:i:s'),
														'modified_datetime' => date('Y-m-d H:i:s'),
													);
													$this->common->insert_data('pw_user_notification', $insert_direct);
													/*********Update notification****End*****/

													// '12th' Level
													$query12 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
													$result = $this->db->query($query12);

													if ($result->num_rows() > 0) {
														$member_data = $result->row();

														$level_income = $this->getLevelIncome(12);

														$update_data12 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
														$this->db->query($update_data12);

														/*********Update notification****Start*****/
														$insert_direct = array(
															'from_user' => $member_data->sponsor_user_id,
															'to_user' => $this->session->userdata('user_id'),
															'balance' => 'NA',
															'log' => 'credit',
															'amount' => $level_income,
															'message' => "Team Income",
															'txn_id' => '#LI' . rand(),
															'status' => 1,
															'created_datetime' => date('Y-m-d H:i:s'),
															'modified_datetime' => date('Y-m-d H:i:s'),
														);
														$this->common->insert_data('pw_user_notification', $insert_direct);
														/*********Update notification****End*****/

														// '13th' Level
														$query13 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
														$result = $this->db->query($query13);

														if ($result->num_rows() > 0) {
															$member_data = $result->row();

															$level_income = $this->getLevelIncome(13);

															$update_data13 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
															$this->db->query($update_data13);

															/*********Update notification****Start*****/
															$insert_direct = array(
																'from_user' => $member_data->sponsor_user_id,
																'to_user' => $this->session->userdata('user_id'),
																'balance' => 'NA',
																'log' => 'credit',
																'amount' => $level_income,
																'message' => "Team Income",
																'txn_id' => '#LI' . rand(),
																'status' => 1,
																'created_datetime' => date('Y-m-d H:i:s'),
																'modified_datetime' => date('Y-m-d H:i:s'),
															);
															$this->common->insert_data('pw_user_notification', $insert_direct);
															/*********Update notification****End*****/

															// '14th' Level
															$query14 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
															$result = $this->db->query($query14);

															if ($result->num_rows() > 0) {
																$member_data = $result->row();

																$level_income = $this->getLevelIncome(14);

																$update_data14 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																$this->db->query($update_data14);

																/*********Update notification****Start*****/
																$insert_direct = array(
																	'from_user' => $member_data->sponsor_user_id,
																	'to_user' => $this->session->userdata('user_id'),
																	'balance' => 'NA',
																	'log' => 'credit',
																	'amount' => $level_income,
																	'message' => "Team Income",
																	'txn_id' => '#LI' . rand(),
																	'status' => 1,
																	'created_datetime' => date('Y-m-d H:i:s'),
																	'modified_datetime' => date('Y-m-d H:i:s'),
																);
																$this->common->insert_data('pw_user_notification', $insert_direct);
																/*********Update notification****End*****/

																// '15th' Level
																$query15 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																$result = $this->db->query($query15);

																if ($result->num_rows() > 0) {
																	$member_data = $result->row();

																	$level_income = $this->getLevelIncome(15);

																	$update_data15 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																	$this->db->query($update_data15);

																	/*********Update notification****Start*****/
																	$insert_direct = array(
																		'from_user' => $member_data->sponsor_user_id,
																		'to_user' => $this->session->userdata('user_id'),
																		'balance' => 'NA',
																		'log' => 'credit',
																		'amount' => $level_income,
																		'message' => "Team Income",
																		'txn_id' => '#LI' . rand(),
																		'status' => 1,
																		'created_datetime' => date('Y-m-d H:i:s'),
																		'modified_datetime' => date('Y-m-d H:i:s'),
																	);
																	$this->common->insert_data('pw_user_notification', $insert_direct);
																	/*********Update notification****End*****/

																	// '16th' Level
																	$query16 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																	$result = $this->db->query($query16);

																	if ($result->num_rows() > 0) {
																		$member_data = $result->row();

																		$level_income = $this->getLevelIncome(16);

																		$update_data16 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																		$this->db->query($update_data16);

																		/*********Update notification****Start*****/
																		$insert_direct = array(
																			'from_user' => $member_data->sponsor_user_id,
																			'to_user' => $this->session->userdata('user_id'),
																			'balance' => 'NA',
																			'log' => 'credit',
																			'amount' => $level_income,
																			'message' => "Team Income",
																			'txn_id' => '#LI' . rand(),
																			'status' => 1,
																			'created_datetime' => date('Y-m-d H:i:s'),
																			'modified_datetime' => date('Y-m-d H:i:s'),
																		);
																		$this->common->insert_data('pw_user_notification', $insert_direct);
																		/*********Update notification****End*****/

																		// '17th' Level
																		$query17 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																		$result = $this->db->query($query17);

																		if ($result->num_rows() > 0) {
																			$member_data = $result->row();

																			$level_income = $this->getLevelIncome(17);

																			$update_data17 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																			$this->db->query($update_data17);

																			/*********Update notification****Start*****/
																			$insert_direct = array(
																				'from_user' => $member_data->sponsor_user_id,
																				'to_user' => $this->session->userdata('user_id'),
																				'balance' => 'NA',
																				'log' => 'credit',
																				'amount' => $level_income,
																				'message' => "Team Income",
																				'txn_id' => '#LI' . rand(),
																				'status' => 1,
																				'created_datetime' => date('Y-m-d H:i:s'),
																				'modified_datetime' => date('Y-m-d H:i:s'),
																			);
																			$this->common->insert_data('pw_user_notification', $insert_direct);
																			/*********Update notification****end*****/

																			// '18th' Level
																			$query18 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																			$result = $this->db->query($query18);

																			if ($result->num_rows() > 0) {
																				$member_data = $result->row();

																				$level_income = $this->getLevelIncome(18);

																				$update_data18 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																				$this->db->query($update_data18);

																				/*********Update notification****Start*****/
																				$insert_direct = array(
																					'from_user' => $member_data->sponsor_user_id,
																					'to_user' => $this->session->userdata('user_id'),
																					'balance' => 'NA',
																					'log' => 'credit',
																					'amount' => $level_income,
																					'message' => "Team Income",
																					'txn_id' => '#LI' . rand(),
																					'status' => 1,
																					'created_datetime' => date('Y-m-d H:i:s'),
																					'modified_datetime' => date('Y-m-d H:i:s'),
																				);
																				$this->common->insert_data('pw_user_notification', $insert_direct);
																				/*********Update notification****End*****/

																				// '19th' Level
																				$query19 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																				$result = $this->db->query($query19);

																				if ($result->num_rows() > 0) {
																					$member_data = $result->row();

																					$level_income = $this->getLevelIncome(19);

																					$update_data19 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																					$this->db->query($update_data19);

																					/*********Update notification****Start*****/
																					$insert_direct = array(
																						'from_user' => $member_data->sponsor_user_id,
																						'to_user' => $this->session->userdata('user_id'),
																						'balance' => 'NA',
																						'log' => 'credit',
																						'amount' => $level_income,
																						'message' => "Team Income",
																						'txn_id' => '#LI' . rand(),
																						'status' => 1,
																						'created_datetime' => date('Y-m-d H:i:s'),
																						'modified_datetime' => date('Y-m-d H:i:s'),
																					);
																					$this->common->insert_data('pw_user_notification', $insert_direct);
																					/*********Update notification****End*****/

																					// '20th' Level
																					$query20 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																					$result = $this->db->query($query20);

																					if ($result->num_rows() > 0) {
																						$member_data = $result->row();

																						$level_income = $this->getLevelIncome(20);

																						$update_data20 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																						$this->db->query($update_data20);

																						/*********Update notification****Start*****/
																						$insert_direct = array(
																							'from_user' => $member_data->sponsor_user_id,
																							'to_user' => $this->session->userdata('user_id'),
																							'balance' => 'NA',
																							'log' => 'credit',
																							'amount' => $level_income,
																							'message' => "Team Income",
																							'txn_id' => '#LI' . rand(),
																							'status' => 1,
																							'created_datetime' => date('Y-m-d H:i:s'),
																							'modified_datetime' => date('Y-m-d H:i:s'),
																						);
																						$this->common->insert_data('pw_user_notification', $insert_direct);
																						/*********Update notification****End*****/
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			// }
		}
	}

	// 20 level
	private function getLevelIncome($level)
	{
		if ($level == 1) {
			$income = 1.11;
		} else if ($level == 2) {
			$income = 1.11;
		} else if ($level == 3) {
			$income = 1.11;
		} else if ($level == 4) {
			$income = 1.11;
		} else if ($level == 5) {
			$income = 1.11;
		} else if ($level == 6) {
			$income = 1.11;
		} else if ($level == 7) {
			$income = 1.11;
		} else if ($level == 8) {
			$income = 1.11;
		} else if ($level == 9) {
			$income = 1.11;
		} else if ($level == 10) {
			$income = 1.11;
		} else if ($level == 11) {
			$income = 1.11;
		} else if ($level == 12) {
			$income = 1.11;
		} else if ($level == 13) {
			$income = 1.11;
		} else if ($level == 14) {
			$income = 1.11;
		} else if ($level == 15) {
			$income = 1.11;
		} else if ($level == 16) {
			$income = 1.11;
		} else if ($level == 17) {
			$income = 1.11;
		} else if ($level == 18) {
			$income = 1.11;
		} else if ($level == 19) {
			$income = 1.11;
		} else if ($level == 20) {
			$income = 1.11;
		}

		return $income;
	}


	/********Non Working payout functions `Start`***********/
	private function apply_non_working($user_id, $package)
	{
		$member_data = $this->common->getSingle('pw_users', array('id' => $user_id));

		/*****insert into `pw_non_working_user`**Start******/
		$insert_data = array(
			'user_id' => $member_data->id,
			'user_name' => $member_data->username,
			'package' => $package,
			'created_datetime' => date('Y-m-d H:i:s'),
		);
		$insert_id =  $this->common->insert_data('pw_non_working_user', $insert_data);
		/*****insert into `pw_non_working_user`**End******/

		/*****insert into `pw_non_working_income`**End******/
		$leveltree = array(
			'user_id' => $this->session->userdata('user_id'),
			'package' => $package,
			'level' => 'NA'
		);
		$this->common->insert_data('pw_non_working_income', $leveltree);
		/*****insert into `pw_non_working_income`**End******/

		/*****insert into `pw_company_matrix`**Start******/
		// if ($package != 30) {
		// 	$trees = $this->common->getSingleFromLast('pw_company_matrix', array('package' => $package));
		// 	$last = $this->common->getSingleFromSecondLast('pw_company_matrix', array('package' => $package));
		// 	$totalcount = $last->tree_count * 3;
		// 	if ($trees->tree_count < $totalcount) {
		// 		$update_user = "UPDATE `pw_company_matrix` SET user_tree = '" . $trees->user_tree . ', ' . $this->session->user_id . "', tree_count = tree_count+1 WHERE package = $package AND `level` = $trees->level";
		// 		$this->db->query($update_user);
		// 	} else {
		// 		$company_tree = array(
		// 			'package' => $package,
		// 			'level' => $trees->level + 1,
		// 			'user_tree' => $this->session->userdata('user_id'),
		// 			'tree_count' => 1
		// 		);
		// 		$this->common->insert_data('pw_company_matrix', $company_tree);
		// 	}
		// }
		/*****insert into `pw_company_matrix`**End******/

		/*******Update Direct Bonus by Condition**Start*******/
		if ($package != 30) {
			$this->updateDirectBonusByPackage($user_id, $package); //for 40, 100, 1000, 5000, 10000			
		}
		/*******Update Direct Bonus by Condition**End*******/

		/********Upgrade Memeber Logic Start********/
		$this->upgrade_plan_non($member_data->id, $package);
		/********Upgrade Memeber Logic End********/
	}

	private function updateDirectBonusByPackage($user_id, $package)
	{
		$member_data = $this->common->getSingle('pw_users', array('id' => $user_id));

		if ($package == 40) {
			$direct_income = 20;

			$sponsor_data1 = $this->common->getSingle('pw_users', array('username' => $member_data->sponsor));

			if (!empty($sponsor_data1->sponsor)) {
				$final_sposnor = $sponsor_data1->sponsor;

				$package_exit = $this->common->getSingle('pw_non_working_user', array('user_name' => $final_sposnor, 'package' => $package));
				if (!empty($package_exit)) {
					$final_sposnor_id = $package_exit->user_id;

					$insert_data = array(
						'session_id' => $final_sposnor_id,
						'username' => $member_data->username,
						'package' => $package,
						'amount' => $direct_income,
						'type' => 'C',
						'comment' => 'Direct Bonus Income',
						'action' => 'direct',
						'created_time' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pw_non_working_payout', $insert_data);

					/********Update Wallet Income*Start*******/
					$update_query = "update pw_users set direct_income_wallet = direct_income_wallet + " . $direct_income . ", direct_income_balance = (direct_income_balance + " . $direct_income . ") where id = '" . $final_sposnor_id . "'";
					$this->db->query($update_query);
					/********Update Wallet Income*End*******/
				}
			}
		} else if ($package == 100) {
			$direct_income = 50;

			$sponsor_data1 = $this->common->getSingle('pw_users', array('username' => $member_data->sponsor));

			if (!empty($sponsor_data1->sponsor)) {
				$sponsor_data2 = $this->common->getSingle('pw_users', array('username' => $sponsor_data1->sponsor));

				if (!empty($sponsor_data2->sponsor)) {
					$final_sposnor = $sponsor_data2->sponsor;

					$package_exit = $this->common->getSingle('pw_non_working_user', array('user_name' => $final_sposnor, 'package' => $package));

					if (!empty($package_exit)) {
						$final_sposnor_id = $package_exit->user_id;

						$insert_data = array(
							'session_id' => $final_sposnor_id,
							'username' => $member_data->username,
							'package' => $package,
							'amount' => $direct_income,
							'type' => 'C',
							'comment' => 'Direct Bonus Income',
							'action' => 'direct',
							'created_time' => date('Y-m-d H:i:s'),
						);
						$this->common->insert_data('pw_non_working_payout', $insert_data);

						/********Update Wallet Income*Start*******/
						$update_query = "update pw_users set direct_income_wallet = direct_income_wallet + " . $direct_income . ", direct_income_balance = (direct_income_balance + " . $direct_income . ") where id = '" . $final_sposnor_id . "'";
						$this->db->query($update_query);
						/********Update Wallet Income*End*******/
					}
				}
			}
		} else if ($package == 1000) {
			$direct_income = 500;

			$sponsor_data1 = $this->common->getSingle('pw_users', array('username' => $member_data->sponsor));

			if (!empty($sponsor_data1->sponsor)) {
				$sponsor_data2 = $this->common->getSingle('pw_users', array('username' => $sponsor_data1->sponsor));

				if (!empty($sponsor_data2->sponsor)) {
					$sponsor_data3 = $this->common->getSingle('pw_users', array('username' => $sponsor_data2->sponsor));

					if (!empty($sponsor_data3->sponsor)) {
						$final_sposnor = $sponsor_data3->sponsor;

						$package_exit = $this->common->getSingle('pw_non_working_user', array('user_name' => $final_sposnor, 'package' => $package));
						if (!empty($package_exit)) {
							$final_sposnor_id = $package_exit->user_id;

							$insert_data = array(
								'session_id' => $final_sposnor_id,
								'username' => $member_data->username,
								'package' => $package,
								'amount' => $direct_income,
								'type' => 'C',
								'comment' => 'Direct Bonus Income',
								'action' => 'direct',
								'created_time' => date('Y-m-d H:i:s'),
							);
							$this->common->insert_data('pw_non_working_payout', $insert_data);

							/********Update Wallet Income*Start*******/
							$update_query = "update pw_users set direct_income_wallet = direct_income_wallet + " . $direct_income . ", direct_income_balance = (direct_income_balance + " . $direct_income . ") where id = '" . $final_sposnor_id . "'";
							$this->db->query($update_query);
							/********Update Wallet Income*End*******/
						}
					}
				}
			}
		} else if ($package == 5000) {
			$direct_income = 2500;

			$sponsor_data1 = $this->common->getSingle('pw_users', array('username' => $member_data->sponsor));

			if (!empty($sponsor_data1->sponsor)) {
				$sponsor_data2 = $this->common->getSingle('pw_users', array('username' => $sponsor_data1->sponsor));

				if (!empty($sponsor_data2->sponsor)) {
					$sponsor_data3 = $this->common->getSingle('pw_users', array('username' => $sponsor_data2->sponsor));

					if (!empty($sponsor_data3->sponsor)) {
						$sponsor_data4 = $this->common->getSingle('pw_users', array('username' => $sponsor_data3->sponsor));

						if (!empty($sponsor_data4->sponsor)) {
							$final_sposnor = $sponsor_data4->sponsor;

							$package_exit = $this->common->getSingle('pw_non_working_user', array('user_name' => $final_sposnor, 'package' => $package));
							if (!empty($package_exit)) {
								$final_sposnor_id = $package_exit->user_id;

								$insert_data = array(
									'session_id' => $final_sposnor_id,
									'username' => $member_data->username,
									'package' => $package,
									'amount' => $direct_income,
									'type' => 'C',
									'comment' => 'Direct Bonus Income',
									'action' => 'direct',
									'created_time' => date('Y-m-d H:i:s'),
								);
								$this->common->insert_data('pw_non_working_payout', $insert_data);
								/********Update Wallet Income*Start*******/
								$update_query = "update pw_users set direct_income_wallet = direct_income_wallet + " . $direct_income . ", direct_income_balance = (direct_income_balance + " . $direct_income . ") where id = '" . $final_sposnor_id . "'";
								$this->db->query($update_query);
								/********Update Wallet Income*End*******/
							}
						}
					}
				}
			}
		} else if ($package == 10000) {
			$direct_income = 5000;

			$sponsor_data1 = $this->common->getSingle('pw_users', array('username' => $member_data->sponsor));

			if (!empty($sponsor_data1->sponsor)) {
				$sponsor_data2 = $this->common->getSingle('pw_users', array('username' => $sponsor_data1->sponsor));

				if (!empty($sponsor_data2->sponsor)) {
					$sponsor_data3 = $this->common->getSingle('pw_users', array('username' => $sponsor_data2->sponsor));

					if (!empty($sponsor_data3->sponsor)) {
						$sponsor_data4 = $this->common->getSingle('pw_users', array('username' => $sponsor_data3->sponsor));

						if (!empty($sponsor_data4->sponsor)) {
							$sponsor_data5 = $this->common->getSingle('pw_users', array('username' => $sponsor_data4->sponsor));

							if (!empty($sponsor_data5->sponsor)) {
								$final_sposnor = $sponsor_data5->sponsor;

								$package_exit = $this->common->getSingle('pw_non_working_user', array('user_name' => $final_sposnor, 'package' => $package));
								if (!empty($package_exit)) {
									$final_sposnor_id = $package_exit->user_id;

									$insert_data = array(
										'session_id' => $final_sposnor_id,
										'username' => $member_data->username,
										'package' => $package,
										'amount' => $direct_income,
										'type' => 'C',
										'comment' => 'Direct Bonus Income',
										'action' => 'direct',
										'created_time' => date('Y-m-d H:i:s'),
									);
									$this->common->insert_data('pw_non_working_payout', $insert_data);

									/********Update Wallet Income*Start*******/
									$update_query = "update pw_users set direct_income_wallet = direct_income_wallet + " . $direct_income . ", direct_income_balance = (direct_income_balance + " . $direct_income . ") where id = '" . $final_sposnor_id . "'";
									$this->db->query($update_query);
									/********Update Wallet Income*End*******/
								}
							}
						}
					}
				}
			}
		}
	}

	private function upgrade_plan_non($member_id, $package)
	{
		// $member_data = $this->common->getSingle('pw_users', array('id' => $member_id));

		$query =  "select * from pw_non_working_user where package = '" . $package . "'";
		$data = $this->db->query($query);

		if ($data->num_rows() > 1) {
			$result = $data->result();

			$membership_data1 = $this->common->getSingle('pw_non_working_user', array('user_id' => $result[0]->user_id, 'package' => $package));

			/***3x3 Matrix ** Tree Structure ***Start**/

			$tree_count = $membership_data1->tree_count;

			$tree_arr = json_decode($membership_data1->tree, true);

			if (empty($tree_arr)) {
				$tree_arr = array();
			}

			if ($tree_count <= 2) {
				array_push($tree_arr, $member_id);

				$tree_count = $tree_count + 1;

				$update_tree =  "UPDATE `pw_non_working_user` SET `tree` = '" . json_encode($tree_arr) . "', tree_count = '" . $tree_count . "' WHERE `user_id` = '" . $membership_data1->user_id . "' and package = '" . $package . "'";
				$this->db->query($update_tree);
			} else {
				$this->tree_recursion_non($member_id, $package);
			}

			/***3x3 Matrix ** Tree Structure ***End**/


			/*****Level Earning Managment for `5` Level****Start*****/
			// $this->plan_user_earning_non($member_data->id, $member_data->username, $package);
			/*****Level Earning Managment for `5` Level****End*****/
		}
	}

	private function tree_recursion_non($member_id, $package)
	{
		$query =  "select * from pw_non_working_user where package = '" . $package . "' and tree_count < 3";
		$data = $this->db->query($query);

		if ($data->num_rows() > 0) {
			$search_sponsor_data = $data->row();

			$tree_count = $search_sponsor_data->tree_count;

			$child_tree_arr = json_decode($search_sponsor_data->tree, true);

			if (empty($child_tree_arr)) {
				$child_tree_arr = array();
			}
			if ($tree_count <= 2) {
				array_push($child_tree_arr, $member_id);

				$tree_count = $tree_count + 1;

				$update_child =  "UPDATE `pw_non_working_user` SET `tree` = '" . json_encode($child_tree_arr) . "', tree_count = '" . $tree_count . "' WHERE `id` = '" . $search_sponsor_data->id . "'";
				$this->db->query($update_child);
			} else {
				$this->tree_recursion_non($member_id, $package);
			}
		}
	}

	private function plan_user_earning_non($member_id, $member_username, $package)
	{
		// '1st' Level
		$query1 = "SELECT * FROM `pw_non_working_user` WHERE package = '" . $package . "' and `tree` LIKE '%\"" . $member_id . "\"%'";
		$result = $this->db->query($query1);

		if ($result->num_rows() > 0) {
			$membership_data = $result->row();

			$level_income = $this->getLevelIncomeByPackage_non($package, 1);

			$update_level_income = $membership_data->level_income + $level_income;

			$update_data1 =  "UPDATE `pw_non_working_user` SET level_income = " . $update_level_income . " WHERE `id` = '" . $membership_data->id . "'";
			$this->db->query($update_data1);

			/*****Update Level Completion********/
			$membership_data_level = $this->common->getSingle('pw_non_working_user', array('id' => $membership_data->id));

			$level_number = 1;

			$complete_level_income = $this->getCompleteLevelIncomeByPackage_non($package, $level_number);

			if (!empty($membership_data_level->level_income) && ($membership_data_level->level_income == $complete_level_income)) {
				$single_level_income = $this->getSingleLevelIncomeByPackage_non($package, $level_number);
				$this->update_level_completion($membership_data_level->user_id, $package, $level_number, $single_level_income);
			}
			/*****Update Level Completion********/

			$insert_data1 = array(
				'session_id' => $membership_data->user_id,
				'username' => $member_username,
				'package' => $package,
				'amount' => $level_income,
				'type' => 'C',
				'comment' => '1st Level Income',
				'action' => 'level',
				'created_time' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_non_working_payout', $insert_data1);

			// '2nd' Level
			$query2 = "SELECT * FROM `pw_non_working_user` WHERE package = '" . $package . "' and `tree` LIKE '%\"" . $membership_data->user_id . "\"%'";
			$result = $this->db->query($query2);

			if ($result->num_rows() > 0) {
				$membership_data = $result->row();

				$level_income = $this->getLevelIncomeByPackage_non($package, 2);

				$update_level_income = $membership_data->level_income + $level_income;

				$update_data1 =  "UPDATE `pw_non_working_user` SET level_income = " . $update_level_income . " WHERE `id` = '" . $membership_data->id . "'";
				$this->db->query($update_data1);

				/*****Update Level Completion********/
				$membership_data_level = $this->common->getSingle('pw_non_working_user', array('id' => $membership_data->id));

				$level_number = 2;

				$complete_level_income = $this->getCompleteLevelIncomeByPackage_non($package, $level_number);

				if (!empty($membership_data_level->level_income) && ($membership_data_level->level_income == $complete_level_income)) {
					$single_level_income = $this->getSingleLevelIncomeByPackage_non($package, $level_number);
					$this->update_level_completion($membership_data_level->user_id, $package, $level_number, $single_level_income);
				}
				/*****Update Level Completion********/

				$insert_data1 = array(
					'session_id' => $membership_data->user_id,
					'username' => $member_username,
					'package' => $package,
					'amount' => $level_income,
					'type' => 'C',
					'comment' => '2nd Level Income',
					'action' => 'level',
					'created_time' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_non_working_payout', $insert_data1);

				// '3rd' Level
				$query3 = "SELECT * FROM `pw_non_working_user` WHERE package = '" . $package . "' and `tree` LIKE '%\"" . $membership_data->user_id . "\"%'";
				$result = $this->db->query($query3);

				if ($result->num_rows() > 0) {
					$membership_data = $result->row();

					$level_income = $this->getLevelIncomeByPackage_non($package, 3);

					$update_level_income = $membership_data->level_income + $level_income;

					$update_data1 =  "UPDATE `pw_non_working_user` SET level_income = " . $update_level_income . " WHERE `id` = '" . $membership_data->id . "'";
					$this->db->query($update_data1);

					/*****Update Level Completion********/
					$membership_data_level = $this->common->getSingle('pw_non_working_user', array('id' => $membership_data->id));

					$level_number = 3;

					$complete_level_income = $this->getCompleteLevelIncomeByPackage_non($package, $level_number);

					if (!empty($membership_data_level->level_income) && ($membership_data_level->level_income == $complete_level_income)) {
						$single_level_income = $this->getSingleLevelIncomeByPackage_non($package, $level_number);
						$this->update_level_completion($membership_data_level->user_id, $package, $level_number, $single_level_income);
					}
					/*****Update Level Completion********/

					$insert_data1 = array(
						'session_id' => $membership_data->user_id,
						'username' => $member_username,
						'package' => $package,
						'amount' => $level_income,
						'type' => 'C',
						'comment' => '3rd Level Income',
						'action' => 'level',
						'created_time' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pw_non_working_payout', $insert_data1);

					// '4th' Level
					$query4 = "SELECT * FROM `pw_non_working_user` WHERE package = '" . $package . "' and `tree` LIKE '%\"" . $membership_data->user_id . "\"%'";
					$result = $this->db->query($query4);

					if ($result->num_rows() > 0) {
						$membership_data = $result->row();

						$level_income = $this->getLevelIncomeByPackage_non($package, 4);

						$update_level_income = $membership_data->level_income + $level_income;

						$update_data1 =  "UPDATE `pw_non_working_user` SET level_income = " . $update_level_income . " WHERE `id` = '" . $membership_data->id . "'";
						$this->db->query($update_data1);

						/*****Update Level Completion********/
						$membership_data_level = $this->common->getSingle('pw_non_working_user', array('id' => $membership_data->id));

						$level_number = 4;

						$complete_level_income = $this->getCompleteLevelIncomeByPackage_non($package, $level_number);

						if (!empty($membership_data_level->level_income) && ($membership_data_level->level_income == $complete_level_income)) {
							$single_level_income = $this->getSingleLevelIncomeByPackage_non($package, $level_number);
							$this->update_level_completion($membership_data_level->user_id, $package, $level_number, $single_level_income);
						}
						/*****Update Level Completion********/

						$insert_data1 = array(
							'session_id' => $membership_data->user_id,
							'username' => $member_username,
							'package' => $package,
							'amount' => $level_income,
							'type' => 'C',
							'comment' => '4th Level Income',
							'action' => 'level',
							'created_time' => date('Y-m-d H:i:s'),
						);
						$this->common->insert_data('pw_non_working_payout', $insert_data1);

						// '5th' Level
						$query5 = "SELECT * FROM `pw_non_working_user` WHERE package = '" . $package . "' and `tree` LIKE '%\"" . $membership_data->user_id . "\"%'";
						$result = $this->db->query($query5);

						if ($result->num_rows() > 0) {
							$membership_data = $result->row();

							$level_income = $this->getLevelIncomeByPackage_non($package, 5);

							$update_level_income = $membership_data->level_income + $level_income;

							$update_data1 =  "UPDATE `pw_non_working_user` SET level_income = " . $update_level_income . " WHERE `id` = '" . $membership_data->id . "'";
							$this->db->query($update_data1);

							/*****Update Level Completion********/
							$membership_data_level = $this->common->getSingle('pw_non_working_user', array('id' => $membership_data->id));

							$level_number = 5;

							$complete_level_income = $this->getCompleteLevelIncomeByPackage_non($package, $level_number);

							if (!empty($membership_data_level->level_income) && ($membership_data_level->level_income == $complete_level_income)) {
								$single_level_income = $this->getSingleLevelIncomeByPackage_non($package, $level_number);
								$this->update_level_completion($membership_data_level->user_id, $package, $level_number, $single_level_income);
							}
							/*****Update Level Completion********/

							$insert_data1 = array(
								'session_id' => $membership_data->user_id,
								'username' => $member_username,
								'package' => $package,
								'amount' => $level_income,
								'type' => 'C',
								'comment' => '5th Level Income',
								'action' => 'level',
								'created_time' => date('Y-m-d H:i:s'),
							);
							$this->common->insert_data('pw_non_working_payout', $insert_data1);

							// '6th' Level
							$query6 = "SELECT * FROM `pw_non_working_user` WHERE package = '" . $package . "' and `tree` LIKE '%\"" . $membership_data->user_id . "\"%'";
							$result = $this->db->query($query6);

							if ($result->num_rows() > 0) {
								$membership_data = $result->row();

								$level_income = $this->getLevelIncomeByPackage_non($package, 6);

								$update_level_income = $membership_data->level_income + $level_income;

								$update_data1 =  "UPDATE `pw_non_working_user` SET level_income = " . $update_level_income . " WHERE `id` = '" . $membership_data->id . "'";
								$this->db->query($update_data1);

								/*****Update Level Completion********/
								$membership_data_level = $this->common->getSingle('pw_non_working_user', array('id' => $membership_data->id));

								$level_number = 6;

								$complete_level_income = $this->getCompleteLevelIncomeByPackage_non($package, $level_number);

								if (!empty($membership_data_level->level_income) && ($membership_data_level->level_income == $complete_level_income)) {
									$single_level_income = $this->getSingleLevelIncomeByPackage_non($package, $level_number);
									$this->update_level_completion($membership_data_level->user_id, $package, $level_number, $single_level_income);
								}
								/*****Update Level Completion********/


								$insert_data1 = array(
									'session_id' => $membership_data->user_id,
									'username' => $member_username,
									'package' => $package,
									'amount' => $level_income,
									'type' => 'C',
									'comment' => '6th Level Income',
									'action' => 'level',
									'created_time' => date('Y-m-d H:i:s'),
								);
								$this->common->insert_data('pw_non_working_payout', $insert_data1);

								// '7th' Level
								$query7 = "SELECT * FROM `pw_non_working_user` WHERE package = '" . $package . "' and `tree` LIKE '%\"" . $membership_data->user_id . "\"%'";
								$result = $this->db->query($query7);

								if ($result->num_rows() > 0) {
									$membership_data = $result->row();

									$level_income = $this->getLevelIncomeByPackage_non($package, 7);

									$update_level_income = $membership_data->level_income + $level_income;

									$update_data1 =  "UPDATE `pw_non_working_user` SET level_income = " . $update_level_income . " WHERE `id` = '" . $membership_data->id . "'";
									$this->db->query($update_data1);

									/*****Update Level Completion********/
									$membership_data_level = $this->common->getSingle('pw_non_working_user', array('id' => $membership_data->id));

									$level_number = 7;

									$complete_level_income = $this->getCompleteLevelIncomeByPackage_non($package, $level_number);

									if (!empty($membership_data_level->level_income) && ($membership_data_level->level_income == $complete_level_income)) {
										$single_level_income = $this->getSingleLevelIncomeByPackage_non($package, $level_number);
										$this->update_level_completion($membership_data_level->user_id, $package, $level_number, $single_level_income);
									}
									/*****Update Level Completion********/

									$insert_data1 = array(
										'session_id' => $membership_data->user_id,
										'username' => $member_username,
										'package' => $package,
										'amount' => $level_income,
										'type' => 'C',
										'comment' => '7th Level Income',
										'action' => 'level',
										'created_time' => date('Y-m-d H:i:s'),
									);
									$this->common->insert_data('pw_non_working_payout', $insert_data1);

									// '8th' Level
									$query8 = "SELECT * FROM `pw_non_working_user` WHERE package = '" . $package . "' and `tree` LIKE '%\"" . $membership_data->user_id . "\"%'";
									$result = $this->db->query($query8);

									if ($result->num_rows() > 0) {
										$membership_data = $result->row();

										$level_income = $this->getLevelIncomeByPackage_non($package, 8);

										$update_level_income = $membership_data->level_income + $level_income;

										$update_data1 =  "UPDATE `pw_non_working_user` SET level_income = " . $update_level_income . " WHERE `id` = '" . $membership_data->id . "'";
										$this->db->query($update_data1);

										/*****Update Level Completion********/
										$membership_data_level = $this->common->getSingle('pw_non_working_user', array('id' => $membership_data->id));

										$level_number = 8;

										$complete_level_income = $this->getCompleteLevelIncomeByPackage_non($package, $level_number);

										if (!empty($membership_data_level->level_income) && ($membership_data_level->level_income == $complete_level_income)) {
											$single_level_income = $this->getSingleLevelIncomeByPackage_non($package, $level_number);
											$this->update_level_completion($membership_data_level->user_id, $package, $level_number, $single_level_income);
										}
										/*****Update Level Completion********/

										$insert_data1 = array(
											'session_id' => $membership_data->user_id,
											'username' => $member_username,
											'package' => $package,
											'amount' => $level_income,
											'type' => 'C',
											'comment' => '8th Level Income',
											'action' => 'level',
											'created_time' => date('Y-m-d H:i:s'),
										);
										$this->common->insert_data('pw_non_working_payout', $insert_data1);

										// '9th' Level
										$query9 = "SELECT * FROM `pw_non_working_user` WHERE package = '" . $package . "' and `tree` LIKE '%\"" . $membership_data->user_id . "\"%'";
										$result = $this->db->query($query9);

										if ($result->num_rows() > 0) {
											$membership_data = $result->row();

											$level_income = $this->getLevelIncomeByPackage_non($package, 9);

											$update_level_income = $membership_data->level_income + $level_income;

											$update_data1 =  "UPDATE `pw_non_working_user` SET level_income = " . $update_level_income . " WHERE `id` = '" . $membership_data->id . "'";
											$this->db->query($update_data1);

											/*****Update Level Completion********/
											$membership_data_level = $this->common->getSingle('pw_non_working_user', array('id' => $membership_data->id));

											$level_number = 9;

											$complete_level_income = $this->getCompleteLevelIncomeByPackage_non($package, $level_number);

											if (!empty($membership_data_level->level_income) && ($membership_data_level->level_income == $complete_level_income)) {
												$single_level_income = $this->getSingleLevelIncomeByPackage_non($package, $level_number);
												$this->update_level_completion($membership_data_level->user_id, $package, $level_number, $single_level_income);
											}
											/*****Update Level Completion********/

											$insert_data1 = array(
												'session_id' => $membership_data->user_id,
												'username' => $member_username,
												'package' => $package,
												'amount' => $level_income,
												'type' => 'C',
												'comment' => '9th Level Income',
												'action' => 'level',
												'created_time' => date('Y-m-d H:i:s'),
											);
											$this->common->insert_data('pw_non_working_payout', $insert_data1);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}


	private function update_level_completion($user_id, $package, $level_number, $level_complete_amount)
	{

		$insert_data = array(
			'user_id' => $user_id,
			'package' => $package,
			'level_complete_amount' => $level_complete_amount,
			'level_number' => $level_number,
			'level_complete_date' => date('Y-m-d H:i:s'),
		);

		$this->common->insert_data('pw_non_working_level', $insert_data);
	}

	private function getLevelIncomeByPackage_non($package, $level)
	{
		if ($package == '30') {
			if ($level == 1) {
				$income = 0.0; // 0.3
			} else if ($level == 2) {
				$income = 0.5; // 0.0
			} else if ($level == 3) {
				$income = 0.5; // 0.0
			} else if ($level == 4) {
				$income = 0.5;
			} else if ($level == 5) {
				$income = 0.5;
			} else if ($level == 6) {
				$income = 0.5;
			} else if ($level == 7) {
				$income = 0.5;
			} else if ($level == 8) {
				$income = 0.5;
			} else if ($level == 9) {
				$income = 1.2;
			}
		} else if ($package == '40') {
			if ($level == 1) {
				$income = 0.0;  // 2.5
			} else if ($level == 2) {
				$income = 2.5; // 0.0
			} else if ($level == 3) {
				$income = 1.5;
			} else if ($level == 4) {
				$income = 1.5;
			} else if ($level == 5) {
				$income = 1;
			} else if ($level == 6) {
				$income = 1;
			} else if ($level == 7) {
				$income = 2.5;
			} else if ($level == 8) {
				$income = 2.5;
			} else if ($level == 9) {
				$income = 5;
			}
		} else if ($package == '100') {
			if ($level == 1) {
				$income = 4; // 4
			} else if ($level == 2) {
				$income = 4;
			} else if ($level == 3) {
				$income = 4;
			} else if ($level == 4) {
				$income = 4;
			} else if ($level == 5) {
				$income = 4;
			} else if ($level == 6) {
				$income = 4;
			} else if ($level == 7) {
				$income = 5;
			} else if ($level == 8) {
				$income = 6;
			} else if ($level == 9) {
				$income = 15;
			}
		} else if ($package == '1000') {
			if ($level == 1) {
				$income = 60;
			} else if ($level == 2) {
				$income = 60;
			} else if ($level == 3) {
				$income = 60;
			} else if ($level == 4) {
				$income = 35;
			} else if ($level == 5) {
				$income = 35;
			} else if ($level == 6) {
				$income = 35;
			} else if ($level == 7) {
				$income = 40;
			} else if ($level == 8) {
				$income = 50;
			} else if ($level == 9) {
				$income = 125;
			}
		} else if ($package == '5000') {
			if ($level == 1) {
				$income = 300;
			} else if ($level == 2) {
				$income = 300;
			} else if ($level == 3) {
				$income = 200;
			} else if ($level == 4) {
				$income = 175;
			} else if ($level == 5) {
				$income = 175;
			} else if ($level == 6) {
				$income = 175;
			} else if ($level == 7) {
				$income = 175;
			} else if ($level == 8) {
				$income = 200;
			} else if ($level == 9) {
				$income = 800;
			}
		} else if ($package == '10000') {
			if ($level == 1) {
				$income = 700;
			} else if ($level == 2) {
				$income = 700;
			} else if ($level == 3) {
				$income = 500;
			} else if ($level == 4) {
				$income = 350;
			} else if ($level == 5) {
				$income = 350;
			} else if ($level == 6) {
				$income = 350;
			} else if ($level == 7) {
				$income = 350;
			} else if ($level == 8) {
				$income = 300;
			} else if ($level == 9) {
				$income = 1400;
			}
		}
		return $income;
	}

	private function getSingleLevelIncomeByPackage_non($package, $level)
	{
		if ($package == '30') {
			if ($level == 1) {
				$income = 0.9;
			} else if ($level == 2) {
				$income = 4.5;
			} else if ($level == 3) {
				$income = 13.5;
			} else if ($level == 4) {
				$income = 40.5;
			} else if ($level == 5) {
				$income = 121.5;
			} else if ($level == 6) {
				$income = 364.5;
			} else if ($level == 7) {
				$income = 1093.5;
			} else if ($level == 8) {
				$income = 3280.5;
			} else if ($level == 9) {
				$income = 23619.6;
			}
		} else if ($package == '40') {
			if ($level == 1) {
				$income = 7.5;
			} else if ($level == 2) {
				$income = 22.5;
			} else if ($level == 3) {
				$income = 40.5;
			} else if ($level == 4) {
				$income = 121.5;
			} else if ($level == 5) {
				$income = 243;
			} else if ($level == 6) {
				$income = 729;
			} else if ($level == 7) {
				$income = 5467.5;
			} else if ($level == 8) {
				$income = 16402.5;
			} else if ($level == 9) {
				$income = 98415;
			}
		} else if ($package == '100') {
			if ($level == 1) {
				$income = 12;
			} else if ($level == 2) {
				$income = 36;
			} else if ($level == 3) {
				$income = 108;
			} else if ($level == 4) {
				$income = 324;
			} else if ($level == 5) {
				$income = 972;
			} else if ($level == 6) {
				$income = 2916;
			} else if ($level == 7) {
				$income = 10935;
			} else if ($level == 8) {
				$income = 39366;
			} else if ($level == 9) {
				$income = 295245;
			}
		} else if ($package == '1000') {
			if ($level == 1) {
				$income = 180;
			} else if ($level == 2) {
				$income = 540;
			} else if ($level == 3) {
				$income = 1620;
			} else if ($level == 4) {
				$income = 2835;
			} else if ($level == 5) {
				$income = 8505;
			} else if ($level == 6) {
				$income = 25515;
			} else if ($level == 7) {
				$income = 87480;
			} else if ($level == 8) {
				$income = 328050;
			} else if ($level == 9) {
				$income = 2460375;
			}
		} else if ($package == '5000') {
			if ($level == 1) {
				$income = 900;
			} else if ($level == 2) {
				$income = 2700;
			} else if ($level == 3) {
				$income = 5400;
			} else if ($level == 4) {
				$income = 14175;
			} else if ($level == 5) {
				$income = 42525;
			} else if ($level == 6) {
				$income = 127575;
			} else if ($level == 7) {
				$income = 382725;
			} else if ($level == 8) {
				$income = 1312200;
			} else if ($level == 9) {
				$income = 15746400;
			}
		} else if ($package == '10000') {
			if ($level == 1) {
				$income = 2100;
			} else if ($level == 2) {
				$income = 6300;
			} else if ($level == 3) {
				$income = 13500;
			} else if ($level == 4) {
				$income = 28350;
			} else if ($level == 5) {
				$income = 85050;
			} else if ($level == 6) {
				$income = 255150;
			} else if ($level == 7) {
				$income = 765450;
			} else if ($level == 8) {
				$income = 1968300;
			} else if ($level == 9) {
				$income = 27556200;
			}
		}
		return $income;
	}

	private function getCompleteLevelIncomeByPackage_non($package, $level)
	{
		if ($package == '30') {
			if ($level == 1) {
				$income = 0.9;
			} else if ($level == 2) {
				$income = 0.9 + 4.5;
			} else if ($level == 3) {
				$income = 0.9 + 4.5 + 13.5;
			} else if ($level == 4) {
				$income = 0.9 + 4.5 + 13.5 + 40.5;
			} else if ($level == 5) {
				$income = 0.9 + 4.5 + 13.5 + 40.5 + 121.5;
			} else if ($level == 6) {
				$income = 0.9 + 4.5 + 13.5 + 40.5 + 121.5 + 364.5;
			} else if ($level == 7) {
				$income = 0.9 + 4.5 + 13.5 + 40.5 + 121.5 + 364.5 + 1093.5;
			} else if ($level == 8) {
				$income = 0.9 + 4.5 + 13.5 + 40.5 + 121.5 + 364.5 + 1093.5 + 3280.5;
			} else if ($level == 9) {
				$income = 0.9 + 4.5 + 13.5 + 40.5 + 121.5 + 364.5 + 1093.5 + 3280.5 + 23619.6;
			}
		} else if ($package == '40') {
			if ($level == 1) {
				$income = 7.5;
			} else if ($level == 2) {
				$income = 7.5 + 22.5;
			} else if ($level == 3) {
				$income = 7.5 + 22.5 + 40.5;
			} else if ($level == 4) {
				$income = 7.5 + 22.5 + 40.5 + 121.5;
			} else if ($level == 5) {
				$income = 7.5 + 22.5 + 40.5 + 121.5 + 243;
			} else if ($level == 6) {
				$income = 7.5 + 22.5 + 40.5 + 121.5 + 243 + 729;
			} else if ($level == 7) {
				$income = 7.5 + 22.5 + 40.5 + 121.5 + 243 + 729 + 5467.5;
			} else if ($level == 8) {
				$income = 7.5 + 22.5 + 40.5 + 121.5 + 243 + 729 + 5467.5 + 16402.5;
			} else if ($level == 9) {
				$income = 7.5 + 22.5 + 40.5 + 121.5 + 243 + 729 + 5467.5 + 16402.5 + 98415;
			}
		} else if ($package == '100') {
			if ($level == 1) {
				$income = 12;
			} else if ($level == 2) {
				$income = 12 + 36;
			} else if ($level == 3) {
				$income = 12 + 36 + 108;
			} else if ($level == 4) {
				$income = 12 + 36 + 108 + 324;
			} else if ($level == 5) {
				$income = 12 + 36 + 108 + 324 + 972;
			} else if ($level == 6) {
				$income = 12 + 36 + 108 + 324 + 972 + 2916;
			} else if ($level == 7) {
				$income = 12 + 36 + 108 + 324 + 972 + 2916 + 10935;
			} else if ($level == 8) {
				$income = 12 + 36 + 108 + 324 + 972 + 2916 + 10935 + 39366;
			} else if ($level == 9) {
				$income = 12 + 36 + 108 + 324 + 972 + 2916 + 10935 + 39366 + 295245;
			}
		} else if ($package == '1000') {
			if ($level == 1) {
				$income = 180;
			} else if ($level == 2) {
				$income = 180 + 540;
			} else if ($level == 3) {
				$income = 180 + 540 + 1620;
			} else if ($level == 4) {
				$income = 180 + 540 + 1620 + 2835;
			} else if ($level == 5) {
				$income = 180 + 540 + 1620 + 2835 + 8505;
			} else if ($level == 6) {
				$income = 180 + 540 + 1620 + 2835 + 8505 + 25515;
			} else if ($level == 7) {
				$income = 180 + 540 + 1620 + 2835 + 8505 + 25515 + 87480;
			} else if ($level == 8) {
				$income = 180 + 540 + 1620 + 2835 + 8505 + 25515 + 87480 + 328050;
			} else if ($level == 9) {
				$income = 180 + 540 + 1620 + 2835 + 8505 + 25515 + 87480 + 328050 + 2460375;
			}
		} else if ($package == '5000') {
			if ($level == 1) {
				$income = 900;
			} else if ($level == 2) {
				$income = 900 + 2700;
			} else if ($level == 3) {
				$income = 900 + 2700 + 5400;
			} else if ($level == 4) {
				$income = 900 + 2700 + 5400 + 14175;
			} else if ($level == 5) {
				$income = 900 + 2700 + 5400 + 14175 + 42525;
			} else if ($level == 6) {
				$income = 900 + 2700 + 5400 + 14175 + 42525 + 127575;
			} else if ($level == 7) {
				$income = 900 + 2700 + 5400 + 14175 + 42525 + 127575 + 382725;
			} else if ($level == 8) {
				$income = 900 + 2700 + 5400 + 14175 + 42525 + 127575 + 382725 + 1312200;
			} else if ($level == 9) {
				$income = 900 + 2700 + 5400 + 14175 + 42525 + 127575 + 382725 + 1312200 + 15746400;
			}
		} else if ($package == '10000') {
			if ($level == 1) {
				$income = 2100;
			} else if ($level == 2) {
				$income = 2100 + 6300;
			} else if ($level == 3) {
				$income = 2100 + 6300 + 13500;
			} else if ($level == 4) {
				$income = 2100 + 6300 + 13500 + 28350;
			} else if ($level == 5) {
				$income = 2100 + 6300 + 13500 + 28350 + 85050;
			} else if ($level == 6) {
				$income = 2100 + 6300 + 13500 + 28350 + 85050 + 255150;
			} else if ($level == 7) {
				$income = 2100 + 6300 + 13500 + 28350 + 85050 + 255150 + 765450;
			} else if ($level == 8) {
				$income = 2100 + 6300 + 13500 + 28350 + 85050 + 255150 + 765450 + 1968300;
			} else if ($level == 9) {
				$income = 2100 + 6300 + 13500 + 28350 + 85050 + 255150 + 765450 + 1968300 + 27556200;
			}
		}
		return $income;
	}

	/********Non Working payout functions `End`***********/

	function upgrade_membership()
	{
		$this->form_validation->set_rules('package', 'Package', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if ($this->form_validation->run() == FALSE) {
			echo 3;
			exit;
		} else {
			$next_package = getMyNextMembershipToUpgrade();

			$package = $this->input->post('package', true);

			if ($package != $next_package) {
				$this->session->set_flashdata('error_message', "Unauthorized access for membership upgradation !");

				echo 2;
				die;
			}

			$user_data = $this->common->getSingle('pw_users', array('id' => $this->session->userdata('user_id'),  'is_delete' => 1));

			if ($user_data->wallet_amount < $package) {
				$this->session->set_flashdata('error_message', "You don't have sufficient amount in your wallet. Minimum <strong>" . $package . "$</strong> is required !");
				echo 3;
				die;
			}

			$upgrade_data = $this->common->getSingle('pw_non_working_user', array('user_id' => $this->session->userdata('user_id'),  'package' => $package));

			if (!empty($upgrade_data)) {
				$this->session->set_flashdata('error_message', 'You have already upgraded this membership !');
				echo 4;
				die;
			}

			$minimum_direct = $this->minimum_direct_by_package($package);
			$direct_member_count = $this->getMyDirectMemberCount();

			if ($direct_member_count < $minimum_direct) {
				$this->session->set_flashdata('error_message', 'Minimum ' . $minimum_direct . ' direct member should be there !');
				echo 5;
				die;
			}

			/********Upgrade Member `user_upgrade_member` Start********/
			$query1 = "update pw_users set wallet_amount = wallet_amount - " . $package . " where id = '" . $this->session->userdata('user_id') . "'";
			$this->db->query($query1);
			/********Upgrade Member `user_upgrade_member` End********/

			/*********Update notification****Start*****/
			$insert_direct = array(
				'from_user' => $this->session->userdata('user_id'),
				'to_user' => $this->session->userdata('user_id'),
				'log' => 'debit',
				'message' => "Package Upgraded",
				'txn_id' => '#PU' . rand(),
				'amount' => $package,
				'balance' => getWalletAmountByUserID($this->session->userdata('user_id')),
				'status' => 1,
				'created_datetime' => date('Y-m-d H:i:s'),
				'modified_datetime' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pw_user_notification', $insert_direct);
			/*********Update notification****End*****/

			// if ($package == 1000 || $package == 5000 || $package == 10000) {
			// 	$cashback = $this->festival_cashback($package);
			// 	$query1 = "update pw_users set wallet_amount = wallet_amount + " . $cashback . " where id = '" . $this->session->userdata('user_id') . "'";
			// 	$this->db->query($query1);
			// 	/*********Update notification****Start*****/
			// 	$insert_direct = array(
			// 		'from_user' => $this->session->userdata('user_id'),
			// 		'to_user' => $this->session->userdata('user_id'),
			// 		'log' => 'credit',
			// 		'message' => "Festival Cashback",
			// 		'txn_id' => '#FC' . rand(),
			// 		'amount' => $cashback,
			// 		'balance' => getWalletAmountByUserID($this->session->userdata('user_id')),
			// 		'status' => 1,
			// 		'created_datetime' => date('Y-m-d H:i:s'),
			// 		'modified_datetime' => date('Y-m-d H:i:s'),
			// 	);
			// 	$this->common->insert_data('pw_user_notification', $insert_direct);
			// 	/*********Update notification****End*****/
			// }

			/********Non Working Start********/
			$this->apply_non_working($user_data->id, $package);
			/********Non Working End********/

			/**********SMS API Code**Start********/

			// if ($package == '30') {
			// 	$membership_name = 'OPAL';
			// } else if ($package == '40') {
			// 	$membership_name = 'JADE';
			// } else if ($package == '100') {
			// 	$membership_name = 'RED BERYL';
			// } else if ($package == '1000') {
			// 	$membership_name = 'BLUE NILE';
			// } else if ($package == '5000') {
			// 	$membership_name = 'ETERNITY';
			// } else if ($package == '10000') {
			// 	$membership_name = 'KOH-I-NOOR';
			// }

			// $message = "Your A/c(" . $user_data->username . ") is upgraded to " . $membership_name;
			// $this->send_sms_notification(urlencode($message), $user_data->mobile);
			/**********SMS API Code**End********/

			$this->session->set_flashdata('error_message', 'You have successfully upgraded member !');

			echo 1;
			die;
		}
	}

	private function getMyDirectMemberCount()
	{
		$query = "select count(*) as total_count from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
		$data =  $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();
			return $result->total_count;
		} else {
			return 0;
		}
	}

	private function minimum_direct_by_package($package)
	{
		if ($package == 40) {
			$minimum_direct = 3;
		} else if ($package == 100) {
			$minimum_direct = 5;
		} else if ($package == 1000) {
			$minimum_direct = 7;
		} else if ($package == 5000) {
			$minimum_direct = 9;
		} else if ($package == 10000) {
			$minimum_direct = 11;
		}

		return $minimum_direct;
	}

	/********Festival Cashback `Start`***********/
	private function festival_cashback($package)
	{
		switch ($package) {
			case 1000:
				$cashback = $package * 0.15; // 15%	
				break;
			case 5000:
				$cashback = $package * 0.25; // 25%	
				break;
			case 10000:
				$cashback = $package * 0.35; // 35%	
				break;
		}
		return $cashback;
	}
	/********Festival Cashback `End`***********/

	/********Uset team earninng`***********/
	private  function user_team_earning($member_id, $matrix_id)
	{
		// '1st' Level
		$query1 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_id . "'";
		$result = $this->db->query($query1);

		if ($result->num_rows() > 0) {
			$member_data = $result->row();
			$result = $this->genealogy->get_user_matrix($member_data->sponsor_user_id); // Income allow only for RajKumar sir tree - 1.11
			if ($result) {
				$level_income = $this->getLevelIncome(1);

				$update_data1 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
				$this->db->query($update_data1);

				/*********Update notification****Start*****/
				$insert_direct = array(
					'from_user' => $member_data->sponsor_user_id,
					'to_user' => $this->session->userdata('user_id'),
					'log' => 'credit',
					'message' => "1st Level Income",
					'txn_id' => '#LI' . rand(),
					'amount' => $level_income,
					'balance' => 'NA',
					'status' => 1,
					'created_datetime' => date('Y-m-d H:i:s'),
					'modified_datetime' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_user_notification', $insert_direct);
				/*********Update notification****End*****/

				// '2nd' Level
				if ($matrix_id == $member_data->sponsor_user_id) {
					goto next;
				}
				$query2 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
				$result = $this->db->query($query2);

				if ($result->num_rows() > 0) {
					$member_data = $result->row();

					$level_income = $this->getLevelIncome(2);

					$update_data2 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
					$this->db->query($update_data2);

					/*********Update notification****Start*****/
					$insert_direct = array(
						'from_user' => $member_data->sponsor_user_id,
						'to_user' => $this->session->userdata('user_id'),
						'log' => 'credit',
						'message' => "2nd Level Income",
						'txn_id' => '#LI' . rand(),
						'amount' => $level_income,
						'balance' => 'NA',
						'status' => 1,
						'created_datetime' => date('Y-m-d H:i:s'),
						'modified_datetime' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pw_user_notification', $insert_direct);
					/*********Update notification****End*****/

					// '3rd' Level
					if ($matrix_id == $member_data->sponsor_user_id) {
						goto next;
					}
					$query3 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
					$result = $this->db->query($query3);

					if ($result->num_rows() > 0) {
						$member_data = $result->row();

						$level_income = $this->getLevelIncome(3);

						$update_data3 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
						$this->db->query($update_data3);

						/*********Update notification****Start*****/
						$insert_direct = array(
							'from_user' => $member_data->sponsor_user_id,
							'to_user' => $this->session->userdata('user_id'),
							'log' => 'credit',
							'message' => "3rd Level Income",
							'txn_id' => '#LI' . rand(),
							'amount' => $level_income,
							'balance' => 'NA',
							'status' => 1,
							'created_datetime' => date('Y-m-d H:i:s'),
							'modified_datetime' => date('Y-m-d H:i:s'),
						);
						$this->common->insert_data('pw_user_notification', $insert_direct);
						/*********Update notification****End*****/

						// '4th' Level
						if ($matrix_id == $member_data->sponsor_user_id) {
							goto next;
						}
						$query4 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
						$result = $this->db->query($query4);

						if ($result->num_rows() > 0) {
							$member_data = $result->row();

							$level_income = $this->getLevelIncome(4);

							$update_data4 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
							$this->db->query($update_data4);

							/*********Update notification****Start*****/
							$insert_direct = array(
								'from_user' => $member_data->sponsor_user_id,
								'to_user' => $this->session->userdata('user_id'),
								'log' => 'credit',
								'amount' => $level_income,
								'balance' => 'NA',
								'message' => "4th Level Income",
								'txn_id' => '#LI' . rand(),
								'status' => 1,
								'created_datetime' => date('Y-m-d H:i:s'),
								'modified_datetime' => date('Y-m-d H:i:s'),
							);
							$this->common->insert_data('pw_user_notification', $insert_direct);
							/*********Update notification****End*****/

							// '5th' Level
							if ($matrix_id == $member_data->sponsor_user_id) {
								goto next;
							}
							$query5 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
							$result = $this->db->query($query5);

							if ($result->num_rows() > 0) {
								$member_data = $result->row();

								$level_income = $this->getLevelIncome(5);

								$update_data5 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
								$this->db->query($update_data5);

								/*********Update notification****Start*****/
								$insert_direct = array(
									'from_user' => $member_data->sponsor_user_id,
									'to_user' => $this->session->userdata('user_id'),
									'balance' => 'NA',
									'log' => 'credit',
									'amount' => $level_income,
									'message' => "5th Level Income",
									'txn_id' => '#LI' . rand(),
									'status' => 1,
									'created_datetime' => date('Y-m-d H:i:s'),
									'modified_datetime' => date('Y-m-d H:i:s'),
								);
								$this->common->insert_data('pw_user_notification', $insert_direct);
								/*********Update notification****End*****/

								// '6th' Level
								if ($matrix_id == $member_data->sponsor_user_id) {
									goto next;
								}
								$query6 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
								$result = $this->db->query($query6);

								if ($result->num_rows() > 0) {
									$member_data = $result->row();

									$level_income = $this->getLevelIncome(6);

									$update_data6 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
									$this->db->query($update_data6);

									/*********Update notification****Start*****/
									$insert_direct = array(
										'from_user' => $member_data->sponsor_user_id,
										'to_user' => $this->session->userdata('user_id'),
										'balance' => 'NA',
										'log' => 'credit',
										'amount' => $level_income,
										'message' => "6th Level Income",
										'txn_id' => '#LI' . rand(),
										'status' => 1,
										'created_datetime' => date('Y-m-d H:i:s'),
										'modified_datetime' => date('Y-m-d H:i:s'),
									);
									$this->common->insert_data('pw_user_notification', $insert_direct);
									/*********Update notification****End*****/

									// '7th' Level
									if ($matrix_id == $member_data->sponsor_user_id) {
										goto next;
									}
									$query7 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
									$result = $this->db->query($query7);

									if ($result->num_rows() > 0) {
										$member_data = $result->row();

										$level_income = $this->getLevelIncome(7);

										$update_data7 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
										$this->db->query($update_data7);

										/*********Update notification****Start*****/
										$insert_direct = array(
											'from_user' => $member_data->sponsor_user_id,
											'to_user' => $this->session->userdata('user_id'),
											'balance' => 'NA',
											'log' => 'credit',
											'amount' => $level_income,
											'message' => "7th Level Income",
											'txn_id' => '#LI' . rand(),
											'status' => 1,
											'created_datetime' => date('Y-m-d H:i:s'),
											'modified_datetime' => date('Y-m-d H:i:s'),
										);
										$this->common->insert_data('pw_user_notification', $insert_direct);
										/*********Update notification****End*****/

										// '8th' Level
										if ($matrix_id == $member_data->sponsor_user_id) {
											goto next;
										}
										$query8 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
										$result = $this->db->query($query8);

										if ($result->num_rows() > 0) {
											$member_data = $result->row();

											$level_income = $this->getLevelIncome(8);

											$update_data8 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
											$this->db->query($update_data8);

											/*********Update notification****Start*****/
											$insert_direct = array(
												'from_user' => $member_data->sponsor_user_id,
												'to_user' => $this->session->userdata('user_id'),
												'balance' => 'NA',
												'log' => 'credit',
												'amount' => $level_income,
												'message' => "8th Level Income",
												'txn_id' => '#LI' . rand(),
												'status' => 1,
												'created_datetime' => date('Y-m-d H:i:s'),
												'modified_datetime' => date('Y-m-d H:i:s'),
											);
											$this->common->insert_data('pw_user_notification', $insert_direct);
											/*********Update notification****End*****/

											// '9th' Level
											if ($matrix_id == $member_data->sponsor_user_id) {
												goto next;
											}
											$query9 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
											$result = $this->db->query($query9);

											if ($result->num_rows() > 0) {
												$member_data = $result->row();

												$level_income = $this->getLevelIncome(9);

												$update_data9 =  "UPDATE `pw_users` SET level_income_wallet = (level_income_wallet+" . $level_income . "), level_income_balance = (level_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
												$this->db->query($update_data9);

												/*********Update notification****Start*****/
												$insert_direct = array(
													'from_user' => $member_data->sponsor_user_id,
													'to_user' => $this->session->userdata('user_id'),
													'balance' => 'NA',
													'log' => 'credit',
													'amount' => $level_income,
													'message' => "9th Level Income",
													'txn_id' => '#LI' . rand(),
													'status' => 1,
													'created_datetime' => date('Y-m-d H:i:s'),
													'modified_datetime' => date('Y-m-d H:i:s'),
												);
												$this->common->insert_data('pw_user_notification', $insert_direct);
												/*********Update notification****End*****/

												// '10th' Level
												if ($matrix_id == $member_data->sponsor_user_id) {
													goto next;
												}
												$query10 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
												$result = $this->db->query($query10);

												if ($result->num_rows() > 0) {
													$member_data = $result->row();

													$level_income = $this->getLevelIncome(10);

													$update_data10 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
													$this->db->query($update_data10);

													/*********Update notification****Start*****/
													$insert_direct = array(
														'from_user' => $member_data->sponsor_user_id,
														'to_user' => $this->session->userdata('user_id'),
														'balance' => 'NA',
														'log' => 'credit',
														'amount' => $level_income,
														'message' => "Team Income",
														'txn_id' => '#LI' . rand(),
														'status' => 1,
														'created_datetime' => date('Y-m-d H:i:s'),
														'modified_datetime' => date('Y-m-d H:i:s'),
													);
													$this->common->insert_data('pw_user_notification', $insert_direct);
													/*********Update notification****End*****/

													// '11th' Level
													if ($matrix_id == $member_data->sponsor_user_id) {
														goto next;
													}
													$query11 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
													$result = $this->db->query($query11);

													if ($result->num_rows() > 0) {
														$member_data = $result->row();

														$level_income = $this->getLevelIncome(11);

														$update_data11 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
														$this->db->query($update_data11);

														/*********Update notification****Start*****/
														$insert_direct = array(
															'from_user' => $member_data->sponsor_user_id,
															'to_user' => $this->session->userdata('user_id'),
															'balance' => 'NA',
															'log' => 'credit',
															'amount' => $level_income,
															'message' => "Team Income",
															'txn_id' => '#LI' . rand(),
															'status' => 1,
															'created_datetime' => date('Y-m-d H:i:s'),
															'modified_datetime' => date('Y-m-d H:i:s'),
														);
														$this->common->insert_data('pw_user_notification', $insert_direct);
														/*********Update notification****End*****/

														// '12th' Level
														if ($matrix_id == $member_data->sponsor_user_id) {
															goto next;
														}
														$query12 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
														$result = $this->db->query($query12);

														if ($result->num_rows() > 0) {
															$member_data = $result->row();

															$level_income = $this->getLevelIncome(12);

															$update_data12 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
															$this->db->query($update_data12);

															/*********Update notification****Start*****/
															$insert_direct = array(
																'from_user' => $member_data->sponsor_user_id,
																'to_user' => $this->session->userdata('user_id'),
																'balance' => 'NA',
																'log' => 'credit',
																'amount' => $level_income,
																'message' => "Team Income",
																'txn_id' => '#LI' . rand(),
																'status' => 1,
																'created_datetime' => date('Y-m-d H:i:s'),
																'modified_datetime' => date('Y-m-d H:i:s'),
															);
															$this->common->insert_data('pw_user_notification', $insert_direct);
															/*********Update notification****End*****/

															// '13th' Level
															if ($matrix_id == $member_data->sponsor_user_id) {
																goto next;
															}
															$query13 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
															$result = $this->db->query($query13);

															if ($result->num_rows() > 0) {
																$member_data = $result->row();

																$level_income = $this->getLevelIncome(13);

																$update_data13 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																$this->db->query($update_data13);

																/*********Update notification****Start*****/
																$insert_direct = array(
																	'from_user' => $member_data->sponsor_user_id,
																	'to_user' => $this->session->userdata('user_id'),
																	'balance' => 'NA',
																	'log' => 'credit',
																	'amount' => $level_income,
																	'message' => "Team Income",
																	'txn_id' => '#LI' . rand(),
																	'status' => 1,
																	'created_datetime' => date('Y-m-d H:i:s'),
																	'modified_datetime' => date('Y-m-d H:i:s'),
																);
																$this->common->insert_data('pw_user_notification', $insert_direct);
																/*********Update notification****End*****/

																// '14th' Level
																if ($matrix_id == $member_data->sponsor_user_id) {
																	goto next;
																}
																$query14 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																$result = $this->db->query($query14);

																if ($result->num_rows() > 0) {
																	$member_data = $result->row();

																	$level_income = $this->getLevelIncome(14);

																	$update_data14 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																	$this->db->query($update_data14);

																	/*********Update notification****Start*****/
																	$insert_direct = array(
																		'from_user' => $member_data->sponsor_user_id,
																		'to_user' => $this->session->userdata('user_id'),
																		'balance' => 'NA',
																		'log' => 'credit',
																		'amount' => $level_income,
																		'message' => "Team Income",
																		'txn_id' => '#LI' . rand(),
																		'status' => 1,
																		'created_datetime' => date('Y-m-d H:i:s'),
																		'modified_datetime' => date('Y-m-d H:i:s'),
																	);
																	$this->common->insert_data('pw_user_notification', $insert_direct);
																	/*********Update notification****Start*****/

																	// '15th' Level
																	if ($matrix_id == $member_data->sponsor_user_id) {
																		goto next;
																	}
																	$query15 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																	$result = $this->db->query($query15);

																	if ($result->num_rows() > 0) {
																		$member_data = $result->row();

																		$level_income = $this->getLevelIncome(15);

																		$update_data15 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																		$this->db->query($update_data15);

																		/*********Update notification****Start*****/
																		$insert_direct = array(
																			'from_user' => $member_data->sponsor_user_id,
																			'to_user' => $this->session->userdata('user_id'),
																			'balance' => 'NA',
																			'log' => 'credit',
																			'amount' => $level_income,
																			'message' => "Team Income",
																			'txn_id' => '#LI' . rand(),
																			'status' => 1,
																			'created_datetime' => date('Y-m-d H:i:s'),
																			'modified_datetime' => date('Y-m-d H:i:s'),
																		);
																		$this->common->insert_data('pw_user_notification', $insert_direct);
																		/*********Update notification****End*****/

																		// '16th' Level
																		if ($matrix_id == $member_data->sponsor_user_id) {
																			goto next;
																		}
																		$query16 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																		$result = $this->db->query($query16);

																		if ($result->num_rows() > 0) {
																			$member_data = $result->row();

																			$level_income = $this->getLevelIncome(16);

																			$update_data16 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																			$this->db->query($update_data16);

																			/*********Update notification****Start*****/
																			$insert_direct = array(
																				'from_user' => $member_data->sponsor_user_id,
																				'to_user' => $this->session->userdata('user_id'),
																				'balance' => 'NA',
																				'log' => 'credit',
																				'amount' => $level_income,
																				'message' => "Team Income",
																				'txn_id' => '#LI' . rand(),
																				'status' => 1,
																				'created_datetime' => date('Y-m-d H:i:s'),
																				'modified_datetime' => date('Y-m-d H:i:s'),
																			);
																			$this->common->insert_data('pw_user_notification', $insert_direct);
																			/*********Update notification****End*****/

																			// '17th' Level
																			if ($matrix_id == $member_data->sponsor_user_id) {
																				goto next;
																			}
																			$query17 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																			$result = $this->db->query($query17);

																			if ($result->num_rows() > 0) {
																				$member_data = $result->row();

																				$level_income = $this->getLevelIncome(17);

																				$update_data17 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																				$this->db->query($update_data17);

																				/*********Update notification****Start*****/
																				$insert_direct = array(
																					'from_user' => $member_data->sponsor_user_id,
																					'to_user' => $this->session->userdata('user_id'),
																					'balance' => 'NA',
																					'log' => 'credit',
																					'amount' => $level_income,
																					'message' => "Team Income",
																					'txn_id' => '#LI' . rand(),
																					'status' => 1,
																					'created_datetime' => date('Y-m-d H:i:s'),
																					'modified_datetime' => date('Y-m-d H:i:s'),
																				);
																				$this->common->insert_data('pw_user_notification', $insert_direct);
																				/*********Update notification****end*****/

																				// '18th' Level
																				if ($matrix_id == $member_data->sponsor_user_id) {
																					goto next;
																				}
																				$query18 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																				$result = $this->db->query($query18);

																				if ($result->num_rows() > 0) {
																					$member_data = $result->row();

																					$level_income = $this->getLevelIncome(18);

																					$update_data18 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																					$this->db->query($update_data18);

																					/*********Update notification****Start*****/
																					$insert_direct = array(
																						'from_user' => $member_data->sponsor_user_id,
																						'to_user' => $this->session->userdata('user_id'),
																						'balance' => 'NA',
																						'log' => 'credit',
																						'amount' => $level_income,
																						'message' => "Team Income",
																						'txn_id' => '#LI' . rand(),
																						'status' => 1,
																						'created_datetime' => date('Y-m-d H:i:s'),
																						'modified_datetime' => date('Y-m-d H:i:s'),
																					);
																					$this->common->insert_data('pw_user_notification', $insert_direct);
																					/*********Update notification****End*****/

																					// '19th' Level
																					if ($matrix_id == $member_data->sponsor_user_id) {
																						goto next;
																					}
																					$query19 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																					$result = $this->db->query($query19);

																					if ($result->num_rows() > 0) {
																						$member_data = $result->row();

																						$level_income = $this->getLevelIncome(19);

																						$update_data19 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																						$this->db->query($update_data19);

																						/*********Update notification****Start*****/
																						$insert_direct = array(
																							'from_user' => $member_data->sponsor_user_id,
																							'to_user' => $this->session->userdata('user_id'),
																							'balance' => 'NA',
																							'log' => 'credit',
																							'amount' => $level_income,
																							'message' => "Team Income",
																							'txn_id' => '#LI' . rand(),
																							'status' => 1,
																							'created_datetime' => date('Y-m-d H:i:s'),
																							'modified_datetime' => date('Y-m-d H:i:s'),
																						);
																						$this->common->insert_data('pw_user_notification', $insert_direct);
																						/*********Update notification****End*****/

																						// '20th' Level
																						if ($matrix_id == $member_data->sponsor_user_id) {
																							goto next;
																						}
																						$query20 = "SELECT * FROM `pw_working_tree` WHERE member_user_id = '" . $member_data->sponsor_user_id . "'";
																						$result = $this->db->query($query20);

																						if ($result->num_rows() > 0) {
																							$member_data = $result->row();

																							$level_income = $this->getLevelIncome(20);

																							$update_data20 =  "UPDATE `pw_users` SET royalty_income_wallet = (royalty_income_wallet+" . $level_income . "), royalty_income_balance = (royalty_income_balance + " . $level_income . ") WHERE `id` = '" . $member_data->sponsor_user_id . "'";
																							$this->db->query($update_data20);

																							/*********Update notification****Start*****/
																							$insert_direct = array(
																								'from_user' => $member_data->sponsor_user_id,
																								'to_user' => $this->session->userdata('user_id'),
																								'balance' => 'NA',
																								'log' => 'credit',
																								'amount' => $level_income,
																								'message' => "Team Income",
																								'txn_id' => '#LI' . rand(),
																								'status' => 1,
																								'created_datetime' => date('Y-m-d H:i:s'),
																								'modified_datetime' => date('Y-m-d H:i:s'),
																							);
																							$this->common->insert_data('pw_user_notification', $insert_direct);
																							/*********Update notification****End*****/
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				next:
				return true;
			}
		}
	}
	/********Uset team earninng`***********/	
}
