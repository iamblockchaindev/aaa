<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Verification extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Genealogy_model', 'genealogy');
    }

    function account($token)
    {
        $data['page_title'] = 'Verification';

        $user_info = $this->genealogy->get_user($token);
        if($user_info) {
            $this->db->where('token', $token);            
            $this->db->update('pw_users', array('is_status' => 1));
            $this->session->set_flashdata('error_message2', 'Activated');
            redirect('login');
        } else {
            $this->session->set_flashdata('error_message2', 'Something Error');
            redirect('login');
        }
        
    }
}
