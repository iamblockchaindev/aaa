<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bluenile extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        $this->load->model('admin/Document_model', 'document');
    }

    function pn_doc(){
        $data['title'] = 'Pending Documents';
        $this->load->view('admin/bluenile_document/pn_document', $data);
    }
}
