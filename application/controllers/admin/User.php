<?php
defined('BASEPATH') or exit('No direct script access allowed');
ini_set('memory_limit', -1);
ini_set('max_execution_time', 300);
class User extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        /****checks it is admin or sub admin****/
        if ($this->session->admin_username == 'PW75482744') {
            $this->session->set_flashdata('error_message', "You don't have permission");
            redirect('admin/document/pending_documents');
        }
        /****checks it is admin or sub admin****/

        $this->load->model('admin/Users_model', 'users');
    }

    function total_users()
    {
        $data['title'] = 'Total Users';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->users->getTotalUserList($start_limit, $per_page);
        $total_records = $this->users->getTotalUserListCount();

        $config['base_url'] = base_url() . 'admin/user/total_users';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/users/total_users', $data);
    }

    function packages()
    {
        $data['title'] = 'Packages';
        $status = $this->input->post('status');
        $leavel = $this->input->post('leave_id');
        $package = $this->input->post('package');

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;

        if ($leavel) {
            $data['data_result'] =  $this->users->get_user_data($leavel);
            $data['leavel'] = $leavel;
        } else if ($leavel && $package) {
            $data['data_result'] = $this->users->get_user_data($leavel, $package);
            $data['package'] = $package;
        } else if ($leavel && $package && $status) {
            $data['data_result'] = $this->users->get_user_data($leavel, $package, $status);
            $data['leavel'] = $leavel;
        } else {
            $data['data_result'] = $this->users->get_user_data();
            $data['leavel'] = $leavel;
        }
        $total_records = count($data['data_result']);
        // $config['base_url'] = base_url() . 'admin/user/packages';
        // $config['total_rows'] = $total_records;
        // $config['per_page'] = $per_page;
        // //$config["uri_segment"] = 3;
        // $config['query_string_segment'] = 'offset';
        // $config['page_query_string'] = true;
        // // custom paging configuration
        // //$config['num_links'] = 4;
        // $config['use_page_numbers'] = TRUE;
        // $config['reuse_query_string'] = TRUE;


        // $config['full_tag_open'] = '<ul class="pagination">';
        // $config['full_tag_close'] = '</ul>';


        // $config['first_link'] = 'First';
        // $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        // $config['first_tag_close'] = '</li>';

        // $config['last_link'] = 'Last';
        // $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        // $config['last_tag_close'] = '</li>';

        // $config['next_link'] = 'Next';
        // $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        // $config['next_tag_close'] = '</li>';

        // $config['prev_link'] = 'Prev';
        // $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        // $config['prev_tag_close'] = '</li>';

        // $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        // $config['cur_tag_close'] = '</a></li>';

        // $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        // $config['num_tag_close'] = '</li>';

        // $this->pagination->initialize($config);

        // $data["links"] = $this->pagination->create_links();



        $data["total_records"] = $total_records;

        $this->load->view('admin/users/packages', $data);
    }

    function view_user()
    {
        $data['title'] = 'User Details';

        $id = $this->input->get('id');

        if (!$id) {
            redirect(base_url());
            die;
        }

        $data['user_data'] = $this->common->getSingle('pw_users', array('id' => $id));

        if (empty($data['user_data'])) {
            redirect(base_url());
            die;
        }

        $this->load->view('admin/users/view_user', $data);
    }

    function edit_user_profile()
    {
        $data['title'] = 'Edit User Profile';

        $token = $this->input->get('token');
        $data['user_data'] = $this->common->getSingle('pw_users', array('token' => $token));

        if (empty($data['user_data'])) {
            redirect(base_url() . 'admin');
        }

        $this->form_validation->set_rules('full_name', 'Name', 'required');        
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/users/edit_user_profile', $data);
        } else {
            $update_data = array(
                'full_name' => $this->input->post('full_name', true),
                'mobile' => $this->input->post('mobile', true),
                'address' => $this->input->post('address', true),
                'modified_time' => date('Y-m-d H:i:s'),
                'email' => $this->input->post('email', true)
            );

            $this->common->update_data('pw_users', array('token' => $token), $update_data);

            $this->session->set_flashdata('error_message', 'Profile updated successfully !');

            redirect(base_url() . 'admin/user/edit_user_profile?token=' . $token);
        }
    }


    function paid_users()
    {
        $data['title'] = 'Paid Users';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->users->getTotalPaidUserList($start_limit, $per_page);
        $total_records = $this->users->getTotalPaidUserListCount();

        $config['base_url'] = base_url() . 'admin/user/paid_users';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/users/paid_users', $data);
    }

    function unpaid_users()
    {
        $data['title'] = 'Unpaid Users';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->users->getTotalUnPaidUserList($start_limit, $per_page);
        $total_records = $this->users->getTotalUnPaidUserListCount();

        $config['base_url'] = base_url() . 'admin/user/unpaid_users';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/users/unpaid_users', $data);
    }

    function active_users_today()
    {
        $data['title'] = "Today's Active Users";

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->users->getTodaysActiveUserList($start_limit, $per_page);
        $total_records = $this->users->getTodaysActiveUserListCount();

        $config['base_url'] = base_url() . 'admin/user/active_users_today';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/users/active_users_today', $data);
    }

    function company_matrix()
    {
        $data['title'] = 'Matrix';
        $this->load->view('admin/users/company_matrix', $data);
    }

    function get_level_matrix()
    {
        $data = $this->users->get_level_matrix();
        $result = array();
        foreach ($data as $row) {
            $result[] = array(
                'id' => $row['id'],
                'levelid' => $row['levelid'],
                'username' => $row['username'],
                'full_name' => $row['full_name'],
                'created_time' => date('d-m-Y', strtotime($row['created_time'])),
                'upgrade_time' =>  date('d-m-Y', strtotime($row['upgrade_time'])),
                'level' => $row['level']
            );
        }
        echo json_encode($result);
    }

    function set_pool_income()
    {
        // ini_set('max_input_vars', 10000001);
        $packagename = $this->input->post('package');
        $users = $this->users->get_level_matrix_($this->input->post('level'), $this->input->post('start'), $this->input->post('end'), $packagename);
        $poollevel = $this->input->post('poollevel');
        $pool = $this->getSingleLevelIncomeByPackage_non($packagename, $this->input->post('poollevel'));
        $poolincome = $pool['poolincome'];
        $package = array();
        $history = array();
        foreach ($users as $user) {
            ini_set("pcre.backtrack_limit", "1048576");
            $pattern = "/$poollevel/i";
            $str = isset($user['level']) ? $user['level'] : "0";
            // if (  strpos($user['level'], $poollevel) !== false) {
            if (preg_match($pattern, $str) == 0) {

                if ($user['level']) {
                    $update[] = array(
                        'id' => $user['levelid'],
                        // 'level' => isset($user['level']) ? $user['level'] . ',' . $poollevel : $poollevel
                        'level' => isset($user['level']) ? ($user['level'] == 'NA' ? $poollevel : $user['level'] . ',' . $poollevel) : $poollevel
                    );
                } else {
                    $insert[] = array(
                        'user_id' => $user['id'],
                        'package' => 30,
                        'level' => 1
                    );
                }

                array_push($package, $user['username']);

                $history[] = array(
                    'from_user' => $user['id'],
                    'to_user' => $user['id'],
                    'balance' => 'NA',
                    'log' => 'credit',
                    'amount' => $poolincome,
                    'message' => "OPAL level - " . $poollevel,
                    'txn_id' => '#LI' . rand(),
                    'status' => 1,
                    'created_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s'),
                );
            }
        }

        if ($insert) {
            $this->db->insert_batch('pw_non_working_income', $insert);
        }
        if ($update) {
            $this->db->update_batch('pw_non_working_income', $update, 'id');
        }

        if ($package) {
            $ids = implode(',', $package);
            $query = "UPDATE pw_non_working_user SET withdrawable_amount = withdrawable_amount + $poolincome,  balance = balance + $poolincome, created_datetime = NOW() WHERE package = 30 AND FIND_IN_SET(user_name, '$ids')";
            $data = $this->db->query($query);
            $rows = $this->db->affected_rows();
            $this->db->insert_batch('pw_user_notification', $history);
            $insert_data = array(
                'package' => 30,
                'level' => $poollevel,
                'company_level' => $this->input->post('level'),
                'from_date' => $this->input->post('start'),
                'to_date' => $this->input->post('end'),
                'amount' => $poolincome,
                'total_users' => $rows,
                'created_time' => date('Y-m-d H:i:s')
            );
            $this->common->insert_data('pw_non_working_distribution', $insert_data);
        }

        $datas['success'] = true;
        $datas['message'] = $update ? "Distributed Successfully" : 'No user found';
        echo json_encode($datas);
    }

    function generate_jade_income()
    {
        // ini_set('max_input_vars', 10000001);
        $packagename = $this->input->post('package');
        $users = $this->users->generate_jade_income($this->input->post('level'), $this->input->post('start'), $this->input->post('end'), $packagename);
        $poollevel = $this->input->post('poollevel');
        $pool = $this->getSingleLevelIncomeByPackage_non($packagename, $this->input->post('poollevel'));
        $poolincome = $pool['poolincome'];
        $poolname = $pool['poolname'];
        $package = array();
        foreach ($users as $user) {
            ini_set("pcre.backtrack_limit", "1048576");
            $pattern = "/$poollevel/i";
            $str = isset($user['level']) ? $user['level'] : "0";
            // if (  strpos($user['level'], $poollevel) !== false) {
            if (preg_match($pattern, $str) == 0) {

                if ($user['level']) {
                    $update[] = array(
                        'id' => $user['levelid'],
                        'level' => isset($user['level']) ? ($user['level'] == 'NA' ? $poollevel : $user['level'] . ',' . $poollevel) : $poollevel
                    );
                } else {
                    $insert[] = array(
                        'user_id' => $user['id'],
                        'package' => $packagename,
                        'level' => 1
                    );
                }

                array_push($package, $user['username']);

                $history[] = array(
                    'from_user' => $user['id'],
                    'to_user' => $user['id'],
                    'balance' => 'NA',
                    'log' => 'credit',
                    'amount' => $poolincome,
                    'message' => $poolname . " level - " . $poollevel,
                    'txn_id' => '#LI' . rand(),
                    'status' => 1,
                    'created_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s'),
                );
            }
        }

        if ($insert) {
            $this->db->insert_batch('pw_non_working_income', $insert);
        }
        if ($update) {
            $this->db->update_batch('pw_non_working_income', $update, 'id');
        }

        if ($package) {
            $ids = implode(',', $package);
            $query = "UPDATE pw_non_working_user SET withdrawable_amount = withdrawable_amount + $poolincome,  balance = balance + $poolincome, upgrade_datetime = NOW() WHERE package = $packagename AND FIND_IN_SET(user_name, '$ids')";
            $data = $this->db->query($query);
            $rows = $this->db->affected_rows();
            $this->db->insert_batch('pw_user_notification', $history);
            $insert_data = array(
                'package' => $packagename,
                'company_level' => $this->input->post('level'),
                'level' => $poollevel,
                'from_date' => $this->input->post('start'),
                'to_date' => $this->input->post('end'),
                'amount' => $poolincome,
                'total_users' => $rows,
                'created_time' => date('Y-m-d H:i:s')
            );
            $this->common->insert_data('pw_non_working_distribution', $insert_data);
        }

        $datas['success'] = true;
        $datas['message'] = $update ? "Distributed Successfully" : 'No user found';
        echo json_encode($datas);
    }

    function get_total_users($level, $package)
    {
        $data['count'] = $this->users->get_total_users($level, $package);
        echo json_encode($data);
    }

    function getSingleLevelIncomeByPackage_non($package, $poollevel)
    {
        if ($package == '30') {
            $poolname = "OPAL";
            switch ($poollevel) {
                case 1:
                    $poolincome = 0.9;
                    break;
                case 2:
                    $poolincome = 4.5;
                    break;
                case 3:
                    $poolincome = 13.5;
                    break;
                case 4:
                    $poolincome = 40.5;
                    break;
                case 5:
                    $poolincome = 121.5;
                    break;
                case 6:
                    $poolincome = 364.5;
                    break;
                case 7:
                    $poolincome = 1093.5;
                    break;
                case 8:
                    $poolincome = 3280.5;
                    break;
                case 9:
                    $poolincome = 23619.6;
                    break;
            }
        } else if ($package == '40') {
            $poolname = "JADE";
            switch ($poollevel) {
                case 1:
                    $poolincome = 7.5;
                    break;
                case 2:
                    $poolincome = 22.5;
                    break;
                case 3:
                    $poolincome = 40.5;
                    break;
                case 4:
                    $poolincome = 121.5;
                    break;
                case 5:
                    $poolincome = 243;
                    break;
                case 6:
                    $poolincome = 729;
                    break;
                case 7:
                    $poolincome = 5467.5;
                    break;
                case 8:
                    $poolincome = 16402.5;
                    break;
                case 9:
                    $poolincome = 98415;
                    break;
            }
        } else if ($package == '100') {
            $poolname = "RED BERYL";
            switch ($poollevel) {
                case 1:
                    $poolincome = 12;
                    break;
                case 2:
                    $poolincome = 36;
                    break;
                case 3:
                    $poolincome = 108;
                    break;
                case 4:
                    $poolincome = 324;
                    break;
                case 5:
                    $poolincome = 972;
                    break;
                case 6:
                    $poolincome = 2916;
                    break;
                case 7:
                    $poolincome = 10935;
                    break;
                case 8:
                    $poolincome = 39366;
                    break;
                case 9:
                    $poolincome = 295245;
                    break;
            }
        } else if ($package == '1000') {
            $poolname = "BLUE NILE";
            switch ($poollevel) {
                case 1:
                    $poolincome = 180;
                    break;
                case 2:
                    $poolincome = 540;
                    break;
                case 3:
                    $poolincome = 1620;
                    break;
                case 4:
                    $poolincome = 2835;
                    break;
                case 5:
                    $poolincome = 8505;
                    break;
                case 6:
                    $poolincome = 25515;
                    break;
                case 7:
                    $poolincome = 87480;
                    break;
                case 8:
                    $poolincome = 328050;
                    break;
                case 9:
                    $poolincome = 2460375;
                    break;
            }
        } else if ($package == '5000') {
            $poolname = "ETERNITY";
            switch ($poollevel) {
                case 1:
                    $poolincome = 900;
                    break;
                case 2:
                    $poolincome = 2700;
                    break;
                case 3:
                    $poolincome = 5400;
                    break;
                case 4:
                    $poolincome = 14175;
                    break;
                case 5:
                    $poolincome = 45525;
                    break;
                case 6:
                    $poolincome = 127575;
                    break;
                case 7:
                    $poolincome = 382725;
                    break;
                case 8:
                    $poolincome = 1312200;
                    break;
                case 9:
                    $poolincome = 15746400;
                    break;
            }
        } else if ($package == '10000') {
            $poolname = "KOH-I-NOOR";
            switch ($poollevel) {
                case 1:
                    $poolincome = 2100;
                    break;
                case 2:
                    $poolincome = 6300;
                    break;
                case 3:
                    $poolincome = 13500;
                    break;
                case 4:
                    $poolincome = 28350;
                    break;
                case 5:
                    $poolincome = 85050;
                    break;
                case 6:
                    $poolincome = 255150;
                    break;
                case 7:
                    $poolincome = 765450;
                    break;
                case 8:
                    $poolincome = 1968300;
                    break;
                case 9:
                    $poolincome = 27556200;
                    break;
            }
        }
        $data['poolincome'] = $poolincome;
        $data['poolname'] = $poolname;
        return $data;
    }

    function user_matrix()
    {
        $data['title'] = 'User Matrix';
        $this->load->view('admin/users/user_matrix', $data);
    }

    function get_user_matrix()
    {
        $data = $this->users->get_user_matrix();
        // $squ= $this->db->last_query();
        // echo $squ;
        // exit(0);
        $result = array();
        foreach ($data as $row) {
            $result[] = array(
                'id' => $row['id'],
                'levelid' => $row['levelid'],
                'username' => $row['username'],
                'full_name' => $row['full_name'],
                'created_time' => date('d-m-Y', strtotime($row['created_time'])),
                'upgrade_time' =>  date('d-m-Y', strtotime($row['upgrade_time'])),
                'level' => $row['level']
            );
        }
        echo json_encode($result);
    }

    function generate_user_income()
    {
        // ini_set('max_input_vars', 10000001);
        $packagename = $this->input->post('package');
        $users = $this->users->get_user_matrix();
        $poollevel = $this->input->post('poollevel');
        $pool = $this->getSingleLevelIncomeByPackage_non($packagename, $this->input->post('poollevel'));
        $poolincome = $pool['poolincome'];
        $poolname = $pool['poolname'];
        $package = array();
        foreach ($users as $user) {
            ini_set("pcre.backtrack_limit", "1048576");
            $pattern = "/$poollevel/i";
            $str = $user['level'];
            // if (  strpos($user['level'], $poollevel) !== false) {
            if (preg_match($pattern, $str) == 0) {

                if ($user['level']) {
                    $update[] = array(
                        'id' => $user['levelid'],
                        // 'level' => isset($user['level']) ? $user['level'] . ',' . $poollevel : $poollevel
                        'level' => isset($user['level']) ? ($user['level'] == 'NA' ? $poollevel : $user['level'] . ',' . $poollevel) : $poollevel
                    );
                } else {
                    $insert[] = array(
                        'user_id' => $user['id'],
                        'package' => $packagename,
                        'level' => 1
                    );
                }

                array_push($package, $user['username']);

                $history[] = array(
                    'from_user' => $user['id'],
                    'to_user' => $user['id'],
                    'balance' => 'NA',
                    'log' => 'credit',
                    'amount' => $poolincome,
                    'message' => "$poolname level - " . $poollevel,
                    'txn_id' => '#LI' . rand(),
                    'status' => 1,
                    'created_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s'),
                );
            }
        }

        if ($insert) {
            $this->db->insert_batch('pw_non_working_income', $insert);
        }
        if ($update) {
            $this->db->update_batch('pw_non_working_income', $update, 'id');
        }

        if ($package) {
            $ids = implode(',', $package);
            $query = "UPDATE pw_non_working_user SET withdrawable_amount = withdrawable_amount + $poolincome,  balance = balance + $poolincome, upgrade_datetime = NOW() WHERE package = $packagename AND FIND_IN_SET(user_name, '$ids')";
            $data = $this->db->query($query);
            $rows = $this->db->affected_rows();
            $this->db->insert_batch('pw_user_notification', $history);
            $insert_data = array(
                'package' => $packagename,
                'level' => $poollevel,
                'company_level' => $this->input->post('level'),
                'from_date' => $this->input->post('start'),
                'to_date' => $this->input->post('end'),
                'amount' => $poolincome,
                'total_users' => $rows,
                'created_time' => date('Y-m-d H:i:s')
            );
            $this->common->insert_data('pw_non_working_distribution', $insert_data);
        }

        $datas['success'] = true;
        $datas['message'] = $update ? "Distributed Successfully" : 'No user found';
        echo json_encode($datas);
    }

    function income_distribution()
    {
        $data['title'] = 'Income Dirtribution';
        $this->load->view('admin/users/income_distribution', $data);
    }

    function get_user_data()
    {
        $data = $this->users->get_user_pool_history();
        $result = array();
        foreach ($data as $row) {
            $result[] = array(
                'id' => $row['id'],
                'levelid' => $row['levelid'],
                'username' => $row['username'],
                'full_name' => $row['full_name'],
                'created_time' => date('d-m-Y', strtotime($row['created_time'])),
                'upgrade_time' =>  date('d-m-Y', strtotime($row['upgrade_time'])),
                'level' => $row['level'],
                'package' => $row['package']
            );
        }
        echo json_encode($result);
    }

    function generate_pool_income()
    {
        // ini_set('max_input_vars', 10000001);
        $packagename = $this->input->post('package');
        $users = $this->users->get_user_pool_history();
        $poollevel = $this->input->post('poollevel');
        $pool = $this->getSingleLevelIncomeByPackage_non($packagename, $this->input->post('poollevel'));
        $poolincome = $pool['poolincome'];
        $poolname = $pool['poolname'];
        $package = array();
        foreach ($users as $user) {
            $pattern = "/$poollevel/i";
            $str = $user['level'];
            // if (  strpos($user['level'], $poollevel) !== false) {
            if (preg_match($pattern, $str) == 0) {

                if ($user['level']) {
                    $update[] = array(
                        'id' => $user['levelid'],
                        // 'level' => isset($user['level']) ? $user['level'] . ',' . $poollevel : $poollevel
                        'level' => isset($user['level']) ? ($user['level'] == 'NA' ? $poollevel : $user['level'] . ',' . $poollevel) : $poollevel
                    );
                } else {
                    $insert[] = array(
                        'user_id' => $user['id'],
                        'package' => $packagename,
                        'level' => 1
                    );
                }

                array_push($package, $user['username']);

                $history[] = array(
                    'from_user' => $user['id'],
                    'to_user' => $user['id'],
                    'balance' => 'NA',
                    'log' => 'credit',
                    'amount' => $poolincome,
                    'message' => "$poolname level - " . $poollevel,
                    'txn_id' => '#LI' . rand(),
                    'status' => 1,
                    'created_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s'),
                );
            }
        }

        if ($insert) {
            $this->db->insert_batch('pw_non_working_income', $insert);
        }
        if ($update) {
            $this->db->update_batch('pw_non_working_income', $update, 'id');
        }

        if ($package) {
            $ids = implode(',', $package);
            $query = "UPDATE pw_non_working_user SET withdrawable_amount = withdrawable_amount + $poolincome,  balance = balance + $poolincome, upgrade_datetime = NOW() WHERE package = $packagename AND FIND_IN_SET(user_name, '$ids')";
            $data = $this->db->query($query);
            $this->db->insert_batch('pw_user_notification', $history);
            $insert_data = array(
                'username' => $this->input->post('user'),
                'package' => $packagename,
                'level' => $poollevel,
                'amount' => $poolincome,
                'created_date' => date('Y-m-d')
            );
            $this->common->insert_data('pw_user_pool_history', $insert_data);
        }

        $datas['success'] = true;
        $datas['message'] = $update ? "Distributed Successfully" : 'No user found';
        echo json_encode($datas);
    }

    function set_user_matrix()
    {
        $users = $this->users->get_matrix_user('8759', $this->input->post('pool_level'));
        redirect(base_url('admin/user/user_matrix'));
    }
}
