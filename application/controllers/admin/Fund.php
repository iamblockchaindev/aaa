<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fund extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        /****checks it is admin or sub admin****/
        if ($this->session->type == 2) {
            $this->session->set_flashdata('error_message', "You don't have permission");
            redirect('admin/user/total_users');
        }
        /****checks it is admin or sub admin****/

        $this->load->model('admin/Fund_model', 'fund');
    }

    function transfer_fund()
    {
        $data['title'] = 'Transfer Fund';

        $this->form_validation->set_rules('member_id', 'Member ID', 'required');
        $this->form_validation->set_rules('amount', 'Amount', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/fund/transfer_fund', $data);
        } else {
            $member_id = $this->input->post('member_id', true);
            $amount = $this->input->post('amount', true);

            $member_data = $this->common->getSingle('pw_users', array('username' => $member_id, 'is_delete' => 1));

            if (empty($member_data)) {
                $this->session->set_flashdata('error_message', "Member ID doesn't exist !");
                redirect(base_url() . 'admin/fund/transfer_fund');
            }

            $query2 = "update pw_users set wallet_amount = wallet_amount + " . $amount . " where username = '" . $member_id . "'";
            $this->db->query($query2);

            /********Insert Data in `transfer_history`**Start*******/
            $insert_data = array(
                'from_user' => 0,
                'to_user' => $member_data->id,
                'transfer_charge' => '0',
                'amount' => $amount,
                'transfer_by' => 'admin',
                'created_datetime' => date('Y-m-d H:i:s'),
            );

            $this->common->insert_data('pw_transfer_history', $insert_data);

            /***pw_user_notification***/
            $insert_data_n = array(
                'from_user' => $member_data->id,
                'to_user' => $member_data->id,
                'log' => 'credit',
                'amount' => $amount,
                'balance' => getWalletAmountByUserID($member_data->id),
                'txn_id' => '#FT' . rand(),
                'message' => "given fund by admin",
                'status' => 1,
                'created_datetime' => date('Y-m-d H:i:s'),
                'modified_datetime' => date('Y-m-d H:i:s'),
            );
            $this->common->insert_data('pw_user_notification', $insert_data_n);
            /***pw_user_notification***/

            /********Insert Data in `transfer_history`**End*******/

            $this->session->set_flashdata('error_message', 'Amount transferred successfully !');

            redirect(base_url() . 'admin/fund/transfer_fund');
        }
    }

    function isMemberAvailabe()
    {
        if (!isset($_POST['member_id'])) {
            redirect(base_url());
        }

        $member_id = $this->input->post('member_id');

        $member_data = $this->common->getSingle('pw_users', array('username' => $member_id, 'is_delete' => 1));

        $member_name = '';

        if (!empty($member_data)) {
            $member_name = ucfirst($member_data->full_name);
            $status = 1;
        } else {
            $status = 2;
        }

        $json_arr = array('status' => $status, 'member_name' => $member_name);

        echo json_encode($json_arr);
        exit;
    }


    function transfer_fund_history()
    {
        $data['title'] = 'Transfer Fund History';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;

        if ($this->input->get('search_input')) {
            $search_text = $this->input->get('search_input');
        } else {
            $search_text = '';
        }

        $data['data_result'] = $this->fund->getMyTransferAmount($start_limit, $per_page, $search_text);
        $total_records = $this->fund->getMyTransferAmountcount($search_text);

        $config['base_url'] = base_url() . 'admin/fund/transfer_fund_history';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        $config['num_links'] = 15;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data['total_records'] = $total_records;

        $this->load->view('admin/fund/transfer_fund_history', $data);
    }
}
