<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Document extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        $this->load->model('admin/Document_model', 'document');
    }

    function pending_documents()
    {
        $data['title'] = 'Pending Documents';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->document->getPendingDocumentList($start_limit, $per_page);
        $total_records = $this->document->getPendingDocumentListCount();

        $config['base_url'] = base_url() . 'admin/document/pending_documents';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/document/pending_documents', $data);
    }    

    function approve_document()
    {
        if ($_POST) {
            $id = $this->input->post('id');
            $type = $this->input->post('type');
            $update_data = array(
                'status' => '1',
                'approved_datetime' => date('Y-m-d H:i:s')
            );
            $where = array('id' => $id);
            $this->common->update_data('pw_users_document', $where, $update_data);
            if ($type == 'id') {
                $this->common->update_data('pw_users', array('id' => $this->input->post('user_id')), array('is_profile_update' => 1));
            }
            echo 1;
        } else {
            redirect(base_url());
        }
    }

    function reject_document()
    {
        if ($_POST) {
            $id = $this->input->post('reject_id', true);
            $type = $this->input->post('type', true);
            $reject_reason = $this->input->post('reject_reason', true);

            $update_data = array(
                'status' => '2',
                'reject_reason' => $reject_reason,
                'reject_datetime' => date('Y-m-d H:i:s')
            );
            $where = array('id' => $id);

            $this->common->update_data('pw_users_document', $where, $update_data);
            if ($type == 'id') {
                $this->common->update_data('pw_users', array('id' => $this->input->post('user_id')), array('is_profile_update' => 0));
            }
            echo 1;
        } else {
            redirect(base_url());
        }
    }

    function approved_documents()
    {
        $data['title'] = 'Approved Documents';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->document->getApprovedDocumentList($start_limit, $per_page);
        $total_records = $this->document->getApprovedDocumentListCount();

        $config['base_url'] = base_url() . 'admin/document/approved_documents';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/document/approved_documents', $data);
    }

    function rejected_documents()
    {
        $data['title'] = 'Approved Documents';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->document->getRejectedDocumentList($start_limit, $per_page);
        $total_records = $this->document->getRejectedDocumentListCount();

        $config['base_url'] = base_url() . 'admin/document/rejected_documents';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/document/rejected_documents', $data);
    }

    function pn_doc(){
        $data['title'] = "Pending Document";

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->document->getPendingDocumentList1($start_limit, $per_page);
        $total_records = $this->document->getPendingDocumentListCount1();

        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;



        $this->load->view('admin/document/pn_document', $data);
    }

    function approve_document1()
    {
        if ($_POST) {
            $id = $this->input->post('id');
            $type = $this->input->post('type');
            $update_data = array(
                'verfied_ornot' => '1',
                'approved_datetime' => date('Y-m-d H:i:s')
            );
            $where = array('id' => $id);
            $this->common->update_data('pw_blueniletrip', $where, $update_data);            
            echo 1;
        } else {
            redirect(base_url());
        }
    }

    function reject_document1()
    {
        if ($_POST) {
            $id = $this->input->post('reject_id', true);
            $type = $this->input->post('type', true);
            $reject_reason = $this->input->post('reject_reason', true);

            $update_data = array(
                'verfied_ornot' => '2',
                'reject_reason' => $reject_reason,
                'reject_datetime' => date('Y-m-d H:i:s')
            );
            $where = array('id' => $id);

            $this->common->update_data('pw_blueniletrip', $where, $update_data);            
        } else {
            redirect(base_url());
        }
    }

    function ap_doc(){
        $data['title'] = "Approved Document";

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->document->getApprovedDocumentList1($start_limit, $per_page);
        $total_records = $this->document->getApprovedDocumentListCount1();

        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/document/ap_document', $data);
    }

    function rj_doc(){
        $data['title'] = "Rejected Document";

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;

        $data['data_result'] = $this->document->getRejectedDocumentList1($start_limit, $per_page);
        $total_records = $this->document->getRejectedDocumentListCount1();

        $config['base_url'] = base_url() . 'admin/document/rejected_documents';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;


        $this->load->view('admin/document/rj_document', $data);
    }
}
