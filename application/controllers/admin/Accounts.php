<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends MY_Controller {

    function __construct() 
    {
		parent::__construct();

		/****checks it is login or not start****/
		$this->_adminLoginCheck();
		/****checks it is login or not End****/
    }

	public function index()
	{
		$data['title'] = 'My Profile';

		$data['user_data'] = $this->common->getSingle('admin_login', array('id'=>$this->session->userdata('admin_id')));


		$this->load->view('admin/profile/my_profile', $data);


	}

	public function edit_profile()
	{

		$data['title'] = 'My Profile';

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('username', 'User Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$data['admin_data'] = $this->common->getSingle('admin_login', array('id'=>$this->session->userdata('admin_id')));

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/profile/edit_profile', $data);
		}
		else
		{
			$name = $this->input->post('name', true);
			$username = $this->input->post('username', true);
			$email = $this->input->post('email', true);

			$update_data = array(
				'name'=>$name,
				'username'=>$username,
				'email'=>$email,
				'modified_datetime'=>date('Y-m-d H:i:s'),
				);

			$this->common->update_data('admin_login', array('id'=>$this->session->userdata('admin_id')), $update_data);

			$this->session->set_flashdata('error_message', 'Profile updated successfully !');

			redirect(base_url().'admin/accounts');
		}

	}

    function change_password()
    {
        $data['title'] = 'Change Password';

        $this->form_validation->set_rules('old_password', 'Old password', 'required');
        $this->form_validation->set_rules('new_password', 'New password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm password', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $user_data = $this->common->getSingle('admin_login', array('id'=>$this->session->userdata('admin_id')));

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('admin/profile/change_password', $data);
        }
        else
        {
            if($user_data->password != MD5($this->input->post('old_password', true)))
            {
                $this->session->set_flashdata('error_message', 'Please enter correct old password !');
            }
            else
            {
                $update_data = array(
                    'password'=>MD5($this->input->post('new_password', true)),
                    'password_d'=>$this->input->post('new_password', true),
                    );

                $this->common->update_data('admin_login', array('id'=>$this->session->userdata('admin_id')), $update_data);

                $this->session->set_flashdata('success_message', 'Password changed successfully !');
            }

            redirect(base_url().'admin/accounts/change_password');
        }
    }



}
