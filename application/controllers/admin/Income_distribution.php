<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Income_distribution extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        /****checks it is admin or sub admin****/
        if ($this->session->type == 2) {
            $this->session->set_flashdata('error_message', "You don't have permission");
            redirect('admin/document/pending_documents');
        }
        /****checks it is admin or sub admin****/

        $this->load->model('admin/Income_distribution_model', 'income');
    }

    function distribution_history()
    {
        $data['title'] = 'Distribution History';
        $this->load->view('admin/income_distribution/distribution_history', $data);
    }

    function get_distribution_history()
    {
        echo json_encode($this->income->getAllDistributionList());
    }

    // function non_working()
    // {
    //     $data['title'] = 'Generate Non working Level Income';

    //     $this->form_validation->set_rules('package', 'package', 'required');
    //     $this->form_validation->set_rules('level', 'level', 'required');
    //     $this->form_validation->set_rules('from_date', 'From date', 'required');
    //     $this->form_validation->set_rules('to_date', 'To date', 'required');

    //     $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

    //     if($this->form_validation->run() == FALSE)
    //     {
    //         $this->load->view('admin/income_distribution/non_working', $data);
    //     }
    //     else
    //     {
    //         $package = $this->input->post('package', true);
    //         $level = $this->input->post('level', true);
    //         $from_date = date('Y-m-d', strtotime($this->input->post('from_date', true)));
    //         $to_date = date('Y-m-d', strtotime($this->input->post('to_date', true)));

    //         $user_list = $this->income->getAllUserListByPackageByDate($package, $level, $from_date, $to_date);

    //         $total_users = 0;

    //         if(empty($user_list))
    //         {
    //             $this->session->set_flashdata('error_message','There is no users !');

    //             redirect(base_url().'admin/income_distribution/non_working');
    //             die;
    //         }  

    //         /******Generae Income******/

    //         $total_users = count($user_list);

    //         // echo '<pre>';

    //         // print_r($user_list); die;

    //         foreach ($user_list as $row) 
    //         {
    // 			$amount = $row->level_complete_amount;

    // 			if(empty($row->levels))
    // 			{
    // 			    $levels = $level;
    // 			}
    // 			else
    // 			{
    // 			    $levels = $row->levels.','.$level;   
    // 			}

    // 			/*** Update**`ula_non_working_user`****Start****/
    // 			$update_query = "update ula_non_working_user set withdrawable_amount = withdrawable_amount + ".$amount.", balance = balance + ".$amount.", levels = '".$levels."' where id = '".$row->id1."'";
    // 			$this->db->query($update_query);
    // 			/*** Update**`ula_non_working_user`****End****/

    // 			/*** Update**`ula_non_working_level`****Start****/
    // 			$update_query = "update ula_non_working_level set level_complete_amount = 0 where id = '".$row->id2."'";
    // 			$this->db->query($update_query);
    // 			/*** Update**`ula_non_working_level`****End****/

    // 			/******ula_user_notification******/

    // 			$insert_ = array(
    // 			'from_user' => $row->user_id,
    // 			'to_user' =>$row->user_id,
    // 			'log' => 'credit',
    // 			'amount' => $amount,
    // 			'balance' => 'NA',
    // 			'txn_id' => '#LI' . rand(),
    // 			'message' => ordinal($level).' Level Income',
    // 			'status' => 1,
    // 			'created_datetime' => date('Y-m-d H:i:s'),
    // 			'modified_datetime' => date('Y-m-d H:i:s'),
    // 			);
    // 			$this->common->insert_data('ula_user_notification', $insert_);
    // 			/******ula_user_notification******/
    //         }                

    //         /******Generae Income******/

    //         /******ula_non_working_distribution******/
    //         $insert_data = array(
    //             'package'=>$package,
    //             'level'=>$level,
    //             'from_date'=>$from_date,
    //             'to_date'=>$to_date,
    //             'amount'=>$amount,
    //             'total_users'=>$total_users,
    //             'comment'=>ordinal($level).' Level Income',
    //             'created_time'=>date('Y-m-d H:i:s'),
    //             );
    //         $this->common->insert_data('ula_non_working_distribution', $insert_data);

    //         $this->session->set_flashdata('error_message','Income distributed successfully !');
    //         redirect(base_url().'admin/income_distribution/non_working');
    //         /******ula_non_working_distribution******/

    //     }
    // }  

    function non_working()
    {
        ini_set("pcre.backtrack_limit", "10000000");
        $data['title'] = 'Generate Non working Level Income';

        $this->form_validation->set_rules('user_id', 'User', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/income_distribution/non_working', $data);
        } else {
            $users = explode(', ', $this->input->post('user_id'));
            $update = array();
            $datas = array();
            foreach ($users as $user) {
                $lvl = $this->input->post('level');
                $check = $this->common->getSingle('pw_non_working_income', array('user_id' => $user, 'package' => $this->input->post('package')));
                switch ($lvl) {
                    case 0:
                        $level = "NA";
                        break;
                    case 1:
                        $level = "1";
                        break;
                    case 2:
                        $level = "1,2";
                        break;
                    case "3":
                        $level = "1,2,3";
                        break;
                    case "4":
                        $level = "1,2,3,4";
                        break;
                    case "5":
                        $level = "1,2,3,4,5";
                        break;
                    case "6":
                        $level = "1,2,3,4,5,6";
                        break;
                    case "7":
                        $level = "1,2,3,4,5,6,7";
                        break;
                    case "8":
                        $level = "1,2,3,4,5,6,7,8";
                        break;
                    case "9":
                        $level = "1,2,3,4,5,6,7,8,9";
                        break;
                }
                if ($check) {
                    array_push($update, $user);
                    // $update[] = array(
                    //     'user_id' => $user,
                    //     'level' => $level,
                    //     'package' => $this->input->post('package')
                    // );
                } else {
                    $datas[] = array(
                        'user_id' => $user,
                        'package' => $this->input->post('package'),
                        'level' => $level
                    );
                }
            }
            if ($datas) {
                $this->db->insert_batch('pw_non_working_income', $datas);
            }
            if ($update) {
                $ids = implode(',', $update);
                $query = "update pw_non_working_income set level ='" . $level . "' WHERE package = '" . $this->input->post('package') . "' and FIND_IN_SET(user_id, '$ids')";
                $data = $this->db->query($query);
            }
            $this->session->set_flashdata('error_message', 'Added Successfully');
            redirect(base_url() . 'admin/income_distribution/non_working');
            /******ula_non_working_distribution******/
        }
    }

    function user_pool()
    {
        $data['title'] = 'Distribution History';
        $data['history'] = $this->db->get('pw_user_pool_history')->result_array();
        $this->load->view('admin/income_distribution/user_history', $data);
    }
}
