<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Users extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        $this->load->model('admin/Users_model', 'users');
    }

    function view_user()
    {
        $data['title'] = 'User Details';
        $username = $this->input->get('search_text');

        if ($username) {
            $data['user'] = array();
            $user = $this->common->getSingle('pw_users', array('username' => $username));
            if ($user) {
                $user_data =  $this->users->check_user_matrix($user->id);
                if ($user_data == true) {
                    $data['user'] = $user;
                    $this->session->set_flashdata('error_message', "");
                } else {
                    $this->session->set_flashdata('error_message', "No user found");
                }
            }
        }
        $this->load->view('admin/users/view_user', $data);
    }

    function edit_user_profile()
    {
        $data['title'] = 'Edit User Profile';
        $token = $this->input->get('token');
        $user = $this->common->getSingle('pw_users', array('token' => $token));
        if ($user) {
            $user_data =  $this->users->check_user_matrix($user->id);
            if ($user_data == true) {
                $data['user_data'] = $user;
                $this->form_validation->set_rules('full_name', 'Name', 'required');                
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('admin/users/edit_user_profile', $data);
                } else {
                    $update_data = array(
                        'full_name' => $this->input->post('full_name', true),
                        'mobile' => $this->input->post('mobile', true),
                        'address' => $this->input->post('address', true),
                        'modified_time' => date('Y-m-d H:i:s'),
                        'email' => $this->input->post('email', true)
                    );

                    $this->common->update_data('pw_users', array('token' => $token), $update_data);
                    $this->session->set_flashdata('error_message', 'Profile updated successfully !');
                    // redirect(base_url() . 'admin/users/view_user?search_text=' . $user->username);
                    redirect(base_url() . 'admin/users/edit_user_profile?token=' . $token);
                }
            } else {
                redirect(base_url('admin/users/view_user'));
            }
        }
    }
}
