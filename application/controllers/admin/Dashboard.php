<?php
defined('BASEPATH') or exit('No direct script access allowed');
ini_set('memory_limit', -1);
ini_set('max_execution_time', 300);
class Dashboard extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_adminLoginCheck();
		/****checks it is login or not End****/

		/****checks it is admin or sub admin****/
		if ($this->session->type == 2) {
			$this->session->set_flashdata('error_message', "You don't have permission");
			redirect('admin/document/pending_documents');
		}
		/****checks it is admin or sub admin****/

		$this->load->model('admin/Dashboard_model', 'dashboard');
	}

	public function index()
	{
		$data['title'] = 'Dashboard';

		$data['today_active'] = $this->dashboard->getTodayActiveUsers();
		$data['total_users'] = $this->dashboard->getTotalUserListCount();
		$data['paid_users'] = $this->dashboard->getTotalPaidUsers();
		$data['unpaid_users'] = $this->dashboard->getTotalUnPaidUsers();

		$data['today_business'] = $this->dashboard->getTodayBusiness();
		$data['total_business'] = $this->dashboard->getTotalBusiness();

		$data['today_withdrawal'] = $this->dashboard->getTodayWithdrawal();
		$data['total_withdrawal'] = $this->dashboard->getTotalWithdrawal();

		$data['today_pending_withdrawal'] = $this->dashboard->getTodayPendingWithdrawal();
		$data['total_pending_withdrawal'] = $this->dashboard->getTotalPendingWithdrawal();
		$this->load->view('admin/dashboard/dashboard', $data);
	}

	function exportdata()
	{
		$type = base64_decode($_GET['type']);
		$data['data'] = $this->dashboard->getpaidunpaiduser($type);
		$this->load->view('admin/users/download_paid_unpaid_users', $data);
	}
}
