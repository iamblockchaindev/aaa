<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backup extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        /****checks it is admin or sub admin****/
        if ($this->session->type == 2) {
            $this->session->set_flashdata('error_message', "You don't have permission");
            redirect('admin/document/pending_documents');
        }
        /****checks it is admin or sub admin****/

        $this->load->model('admin/Backup_model', 'backup');
    }

    function create_backup()
    {
        if ($_POST) {
            $this->load->dbutil();
            $prefs = array(
                'format' => 'zip',
                'filename' => 'my_db_backup.sql'
            );
            $backup = &$this->dbutil->backup($prefs);
            $db_name = 'backup-on-' . date("Y-m-d-H-i-s") . '.zip';
            $save = 'uploads/database_backup/' . $db_name;
            $this->load->helper('file');
            write_file($save, $backup);

            $insert_data = array(
                'file_name' => $db_name,
                'created_datetime' => date('Y-m-d-H:i:s')
            );
            $this->common->insert_data('pw_database_backup', $insert_data);

            $this->session->set_flashdata('error_message', 'Backup generated successfully !');

            $this->load->helper('download');
            force_download($db_name, $backup);

            echo 1;
            exit;
        } else {
            redirect(base_url() . 'admin');
        }
    }

    function backup_list()
    {
        $data['title'] = 'Backup List';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;

        $data['data_result'] = $this->backup->getAllBackupList($start_limit, $per_page);
        $total_records = $this->backup->getAllBackupListCount();

        $config['base_url'] = base_url() . 'admin/backup/backup_list';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        $config['num_links'] = 15;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $this->load->view('admin/backup/backup_list', $data);
    }

    function delete_backup()
    {
        if ($_POST) {
            $id = $this->input->post('id');
            $file_name = $this->input->post('file_name');

            $query = "delete from pw_database_backup where id = '" . $id . "'";
            $this->db->query($query);

            $file_path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/database_backup/';

            $filename = $file_path . $file_name;
            if (is_file($filename)) {
                unlink($filename);
            }

            $this->session->set_flashdata('error_message', 'Backup deleted successfully !');

            echo 1;
            exit;
        } else {
            redirect(base_url() . 'admin');
        }
    }
}
