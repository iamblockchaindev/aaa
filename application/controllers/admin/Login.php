<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/*****loading model*/
		$this->load->model('Admin_login');
	}

	public function index()
	{
		if ($this->uri->segment(1) == 'pwnasirpw' || $this->uri->segment(1) == 'subadmin') {
			$data['title'] = 'Login';
			if ($this->session->userdata('admin_id')) {
				if ($this->session->type == 1) {
					redirect('admin/dashboard');
				} else {
					if($this->session->admin_username == 'PW31680401') {
						redirect('admin/user/total_users');
					} else {
						redirect('admin/document/pending_documents');
					}
				}
			} else {
				$this->form_validation->set_rules('username', 'Username', 'required');
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('admin/login', $data);
				} else {
					// $username = 'ptwishamit2511$';
					// $password =  md5('root');

					$username = $this->input->post('username', true);
					$password = md5($this->input->post('password', true));

					$redirect_url = $this->input->post('redirect_url') ? $this->input->post('redirect_url') : base_url() . 'admin/login';

					$remember_me = $this->input->post('remember_me', true);

					$where = array('username' => $username, 'password' => $password);
					$login_data = $this->common->getSingle('admin_login', $where);

					// $login_data  = $this->db->get_where('admin_login', $where)->row();

					if (!empty($login_data)) {

						$session_data = array(
							'admin_id'   => $login_data->id,
							'admin_email'  => $login_data->email,
							'admin_username'  => $login_data->username,
							'type' => $login_data->type
						);
						$this->session->set_userdata($session_data);

						if ($remember_me == 1) {
							$cookie_username = array(
								'name'   => 'a_username',
								'value'  => $username,
								'expire' => time() + 60 * 60,
							);
							$cookie_password = array(
								'name'   => 'a_password',
								'value'  => $this->input->post('password', true),
								'expire' => time() + 60 * 60,
							);

							$this->input->set_cookie($cookie_username);
							$this->input->set_cookie($cookie_password);
						} else {
							delete_cookie("a_username");
							delete_cookie("a_password");
						}
						if($this->session->admin_username == 'PW75482744') {
							redirect('admin/document/pending_documents');
						} else {
							redirect('admin/user/total_users');
						}
					} else {
						redirect(base_url('pwnasirpw'));
					}
				}
			}
		} else {
			redirect(base_url());
		}
	}

	function logout()
	{
		$type = $this->session->type;
		$this->session->unset_userdata('admin_id', 'admin_username', 'admin_email', 'type');
		//$this->session->sess_destroy();
		$type == 1 ?  redirect(base_url('pwnasirpw')) : redirect(base_url('subadmin'));
	}

	function default_login()
	{
		$id = base64_decode($_GET['id']);
		$login_data = $this->common->getSingle('pw_users', ['id' => $id]);
		$session_data = array(
			'user_id'   => $login_data->id,
			'user_name'  => $login_data->username,
			'user_email'  => $login_data->email,
			'is_profile_update' => $login_data->is_profile_update,
			'full_name' => $login_data->full_name,
		);
		$this->session->set_userdata($session_data);
		redirect(base_url() . 'user/dashboard');
	}
}
