<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Fund_model extends CI_Model
{

	// function getMyTransferAmount($start_limit, $end_limit)
	// {

	// 	$query = "SELECT * FROM `pw_transfer_history` WHERE  `transfer_by` = 'user' and  status=0 and `from_user` = '".$this->session->userdata('user_id')."' order by id desc LIMIT $start_limit, $end_limit";
	// 	$data = $this->db->query($query);

	// 	if($data->num_rows())
	// 	{
	// 		return $data->result();
	// 	}
	// 	else
	// 	{
	// 		return 0;
	// 	}
	// }	

	// function getMyTransferAmountFilter($member = '', $status = '', $start = '', $end = '')
	// {

	// 	$where = '';
	// 	if ($member) {
	// 		$where .= " and to_user = '$member'";
	// 	}
	// 	if ($status) {
	// 		$where .= " and status	 = '$status'";
	// 	}
	// 	if ($start && $end) {
	// 		$where .= " and DATE(created_datetime)>= '$start' and DATE(created_datetime) <='$end'";
	// 	}
	// 	$query = "select * from pw_transfer_history WHERE transfer_by='user' and from_user= '" . $this->session->userdata('user_id') . "'" . $where . " order by id desc";

	// 	$data = $this->db->query($query);

	// 	if ($data->num_rows()) {
	// 		return $data->result();
	// 	} else {
	// 		return 0;
	// 	}
	// }

	// function getMyTransferAmountCount()
	// {
	// 	// $query = "SELECT * FROM `pw_transfer_history` WHERE  `transfer_by` = 'user' and status=0 and `from_user` = '".$this->session->userdata('user_id')."'";
	// 	// $data = $this->db->query($query);

	// 	// return $data->num_rows();
	// 	$this->db->select('t.*, x.username');
	// 	$this->db->join('pw_users x', 'x.id = t.from_user');
	// 	$this->db->order_by('t.id', 'desc');
	// 	$this->db->where(array('transfer_by' => 'user', 'status' => 0,  'from_user' => $this->session->userdata('user_id')));
	// 	return $this->db->get('pw_transfer_history t')->num_rows();
	// }


	// function getMyAddFund($start_limit, $end_limit, $search_text,$userid)
	// {
	// 	$query = "SELECT pw_add_fund_history.*, pw_users.wallet_amount FROM `pw_add_fund_history` INNER JOIN pw_users ON pw_add_fund_history.user_id=pw_users.id WHERE user_id= $userid order by id desc LIMIT $start_limit, $end_limit";
	// 	$data = $this->db->query($query);

	// 	if($data->num_rows())
	// 	{
	// 		return $data->result();
	// 	}
	// 	else
	// 	{
	// 		return 0;
	// 	}
	// }

	// function getMyAddFundCount($search_text, $userid)
	// {
	// 	$query = "SELECT * FROM `pw_add_fund_history` WHERE user_id=$userid";
	// 	$data = $this->db->query($query);

	// 	return $data->num_rows();
	// }

	function getMyTransferAmount()
	{
		$this->db->select('h.*, u.username');
		$this->db->where('DATE_FORMAT(h.created_datetime, \'%Y-%m-%d\') BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()');
		$this->db->where(array('transfer_by' => 'user', 'status' => 0, 'from_user' => $this->session->user_id));
		$this->db->join('pw_users u', 'u.id = h.to_user', 'left');
		$this->db->order_by('id', 'desc');
		return $this->db->get('pw_transfer_history h')->result_array();
	}

	function getMyAddFund()
	{
		$this->db->where(array('user_id' => $this->session->user_id));
		$this->db->order_by('id', 'desc');
		return $this->db->get('pw_add_fund_history')->result();
	}

	
}
