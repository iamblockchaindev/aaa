<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Genealogy_model extends CI_Model
{

	private function set_group_concat_max_session()
	{
		$query = "SET SESSION group_concat_max_len = 18446744073709551615; ";
		$this->db->query($query);
	}

	function getAllDownline($level)
	{

		$this->set_group_concat_max_session();
		// if ($this->input->post('level')) {
		// 	$level = $this->input->post('level');
		// } else {
		// 	$level = '';
		// }

		$all_user_ids = '';

		if ($level == 1) {

			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user_ids = $row->all_user;
			}
		}

		if ($level == 2) {

			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user_ids = $row->all_user;
				}
			}
		}

		if ($level == 3) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user_ids = $row->all_user;
					}
				}
			}
		}

		if ($level == 4) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user_ids = $row->all_user;
						}
					}
				}
			}
		}

		if ($level == 5) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user_ids = $row->all_user;
							}
						}
					}
				}
			}
		}

		if ($level == 6) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user_ids = $row->all_user;
								}
							}
						}
					}
				}
			}
		}

		if ($level == 7) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user_ids = $row->all_user;
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 8) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user_ids = $row->all_user;
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 9) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user_ids = $row->all_user;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 10) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user_ids = $row->all_user;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 11) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user_ids = $row->all_user;
														// 			echo $all_user_ids;
														//                                     			// // print_r();
														//                                     			exit;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 12) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user = $row->all_user;

														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);

														$row = $data->row();

														if (!empty($row->all_user)) {
															$all_user_ids = $row->all_user;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 13) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user = $row->all_user;

														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);

														$row = $data->row();

														if (!empty($row->all_user)) {
															$all_user = $row->all_user;

															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);

															$row = $data->row();

															if (!empty($row->all_user)) {
																$all_user_ids = $row->all_user;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 14) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user = $row->all_user;

														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);

														$row = $data->row();

														if (!empty($row->all_user)) {
															$all_user = $row->all_user;

															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);

															$row = $data->row();

															if (!empty($row->all_user)) {
																$all_user = $row->all_user;

																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																$data = $this->db->query($query);

																$row = $data->row();

																if (!empty($row->all_user)) {
																	$all_user_ids = $row->all_user;
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 15) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user = $row->all_user;

														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);

														$row = $data->row();

														if (!empty($row->all_user)) {
															$all_user = $row->all_user;

															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);

															$row = $data->row();

															if (!empty($row->all_user)) {
																$all_user = $row->all_user;

																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																$data = $this->db->query($query);

																$row = $data->row();

																if (!empty($row->all_user)) {
																	$all_user = $row->all_user;

																	$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																	$data = $this->db->query($query);

																	$row = $data->row();

																	if (!empty($row->all_user)) {
																		$all_user_ids = $row->all_user;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 16) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user = $row->all_user;

														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);

														$row = $data->row();

														if (!empty($row->all_user)) {
															$all_user = $row->all_user;

															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);

															$row = $data->row();

															if (!empty($row->all_user)) {
																$all_user = $row->all_user;

																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																$data = $this->db->query($query);

																$row = $data->row();

																if (!empty($row->all_user)) {
																	$all_user = $row->all_user;

																	$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																	$data = $this->db->query($query);

																	$row = $data->row();

																	if (!empty($row->all_user)) {
																		$all_user = $row->all_user;

																		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																		$data = $this->db->query($query);

																		$row = $data->row();

																		if (!empty($row->all_user)) {
																			$all_user_ids = $row->all_user;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 17) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user = $row->all_user;

														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);

														$row = $data->row();

														if (!empty($row->all_user)) {
															$all_user = $row->all_user;

															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);

															$row = $data->row();

															if (!empty($row->all_user)) {
																$all_user = $row->all_user;

																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																$data = $this->db->query($query);

																$row = $data->row();

																if (!empty($row->all_user)) {
																	$all_user = $row->all_user;

																	$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																	$data = $this->db->query($query);

																	$row = $data->row();

																	if (!empty($row->all_user)) {
																		$all_user = $row->all_user;

																		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																		$data = $this->db->query($query);

																		$row = $data->row();

																		if (!empty($row->all_user)) {
																			$all_user = $row->all_user;

																			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																			$data = $this->db->query($query);

																			$row = $data->row();

																			if (!empty($row->all_user)) {
																				$all_user_ids = $row->all_user;
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 18) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user = $row->all_user;

														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);

														$row = $data->row();

														if (!empty($row->all_user)) {
															$all_user = $row->all_user;

															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);

															$row = $data->row();

															if (!empty($row->all_user)) {
																$all_user = $row->all_user;

																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																$data = $this->db->query($query);

																$row = $data->row();

																if (!empty($row->all_user)) {
																	$all_user = $row->all_user;

																	$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																	$data = $this->db->query($query);

																	$row = $data->row();

																	if (!empty($row->all_user)) {
																		$all_user = $row->all_user;

																		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																		$data = $this->db->query($query);

																		$row = $data->row();

																		if (!empty($row->all_user)) {
																			$all_user = $row->all_user;

																			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																			$data = $this->db->query($query);

																			$row = $data->row();

																			if (!empty($row->all_user)) {
																				$all_user = $row->all_user;

																				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																				$data = $this->db->query($query);

																				$row = $data->row();

																				if (!empty($row->all_user)) {
																					$all_user_ids = $row->all_user;
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 19) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user = $row->all_user;

														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);

														$row = $data->row();

														if (!empty($row->all_user)) {
															$all_user = $row->all_user;

															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);

															$row = $data->row();

															if (!empty($row->all_user)) {
																$all_user = $row->all_user;

																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																$data = $this->db->query($query);

																$row = $data->row();

																if (!empty($row->all_user)) {
																	$all_user = $row->all_user;

																	$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																	$data = $this->db->query($query);

																	$row = $data->row();

																	if (!empty($row->all_user)) {
																		$all_user = $row->all_user;

																		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																		$data = $this->db->query($query);

																		$row = $data->row();

																		if (!empty($row->all_user)) {
																			$all_user = $row->all_user;

																			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																			$data = $this->db->query($query);

																			$row = $data->row();

																			if (!empty($row->all_user)) {
																				$all_user = $row->all_user;

																				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																				$data = $this->db->query($query);

																				$row = $data->row();

																				if (!empty($row->all_user)) {
																					$all_user = $row->all_user;

																					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																					$data = $this->db->query($query);

																					$row = $data->row();

																					if (!empty($row->all_user)) {
																						$all_user_ids = $row->all_user;
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($level == 20) {
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
			$data = $this->db->query($query);

			$row = $data->row();

			if (!empty($row->all_user)) {
				$all_user = $row->all_user;

				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);

				$row = $data->row();

				if (!empty($row->all_user)) {
					$all_user = $row->all_user;

					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);

					$row = $data->row();

					if (!empty($row->all_user)) {
						$all_user = $row->all_user;

						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);

						$row = $data->row();

						if (!empty($row->all_user)) {
							$all_user = $row->all_user;

							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);

							$row = $data->row();

							if (!empty($row->all_user)) {
								$all_user = $row->all_user;

								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);

								$row = $data->row();

								if (!empty($row->all_user)) {
									$all_user = $row->all_user;

									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);

									$row = $data->row();

									if (!empty($row->all_user)) {
										$all_user = $row->all_user;

										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);

										$row = $data->row();

										if (!empty($row->all_user)) {
											$all_user = $row->all_user;

											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);

											$row = $data->row();

											if (!empty($row->all_user)) {
												$all_user = $row->all_user;

												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);

												$row = $data->row();

												if (!empty($row->all_user)) {
													$all_user = $row->all_user;

													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);

													$row = $data->row();

													if (!empty($row->all_user)) {
														$all_user = $row->all_user;

														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);

														$row = $data->row();

														if (!empty($row->all_user)) {
															$all_user = $row->all_user;

															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);

															$row = $data->row();

															if (!empty($row->all_user)) {
																$all_user = $row->all_user;

																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																$data = $this->db->query($query);

																$row = $data->row();

																if (!empty($row->all_user)) {
																	$all_user = $row->all_user;

																	$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																	$data = $this->db->query($query);

																	$row = $data->row();

																	if (!empty($row->all_user)) {
																		$all_user = $row->all_user;

																		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																		$data = $this->db->query($query);

																		$row = $data->row();

																		if (!empty($row->all_user)) {
																			$all_user = $row->all_user;

																			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																			$data = $this->db->query($query);

																			$row = $data->row();

																			if (!empty($row->all_user)) {
																				$all_user = $row->all_user;

																				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																				$data = $this->db->query($query);

																				$row = $data->row();

																				if (!empty($row->all_user)) {
																					$all_user = $row->all_user;

																					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																					$data = $this->db->query($query);

																					$row = $data->row();

																					if (!empty($row->all_user)) {
																						$all_user = $row->all_user;

																						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																						$data = $this->db->query($query);

																						$row = $data->row();

																						if (!empty($row->all_user)) {
																							$all_user_ids = $row->all_user;
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (!empty($all_user_ids)) {

			$this->db->select('y.*, x.full_name as name');
			$this->db->where("FIND_IN_SET(y.id, '$all_user_ids')");
			$this->db->join('pw_users x', 'x.username = y.sponsor', 'left');
			$this->db->order_by('y.created_time', 'desc');
			return $this->db->get('pw_users y')->result_array();

			// $where = '';
			// $query = "SELECT * FROM `pw_users` WHERE FIND_IN_SET(id, '" . $all_user_ids . "')" . $where . " order by created_time desc";
			// $data = $this->db->query($query);
			// if ($data->num_rows() > 0) {
			// 	return $data->result();
			// } else {
			// 	return 0;
			// }
		} else {
			return 0;
		}
	}

	function getAllMyUsers($status = '')
	{
		$this->set_group_concat_max_session();
		$all_user_ids = array();
		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
		$data = $this->db->query($query);
		$row = $data->row();

		if (!empty($row->all_user)) {
			array_push($all_user_ids, $row->all_user);
			$all_user = $row->all_user;
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
			$data = $this->db->query($query);
			$row = $data->row();
			if (!empty($row->all_user)) {
				array_push($all_user_ids, $row->all_user);
				$all_user = $row->all_user;
				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);
				$row = $data->row();
				if (!empty($row->all_user)) {
					array_push($all_user_ids, $row->all_user);
					$all_user = $row->all_user;
					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);
					$row = $data->row();
					if (!empty($row->all_user)) {
						array_push($all_user_ids, $row->all_user);
						$all_user = $row->all_user;
						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);
						$row = $data->row();
						if (!empty($row->all_user)) {
							array_push($all_user_ids, $row->all_user);
							$all_user = $row->all_user;
							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);
							$row = $data->row();
							if (!empty($row->all_user)) {
								array_push($all_user_ids, $row->all_user);
								$all_user = $row->all_user;
								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);
								$row = $data->row();
								if (!empty($row->all_user)) {
									array_push($all_user_ids, $row->all_user);
									$all_user = $row->all_user;
									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);
									$row = $data->row();
									if (!empty($row->all_user)) {
										array_push($all_user_ids, $row->all_user);
										$all_user = $row->all_user;
										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);
										$row = $data->row();
										if (!empty($row->all_user)) {
											array_push($all_user_ids, $row->all_user);
											$all_user = $row->all_user;
											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);
											$row = $data->row();
											if (!empty($row->all_user)) {
												array_push($all_user_ids, $row->all_user);
												$all_user = $row->all_user;
												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);
												$row = $data->row();
												if (!empty($row->all_user)) {
													array_push($all_user_ids, $row->all_user);
													$all_user = $row->all_user;
													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);
													$row = $data->row();
													if (!empty($row->all_user)) {
														array_push($all_user_ids, $row->all_user);
														$all_user = $row->all_user;
														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);
														$row = $data->row();
														if (!empty($row->all_user)) {
															array_push($all_user_ids, $row->all_user);
															$all_user = $row->all_user;
															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);
															$row = $data->row();
															if (!empty($row->all_user)) {
																array_push($all_user_ids, $row->all_user);
																$all_user = $row->all_user;
																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																$data = $this->db->query($query);
																$row = $data->row();
																if (!empty($row->all_user)) {
																	array_push($all_user_ids, $row->all_user);
																	$all_user = $row->all_user;
																	$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																	$data = $this->db->query($query);
																	$row = $data->row();
																	if (!empty($row->all_user)) {
																		array_push($all_user_ids, $row->all_user);
																		$all_user = $row->all_user;
																		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																		$data = $this->db->query($query);
																		$row = $data->row();
																		if (!empty($row->all_user)) {
																			array_push($all_user_ids, $row->all_user);
																			$all_user = $row->all_user;
																			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																			$data = $this->db->query($query);
																			$row = $data->row();
																			if (!empty($row->all_user)) {
																				array_push($all_user_ids, $row->all_user);
																				$all_user = $row->all_user;
																				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																				$data = $this->db->query($query);
																				$row = $data->row();
																				if (!empty($row->all_user)) {
																					array_push($all_user_ids, $row->all_user);
																					$all_user = $row->all_user;
																					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																					$data = $this->db->query($query);
																					$row = $data->row();
																					if (!empty($row->all_user)) {
																						array_push($all_user_ids, $row->all_user);
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		$ids = implode(', ', $all_user_ids);
		$idss = explode(',', $ids);
		$active = array();
		$inactive = array();
		$where = '';

		if (!empty($all_user_ids)) {
			$this->db->from('pw_users');
			$this->db->where_in('id', $idss);
			$this->db->order_by('created_time', 'desc');
			$active = $this->db->get()->result();
		}
		if (!empty($status) && ($status == 'active')) {
			return $active;
			exit;
		} else {
			$where .= " and package is NULL";
			$member_id = getMemberNameByID($this->session->userdata('user_id'));
			$query = "select * from pw_users where sponsor = '" . $member_id . "'" . $where . " order by created_time desc";
			$data = $this->db->query($query);
			if ($data->num_rows() > 0) {
				$inactive =  $data->result();
			}
			if (!empty($status) && ($status == 'inactive')) {
				return $inactive;
			} else {
				$merge = array_merge($active, $inactive);
				return $merge;
			}
		}
	}

	function get_user($token)
	{
		return $this->db->get_where('pw_users', array('token' => $token))->result_array();
	}

	function check_user_matrix($sponsorId, $member_id)
	{
		$query = "select * from pw_user_matrix";
		$data = $this->db->query($query);
		$count = $data->num_rows();
		foreach ($data->result() as $row) {
			$count--;
			$all_user = explode(',', $row->user_tree);
			$result = in_array($sponsorId, $all_user);
			if ($result) {
				if ($count == 0) {
					$insert = array(
						'level' => $row->level + 1,
						'user_tree' => $member_id
					);
					$this->db->insert('pw_user_matrix', $insert);
				} else {
					$update = array(
						'user_tree' => $row->user_tree . ',' . $member_id
					);
					$this->db->where('id', $row->id);
					$this->db->update('pw_user_matrix', $update);
				}
				return true;
			}
		}
		return false;
	}

	function get_user_matrix($sponsorId)
	{
		$query = "select * from pw_user_matrix";
		$data = $this->db->query($query);
		foreach ($data->result() as $row) {
			$all_user = explode(',', $row->user_tree);
			$result = in_array($sponsorId, $all_user);
			if ($result) {
				return $row->level;
			}
		}
		return $result;
	}

	function get_personal_matrix($sponsorId)
	{
		$query = "select * from pw_personal_matrix";
		$data = $this->db->query($query);
		foreach ($data->result() as $row) {
			$all_user = explode(',', $row->user_tree);
			$result = in_array($sponsorId, $all_user);
			if ($result) {
				return true;
			}
		}
		return false;
	}
}
