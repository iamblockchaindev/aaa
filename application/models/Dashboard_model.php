<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model
{
	function getMyWalletAmount()
	{
		$query = "select wallet_amount from pw_users where id = '" . $this->session->userdata('user_id') . "'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->wallet_amount;
		} else {
			return 0;
		}
	}
	function get_total_deposit()
	{
		$this->db->select("sum(amount) as total");
		return $this->db->get_where('pwt_user_notification', array('from_user' => $this->session->user_id, 'type' => 'fund', 'status' => 100))->row()->total;
	}
	function count_total_exchange()
	{
		$this->db->select("sum(conv_amount) as total");
		return $this->db->get_where('pwt_exchange_history', array('from_currency' => 'pwt', 'user_id' => $this->session->user_id, 'date >=' => '2021-08-20'))->row()->total;
	}

	function getMyTotalWithdrawal()
	{
		$query = "select SUM(total_amount) as total_amount from pw_withdrawal where session_id = '" . $this->session->userdata('user_id') . "' and status = 'G'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->total_amount;
		} else {
			return 0;
		}
	}
	
	function getMyTotalCryptoBonus()
	{
		$query = "select crypto_bonus from pw_users where id = '" . $this->session->userdata('user_id') . "'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->crypto_bonus;
		} else {
			return 0;
		}
	}

	function get_my_team()
	{
		$member_id = getMemberNameByID($this->session->userdata('user_id'));
		return $this->db->get_where('pw_users', array('sponsor' => $member_id))->num_rows();
	}

	function get_my_direct()
	{
		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $this->session->userdata('user_id') . "'";
		$data = $this->db->query($query);

		$row = $data->row();

		if (!empty($row->all_user)) {
			$all_user_ids = $row->all_user;
		}

		if (!empty($all_user_ids)) {			

			$query = "SELECT * FROM `pw_users` WHERE FIND_IN_SET(id, '" . $all_user_ids . "')" ;
			$data = $this->db->query($query);

			if ($data->num_rows() > 0) {
				return $data->result();
			} else {
				return 0;
			}
		}
	}
	
	function get_logs() {
		$this->db->order_by('id', 'desc');
		$this->db->limit('7');
		return $this->db->get_where('pw_logs', array('userid' => $this->session->user_id))->result_array();
	}
}
