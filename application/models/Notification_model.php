<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Notification_model extends CI_Model
{
	function getMyNotificationHistory()
	{		
        $this->db->select('pw_user_notification.*, pw_users.username');
		$this->db->where(array('from_user' => $this->session->user_id));
        $this->db->join('pw_users', 'pw_users.id = pw_user_notification.to_user', 'left');
		$this->db->order_by('pw_user_notification.id', 'desc');
		// $this->db->limit(100);
		$this->db->where('pw_user_notification.created_datetime BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()');
		return $this->db->get('pw_user_notification')->result_array();
	}	

	function get_history($end_limit)
	{

		$query = "select * from pw_transaction_history where user_id = '" . $this->session->userdata('user_id') . "' order by pw_transaction_history.id desc LIMIT $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return 0;
		}
	}

	function get_history_filter($status, $start, $end)
	{

		$where = '';
		$where .= " and status = '$status' and DATE(created_datetime)>= '$start' and DATE(created_datetime) <='$end'";
		$query = "select * from pw_transaction_history where user_id = '" . $this->session->userdata('user_id') . "'" . $where . "' order by pw_transaction_history.id desc";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return 0;
		}
	}
}