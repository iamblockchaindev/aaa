<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ticket_model extends CI_Model 
{
	function getMyTotalTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pw_tickets` WHERE user_id = '".$this->session->userdata('user_id')."'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}

	function getMyPendingTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pw_tickets` WHERE user_id = '".$this->session->userdata('user_id')."' and status = 'open'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}

	function getMyInProcessTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pw_tickets` WHERE user_id = '".$this->session->userdata('user_id')."' and status = 'process'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}	

	function getMyCloseTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pw_tickets` WHERE user_id = '".$this->session->userdata('user_id')."' and status = 'close'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}


	// function getTicketListByType($type, $end_limit)
	// {
		
	// 	$WHERE = "";

    // 	if($type == 'all')
    // 	{
    //     	$WHERE .= "";
    // 	}
    // 	else if($type == 'open')
    // 	{
    // 		$WHERE .= " and status = 'open'";
    // 	}
    // 	else if($type == 'process')
    // 	{
    // 		$WHERE .= " and status = 'process'";
    // 	}
    // 	else if($type == 'close')
    // 	{
    // 		$WHERE .= " and status = 'close'";
    // 	}		

	// 	$query = "SELECT * FROM `pw_tickets` WHERE `user_id` = '".$this->session->userdata('user_id')."'".$WHERE." order by id desc LIMIT $end_limit";
	// 	$data = $this->db->query($query);

	// 	if($data->num_rows())
	// 	{
	// 		return $data->result();
	// 	}
	// 	else
	// 	{
	// 		return 0;
	// 	}
	// }

	function getTicketListByType()
	{
		$type = $this->input->get('type', true);

		$WHERE = "";

    	if($type == 'all')
    	{
        	$WHERE .= "";
    	}
    	else if($type == 'open')
    	{
    		$WHERE .= " and status = 'open'";
    	}
    	else if($type == 'process')
    	{
    		$WHERE .= " and status = 'process'";
    	}
    	else if($type == 'close')
    	{
    		$WHERE .= " and status = 'close'";
    	}		

		$query = "SELECT * FROM `pw_tickets` WHERE `user_id` = '".$this->session->userdata('user_id')."'".$WHERE." order by id desc";
		$data = $this->db->query($query);
		return $data->result();		
	}	
	
	function getMyTicketChatList()
	{
		$ticket_id = $this->input->get('ticket_id', true);	

		$query = "SELECT pw_tickets_chat.*, pw_users.profile_pic FROM pw_tickets_chat join pw_users on pw_tickets_chat.user_id = pw_users.id WHERE pw_tickets_chat.user_id = '".$this->session->userdata('user_id')."' and pw_tickets_chat.ticket_id = '".$ticket_id."' order by pw_tickets_chat.id asc";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			return $data->result();
		}
		else
		{
			return 0;
		}
	}

}
?>
