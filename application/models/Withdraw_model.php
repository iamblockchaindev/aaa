<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Withdraw_model extends CI_Model 
{
	// function getAllWithdrawDetails($start_limit, $end_limit, $status)
	// {
	// 	$where = '';

	// 	if(!empty($status))
	// 	{
	// 		$where .= " and status = '".$status."'";
	// 	}

	// 	$query = "select pw_withdrawal.* from pw_withdrawal where session_id = '".$this->session->userdata('user_id')."'".$where." order by pw_withdrawal.id desc LIMIT $start_limit, $end_limit";

	// 	$data = $this->db->query($query);
	// 	if($data->num_rows() > 0)
	// 	{
	// 		return $data->result();
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }	

	// function getAllWithdrawFilter($start, $end, $status)
	// {
	// 	$where = '';

	// 	if(!empty($status))
	// 	{			
	// 		$where .= " and status = '$status' and DATE(created_datetime)>= '$start' and DATE(created_datetime) <='$end'";		
	// 	}

	// 	$query = "select pw_withdrawal.* from pw_withdrawal where session_id = '".$this->session->userdata('user_id')."'".$where." order by pw_withdrawal.id desc";

	// 	$data = $this->db->query($query);
	// 	if($data->num_rows() > 0)
	// 	{
	// 		return $data->result();
	// 	}
	// 	else
	// 	{
	// 		return 0;
	// 	}
	// }

	// function getAllWithdrawDetailsCount($status)
	// {
	// 	$where = '';

	// 	if(!empty($status))
	// 	{
	// 		$where .= " and status = '".$status."'";
	// 	}

	// 	$query = "select pw_withdrawal.* from pw_withdrawal where session_id = '".$this->session->userdata('user_id')."'".$where." order by pw_withdrawal.id desc";

	// 	$data = $this->db->query($query);
	// 	return $data->num_rows();
	// }

	function getAllWithdrawDetails()
	{
		$this->db->where(array('session_id' => $this->session->user_id));
		$this->db->order_by('id', 'desc');
		return $this->db->get('pw_withdrawal')->result();				
	}
}
