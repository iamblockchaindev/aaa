<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function getTotalUserList($start_limit, $end_limit)
	{
		$where = '';
		if ($this->session->type == 2) {
			$where .= " and id NOT IN (1,680,681,682,683,684,685,686,687,688 )";
		}

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%' or mobile='" . $search_text . "' or email = '" . $search_text . "'";
		}

		$query = "select * from pw_users where is_delete = '1'" . $where . " order by id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getTotalUserListCount()
	{
		$where = '';

		if ($this->session->type == 2) {
			$where .= "and id NOT IN (1,680,681,682,683,684,685,686,687,688 )";
		}

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%' or mobile='" . $search_text . "' or email = '" . $search_text . "'";
		}

		$query = "select * from pw_users where is_delete = '1'" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getTotalPaidUserList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->session->type == 2) {
			$where .= "and id NOT IN (1,680,681,682,683,684,685,686,687,688 )";
		}

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%' or mobile='" . $search_text . "' or email = '" . $search_text . "'";
		}

		$query = "select * from pw_users where package is not null and is_delete = '1'" . $where . " order by id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getTotalPaidUserListCount()
	{
		$where = '';

		if ($this->session->type == 2) {
			$where .= "and id NOT IN (1,680,681,682,683,684,685,686,687,688 )";
		}

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%' or mobile='" . $search_text . "' or email = '" . $search_text . "'";
		}

		$query = "select * from pw_users where package is not null and is_delete = '1'" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getTotalUnPaidUserList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->session->type == 2) {
			$where .= "and id NOT IN (1,680,681,682,683,684,685,686,687,688 )";
		}

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%' or mobile='" . $search_text . "' or email = '" . $search_text . "'";
		}

		$query = "select * from pw_users where package is null and is_delete = '1'" . $where . " order by id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getTotalUnPaidUserListCount()
	{
		$where = '';

		if ($this->session->type == 2) {
			$where .= "and id NOT IN (1,680,681,682,683,684,685,686,687,688 )";
		}

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%' or mobile='" . $search_text . "' or email = '" . $search_text . "'";
		}

		$query = "select * from pw_users where package is null and is_delete = '1'" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}


	function getTodaysActiveUserList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%' or mobile='" . $search_text . "' or email = '" . $search_text . "'";
		}

		$query = "select * from pw_users where package is not null and is_delete = '1' and date_format(upgrade_time, '%Y-%m-%d') = '" . date('Y-m-d') . "'" . $where . " order by id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getTodaysActiveUserListCount()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%' or mobile='" . $search_text . "' or email = '" . $search_text . "'";
		}

		$query = "select * from pw_users where package is not null and is_delete = '1' and date_format(upgrade_time, '%Y-%m-%d') = '" . date('Y-m-d') . "'" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getcityname($id)
	{

		return  $this->db->get_where('pw_cities', array('id' => $id))->row();
		echo $this->db->last_query();
	}

	function get_user_data($leavel = '', $package = '')

	{
		if ($leavel) {
			$this->db->where('pl.level_number', $leavel);
		}
		if ($package) {
			$this->db->where('pu.package', $package);
		}
		$this->db->join('pw_non_working_level pl', 'pl.user_id = pu.id', 'left');
		$this->db->where('is_delete', 1);
		$this->db->order_by('pu.id', 'desc');
		return $this->db->get('pw_users pu')->result();
		// $where = '';

		// if($leavel) {
		// 	// $where = "and ".$member."%'";
		// }

		// if($this->input->get('search_text'))
		// {
		// 	$search_text = $this->input->get('search_text');

		// 	$where .= " and username like '%".$search_text."%' or mobile='".$search_text."' or email = '".$search_text."'";
		// }
		// if($end_limit != "" ) {
		// 	$query = "select * from pw_users where is_delete = '1'".$where. " order by id desc LIMIT $start_limit, $end_limit";
		// } else {
		// 	$query = "select * from pw_users where is_delete = '1'".$where. " order by id desc";
		// }

		// $data = $this->db->query();
		// if($data->num_rows() > 0)
		// {
		// 	return $data->result();
		// }
		// else
		// {
		// 	return false;
		// }
	}

	function get_level_matrix()
	{
		$level = $this->input->post('level');
		$package = $this->input->post('package');
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$res = $this->db->get_where('pw_company_matrix', array('level' => $level, 'package' => $package))->row_array();
		if ($package == 30) {
			$this->db->select('pu.id, pu.username, pu.full_name, pu.created_time, pu.upgrade_time, pn.id as levelid, pn.level');
			$this->db->join('pw_non_working_income pn', 'pn.user_id = pu.id', 'left');
			if ($end) {
				$this->db->where(array('DATE_FORMAT(upgrade_time,"%Y-%m-%d") >=' =>  $start, 'DATE_FORMAT(upgrade_time,"%Y-%m-%d") <=' =>  $end));
			}
			$this->db->order_by('pu.upgrade_time', 'desc');
		} else {
			$this->db->select('pu.id, pu.username, pu.full_name, pu.created_time, pw.created_datetime as upgrade_time, pn.id as levelid, pn.level');
			$this->db->join('pw_non_working_income pn', 'pn.user_id = pu.id');
			$this->db->join('pw_non_working_user pw', 'pw.user_id = pn.user_id');
			if ($end) {
				$this->db->where(array('DATE_FORMAT(pw.created_datetime,"%Y-%m-%d") >=' =>  $start, 'DATE_FORMAT(pw.created_datetime,"%Y-%m-%d") <=' =>  $end));
			}
			$this->db->where('pw.package', $package);
			$this->db->order_by('pw.created_datetime', 'desc');
		}
		$this->db->where('pn.package', $package);
		$this->db->where("FIND_IN_SET(pu.id,'" . str_replace(", ", ",", $res['user_tree']) . "' )");
		return $this->db->get('pw_users pu')->result_array();

		// 		$sql =  $this->db->get_compiled_select('pw_users pu');
		// 		var_dump($sql);
	}

	function get_level_matrix_($level, $start, $end, $package)
	{

		$res = $this->db->get_where('pw_company_matrix', array('level' => $level, 'package' => $package))->row_array();
		$this->db->select('pu.id, pu.username, pu.full_name, pu.created_time, pu.upgrade_time, pn.id as levelid, pn.level');
		$this->db->join('pw_non_working_income pn', 'pn.user_id = pu.id', 'left');
		if ($end) {
			$this->db->where(array('DATE_FORMAT(upgrade_time,"%Y-%m-%d") >=' =>  $start, 'DATE_FORMAT(upgrade_time,"%Y-%m-%d") <=' =>  $end));
		}
		$this->db->where("FIND_IN_SET(pu.id,'" . str_replace(", ", ",", $res['user_tree']) . "' )");
		$this->db->order_by('pn.level', 'asc');
		return $this->db->get('pw_users pu')->result_array();
	}

	function generate_jade_income($level, $start, $end, $package)
	{

		$res = $this->db->get_where('pw_company_matrix', array('level' => $level, 'package' => $package))->row_array();
		$this->db->select('pu.id, pu.username, pu.full_name, pu.created_time, pw.created_datetime as upgrade_time, pn.id as levelid, pn.level');
		$this->db->join('pw_non_working_income pn', 'pn.user_id = pu.id');
		$this->db->join('pw_non_working_user pw', 'pw.user_id = pn.user_id');
		if ($end) {
			$this->db->where(array('DATE_FORMAT(pw.created_datetime,"%Y-%m-%d") >=' =>  $start, 'DATE_FORMAT(pw.created_datetime,"%Y-%m-%d") <=' =>  $end));
		}
		$this->db->where(array('pn.package' => $package, 'pw.package' => $package));
		$this->db->where("FIND_IN_SET(pu.id,'" . str_replace(", ", ",", $res['user_tree']) . "' )");
		$this->db->order_by('pn.level', 'asc');
		return $this->db->get('pw_users pu')->result_array();
	}

	function get_total_users($level, $package)
	{
		// $level = $this->input->post('level');
		return $this->db->get_where('pw_company_matrix', array('level' => $level, 'package' => $package))->row()->tree_count;
	}

	private function set_group_concat_max_session()
	{
		$query = "SET SESSION group_concat_max_len = 18446744073709551615; ";
		$this->db->query($query);
	}

	function get_user_matrix()
	{
		$this->set_group_concat_max_session();
		$user = $this->input->post('user');
		$package = $this->input->post('package');
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$all_user_ids = array();
		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id ='" . $user . "'";
		$data = $this->db->query($query);
		$row = $data->row();

		if (!empty($row->all_user)) {
			array_push($all_user_ids, $row->all_user);
			$all_user = $row->all_user;
			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
			$data = $this->db->query($query);
			$row = $data->row();
			if (!empty($row->all_user)) {
				array_push($all_user_ids, $row->all_user);
				$all_user = $row->all_user;
				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);
				$row = $data->row();
				if (!empty($row->all_user)) {
					array_push($all_user_ids, $row->all_user);
					$all_user = $row->all_user;
					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
					$data = $this->db->query($query);
					$row = $data->row();
					if (!empty($row->all_user)) {
						array_push($all_user_ids, $row->all_user);
						$all_user = $row->all_user;
						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
						$data = $this->db->query($query);
						$row = $data->row();
						if (!empty($row->all_user)) {
							array_push($all_user_ids, $row->all_user);
							$all_user = $row->all_user;
							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
							$data = $this->db->query($query);
							$row = $data->row();
							if (!empty($row->all_user)) {
								array_push($all_user_ids, $row->all_user);
								$all_user = $row->all_user;
								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
								$data = $this->db->query($query);
								$row = $data->row();
								if (!empty($row->all_user)) {
									array_push($all_user_ids, $row->all_user);
									$all_user = $row->all_user;
									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
									$data = $this->db->query($query);
									$row = $data->row();
									if (!empty($row->all_user)) {
										array_push($all_user_ids, $row->all_user);
										$all_user = $row->all_user;
										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
										$data = $this->db->query($query);
										$row = $data->row();
										if (!empty($row->all_user)) {
											array_push($all_user_ids, $row->all_user);
											$all_user = $row->all_user;
											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
											$data = $this->db->query($query);
											$row = $data->row();
											if (!empty($row->all_user)) {
												array_push($all_user_ids, $row->all_user);
												$all_user = $row->all_user;
												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
												$data = $this->db->query($query);
												$row = $data->row();
												if (!empty($row->all_user)) {
													array_push($all_user_ids, $row->all_user);
													$all_user = $row->all_user;
													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
													$data = $this->db->query($query);
													$row = $data->row();
													if (!empty($row->all_user)) {
														array_push($all_user_ids, $row->all_user);
														$all_user = $row->all_user;
														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
														$data = $this->db->query($query);
														$row = $data->row();
														if (!empty($row->all_user)) {
															array_push($all_user_ids, $row->all_user);
															$all_user = $row->all_user;
															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
															$data = $this->db->query($query);
															$row = $data->row();
															if (!empty($row->all_user)) {
																array_push($all_user_ids, $row->all_user);
																$all_user = $row->all_user;
																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																$data = $this->db->query($query);
																$row = $data->row();
																if (!empty($row->all_user)) {
																	array_push($all_user_ids, $row->all_user);
																	$all_user = $row->all_user;
																	$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																	$data = $this->db->query($query);
																	$row = $data->row();
																	if (!empty($row->all_user)) {
																		array_push($all_user_ids, $row->all_user);
																		$all_user = $row->all_user;
																		$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																		$data = $this->db->query($query);
																		$row = $data->row();
																		if (!empty($row->all_user)) {
																			array_push($all_user_ids, $row->all_user);
																			$all_user = $row->all_user;
																			$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																			$data = $this->db->query($query);
																			$row = $data->row();
																			if (!empty($row->all_user)) {
																				array_push($all_user_ids, $row->all_user);
																				$all_user = $row->all_user;
																				$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																				$data = $this->db->query($query);
																				$row = $data->row();
																				if (!empty($row->all_user)) {
																					array_push($all_user_ids, $row->all_user);
																					$all_user = $row->all_user;
																					$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																					$data = $this->db->query($query);
																					$row = $data->row();
																					if (!empty($row->all_user)) {
																						array_push($all_user_ids, $row->all_user);
																						$all_user = $row->all_user;
																						$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																						$data = $this->db->query($query);
																						$row = $data->row();
																						if (!empty($row->all_user)) {
																							array_push($all_user_ids, $row->all_user);
																							$all_user = $row->all_user;
																							$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																							$data = $this->db->query($query);
																							$row = $data->row();
																							if (!empty($row->all_user)) {
																								array_push($all_user_ids, $row->all_user);
																								$all_user = $row->all_user;
																								$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																								$data = $this->db->query($query);
																								$row = $data->row();
																								if (!empty($row->all_user)) {
																									array_push($all_user_ids, $row->all_user);
																									$all_user = $row->all_user;
																									$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																									$data = $this->db->query($query);
																									$row = $data->row();
																									if (!empty($row->all_user)) {
																										array_push($all_user_ids, $row->all_user);
																										$all_user = $row->all_user;
																										$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																										$data = $this->db->query($query);
																										$row = $data->row();
																										if (!empty($row->all_user)) {
																											array_push($all_user_ids, $row->all_user);
																											$all_user = $row->all_user;
																											$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																											$data = $this->db->query($query);
																											$row = $data->row();
																											if (!empty($row->all_user)) {
																												array_push($all_user_ids, $row->all_user);
																												$all_user = $row->all_user;
																												$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																												$data = $this->db->query($query);
																												$row = $data->row();
																												if (!empty($row->all_user)) {
																													array_push($all_user_ids, $row->all_user);
																													$all_user = $row->all_user;
																													$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																													$data = $this->db->query($query);
																													$row = $data->row();
																													if (!empty($row->all_user)) {
																														array_push($all_user_ids, $row->all_user);
																														$all_user = $row->all_user;
																														$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																														$data = $this->db->query($query);
																														$row = $data->row();
																														if (!empty($row->all_user)) {
																															array_push($all_user_ids, $row->all_user);
																															$all_user = $row->all_user;
																															$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																															$data = $this->db->query($query);
																															$row = $data->row();
																															if (!empty($row->all_user)) {
																																array_push($all_user_ids, $row->all_user);
																																$all_user = $row->all_user;
																																$query = "select GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
																																$data = $this->db->query($query);
																																$row = $data->row();
																																if (!empty($row->all_user)) {
																																	array_push($all_user_ids, $row->all_user);
																																}
																															}
																														}
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		array_push($all_user_ids, '8759');
		$ids = implode(', ', $all_user_ids);
		$idss = explode(',', $ids);
		if ($package == 30) {
			$this->db->select('pu.id, pu.username, pu.full_name, pu.created_time, pu.upgrade_time, pn.id as levelid, pn.level');
			$this->db->join('pw_non_working_income pn', 'pn.user_id = pu.id');
			if ($end) {
				$this->db->where(array('DATE_FORMAT(upgrade_time,"%Y-%m-%d") >=' =>  $start, 'DATE_FORMAT(upgrade_time,"%Y-%m-%d") <=' =>  $end));
			}
			$this->db->order_by('pu.upgrade_time', 'desc');
		} else if ($package != 30) {
			$this->db->select('pu.id, pu.username, pu.full_name, pu.created_time, pw.created_datetime as upgrade_time, pn.id as levelid, pn.level');
			$this->db->join('pw_non_working_income pn', 'pn.user_id = pu.id');
			$this->db->join('pw_non_working_user pw', 'pw.user_id = pn.user_id');
			if ($end) {
				$this->db->where(array('DATE_FORMAT(pw.created_datetime,"%Y-%m-%d") >=' =>  $start, 'DATE_FORMAT(pw.created_datetime,"%Y-%m-%d") <=' =>  $end));
			}
			$this->db->where('pw.package', $package);
			$this->db->order_by('pw.created_datetime', 'desc');
		}
		$this->db->where('pn.package', $package);
		$this->db->where_in('pu.id', $idss);
		return $this->db->get('pw_users pu')->result_array();
	}

	function get_user_pool_history()
	{
		$user = $this->input->post('user');
		$package = $this->input->post('package');
		if ($package == 30) {
			$this->db->select('pu.id, pn.package, pu.username, pu.full_name, pu.created_time, pu.upgrade_time, pn.id as levelid, pn.level');
			$this->db->join('pw_non_working_income pn', 'pn.user_id = pu.id');
			$this->db->order_by('pu.upgrade_time', 'desc');
		} else if ($package != 30) {
			$this->db->select('pu.id, pn.package, pu.username, pu.full_name, pu.created_time, pw.created_datetime as upgrade_time, pn.id as levelid, pn.level');
			$this->db->join('pw_non_working_income pn', 'pn.user_id = pu.id');
			$this->db->join('pw_non_working_user pw', 'pw.user_id = pn.user_id');
			$this->db->where('pw.package', $package);
			$this->db->order_by('pw.created_datetime', 'desc');
		}
		$this->db->where('pn.package', $package);
		$this->db->where_in('pu.username', $user);
		return $this->db->get('pw_users pu')->result_array();
	}

	function get_matrix_user($userid, $level)
	{
		if ($level == 1) {
			$query = "insert into pw_user_matrix (level, user_tree) select $level, GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where sponsor_user_id = '" . $userid . "'";
			$data = $this->db->query($query);
		} else {
			$this->set_group_concat_max_session();
			$query = "select user_tree from pw_user_matrix where level = '" . ($level - 1) . "'";
			$data = $this->db->query($query);
			$row = $data->row();
			if (!empty($row->user_tree)) {
				$all_user = $row->user_tree;
				$query = "insert into pw_user_matrix (level, user_tree) select $level, GROUP_CONCAT(member_user_id SEPARATOR ',') AS `all_user` from pw_working_tree where FIND_IN_SET(sponsor_user_id, '" . $all_user . "')";
				$data = $this->db->query($query);
			}
		}
	}

	function set_user_matrix()
	{
		$query = "select * from pw_user_matrix where level = 1";
		$data = $this->db->query($query);
		foreach ($data->result() as $row) {
			$insert = array(
				'level' => 1,
				'user_tree' => $row->user_tree
			);
			$this->db->insert('pw_personal_matrix', $insert);
		}
	}

	function check_user_matrix($userid)
	{
		$query = "select * from pw_user_matrix";
		$data = $this->db->query($query);
		foreach ($data->result() as $row) {
			$all_user = explode(',', $row->user_tree);
			$result = in_array($userid, $all_user);
			if ($result) {
				return true;
			}
		}
		return $result;
	}	
}
