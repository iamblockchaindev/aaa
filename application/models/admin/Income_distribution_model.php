<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Income_distribution_model extends CI_Model 
{
	function getAllUserListByPackageByDate($package, $level, $from_date, $to_date)
	{
		// $query = "select ula_non_working_user.id as id1, ula_non_working_level.id as id2, ula_non_working_user.levels, ula_non_working_user.user_id, ula_non_working_user.user_name, ula_non_working_level.level_complete_amount from ula_non_working_user join ula_non_working_level on ula_non_working_user.user_id = ula_non_working_level.user_id where .ula_non_working_user.package = '".$package."' and !FIND_IN_SET('".$level."', ula_non_working_user.levels) and ula_non_working_level.level_number = '".$level."' and ula_non_working_level.level_complete_amount != 0 and ((DATE(.ula_non_working_user.created_datetime) >= '".$from_date."') and (DATE(ula_non_working_user.created_datetime) <= '".$to_date."'))";

		$query = "select ula_non_working_user.id as id1, ula_non_working_level.id as id2, ula_non_working_user.levels, ula_non_working_user.user_id, ula_non_working_user.user_name, ula_non_working_level.level_complete_amount from ula_non_working_level join ula_non_working_user on ula_non_working_level.user_id = ula_non_working_user.user_id where ula_non_working_user.package = '".$package."' and !FIND_IN_SET('".$level."', ula_non_working_user.levels) and ula_non_working_level.package = '".$package."' and ula_non_working_level.level_number = '".$level."' and ula_non_working_level.level_complete_amount != 0 and ((DATE(.ula_non_working_user.created_datetime) >= '".$from_date."') and (DATE(ula_non_working_user.created_datetime) <= '".$to_date."'))";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getAllDistributionList()
	{		
		return $this->db->order_by('id', 'desc')->get('pw_non_working_distribution')->result_array();
	}	

}
