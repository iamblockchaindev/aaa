<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ticket_model extends CI_Model 
{
	function getAllTotalTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pw_tickets`";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}

	function getAllPendingTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pw_tickets` WHERE status = 'open'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}

	function getAllInProcessTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pw_tickets` WHERE status = 'process'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}	

	function getAllCloseTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pw_tickets` WHERE status = 'close'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}


	function getTicketListByType($start_limit, $end_limit)
	{
		$status = $this->input->get('type', true);	

		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.username like '%".$search_text."%'";
		}

		$query = "SELECT pw_tickets.*, pw_users.username FROM `pw_tickets` join `pw_users` on pw_tickets.user_id = pw_users.id WHERE `status` = '".$status."'".$where." order by pw_tickets.id desc LIMIT $start_limit, $end_limit";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getTicketListByTypeCount()
	{
		$status = $this->input->get('type', true);	

		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.username like '%".$search_text."%'";
		}	

		$query = "SELECT * FROM `pw_tickets` join `pw_users` on pw_tickets.user_id = pw_users.id WHERE `status` = '".$status."'".$where;
		$data = $this->db->query($query);

		return $data->num_rows();
	}


	function getMyTicketChatList()
	{
		$ticket_id = $this->input->get('ticket_id', true);	

		$query = "SELECT pw_tickets_chat.*, pw_users.profile_pic FROM pw_tickets_chat join pw_users on pw_tickets_chat.user_id = pw_users.id WHERE pw_tickets_chat.ticket_id = '".$ticket_id."' order by pw_tickets_chat.id asc";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

}
?>
