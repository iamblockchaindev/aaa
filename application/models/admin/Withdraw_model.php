<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Withdraw_model extends CI_Model 
{
	function getAllPendingWithdrawal($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.username like '%".$search_text."%'";
		}

		$query = "select pw_withdrawal.*, pw_users.full_name from pw_withdrawal join pw_users on pw_withdrawal.session_id = pw_users.id where pw_withdrawal.status = 'P'".$where." order by pw_withdrawal.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getAllPendingWithdrawalCount()
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.username like '%".$search_text."%'";
		}

		$query = "select pw_withdrawal.*, pw_users.full_name from pw_withdrawal join pw_users on pw_withdrawal.session_id = pw_users.id where pw_withdrawal.status = 'P'".$where;

		$data = $this->db->query($query);
		return $data->num_rows();
	}

	function getAllGivenWithdrawal($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.username like '%".$search_text."%'";
		}

		$query = "select pw_withdrawal.*, pw_users.full_name from pw_withdrawal join pw_users on pw_withdrawal.session_id = pw_users.id where pw_withdrawal.status = 'G'".$where." order by pw_withdrawal.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getAllGivenWithdrawalCount()
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.username like '%".$search_text."%'";
		}

		$query = "select pw_withdrawal.*, pw_users.full_name from pw_withdrawal join pw_users on pw_withdrawal.session_id = pw_users.id where pw_withdrawal.status = 'G'".$where;

		$data = $this->db->query($query);
		return $data->num_rows();
	}

	function getAllRejectedWithdrawal($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.username like '%".$search_text."%'";
		}

		$query = "select pw_withdrawal.*, pw_users.full_name from pw_withdrawal join pw_users on pw_withdrawal.session_id = pw_users.id where pw_withdrawal.status = 'R'".$where." order by pw_withdrawal.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getAllRejectedWithdrawalCount()
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.username like '%".$search_text."%'";
		}

		$query = "select pw_withdrawal.*, pw_users.full_name from pw_withdrawal join pw_users on pw_withdrawal.session_id = pw_users.id where pw_withdrawal.status = 'R'".$where;

		$data = $this->db->query($query);
		return $data->num_rows();
	}

}
?>
