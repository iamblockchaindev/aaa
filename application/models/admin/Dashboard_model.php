<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model 
{
	function getTotalUserListCount()
	{
		$query = "select count(*) as total_users from pw_users where is_delete = '1'";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_users;
		}
		else
		{
			return 0;
		}
	}

	function getTotalPaidUsers()
	{
		$query = "select count(*) as total_users from pw_users where package is not null and is_delete = '1'";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_users;
		}
		else
		{
			return 0;
		}
	}

	function getTotalUnPaidUsers()
	{
		$query = "select count(*) as total_users from pw_users where package is null and is_delete = '1'";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_users;
		}
		else
		{
			return 0;
		}
	}

	function getTodayActiveUsers()
	{
		$query = "select count(*) as total_users from pw_users where package is not null and is_delete = '1' and date_format(upgrade_time, '%Y-%m-%d') = '".date('Y-m-d')."'";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_users;
		}
		else
		{
			return 0;
		}
	}

	function getTodayBusiness()
	{
		$query = "select sum(package) as total_amount from pw_non_working_user where date_format(created_datetime, '%Y-%m-%d') = '".date('Y-m-d')."'";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_amount;
		}
		else
		{
			return 0;
		}
	}

	function getTotalBusiness()
	{
		$query = "select sum(package) as total_amount from pw_non_working_user";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_amount;
		}
		else
		{
			return 0;
		}
	}

	function getTodayWithdrawal()
	{
		$query = "select sum(amount) as total_amount from pw_withdrawal where status = 'G' and date_format(given_datetime, '%Y-%m-%d') = '".date('Y-m-d')."'";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_amount;
		}
		else
		{
			return 0;
		}
	}
	function getTodayPendingWithdrawal(){
		$query = "select sum(amount) as total_amount from pw_withdrawal where status = 'P' and date_format(given_datetime, '%Y-%m-%d') = '".date('Y-m-d')."'";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_amount;
		}
		else
		{
			return 0;
		}
	}
	function getTotalWithdrawal()
	{
		$query = "select sum(amount) as total_amount from pw_withdrawal where status = 'G'";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_amount;
		}
		else
		{
			return 0;
		}
	}
	function getTotalPendingWithdrawal(){
		$query = "select sum(amount) as total_amount from pw_withdrawal where status = 'P'";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			$result = $data->row();

			return $result->total_amount;
		}
		else
		{
			return 0;
		}
	}

	function getpaidunpaiduser($type){
		if($type==1){
		$query = "select * from pw_users where package is not null and is_delete = '1'";
		}else{
		$query = "select * from pw_users where package is null and is_delete = '1'";
		}
		return $this->db->query($query)->result();
	}
}
?>
