<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Document_model extends CI_Model
{
	function getPendingDocumentList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select pw_users_document.*, pw_users.full_name from pw_users_document join pw_users on pw_users_document.user_id = pw_users.id where pw_users_document.status = 0" . $where . " LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getPendingDocumentList1($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select pw_blueniletrip.*, pw_users.full_name from pw_blueniletrip join pw_users on pw_blueniletrip.user_id = pw_users.id where pw_blueniletrip.verfied_ornot = 0" . $where . " LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getPendingDocumentListCount()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select * from pw_users_document join pw_users on pw_users_document.user_id = pw_users.id where pw_users_document.status = 0" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getPendingDocumentListCount1()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select * from pw_blueniletrip join pw_users on pw_blueniletrip.user_id = pw_users.id where pw_blueniletrip.verfied_ornot = 0" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getApprovedDocumentList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select pw_users_document.*, pw_users.full_name from pw_users_document join pw_users on pw_users_document.user_id = pw_users.id where pw_users_document.status = 1" . $where . " order by pw_users_document.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getApprovedDocumentList1($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select pw_blueniletrip.*, pw_users.full_name from pw_blueniletrip join pw_users on pw_blueniletrip.user_id = pw_users.id where pw_blueniletrip.verfied_ornot = 1" . $where . " order by pw_blueniletrip.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getApprovedDocumentListCount()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select * from pw_users_document join pw_users on pw_users_document.user_id = pw_users.id where pw_users_document.status = 1" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getApprovedDocumentListCount1()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select * from pw_blueniletrip join pw_users on pw_blueniletrip.user_id = pw_users.id where pw_blueniletrip.verfied_ornot = 1" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getRejectedDocumentList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select pw_users_document.*, pw_users.full_name from pw_users_document join pw_users on pw_users_document.user_id = pw_users.id where pw_users_document.status = 2" . $where . " order by pw_users_document.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getRejectedDocumentList1($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select pw_blueniletrip.*, pw_users.full_name from pw_blueniletrip join pw_users on pw_blueniletrip.user_id = pw_users.id where pw_blueniletrip.verfied_ornot = 2" . $where . " order by pw_blueniletrip.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getRejectedDocumentListCount()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select * from pw_users_document join pw_users on pw_users_document.user_id = pw_users.id where pw_users_document.status = 2" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getRejectedDocumentListCount1()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pw_users.full_name like '%" . $search_text . "%'";
		}

		$query = "select * from pw_blueniletrip join pw_users on pw_blueniletrip.user_id = pw_users.id where pw_blueniletrip.verfied_ornot = 2" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}
}
