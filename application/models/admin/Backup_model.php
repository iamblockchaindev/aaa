<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Backup_model extends CI_Model {
    
	function getAllBackupList($start_limit, $end_limit)
	{
		$query = "select * from pw_database_backup order by id desc LIMIT $start_limit, $end_limit";
		$data = $this->db->query($query);

		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getAllBackupListCount()
	{
		$query = "select * from pw_database_backup order by id desc";
		$data = $this->db->query($query);

		return $data->num_rows();		
	}

}
?>
