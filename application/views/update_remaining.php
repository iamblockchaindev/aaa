<?php $this->load->view('common/header'); ?>

<style type="text/css">
label.error
{
  color: red;
  padding: 0 !important;
}

.success_message
{
  color: green;
}

</style>

    <!--header section start-->
		<section class="breadcrumb-section contact-bg section-padding">
			<div class="container">
			    <div class="row">
			        <div class="col-md-6 col-md-offset-3 text-center">
			            <h1>Complete your profile</h1>
			             <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p> -->
			        </div>
			    </div>
			</div>
		</section><!--Header section end-->

<!--login section start-->
<div class="login-section section-padding login-bg">
	<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="bottom_login_head">
        <h2>Complete Profile</h2>
      </div>  
    </div>    
  </div>
		<div class="row">
			 <div class="col-md-6 col-md-offset-3">
      <div class="main-login main-center">        

<?php echo validation_errors(); ?>

          <form id="data_form" name="data_form" class="form-horizontal" action="" method="post" >
            
               <div class="form-group">
                <label for="username" class="cols-sm-2 control-label">Country</label>
                <div class="cols-sm-10">

                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fas fa-globe-asia"></i>
                    </span>
                  
                    <select name="country" id="country" class="form-control myinput required" onchange="change_country();">

                    <option value="">Select</option>
                    <?php
                    $country_list = getCountryList();

                    if(!empty($country_list))
                    {
                      foreach ($country_list as $row) 
                      {
                      ?>
                        <option value="<?php echo $row->id; ?>" data-countrycode="<?php echo $row->phonecode; ?>" <?php if(!empty($country_code) &&($country_code == $row->sortname)) echo "selected"; ?>><?php echo $row->name; ?></option>
                      <?php
                      }
                    }
                    ?>
                    </select>
                  </div>
                </div>
              </div>

               <div class="form-group">
                <label for="username" class="cols-sm-2 control-label">State</label>
                <div class="cols-sm-10">

                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fas fa-globe-asia"></i>
                    </span>
                  
                    <select name="state" id="state" class="form-control myinput required" onchange="change_state();">

                    <option value="">Select</option>

                    </select>
                  </div>
                </div>
              </div>

               <div class="form-group">
                <label for="username" class="cols-sm-2 control-label">City</label>
                <div class="cols-sm-10">

                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fas fa-globe-asia"></i>
                    </span>
                  
                    <select name="city" id="city" class="form-control myinput required">

                    <option value="">Select</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group ">
                <button type="button" class="submit-btn btn btn-lg btn-block login-button submit_button_class" onclick="submit_data();" style="width: 30%">Submit</button>
              </div>

          </form>
        </div>   
       </div>
		</div>
	</div>
</div>
<!--login section end-->
    
<?php $this->load->view('common/footer'); ?>

<script src="<?php echo base_url(); ?>assets/user_panel/js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/user_panel/js/bootstrap-dialog.js"></script>

<script type="text/javascript">

$("#data_form").validate({

    errorPlacement: function(error, element) 
    {
          //element.before(error);

          element.parents('div.input-group').after(error);
    }   
});

function submit_data() 
{
  var form = $("#data_form");
  if(form.valid() == false)
  {
      return false;
  }
  else
  {
    $(".submit_button_class").html('<i class="fa fa-spinner" aria-hidden="true"></i>');
    $(".submit_button_class").attr('disabled', true);

    $("#data_form").submit();
  }
}

function change_country() 
{
    $("#state").html("<option value=''>Loading</option>");

    var country_id = $("#country").val();

    var dataString = 'country_id='+country_id;
    var url = '<?php echo base_url(); ?>login/getStateListByCountryID';

    $.ajax({
    type:"POST",
    data: dataString,
    url: url,
    dataType:'html',
    success:function(response)
    {
        $("#state").html(response);
    }

    });

}

function change_state()
{
    $("#city").html("<option value=''>Loading</option>");

    var state_id = $("#state").val();    

    var dataString = 'state_id='+state_id;
    var url = '<?php echo base_url(); ?>login/getCityListByStateID';

    $.ajax({
    type:"POST",
    data: dataString,
    url: url,
    dataType:'html',
    success:function(response)
    {
        $("#city").html(response);
    }

    });
}

</script>