<?php
$session_data = getSessionData();
?>
<html>

<head>
    <link href="https://fonts.googleapis.com/css2?family=Share&display=swap" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <style>
        @-webkit-keyframes blinker {
            from {
                opacity: 1.0;
            }

            to {
                opacity: 0.0;
            }




        }

        .blink {
            text-decoration: blink;
            -webkit-animation-name: blinker;
            -webkit-animation-duration: 0.6s;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: ease-in-out;
            -webkit-animation-direction: alternate;
        }

        @media screen and (max-width: 600px) {
            .ftr {
                font-size: smaller;
            }
        }
    </style>




   <!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"/LFfv1ah9W20em", domain:"pointwish.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=/LFfv1ah9W20em" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->  


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-177830641-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-177830641-1');
    </script>

    <!-- END:Global site tag (gtag.js) - Google Analytics -->


    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">

    <title>Point Wish</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/front/image/favicon.png') ?>">

    <header class="header-one">
        <div id="preloader"></div>


        <!-- Start top bar -->

        <!-- End top bar -->
        <!-- header-area start -->



        <div id="sticker" class="header-area hidden-xs stick">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <!-- logo start -->
                            <div class="col-md-3 col-sm-3">
                                <div class="logo">
                                    <!-- Brand -->
                                    <a class="navbar-brand page-scroll" href="<?php echo base_url(); ?>home">
                                        <img src="<?= base_url('assets/front/image/head.png') ?>" alt="">
                                    </a>
                                </div>
                                <!-- logo end -->
                            </div>
                            <div class="col-md-9 col-sm-9">
                                <div class="header-right-link">
                                    <!-- search option end -->
                                    <?php
                                    if ($this->session->userdata('user_id')) {
                                    ?>
                                        <a class="s-menu" href="<?= base_url('user/dashboard') ?>">Dashboard</a>
                                    <?php
                                    } else {
                                    ?>
                                        <a class="s-menu" href="<?= base_url('login') ?>">Login</a>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <!-- mainmenu start -->
                                <nav class="navbar navbar-default">
                                    <div class="collapse navbar-collapse" id="navbar-example">
                                        <div class="main-menu">
                                            <ul class="nav navbar-nav navbar-right">
                                                <li><a style="color: #0F0F80;" class="pages" href="<?= base_url() ?>home">Home</a></li>
                                                <li><a class="pages" href="<?php echo base_url(); ?>home/#news">News</a></li>
                                                <li><a class="pages" href="<?php echo base_url(); ?>home/#pool">Pool</a></li>
                                                <li><a class="pages" href="<?php echo base_url(); ?>home/#achiever">Achiever</a></li>
                                                <li><a href="<?php echo base_url(); ?>page/about">About us</a></li>
                                                <li><a class="pages" href="<?php echo base_url(); ?>page/contact_us">Contact Us</a></li>
                                                

                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                                <!-- mainmenu end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header-area end -->
        <!-- mobile-menu-area start -->
        <div class="mobile-container">

            <!-- Top Navigation Menu -->
            <div class="topnav" style="display: none;">
                <a href="#home" class="active"><img src="<?= base_url('assets/front/image/head.png') ?>" height="50px" width="200px;"></a>
                <div id="myLinks">
                    <a class="pages" href="<?= base_url() ?>home">Home</a>
                    <a class="pages" href="<?php echo base_url(); ?>home/#news">News</a>
                    <a class="pages" href="<?php echo base_url(); ?>home/#pool">Pool</a>
                    <a class="pages" href="<?php echo base_url(); ?>home/#achiever">Achiever</a>
                    <a href="<?php echo base_url(); ?>page/about">About us</a>
                    <a class="pages" href="<?php echo base_url(); ?>page/contact_us">Contact Us</a>
                    <?php if ($this->session->userdata('user_id')) { ?>
                        <a href="<?= base_url('user/dashboard') ?>">Dashboard</a>
                        <?php } else { ?>
                        <a href="<?= base_url('login') ?>">Login / Register</a>
                        <?php
                        }
                        ?>
                </div>
                <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                    <i class="fa fa-bars" style="margin-top: 14px; color:black;"></i>
                </a>
            </div>



            <!-- End smartphone / tablet look -->
        </div>
        <!-- mobile-menu-area end -->

    </header>