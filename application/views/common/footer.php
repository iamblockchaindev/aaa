<section id="contact">
    <footer class="footer1">
        <div class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="footer-content logo-footer">
                            <div class="footer-head">
                                <div class="footer-logo">
                                    <a class="footer-black-logo" href="#"><img src="#" alt=""></a>
                                </div>
                                <p>
                                    We are ready to help our customer. <br>More relaiable, more secure and more reward. 
                                </p>
                                <div class="subs-feilds">
                                    <div class="suscribe-input">
                                        <input type="email" class="email form-control width-80" id="sus_email"
                                            placeholder="Type Email">
                                        <button type="submit" id="sus_submit" class="add-btn">Subscribe</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end single footer -->

                    <div class="col-md-4 col-sm-3 col-xs-12">
                        <div class="footer-content">
                            <div class="footer-head">
                                <h4>Services Link</h4>
                                <ul class="footer-list">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user mx-auto"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg><a style="color: white; margin-left:5%; line-height: 1.8;" href="<?php echo base_url(); ?>page/about">About us</a>
                                    <!--<br><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone-call mx-auto"><path d="M15.05 5A5 5 0 0 1 19 8.95M15.05 1A9 9 0 0 1 23 8.94m-1 7.98v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg><a style="color: white; margin-left:5%;" href="<?= base_url()?>page/contact_us">Franchise</a>-->
                                    <br><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mx-auto"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg><a href="<?php echo base_url(); ?>page/terms" style="color: white; margin-left:5%; line-height: 1.8;" class="text-center text-xs mt-2">T & C</a>
                                    <br><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-unlock mx-auto"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 9.9-1"></path></svg><a href="<?php echo base_url(); ?>page/privacy" style="color: white; margin-left:5%;">Privacy</a>

                                </ul>
                                <!--<ul class="footer-list hidden-sm">-->
                                <!--    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mx-auto"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg><a href="<?php echo base_url(); ?>page/terms" style="color: white; margin-left:5%; line-height: 1.8;" class="text-center text-xs mt-2">T & C</a>-->
                                <!--    <br><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-unlock mx-auto"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 9.9-1"></path></svg><a href="<?php echo base_url(); ?>page/privacy" style="color: white; margin-left:5%;">Privacy</a>-->
                                <!--</ul>-->
                            </div>
                        </div>
                    </div>
                    <!-- end single footer -->
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="footer-content last-content">
                            <div class="footer-head">
                                <h4>Information</h4>
                                <div class="footer-contacts">
                                    <!--<p><span>Tel :</span> +00 0000000000</p>-->
                                    <p><span>Email :</span> support@pointwish.com</p>
                                    <p><span>Location :</span> 332, Cranbook Road, U.K</p>
                                    <p style="margin-left: 12%"><a class="social" href="https://www.facebook.com/Pointwishofficial-115264473669383" target="_blank"><img src="<?= base_url('assets/front/image/social_media/facebook.png') ?>" height="30px;" width="30px;" ></a>
                                <a href="https://www.instagram.com/pointwishofficial/" target="_blank"><img src="<?= base_url('assets/front/image/social_media/instagram.png') ?>" height="30px;" width="30px;" style="margin-left:2px;"></a>
                                <a href="support@pointwish.com" target="_blank"><img src="<?= base_url('assets/front/image/social_media/mail.png') ?>" height="30px;" width="30px;" style="margin-left:2px;"></a>
                                <a href="https://t.me/joinchat/SGZOmhWDIW2OKLPrvtfErA" target="_blank"><img src="<?= base_url('assets/front/image/social_media/telegram.png') ?>"  height="30px;" width="30px;" style="margin-left:2px;"></a>
                                <a href="https://twitter.com/pointwishoffic1" target="_blank"><img src="<?= base_url('assets/front/image/social_media/twitter.png') ?>"  height="30px;" width="30px;" style="margin-left:2px;"></a><br>
                                <a href="https://www.youtube.com/channel/UCb2bZnQWhQ8PIQA4qK_bDhA/" target="_blank"><img src="<?= base_url('assets/front/image/social_media/youtube.png') ?>" height="30px;" width="30px;" style="margin-left:2px; margin-top:10px"></a>
                                <a href="https://in.pinterest.com/pointwishofficial/_saved/" target="_blank"><img src="<?= base_url('assets/front/image/social_media/pintrest.png') ?>" height="30px;" width="30px;" style="margin-left:2px; margin-top:10px"></a>
                                <a href="https://www.reddit.com/user/pointwishofficial/" target="_blank"><img src="<?= base_url('assets/front/image/social_media/reddit.png') ?>" height="30px;" width="30px;" style="margin-left:2px; margin-top:10px;"></a>
                                <a href="https://discord.com/channels/@me" target="_blank"><img src="<?= base_url('assets/front/image/social_media/Discord_New.png') ?>" height="30px;" width="30px;" style="margin-left:2px; margin-top:10px;"></a>
                                <a href="https://medium.com/@pointwishofficial" target="_blank"><img src="<?= base_url('assets/front/image/social_media/medium.png') ?>" height="30px;" width="30px;" style="margin-left:2px; margin-top:10px;"></a>

                                    </p>

                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-area-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="copyright">
                            <p class="ftr" style="text-align: center;">
                                COPY RIGHTS © 2020 POINTWISH LTD. ALL RIGHTS RESERVED.
                                
                                
                            </p>
                            
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>
    </footer>
</section>