<?php $this->load->view('common/header'); ?>

<style type="text/css">
label.error
{
  color: red;
  padding: 0 !important;
}

.success_message
{
  color: green;
}

</style>

    <!--header section start-->
		<section class="breadcrumb-section contact-bg section-padding">
			<div class="container">
			    <div class="row">
			        <div class="col-md-6 col-md-offset-3 text-center">
			            <h1>Mobile Verification</h1>
			             <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p> -->
			        </div>
			    </div>
			</div>
		</section><!--Header section end-->

<!--login section start-->
<div class="login-section section-padding login-bg">
	<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="bottom_login_head">
        <h2>Mobile Verification</h2>
      </div>  
    </div>    
  </div>
		<div class="row">
			 <div class="col-md-6 col-md-offset-3">
      <div class="main-login main-center">        

<?php echo validation_errors(); ?>

          <form id="data_form" name="data_form" class="form-horizontal" action="" method="post" >
            
              <div class="form-group">
                <label for="password" class="cols-sm-2 control-label">Mobile Number</label>
                <div class="cols-sm-10">
                  <div class="input-group">
                    <span class="input-group-addon" id="country_code_text"><?php if(!empty($user_data->country_code)) echo $user_data->country_code; ?></span>
                    <input type="hidden" id="country_code" name="country_code">

                    <input type="text" class="form-control required" name="mobile_number" id="mobile_number"  placeholder="Your Mobile Number" value="<?php if(!empty($user_data->mobile)) echo $user_data->mobile; ?>" onkeypress="return isNumber(event);"/>
                  </div>

                  <button type="button" class="btn btn-success login-button submit_button_class pull-right" id="send_otp_button" onclick="send_otp();" style="width: 25%" disabled="">Send OTP</button> 

                </div>
              </div>

              <div class="form-group">
                <label for="password" class="cols-sm-2 control-label">Enter Mobile OTP</label>
                <div class="cols-sm-10">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fas fa-lock"></i></span>
                    <input type="text" class="form-control required" name="mobile_otp" id="mobile_otp"  placeholder="Your Mobile OTP" onkeypress="return isNumber(event);"/>
                  </div>    
                  <span id="otp_message" class="success_message"></span>

                </div>
              </div>

              <div class="form-group ">
                <button type="button" class="submit-btn btn btn-lg btn-block login-button submit_button_class" onclick="submit_data();" style="width: 30%">Submit</button>
              </div>

          </form>
        </div>   
       </div>
		</div>
	</div>
</div>
<!--login section end-->
    
<?php $this->load->view('common/footer'); ?>

<script src="<?php echo base_url(); ?>assets/user_panel/js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/user_panel/js/bootstrap-dialog.js"></script>

<script type="text/javascript">

var errorMsg = '', $valid = false;
$.validator.addMethod("is_otp_verify",function(val, elem){
   $.ajax({
      async:false,
      url:'<?php echo base_url(); ?>login/is_otp_verify',
      type:"get",
      dataType:"json",
      data : {mobile_otp:$("#mobile_otp").val()},
      success:function(response)
      {
         if(response.status == 1)
         {
            errorMsg = 'OTP is Correct !';
            $valid = true;

            $("#otp_message").html('Correct OTP');

         }
         else if(response.status == 2)
         {
            errorMsg = 'Incorrect OTP !';
            $valid = false;

            $("#otp_message").html('');
         }
      }
   });

   $.validator.messages["is_otp_verify"] = errorMsg;
   return $valid;
},'');


$("#data_form").validate({

    rules: { 
        mobile_otp: {  
            required: true,
            is_otp_verify: true,
        },  
    },  
    errorPlacement: function(error, element) 
    {
      element.parents('div.input-group').after(error);
    }    
});

function submit_data() 
{
  var form = $("#data_form");
  if(form.valid() == false)
  {
      return false;
  }
  else
  {
    $(".submit_button_class").html('<i class="fa fa-spinner" aria-hidden="true"></i>');
    $(".submit_button_class").attr('disabled', true);

    $("#data_form").submit();
  }
}

function isNumber(evt) 
{
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
  return false;
  }
  return true;
}

function send_otp()
{
  $("#send_otp_button").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
  $("#send_otp_button").attr('disabled', true);

  var token = "<?php if(!empty($user_data->token)) echo $user_data->token; ?>";

  var mobile_number = $("#mobile_number").val();
  var dataString = "token="+token+"&mobile_number="+mobile_number;
  var url = '<?php echo base_url(); ?>login/send_otp';

  $.ajax({
    async:false,
    type:"POST",
    data:dataString,
    url:url,
    dataType:"json",
    success:function(response)
    {
      timer(60);

      BootstrapDialog.show({
      title: "Message",
      message: "Please check your mobile number for otp !",
      });   

    }
  });
}
</script>

<script>
/********Count Down**Start********/
function timer(remaining) {
  let timerOn = true;

  var m = Math.floor(remaining / 60);
  var s = remaining % 60;
  
  m = m < 10 ? '0' + m : m;
  s = s < 10 ? '0' + s : s;
  document.getElementById('send_otp_button').innerHTML = m + ':' + s;
  remaining -= 1;
  
  if(remaining >= 0 && timerOn) {
    setTimeout(function() {
        timer(remaining);
    }, 1000);
    return;
  }

  if(!timerOn) {
    // Do validate stuff here
    return;
  }
  
  // Do timeout stuff here
  $("#send_otp_button").html('Send OTP')
  $("#send_otp_button").attr('disabled', false);
}
/********Count Down**End********/
</script>

<?php
  if(!empty($user_data->otp_send_time))
  {
    $is_expired = otp_send_eligiblity($user_data->otp_send_time);
    $seconds = timeIntervalinMin();

    if($is_expired == 1)
    {
    ?>
      <script type="text/javascript">
        timer(60-<?php echo $seconds; ?>);
      </script>
    <?php
    }
    else
    {
    ?>
      <script type="text/javascript">
        $("#send_otp_button").attr('disabled', false);
      </script>      
    <?php
    }
  }
  else
  {
  ?>
      <script type="text/javascript">
        $("#send_otp_button").attr('disabled', false);
      </script>  
  <?php
  }
?>
