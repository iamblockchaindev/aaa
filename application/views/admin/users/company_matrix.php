<?php $this->load->view('admin/common/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" ng-controller="User_Controller" ng-cloak>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (!empty($title)) echo $title; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Income Distribution</a></li>
            <li class="active"><?php if (!empty($title)) echo $title; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php if (!empty($title)) echo $title; ?>
                        </h3>

                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo validation_errors(); ?>

                        <form id="data_form" name="data_form" action="" method="POST">

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Package</label>
                                            <select class="form-control required" id="package" name="package" ng-model="package">
                                                <option value="">Select</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="100">100</option>
                                                <option value="1000">1000</option>
                                                <option value="5000">5000</option>
                                                <option value="10000">10000</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Company Level</label>
                                            <select class="form-control required" id="level" name="level" ng-model="level" ng-change="get_total_users(level, package)">
                                                <option value="">Select</option>
                                                <option value="1">Level 1</option>
                                                <option value="2">Level 2</option>
                                                <option value="3">Level 3</option>
                                                <option value="4">Level 4</option>
                                                <option value="5">Level 5</option>
                                                <option value="6">Level 6</option>
                                                <option value="7">Level 7</option>
                                                <option value="8">Level 8</option>
                                                <option value="9">Level 9</option>
                                            </select>
                                            <b ng-show="trees_count">Total Users: </b>{{trees_count}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Pool Level</label>
                                            <select required class="form-control required" id="pool_level" name="pool_level" ng-model="pool_level">
                                                <option value="">Select</option>
                                                <option value="1">Level 1</option>
                                                <option value="2">Level 2</option>
                                                <option value="3">Level 3</option>
                                                <option value="4">Level 4</option>
                                                <option value="5">Level 5</option>
                                                <option value="6">Level 6</option>
                                                <option value="7">Level 7</option>
                                                <option value="8">Level 8</option>
                                                <option value="9">Level 9</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">From Date</label>
                                            <input type="date" class="form-control required" ng-model="start" id="from_date" name="from_date" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">To Date</label>
                                            <input type="date" min="start" ng-model="end" class="form-control required" id="to_date" name="to_date" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <button type="button" class="btn btn-success" ng-click="get_level_matrix();"> Get Users</button>
                                    <button ng-show="trees.length && package == 30 " type="button" class="btn btn-success" ng-click="submit_data(data_form);"> Generate Income</button>
                                    <button ng-show="trees.length && package != 30" type="button" class="btn btn-success" ng-click="generate_jade_income(data_form);"> Generate Income</button>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="fa fa-3x fa-spin fa-spinner mailbox-attachment-icon" ng-show="matrixLoader">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>

                    <div ng-show="!matrixLoader && trees.length" style="overflow:auto">
                        <table datatable="ng" class="display table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Member ID</th>
                                    <th>Member Name</th>
                                    <th>DOJ</th>
                                    <th>Upgrade Date</th>
                                    <th>Level</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="tree in trees">
                                    <td>{{$index+1}}</td>
                                    <td>{{tree.username}}</td>
                                    <td>{{tree.full_name}}</td>
                                    <td>{{tree.created_time}}</td>
                                    <td>{{tree.upgrade_time}}</td>
                                    <td>{{tree.level}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->


            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<!-- <script type="text/javascript">
    $(function() {
        $("#from_date").datepicker({
            numberOfMonths: 1,
            onSelect: function(selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#to_date").datepicker("option", "minDate", dt);
                $("#from_date").datepicker("option", "dateFormat", "dd-mm-yy");
            },

        });
        $("#to_date").datepicker({
            numberOfMonths: 1,
            onSelect: function(selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() - 1);
                $("#from_date").datepicker("option", "maxDate", dt);
                $("#to_date").datepicker("option", "dateFormat", "dd-mm-yy");
            }
        });
    });
</script> -->