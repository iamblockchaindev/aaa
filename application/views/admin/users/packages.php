<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Users Management</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">

            <div>
              <form action="<?php echo base_url('admin/user/packages'); ?>" method="POST">
                <table class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <th>Status</th>
                    <th>Leavels</th>
                    <th>Package</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Actions</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div class="form-group">
                          <div class="form-line">
                            <select class="form-control" id="status" name="status" placeholder="Select Leavel">
                              <option value="">Select</option>
                              <!-- <option value="bank">Bank</option> -->
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                            </select>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <div class="form-line">
                            <select class="form-control" id="leave_id" value="<?= isset($leavel) ? $leavel : '' ?>" name="leave_id" placeholder="Select Leavel">
                              <option value="">Select Leavel</option>
                              <!-- <option value="bank">Bank</option> -->
                              <option value="1" <?= isset($leavel) ? ($leavel == 1 ? 'selected' : '') : '' ?>>Leavel 1</option>
                              <option value="2" <?= isset($leavel) ? ($leavel == 2 ? 'selected' : '') : '' ?>>Leavel 2</option>
                              <option value="3" <?= isset($leavel) ? ($leavel == 3 ? 'selected' : '') : '' ?>>Leavel 3</option>
                              <option value="4" <?= isset($leavel) ? ($leavel == 4 ? 'selected' : '') : '' ?>>Leavel 4</option>
                              <option value="5" <?= isset($leavel) ? ($leavel == 5 ? 'selected' : '') : '' ?>>Leavel 5</option>
                              <option value="6" <?= isset($leavel) ? ($leavel == 6 ? 'selected' : '') : '' ?>>Leavel 6</option>
                              <option value="7" <?= isset($leavel) ? ($leavel == 7 ? 'selected' : '') : '' ?>>Leavel 7</option>
                              <option value="8" <?= isset($leavel) ? ($leavel == 8 ? 'selected' : '') : '' ?>>Leavel 8</option>
                              <option value="9" <?= isset($leavel) ? ($leavel == 9 ? 'selected' : '') : '' ?>>Leavel 9</option>
                            </select>

                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <div class="form-line">
                            <select class="form-control" id="package" name="package" placeholder="Select Package">
                              <option value="">Select Package</option>
                              <!-- <option value="bank">Bank</option> -->
                              <option value="30">30</option>
                              <option value="40">40</option>
                              <option value="100">100</option>
                              <option value="1000">1000</option>
                              <option value="5000">5000</option>
                              <option value="10000">10000</option>                              
                            </select>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <div class="form-line">
                            <input class="form-control" type="date" placeholder="Start Date" />
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="form-group">
                          <div class="form-line">
                            <input type="date" class="form-control" placeholder="End Date" />
                          </div>
                        </div>
                      </td>
                      <td>
                        <button class="btn btn-success" type="submit">Search</button>
                        <a href="<?php echo base_url(); ?>admin/user/total_users"><button class="btn btn-info" type="button">Reset</button></a>
                      </td>
                    </tr>
                  </tbody>
                </table>

                <!-- <input style="background-image:url('<?php echo base_url(); ?>assets/admin_panel/images/searchicon.png');" type="text" class="search_input" id="search_text" name="search_text" placeholder="Search.." value="<?php if (!empty($_GET['search_text'])) echo $_GET['search_text']; ?>" > -->





              </form>
            </div>

            <div class="table-responsive">

              <p class="pull-right"><strong>Total Users:</strong> <?php if (!empty($total_records)) echo $total_records; ?></p>

              <div class="clearfix"></div>

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <table id="example1" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Login</th>
                    <th>Member ID</th>
                    <th>Password</th>
                    <th>Member Name</th>
                    <th>DOJ</th>
                    <th>Sponsor ID</th>
                    <th>Package</th>
                    <th>City</th>
                    <th>Upgrade Date</th>

                    <th>Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($data_result)) {
                    $CI = &get_instance();
                    $obj = $CI->load->model('admin/Users_model', 'users');
                    if (!empty($_GET['offset'])) {
                      $i = ($_GET['offset'] * 10) - 9;
                    } else {
                      $i = 1;
                    }

                    foreach ($data_result as $row) {
                      $cityName = $row->city != 0 ? $obj->users->getcityname($row->city) : '';


                  ?>
                      <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>
                        <td><a target="_blank" href="<?= site_url('admin/dashboard/default_login?id=' . base64_encode($row->id)) ?>" class="btn btn-info btn-sm">Login</a></td>
                        <td><?php echo $row->username; ?></td>
                        <td>
                          <?php echo $row->password_decrypted; ?>
                        </td>
                        <td><?php echo $row->full_name; ?></td>
                        <td><?php if (!empty($row->created_time)) echo  date('d M Y h:i A', strtotime($row->created_time)); ?>
                        </td>
                        <td><?php echo $row->sponsor; ?></td>
                        <td><?php echo $row->package; ?></td>
                        <td><?= !empty($cityName->name) ?  $cityName->name : '' ?></td>
                        <td><?php if (!empty($row->upgrade_time)) echo  date('d M Y h:i A', strtotime($row->upgrade_time)); ?></td>

                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-danger">Action</button>
                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="<?php echo base_url(); ?>admin/user/edit_user_profile?token=<?php echo $row->token; ?>">Edit</a></li>
                            </ul>
                          </div>
                        </td>

                      </tr>
                    <?php
                      $i++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="9" align="center">No Data Found !</td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>

              <?php
              if (!empty($links)) {
              ?>
                <!-- <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav> -->
              <?php
              }
              ?>

            </div>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    var table = $('#example1').DataTable({
      'columnDefs': [{
        'targets': 0,
        'checkboxes': {
          'selectRow': true
        }
      }],
      'select': {
        'style': 'multi'
      },
      'order': [
        [1, 'asc']
      ]
    });


  });
</script>

<?php $this->load->view('admin/common/footer'); ?>