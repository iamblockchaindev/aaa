<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">My Account</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
            <?php $url = $this->session->type == 2 ? 'admin/users/view_user' : 'admin/user/total_users';  ?>
            <a href="<?php echo base_url($url); ?>" class="btn btn-primary pull-right">Back</a>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <?php echo validation_errors(); ?>

            <form id="data_form" name="data_form" action="" method="POST" enctype="multipart/form-data">

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <div class="form-body">

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group has-success">
                      <label class="control-label">Full Name</label>
                      <input type="text" class="form-control required" id="full_name" name="full_name" value="<?php if (!empty($user_data->full_name)) echo $user_data->full_name; ?>">
                    </div>
                  </div>
                  <!--/span-->

                  <div class="col-md-4">
                    <div class="form-group has-success">
                      <label class="control-label">Username</label>
                      <input type="text" class="form-control required" value="<?php if (!empty($user_data->full_name)) echo $user_data->full_name; ?>" readonly="">
                    </div>
                  </div>
                  <!--/span-->

                  <div class="col-md-4">
                    <div class="form-group has-success">
                      <label class="control-label">Email</label>
                      <input type="text" name="email" class="form-control required" value="<?php if (!empty($user_data->email)) echo $user_data->email; ?>">
                    </div>
                  </div>
                  <!--/span-->

                  <div class="col-md-4">
                    <div class="form-group has-success">
                      <label class="control-label">Mobile</label>
                      <input type="text" class="form-control" id="mobile" name="mobile" value="<?php if (!empty($user_data->mobile)) echo $user_data->mobile; ?>">
                    </div>
                  </div>
                  <!--/span-->

                  <div class="col-md-4">
                    <div class="form-group has-success">
                      <label class="control-label">Address</label>
                      <input type="text" class="form-control" id="address" name="address" value="<?php if (!empty($user_data->address)) echo $user_data->address; ?>">
                    </div>
                  </div>
                  <!--/span-->

                </div>
                <!--/row-->

              </div>
              <!--/row-->

              <div class="row">


              </div>
              <!--/row-->


          </div>
          <div class="form-actions">
            <button type="submit" class="btn btn-success"> Update</button>
            <!-- <a href="#" class="btn btn-inverse">Cancel</a> -->
          </div>
          </form>
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->


    </div>
    <!-- /.col -->

</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>

<script type="text/javascript">
  $("#data_form").validate();
</script>