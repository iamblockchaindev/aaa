<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Users Management</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">

            <div>
              <form id="" name="" action="<?php echo base_url(); ?>admin/users/view_user" method="GET">
                <input style="background-image:url('<?php echo base_url(); ?>assets/admin_panel/images/searchicon.png');" type="text" class="search_input" id="search_text" name="search_text" placeholder="Search.." value="<?php if (!empty($_GET['search_text'])) echo $_GET['search_text']; ?>">
                <button class="btn btn-success" type="submit">Search</button>
                <a href="<?php echo base_url(); ?>admin/users/view_user"><button class="btn btn-info" type="button">Reset</button></a>



              </form>
            </div>

            <div class="table-responsive">

              <div class="clearfix"></div>

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>
              <?php if ($user) { ?>
                <table id="myTable" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>S.No.</th>
                      <th>Login</th>
                      <th>Member ID</th>
                      <th>Password</th>
                      <th>Member Name</th>
                      <th>DOJ</th>
                      <th>Sponsor ID</th>
                      <th>Package</th>
                      <th>Upgrade Date</th>
                      <th>Action </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr id="tr_<?php echo $user->id; ?>">
                      <td>1.</td>
                      <td><a target="_blank" href="<?= site_url('admin/login/default_login?id=' . base64_encode($user->id)) ?>" class="btn btn-info btn-sm">Login</a></td>
                      <td><?php echo $user->username; ?></td>
                      <td>
                        <?php echo $user->password_decrypted; ?>
                      </td>
                      <td><?php echo $user->full_name; ?></td>
                      <td><?php if (!empty($user->created_time)) echo  date('d M Y h:i A', strtotime($user->created_time)); ?>
                      </td>
                      <td><?php echo $user->sponsor; ?></td>
                      <td><?php echo $user->package; ?></td>
                      <td><?php if (!empty($user->upgrade_time)) echo  date('d M Y h:i A', strtotime($user->upgrade_time)); ?></td>
                      <td>
                        <div class="btn-group">
                          <a href="<?php echo base_url(); ?>admin/users/edit_user_profile?token=<?php echo $user->token; ?>"><button type="button" class="btn btn-primary">Edit</button></a>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              <?php } ?>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>