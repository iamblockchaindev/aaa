<?php
$filename = base64_decode($_GET['type'])==0 ? "Unpaid_Users_List" : "Paid_Users_List";
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$filename.".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <tr>
        <td colspan="8" rowspan="3">
            <center><b><?= base64_decode($_GET['type'])==0 ? "In-Active " : "Active " ?> Users List , Total users (<?= count($data)?>)</center>
        </td>
    </tr>
</table>

            <table border=1>
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>User ID</th>
                        <th>User Name</th>
                        <th>Country Code</th>
                       
                        <th>User Mobile No.</th>
                        <th>Email Address</th>
                        <th>Sponsor ID</th>
                        <th>Date Of Registration</th>        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                     $CI =& get_instance();
                     $obj = $CI->load->model('admin/Users_model','users');
                    
                    if(!empty($data)) { $i=1; foreach($data as $row) { 
                       
                    // $cityName = $row->city!=0 ? $obj->users->getcityname($row->city) : '';    
                    ?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $row->username ?></td>
                        <td><?= $row->full_name ?></td>
                        <td>+<?= $row->country_code ?></td>
                        <!-- <td><?= !empty($cityName->name) ?  $cityName->name : '' ?></td> -->
                        <td><?= $row->mobile ?></td>
                        <td><?= $row->email ?></td>
                        <td><?= $row->sponsor ?></td>
                        <td><?= date('d-m-Y',strtotime($row->created_time)) ?></td>
                        
                    </tr>
                    <?php } } else { ?> <?php } ?>
                </tbody>
            </table>
       