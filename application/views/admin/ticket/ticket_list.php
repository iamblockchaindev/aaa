<?php $this->load->view('admin/common/header'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ticket
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Ticket</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php if(!empty($total_pending_ticket)) echo $total_pending_ticket; else echo 0; ?> </h3>

              <p>Open Tickets</p>
            </div>
            <div class="icon">
              <i class="ion ion-email"></i>
            </div>
            <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=open" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php if(!empty($total_inprocess_ticket)) echo $total_inprocess_ticket; else echo 0; ?></h3>

              <p>In-process Tickets</p>
            </div>
            <div class="icon">
              <i class="ion ion-email"></i>
            </div>
            <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=process" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php if(!empty($total_close_ticket)) echo $total_close_ticket; else echo 0; ?></h3>

              <p>Closed Tickets</p>
            </div>
            <div class="icon">
              <i class="ion ion-email"></i>
            </div>
            <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=close" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->



    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>