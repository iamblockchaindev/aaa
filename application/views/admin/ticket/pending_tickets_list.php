<?php $this->load->view('admin/common/header'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if(!empty($title)) echo $title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Users Management</a></li>
        <li class="active"><?php if(!empty($title)) echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
              <?php if(!empty($title)) echo $title; ?>            
              </h3>
              <a href="<?php echo base_url(); ?>admin/ticket" class="btn btn-primary pull-right">Back</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">


            <div class="table-responsive">

        <p class="pull-right"><strong>Total Records:</strong> <?php if(!empty($total_records)) echo $total_records; ?></p>

<div class="clearfix"></div>

<?php
if($this->session->flashdata('error_message'))
{
?>
<div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata('error_message'); ?>
  </div>

<?php
}
?>

            <table id="myTable" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No.</th>                         
                        <th>Ticket ID</th>                        
                        <th>Member ID</th>                        
                        <th>Subject</th>                        
                        <th>First Name</th>                        
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Attachment</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if(!empty($data_result))
                {

                    if(!empty($_GET['offset']))
                    {
                      $i = ($_GET['offset'] * 10) - 9;
                    }
                    else
                    {
                      $i = 1;
                    }

                    foreach ($data_result as $row) 
                    {
                
                ?>
                    <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>
                        <td><?php echo $row->ticket_id; ?></td>
                        <td><?php echo $row->username; ?></td>
                        <td><?php echo $row->subject; ?></td>

                        <td><?php echo $row->first_name; ?></td>
                        <td><?php echo $row->last_name; ?></td>
                        <td><?php echo $row->email; ?></td>

                        <td>
                        <?php
                        if(!empty($row->attachment))
                        {
                        ?>
                            <a href="<?php echo base_url(); ?>uploads/ticket_attachment/<?php echo $row->attachment; ?>" class="btn btn-primary" download="">Download</a> 
                        <?php
                        }
                        ?>
                        </td>

                        <td id="status_area_<?php echo $row->ticket_id; ?>">
                            <?php
                            if($row->status == 'open')
                            {
                            ?>
                                <a href="javascript:void(0);" class="label label-default">Open</a>
                            <?php 
                            } 
                            else if($row->status == 'process')
                            {
                            ?>
                                <a href="javascript:void(0);" class="label label-danger">In Process</a>
                            <?php 
                            }
                            else if($row->status == 'close')
                            {
                            ?>
                                <a href="javascript:void(0);" class="label label-success">Closed</a>
                            <?php
                            }
                            ?>
                        </td>
                        
                        <td>                            
                            <a href="<?php echo base_url(); ?>admin/ticket/view_ticket?ticket_id=<?php echo $row->ticket_id; ?>&vt=o" class="btn btn-primary" target="_blank"> View Details</a>
                            <br>
                            <br>
                            <a href="<?php echo base_url(); ?>admin/ticket/view_chat?ticket_id=<?php echo $row->ticket_id; ?>&vt=o" class="btn btn-success" target="_blank"> Conversation</a>
                            <br>
                            <br>                            
                            <a href="javascript:void(0);" class="btn btn-danger" onclick="close_ticket('<?php echo $row->ticket_id; ?>');"> Close ticket</a>
                        </td>

                    </tr>
                <?php
                $i++;
                    }
                }
                else
                {
                ?>
                <tr><td colspan="9" align="center">No Data Found !</td></tr>
                <?php
                }
                ?>                   
                </tbody>
            </table>

            <?php
            if(!empty($links))
            {
            ?>
            <nav aria-label="Page navigation example">
              <ul class="pagination">
                <?php echo $links; ?>
              </ul>
            </nav>
            <?php
            }
            ?>
            
            </div>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>


<script type="text/javascript">
function close_ticket(id)
{
  BootstrapDialog.show({
  title: "Confirm",
  message: "Do you really want to perform this action ?",
  buttons: [
  {
  label: 'Yes',
  cssClass: 'btn-primary ',
  action: function(dialogItself){

  var url = '<?php echo base_url(); ?>admin/ticket/close_ticket';
  var dataString = 'id='+id;

  $.ajax({
    type:"POST",
    data:dataString,
    url:url,
    dataType:"json",
    success:function(response)
    {
      dialogItself.close();

      $("#status_area_"+id).html('close');

      BootstrapDialog.show({
      title: "Message",
      message: "Status Changed Successfully !",
      });
    }

  });    

  }
  },
  {
      label: 'No',
      cssClass: 'btn-warning',
      action: function(dialogItself){
      dialogItself.close();
   }
  }]
  });

}

</script>