<?php $this->load->view('admin/common/header'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if(!empty($title)) echo $title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">My Account</a></li>
        <li class="active"><?php if(!empty($title)) echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
              <?php if(!empty($title)) echo $title; ?>
                
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<?php echo validation_errors(); ?>

    <form id="data_form" name="data_form" action="" method="POST" enctype="multipart/form-data">
        <div class="form-body">


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group has-success">
                        <label class="control-label">Full Name</label>
                            <input type="text" class="form-control required" id="name" name="name" value="<?php if(!empty($admin_data->name)) echo $admin_data->name; ?>">
                         </div>
                </div>
                <!--/span-->

            </div>
            <!--/row-->  

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group has-success">
                        <label class="control-label">Username</label>
                            <input type="text" class="form-control required" id="username" name="username" value="<?php if(!empty($admin_data->username)) echo $admin_data->username; ?>">
                         </div>
                </div>
                <!--/span-->

            </div>
            <!--/row-->    
                                        
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group has-success">
                        <label class="control-label">Email</label>
                            <input type="text" class="form-control required" id="email" name="email" value="<?php if(!empty($admin_data->email)) echo $admin_data->email; ?>">
                         </div>
                </div>
                <!--/span-->

            </div>
            <!--/row-->    

        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-success"> Update</button>
            <!-- <a href="#" class="btn btn-inverse">Cancel</a> -->
        </div>
    </form>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>

<script type="text/javascript">
$("#data_form").validate();    
</script>
