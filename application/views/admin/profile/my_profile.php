<?php $this->load->view('admin/common/header'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if(!empty($title)) echo $title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">My Account</a></li>
        <li class="active"><?php if(!empty($title)) echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
              <?php if(!empty($title)) echo $title; ?>
                
              </h3>
              <a href="<?php echo base_url(); ?>admin/accounts/edit_profile" class="btn btn-primary pull-right">Edit</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<?php
if($this->session->flashdata('error_message'))
{
?>
<div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata('error_message'); ?>
  </div>

<?php
}
?>

              <table class="table table-hover">
                <tr>
                    <td style="width: 30%"><strong>Full Name:</strong></td>
                    <td><?php if(!empty($user_data->name)) echo $user_data->name; ?></td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>Username:</strong></td>
                    <td><?php if(!empty($user_data->username))  echo $user_data->username; ?></td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>Email:</strong></td>
                    <td><?php if(!empty($user_data->email))  echo $user_data->email; ?></td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>Last Login:</strong></td>
                    <td><?php echo date('d-m-Y', strtotime($user_data->last_login)); ?></td>
                </tr> 
              </table>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>