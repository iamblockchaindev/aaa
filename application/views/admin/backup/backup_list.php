<?php $this->load->view('admin/common/header'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if(!empty($title)) echo $title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php if(!empty($title)) echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
              <?php if(!empty($title)) echo $title; ?>
                
              </h3>

              <a href="javascript:void(0);" onclick="create_backup();"> <button type="button" class="btn btn-primary pull-right">Create Backup</button></a>

            </div>
            <!-- /.box-header -->
            <div class="box-body ">


            <div class="table-responsive">

        <p class="pull-right"><strong>Total Records:</strong> <?php if(!empty($total_records)) echo $total_records; ?></p>

<div class="clearfix"></div>

<?php
if($this->session->flashdata('error_message'))
{
?>
<div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata('error_message'); ?>
  </div>

<?php
}
?>

            <table id="myTable" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No.</th>  
                        <th>Backup Date</th>    
                        <th></th>                        
                        <th>Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                <?php
                if(!empty($data_result))
                {
                    if(!empty($_GET['offset']))
                    {
                      $i = ($_GET['offset'] * 10) - 9;
                    }
                    else
                    {
                      $i = 1;
                    }
                    
                    foreach ($data_result as $row) 
                    {
                
                ?>
                    <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>
                        <td><?php echo date('d-m-Y h:i A', strtotime($row->created_datetime)); ?></td>
                        <td>
                        <a class="btn btn-success" href="<?php echo base_url();?>uploads/database_backup/<?php echo $row->file_name; ?>" download>Download</a>
                        </td>

                        <td>
                            <button class="btn btn-primary" onclick="delete_backup('<?php echo $row->id; ?>', '<?php echo $row->file_name; ?>')">Delete</button>
                        </td>

                    </tr>
                <?php
                $i++;
                    }
                }
                else
                {
                ?>
                <tr><td colspan="9" align="center">No Data Found !</td></tr>
                <?php
                }
                ?>                    
                </tbody>
            </table>

            <?php
            if(!empty($links))
            {
            ?>
            <nav aria-label="Page navigation example">
              <ul class="pagination">
                <?php echo $links; ?>
              </ul>
            </nav>
            <?php
            }
            ?>
            
            </div>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>


<?php
if($this->session->flashdata("error_message")) 
{
?>
    <script type="text/javascript">
            BootstrapDialog.show({
            title: "Message",
            message: "<?php echo $this->session->flashdata("error_message");?>",
            });       
    </script>
<?php    
}
?>

<script type="text/javascript">
function delete_backup(id, file_name)
{
  BootstrapDialog.show({
  title: "Confirm",
  message: "Do you really want to delete it ?",
  buttons: [
  {
  label: 'Yes',
  cssClass: 'btn-primary yes_class',
  action: function(dialogItself){

  $(".yes_class").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
  $(".yes_class").attr('disabled', true);

  var url = '<?php echo base_url(); ?>admin/backup/delete_backup';
  var dataString = 'id='+id+'&file_name='+file_name;

  $.ajax({
    type:"POST",
    data:dataString,
    url:url,
    dataType:"json",
    success:function(response)
    {
      dialogItself.close();

      $("#tr_"+id).remove();
      
      BootstrapDialog.show({
        title: "Message",
        message: "You have successfully deleted",
        });    
    }

  });    

  }
  },
  {
      label: 'No',
      cssClass: 'btn-warning',
      action: function(dialogItself){
      dialogItself.close();
   }
  }]
  });

}

function create_backup(id)
{
  BootstrapDialog.show({
  title: "Confirm",
  message: "Do you really want to perform this action ?",
  buttons: [
  {
  label: 'Yes',
  cssClass: 'btn-primary yes_class',
  action: function(dialogItself)
  {

    $(".yes_class").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
    $(".yes_class").attr('disabled', true);

    var url = '<?php echo base_url(); ?>admin/backup/create_backup';
    var dataString = 'test=1';

    $.ajax({
    type:"POST",
    data:dataString,
    url:url,
    //dataType:"json",
    success:function(response)
    {
        window.location.reload(true);
    }

    });    

  }
  },
  {
      label: 'No',
      cssClass: 'btn-warning',
      action: function(dialogItself){
      dialogItself.close();
   }
  }]
  });

}
</script>