<?php $this->load->view('admin/common/header'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if(!empty($title)) echo $title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Users Management</a></li>
        <li class="active"><?php if(!empty($title)) echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
              <?php if(!empty($title)) echo $title; ?>
                
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">

<div>
    <form id="" name="" action="<?php echo base_url(); ?>admin/document/approved_documents" method="GET">
        <input style="background-image:url('<?php echo base_url(); ?>assets/admin_panel/images/searchicon.png');" type="text" class="search_input" id="search_text" name="search_text" placeholder="Search user.." value="<?php if(!empty($_GET['search_text'])) echo $_GET['search_text']; ?>" >

        <button class="btn btn-success" type="submit">Search</button>
        <a href="<?php echo base_url(); ?>admin/document/approved_documents"><button class="btn btn-info" type="button">Reset</button></a>



    </form>
</div>

            <div class="table-responsive">

        <p class="pull-right"><strong>Total Records:</strong> <?php if(!empty($total_records)) echo $total_records; ?></p>

<div class="clearfix"></div>

<?php
if($this->session->flashdata('error_message'))
{
?>
<div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata('error_message'); ?>
  </div>

<?php
}
?>

            <table id="myTable" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No.</th>  
                        <th>User Name</th>    
                        <th>Document Name</th>                       
                        <th>Approved Time</th>                       
                        <th>Status</th>                      
                    </tr>
                </thead>
                <tbody>
                <?php
                if(!empty($data_result))
                {

                    if(!empty($_GET['offset']))
                    {
                      $i = ($_GET['offset'] * 10) - 9;
                    }
                    else
                    {
                      $i = 1;
                    }

                    foreach ($data_result as $row) 
                    {
                
                ?>
                    <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>

                        <td><?php echo $row->full_name; ?></td>
                        <td>
                          <?php 
                          if($row->document_type == 'photo')
                          {
                            echo "<strong>Real Photo</strong>";
                          }
                          else if($row->document_type == 'id')
                          {
                            echo "<strong>ID Proof</strong>";
                          }
                          else if($row->document_type == 'address')
                          {
                            echo "<strong>Address Proof</strong>";
                          }
                          else if($row->document_type == 'cheque')
                          {
                            echo "<strong>Cancel Cheque</strong>";
                          }
                          ?>
                          <br/>
                          <a href="<?php echo base_url(); ?>uploads/user_documents/<?php echo $row->document_file; ?>" class="btn btn-primary" download>Download</a>
                        </td>

                        <td><?php echo date('d-m-Y h:i A', strtotime($row->approved_datetime)); ?></td>

                        <td>
                          <a href="javascript:void(0);" class="btn btn-primary" style="cursor: default;" id="status_text_<?php echo $row->id; ?>">Approved</a>
                        </td>

                    </tr>
                <?php
                $i++;
                    }
                }
                else
                {
                ?>
                <tr><td colspan="9" align="center">No Data Found !</td></tr>
                <?php
                }
                ?>                   
                </tbody>
            </table>

            <?php
            if(!empty($links))
            {
            ?>
            <nav aria-label="Page navigation example">
              <ul class="pagination">
                <?php echo $links; ?>
              </ul>
            </nav>
            <?php
            }
            ?>
            
            </div>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>