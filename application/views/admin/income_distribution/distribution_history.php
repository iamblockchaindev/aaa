<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" ng-controller="User_Controller" ng-init="get_distribution_history()" ng-cloak>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Income Distribution</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">




            <div class="table-responsive">

              <p class="pull-right"><strong>Total Records:</strong> {{history.length}}</p>

              <div class="clearfix"></div>

              <table datatable="ng" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Package</th>
                    <th>Company Level</th>
                    <th>Pool Level</th>
                    <th>From Date</th>
                    <th>To Date</th>
                    <th>Amount</th>
                    <th>Total Users</th>
                  </tr>
                </thead>
                <tbody>

                  <!-- <tr id="tr_<?php echo $row['id']; ?>">
                      <td><?php echo $count++; ?>.</td>
                      <td><?php echo $row['package']; ?></td>
                      <td><?php echo $row['level']; ?></td>
                      <td><?php echo date('d-m-Y', strtotime($row['from_date'])); ?></td>
                      <td><?php echo date('d-m-Y', strtotime($row['to_date'])); ?></td>
                      <td><?php echo $row['amount']; ?></td>
                      <td><?php echo $row['total_users']; ?></td>
                    </tr> -->
                  <tr id="tr_{{$index+1}}" ng-repeat="row in history">
                    <td>{{$index+1}}.</td>
                    <td>{{row.package}}</td>
                    <td>{{row.company_level}}</td>
                    <td>{{row.level}}</td>
                    <td>{{row.from_date}}</td>
                    <td>{{row.to_date}}</td>
                    <td>{{row.amount}}</td>
                    <td>{{row.total_users}}</td>
                  </tr>


                </tbody>
              </table>



            </div>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#non_working_distribution_main_li").addClass("menu-open");
    $("#non_working_distribution_child_li").show();
    $("#distribution_history_li").addClass("active");
  });
</script>