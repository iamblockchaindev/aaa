<?php $this->load->view('admin/common/header'); ?>

<style type="text/css">
  .success_message {
    color: green;
  }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Income Distribution</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>
            </h3>

          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <?php echo validation_errors(); ?>

            <form id="data_form" name="data_form" action="" method="POST" enctype="multipart/form-data">

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <div class="form-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Package</label>
                      <select class="form-control required" id="package" name="package">
                        <option value="">Select</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="100">100</option>
                        <option value="1000">1000</option>
                        <option value="5000">5000</option>
                        <option value="10000">10000</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Level</label>
                      <select class="form-control required" id="level" name="level">
                        <option value="">Select</option>
                        <option value="0">Level 0</option>
                        <option value="1">Level 1</option>
                        <option value="2">Level 2</option>
                        <option value="3">Level 3</option>
                        <option value="4">Level 4</option>
                        <option value="5">Level 5</option>
                        <option value="6">Level 6</option>
                        <option value="7">Level 7</option>
                        <option value="8">Level 8</option>
                        <option value="9">Level 9</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <textarea name="user_id" id="user_id" cols="83" rows="10"></textarea>
                      <!-- <label class="control-label">Package</label>
                      <select class="form-control required" id="package" name="package">
                        <option value="">Select</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="100">100</option>
                        <option value="1000">1000</option>
                        <option value="5000">5000</option>
                        <option value="10000">10000</option>
                      </select> -->
                    </div>
                  </div>
                </div>



                <!--/row-->

                <!-- <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Level</label>
                      <select class="form-control required" id="level" name="level">
                        <option value="">Select</option>
                        <option value="1">Level 1</option>
                        <option value="2">Level 2</option>
                        <option value="3">Level 3</option>
                        <option value="4">Level 4</option>
                        <option value="5">Level 5</option>
                        <option value="6">Level 6</option>
                        <option value="7">Level 7</option>
                        <option value="8">Level 8</option>
                        <option value="9">Level 9</option>
                      </select>
                    </div>
                  </div>                  
                </div> -->
                <!--/row-->

                <!-- <div class="row">
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="control-label">From Date</label>
                      <input type="text" class="form-control required" id="from_date" name="from_date" autocomplete="off">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="control-label">To Date</label>
                      <input type="text" class="form-control required" id="to_date" name="to_date" autocomplete="off">
                    </div>
                  </div>
                </div> -->
                <!--/row-->

              </div>
              <div class="form-actions">
                <button type="button" class="btn btn-success" onclick="submit_data();"> Generate</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#non_working_distribution_main_li").addClass("menu-open");
    $("#non_working_distribution_child_li").show();
    $("#non_working_li").addClass("active");
  });
</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
  $(function() {
    $("#from_date").datepicker({
      numberOfMonths: 1,
      onSelect: function(selected) {
        var dt = new Date(selected);
        dt.setDate(dt.getDate() + 1);
        $("#to_date").datepicker("option", "minDate", dt);
        $("#from_date").datepicker("option", "dateFormat", "dd-mm-yy");
      },

    });
    $("#to_date").datepicker({
      numberOfMonths: 1,
      onSelect: function(selected) {
        var dt = new Date(selected);
        dt.setDate(dt.getDate() - 1);
        $("#from_date").datepicker("option", "maxDate", dt);
        $("#to_date").datepicker("option", "dateFormat", "dd-mm-yy");
      }
    });
  });
</script>

<?php
if ($this->session->flashdata("error_message")) {
?>
  <script type="text/javascript">
    BootstrapDialog.show({
      title: "Message",
      message: "<?php echo $this->session->flashdata("error_message"); ?>",
    });
  </script>
<?php
}
?>

<script type="text/javascript">
  $("#data_form").validate();


  function submit_data() {
    var form = $("#data_form");

    if (form.valid() == false) {
      return false;
    } else {
      accept_submit();
    }
  }


  function accept_submit() {
    BootstrapDialog.show({
      title: "Confirm",
      message: "Do you really want to perform this action ?",
      buttons: [{
          label: 'Yes',
          cssClass: 'btn-primary yes_class',
          action: function(dialogItself) {
            $(".yes_class").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $(".yes_class").attr('disabled', true);

            $("#submit_button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $("#submit_button_id").attr('disable', true);

            $("#data_form").submit();
          }
        },
        {
          label: 'No',
          cssClass: 'btn-warning',
          action: function(dialogItself) {
            dialogItself.close();
          }
        }
      ]
    });

  }
</script>