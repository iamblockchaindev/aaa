<?php $this->load->view('admin/common/header'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if(!empty($title)) echo $title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Users Management</a></li>
        <li class="active"><?php if(!empty($title)) echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
              <?php if(!empty($title)) echo $title; ?>
                
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">


            <div class="table-responsive">

        <p class="pull-right"><strong>Total Records:</strong> <?php if(!empty($total_records)) echo $total_records; ?></p>

<div class="clearfix"></div>

<?php
if($this->session->flashdata('error_message'))
{
?>
<div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata('error_message'); ?>
  </div>

<?php
}
?>

            <table id="myTable" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No.</th>  
                        <th>User Name</th>    
                        <th>Transfer Date</th>                       
                        <th>Amount</th>                      
                        <th>Transfer Charge </th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if(!empty($data_result))
                {

                    if(!empty($_GET['offset']))
                    {
                      $i = ($_GET['offset'] * 10) - 9;
                    }
                    else
                    {
                      $i = 1;
                    }

                    foreach ($data_result as $row) 
                    {
                
                ?>
                    <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>
                        <td><?php echo getMemberNameByID($row->to_user); ?></td>

                        <td><?php echo date('d M Y',strtotime($row->created_datetime)); ?></td>
                        <td><?php echo $row->amount; ?> $</td>
                        <td><?php echo $row->transfer_charge; ?> %</td>
                    </tr>
                <?php
                $i++;
                    }
                }
                else
                {
                ?>
                <tr><td colspan="9" align="center">No Data Found !</td></tr>
                <?php
                }
                ?>                   
                </tbody>
            </table>

            <?php
            if(!empty($links))
            {
            ?>
            <nav aria-label="Page navigation example">
              <ul class="pagination">
                <?php echo $links; ?>
              </ul>
            </nav>
            <?php
            }
            ?>
            
            </div>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>

<script type="text/javascript">
function approve_document(id)
{
  BootstrapDialog.show({
  title: "Confirm",
  message: "Do you really want to approve it ?",
  buttons: [
  {
  label: 'Yes',
  cssClass: 'btn-primary ',
  action: function(dialogItself){

  var url = '<?php echo base_url(); ?>admin/document/approve_document';
  var dataString = 'id='+id;

  $.ajax({
    type:"POST",
    data:dataString,
    url:url,
    dataType:"json",
    success:function(response)
    {
      dialogItself.close();

      $("#change_status_td_"+id).html('');
      $("#status_text_"+id).html('Approve');
      $("#status_text_"+id).removeClass('btn-warning');
      $("#status_text_"+id).addClass('btn-primary');


      BootstrapDialog.show({
      title: "Message",
      message: "Dcoument aproved successfully !",
      });   

    }

  });    

  }
  },
  {
      label: 'No',
      cssClass: 'btn-warning',
      action: function(dialogItself){
      dialogItself.close();
   }
  }]
  });

}

function open_reject_popup(id)
{
  $("#reject_id").val(id);
  $("#reject_reason").val('');

  $("#reject_popup").modal('show');
}


function submit_reject()
{
  var form = $("#reject_form");

  if(form.valid() == false)
  {
      return false;
  }
  else
  {
    $("#reject_button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
    $("#reject_button_id").attr('disable', true);

    var reject_id = $("#reject_id").val();
    var reject_reason = $("#reject_reason").val();

    var url = '<?php echo base_url(); ?>admin/document/reject_document';
    var dataString = 'reject_id='+reject_id+'&reject_reason='+reject_reason;

    $.ajax({
      type:"POST",
      data:dataString,
      url:url,
      dataType:"json",
      success:function(response)
      {        
        $("#reject_popup").modal('hide');

        $("#change_status_td_"+reject_id).html('');
        $("#status_text_"+reject_id).html('Rejected');
        $("#status_text_"+reject_id).removeClass('btn-warning');
        $("#status_text_"+reject_id).addClass('btn-danger');


        BootstrapDialog.show({
        title: "Message",
        message: "Dcoument rejected successfully !",
        });

      }

    });     

  }

}

</script>

<!-- sample modal content -->
<div id="reject_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reject </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">


            <form id="reject_form" name="reject_form" class="form-horizontal" action="">

            <input type="hidden" id="reject_id" name="reject_id">

              <div class="form-group">
                <label class="control-label col-sm-2" for="reason">Reason:</label>
                <div class="col-sm-10">
                  <textarea class="form-control required" id="reject_reason" name="reject_reason"></textarea>
                </div>
              </div>

              <div class="form-group">        
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="button" class="btn btn-info" id="reject_button_id" onclick="submit_reject();">Submit</button>
                </div>
              </div>
            </form>


            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
