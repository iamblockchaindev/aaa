<script>
  $(document).ready(function() {
   var table = $('#example').DataTable({     
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']]
   });     
});
</script>

  <footer class="main-footer">
    
    <strong>Copyright &copy; <?php echo date('Y'); ?> All rights
    reserved.
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<!-- <script src="<?php echo base_url();?>assets/admin_panel/bower_components/jquery/dist/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<!--<script src="<?php echo base_url();?>assets/admin_panel/bower_components/raphael/raphael.min.js"></script>-->
<!--<script src="<?php echo base_url();?>assets/admin_panel/bower_components/morris.js/morris.min.js"></script>-->
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/admin_panel/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/admin_panel/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/admin_panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/admin_panel/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="<?php echo base_url();?>assets/admin_panel/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/admin_panel/dist/js/demo.js"></script>

  <!-- Data Table -->
  <script src="<?php echo base_url();?>assets/admin_panel/plugins/datatable/datatables.min.js"></script>
  <script src="<?php echo base_url();?>assets/admin_panel/plugins/datatable/dataTables.checkboxes.min.js"></script>  
  
  <script src="<?= base_url('assets/user_panel/plugins/toastr/toastr.min.js?v=' . rand()) ?>" ></script>
  
  <script src="<?= base_url('assets/user_panel/js/myapp.js?v='.rand()) ?>"></script>

</body>
</html>

<script src="<?php echo base_url(); ?>assets/admin_panel/js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_panel/js/bootstrap-dialog.js"></script>
