<!DOCTYPE html>
<html ng-app="pw">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Point Wish | <?php if (!empty($title)) echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/front/image/favicon.png') ?>">

  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/morris.js/morris.css">-->
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- BEGIN: Toaster CSS Assets-->
  <link rel="stylesheet" href="<?= base_url('assets/user_panel/plugins/toastr/toastr.css?v=' . rand()) ?>" />
  <!-- END: CSS Assets-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <script src="<?= base_url('assets/user_panel/js/angular.min.js') ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-datatables/0.6.4/angular-datatables.min.js"></script>

  <link rel="stylesheet" href="<?php echo base_url('assets/admin_panel/dist/css/AdminLTE.css?v=' . rand()); ?>">

  <!-- Data Table -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/plugins/datatable/datatables.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/plugins/datatable/dataTables.checkboxes.css">

  <script>
    var BASE_URL = "<?php echo base_url(); ?>";
  </script>


</head>

<body class="hold-transition skin-blue sidebar-mini" ng-controller="App_Controller">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="<?php echo base_url(); ?>admin" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
          <img src="<?php echo base_url(); ?>assets/user_panel/dist/images/logo.svg">
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
          <img src="<?php echo base_url(); ?>assets/user_panel/dist/images/logo.svg">

        </span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu" style="display: none;">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <!-- start message -->
                      <a href="#">
                        <div class="pull-left">
                          <img src="<?php echo base_url(); ?>assets/admin_panel/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          Support Team
                          <small><i class="fa fa-clock-o"></i> 5 mins</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <!-- end message -->
                    <li>
                      <a href="#">
                        <div class="pull-left">
                          <img src="<?php echo base_url(); ?>assets/admin_panel/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          AdminLTE Design Team
                          <small><i class="fa fa-clock-o"></i> 2 hours</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <div class="pull-left">
                          <img src="<?php echo base_url(); ?>assets/admin_panel/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          Developers
                          <small><i class="fa fa-clock-o"></i> Today</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <div class="pull-left">
                          <img src="<?php echo base_url(); ?>assets/admin_panel/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          Sales Department
                          <small><i class="fa fa-clock-o"></i> Yesterday</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <div class="pull-left">
                          <img src="<?php echo base_url(); ?>assets/admin_panel/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          Reviewers
                          <small><i class="fa fa-clock-o"></i> 2 days</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
              </ul>
            </li>
            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu" style="display: none;">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                        page and may cause design problems
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-users text-red"></i> 5 new members joined
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-user text-red"></i> You changed your username
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
              </ul>
            </li>

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>assets/admin_panel/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo ucwords($this->session->userdata('admin_username')); ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo base_url(); ?>assets/admin_panel/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                  <p>
                    <?php echo ucwords($this->session->userdata('admin_username')); ?>

                  </p>
                </li>
                <!-- Menu Body -->


                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="<?php echo base_url(); ?>admin/accounts" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url(); ?>admin/logout" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->

          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="<?php echo base_url(); ?>assets/admin_panel/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p><?php echo ucwords($this->session->userdata('admin_username')); ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MAIN NAVIGATION</li>

          <?php if ($this->session->type == 1) { ?>
            <li class="active"><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <?php } ?>
          <li class="active treeview">
            <a href="#">
              <i class="fa fa-users"></i> <span>My Account </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="active"><a href="<?php echo base_url(); ?>admin/accounts"><i class="fa fa-circle-o"></i> My Profile</a></li>
              <li><a href="<?php echo base_url(); ?>admin/accounts/edit_profile"><i class="fa fa-circle-o"></i> Edit Profile</a></li>
              <li><a href="<?php echo base_url(); ?>admin/accounts/change_password"><i class="fa fa-circle-o"></i> Change Password</a></li>
            </ul>
          </li>
          <?php if ($this->session->type == 1) { ?>
            <li class=""><a href="<?php echo base_url(); ?>admin/backup/backup_list"><i class="fa fa-dashboard"></i> <span>Backup</span></a></li>
          <?php } ?>

          <?php if ($this->session->type == 1) { ?>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-users"></i> <span>Users Management </span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">

                <li><a href="<?php echo base_url(); ?>admin/user/income_distribution"><i class="fa fa-circle-o"></i>Income Distribution</a></li>
                <li><a href="<?php echo base_url(); ?>admin/user/user_matrix"><i class="fa fa-circle-o"></i>User Matrix</a></li>
                <li><a href="<?php echo base_url(); ?>admin/user/company_matrix"><i class="fa fa-circle-o"></i>Company Matrix</a></li>
                <li><a href="<?php echo base_url(); ?>admin/user/total_users"><i class="fa fa-circle-o"></i> Total Users</a></li>
                <li><a href="<?php echo base_url(); ?>admin/user/paid_users"><i class="fa fa-circle-o"></i> Paid Users</a></li>
                <li><a href="<?php echo base_url(); ?>admin/user/unpaid_users"><i class="fa fa-circle-o"></i> Unpaid Users</a></li>
              </ul>
            </li>
          <?php } ?>

          <?php if ($this->session->admin_username == 'PW31680401') { ?>
            <li><a href="<?php echo base_url(); ?>admin/user/total_users"><i class="fa fa-circle-o"></i> Total Users</a></li>
          <?php } ?>

          <li class="active treeview">
            <a href="#">
              <i class="fa fa-users"></i> <span>Documents Management </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>admin/document/pending_documents"><i class="fa fa-circle-o"></i> Pending Documents</a></li>
              <li><a href="<?php echo base_url(); ?>admin/document/approved_documents"><i class="fa fa-circle-o"></i> Approved Documents</a></li>
              <li><a href="<?php echo base_url(); ?>admin/document/rejected_documents"><i class="fa fa-circle-o"></i> Rejected Documents</a></li>
            </ul>
          </li>

          <li class="active treeview">
            <a href="#">
              <i class="fa fa-users"></i> <span>Bluenile Documents </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>admin/document/pn_doc"><i class="fa fa-circle-o"></i> Pending Documents</a></li>
              <li><a href="<?php echo base_url(); ?>admin/document/ap_doc"><i class="fa fa-circle-o"></i> Approved Documents</a></li>
              <li><a href="<?php echo base_url(); ?>admin/document/rj_doc"><i class="fa fa-circle-o"></i> Rejected Documents</a></li>
            </ul>
          </li>

          <?php if ($this->session->type == 1) { ?>
            <li class="active treeview" id="non_working_distribution_main_li">
              <a href="#">
                <i class="fa fa-users"></i> <span>Non-working Distribution</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" id="non_working_distribution_child_li">
                <li id="user_pool_li"><a href="<?php echo base_url(); ?>admin/income_distribution/user_pool"><i class="fa fa-circle-o"></i>User Pool Distribution</a></li>
                <li id="non_working_li"><a href="<?php echo base_url(); ?>admin/income_distribution/non_working"><i class="fa fa-circle-o"></i> Non-working Distribution</a></li>
                <li id="distribution_history_li"><a href="<?php echo base_url(); ?>admin/income_distribution/distribution_history"><i class="fa fa-circle-o"></i> Distribution History</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-users"></i> <span>Transfer Fund </span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>admin/fund/transfer_fund"><i class="fa fa-circle-o"></i> Transfer Fund</a></li>
                <li><a href="<?php echo base_url(); ?>admin/fund/transfer_fund_history"><i class="fa fa-circle-o"></i> Transfer History</a></li>
              </ul>
            </li>

            <li class="active treeview">
              <a href="#">
                <i class="fa fa-users"></i> <span>Withdrawal Management </span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>admin/withdraw/pending_withdrawal"><i class="fa fa-circle-o"></i> Pending Withdrawals</a></li>
                <li><a href="<?php echo base_url(); ?>admin/withdraw/given_withdrawal"><i class="fa fa-circle-o"></i> Given Withdrawals</a></li>
                <li><a href="<?php echo base_url(); ?>admin/withdraw/rejected_withdrawal"><i class="fa fa-circle-o"></i> Rejected Withdrawals</a></li>
              </ul>
            </li>
          <?php } ?>
          <li class="active"><a href="<?php echo base_url(); ?>admin/ticket"><i class="fa fa-dashboard"></i> <span>Tickets</span></a></li>

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>