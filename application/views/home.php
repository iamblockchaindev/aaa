<?php
//  $this->load->view('common/header'); 
?>

<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="HandheldFriendly" content="true">

<title>Point Wish</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- favicon -->
<link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/front/image/favicon.png') ?>">

<!-- all css here -->
<!-- bootstrap v3.3.6 css -->
<link rel="stylesheet" href="<?= base_url('assets/front/css/bootstrap.min.css') ?>">
<!-- owl.carousel css -->
<link rel="stylesheet" href="<?= base_url('assets/front/css/owl.carousel.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/front/css/owl.transitions.css') ?>">
<!-- Animate css -->
<link rel="stylesheet" href="<?= base_url('assets/front/css/animate.css') ?>">
<!-- meanmenu css -->
<link rel="stylesheet" href="<?= base_url('assets/front/css/meanmenu.min.css') ?>">
<!-- font-awesome css -->
<link rel="stylesheet" href="<?= base_url('assets/front/css/font-awesome.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/front/css/themify-icons.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/front/css/flaticon.css') ?>">
<!-- magnific css -->
<link rel="stylesheet" href="<?= base_url('assets/front/css/magnific.min.css') ?>">
<!-- style css -->
<link rel="stylesheet" href="<?= base_url('assets/front/css/style.css') ?>">
<!-- responsive css -->
<link rel="stylesheet" href="<?= base_url('assets/front/css/responsive.css') ?>">
<link href="<?= base_url('assets/front/css/custom.css') ?>" rel="stylesheet">

<!-- modernizr css -->

<!-- jquery latest version -->
<script src="<?= base_url('assets/front/js/jquery-2.2.4.min.js') ?>"></script>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


<style>
    body {
        line-height: 1.25;
    }

    .carousel-indicators .active {
        margin: 0;
        width: 12px;
        height: 12px;
        background-color: #000 !important;
    }

    .carousel-indicators li {
        display: inline-block;
        width: 10px;
        height: 10px;
        margin: 1px;
        text-indent: -999px;
        border: 1px solid #000 !important;
        border-radius: 10px;
        cursor: pointer;
        background-color: #000 \9;
        background-color: rgba(0, 0, 0, 0);
    }




    table {
        border: 1px solid #ccc;
        border-collapse: collapse;
        margin: 0;
        padding: 0;
        width: 100%;
        table-layout: fixed;
    }

    table caption {
        font-size: 1.5em;
        margin: .5em 0 .75em;
    }

    table tr {
        background-color: #f8f8f8;
        border: 1px solid #ddd;
        padding: .35em;
    }

    table th,
    table td {
        padding: .625em;
        text-align: center;
    }

    table th {
        font-size: .85em;
        letter-spacing: .1em;
        text-transform: uppercase;
    }

    .intro-area-3 {
        display: none;
    }

    .cookie-overlay {
        padding: 20px;
        position: fixed;
        bottom: 1rem;
        left: 1rem;
        background: #fff;
        z-index: 2051;
        line-height: 20px;
        font-size: 14px;
        border-radius: 6px;

        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
    }

    .d-none {
        display: none;
    }

    .d-block {
        display: block;
    }

    @media screen and (max-width: 600px) {
        table {
            border: 0;
        }

        .front-img {
            height: 30% !important;
        }

        .topnav {
            display: inline-block !important;
            width: 100%;
            position: fixed;
        }

        table caption {
            font-size: 1.3em;
        }

        table thead {
            border: none;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }

        table tr {
            border-bottom: 3px solid #ddd;
            display: block;
            margin-bottom: .625em;
        }

        table td {
            border-bottom: 1px solid #ddd;
            display: block;
            font-size: .8em;
            text-align: right;
        }

        table td::before {
            content: attr(data-label);
            float: left;
            font-weight: bold;
            text-transform: uppercase;
        }

        table td:last-child {
            border-bottom: 0;
        }
    }
</style>

<style>
    #slider {
        margin-top: 50px;
        overflow: hidden;
    }

    .slides {
        overflow: hidden;
        animation-name: fade;
        animation-duration: 1s;
        display: none;
    }

    .img-slide {
        width: 100%;
        height: 90%;
    }

    #dot {
        margin: 0 auto;
        text-align: center;
        background-color: #20024c;
    }

    .dot {
        display: inline-block;
        border-radius: 50%;
        background: grey;
        padding: 5px;
        margin: 10px 5px;
    }

    .dot.active {
        background: #fff;
    }

    @media (max-width:600px) {
        #slider {
            width: 100%;
        }

        .img-slide {
            width: 100%;
            margin-top: 50px !important;
            height: 30%;
        }

        #slider {
            margin-top: 0px !important;
            overflow: hidden;
        }
    }
</style>

<style>
    #loader {
        position: fixed;
        width: 100%;
        height: 100%;
        overflow: visible;
        z-index: 999999;
        background: #fff url('assets/front/image/loader/loader2.gif') no-repeat center center;
    }
</style>


<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
    _atrk_opts = {
        atrk_acct: "/LFfv1ah9W20em",
        domain: "pointwish.com",
        dynamic: true
    };
    (function() {
        var as = document.createElement('script');
        as.type = 'text/javascript';
        as.async = true;
        as.src = "https://certify-js.alexametrics.com/atrk.js";
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(as, s);
    })();
</script>
<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=/LFfv1ah9W20em" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

</head>
<header class="header-one">
    <!-- header-area start -->
    <div id="sticker" class="header-area hidden-xs stick">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <!-- logo start -->
                        <div class="col-md-3 col-sm-3">
                            <div class="logo">
                                <!-- Brand -->
                                <a class="navbar-brand page-scroll" href="#">
                                    <img src="<?= base_url('assets/front/image/head.png') ?>" alt="">
                                </a>
                            </div>
                            <!-- logo end -->
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <div class="header-right-link">
                                <!-- search option end -->
                                <?php
                                if ($this->session->userdata('user_id')) {
                                ?>
                                    <a class="s-menu" href="<?= base_url('user/dashboard') ?>">Dashboard</a>
                                <?php
                                } else {
                                ?>
                                    <a class="s-menu" href="<?= base_url('login') ?>">Login&nbsp;/&nbsp;Register</a>
                                <?php
                                }
                                ?>
                            </div>
                            <!-- mainmenu start -->
                            <nav class="navbar navbar-default">
                                <div class="collapse navbar-collapse" id="navbar-example">
                                    <div class="main-menu">
                                        <ul class="nav navbar-nav navbar-right">
                                            <li><a style="color: #0F0F80;" class="pages" href="#">Home</a></li>
                                            <li><a class="pages" href="#news">News</a></li>
                                            <li><a class="pages" href="#pool">Pool</a></li>
                                            <li><a class="pages" href="#achiever">Achiever</a></li>
                                            <li><a href="<?php echo base_url(); ?>page/about">About us</a></li>
                                            <li><a class="pages" href="<?php echo base_url(); ?>page/contact_us">Contact Us</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!-- mainmenu end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header-area end -->
    <!-- mobile-menu-area start -->
    <div class="mobile-container">

        <!-- Top Navigation Menu -->
        <div class="topnav" style="display: none;">
            <a href="#" class="active"><img src="<?= base_url('assets/front/image/head.png') ?>" height="50px" width="200px;"></a>
            <div id="myLinks" style=" background-color:#FFF;">
                <a class="pages" href="#" style="color: #240255;"><b>Home</b></a>
                <a class="pages" href="<?= base_url() ?>#news" style="color: #240255;"><b>News</b></a>
                <a class="pages" href="<?= base_url() ?>#pool" style="color: #240255;"><b>Pool</b></a>
                <a class="pages" href="<?= base_url() ?>#achiever" style="color: #240255;"><b>Achiever</b></a>
                <a href="<?php echo base_url(); ?>page/about" style="color: #240255;"><b>About us<b></a>
                <a class="pages" href="<?php echo base_url(); ?>page/contact_us" style="color: #240255;"><b>Contact Us</b></a>
                <?php if ($this->session->userdata('user_id')) { ?>
                    <a href="<?= base_url('user/dashboard') ?>" style="color: #240255;"><b>Dashboard</b></a>
                <?php } else { ?>
                    <a href="<?= base_url('login') ?>" style="color: #240255;"><b>Login / Register<b></a>
                <?php
                }
                ?>
            </div>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars" style="margin-top: 14px; color:black;"></i>
            </a>
        </div>



        <!-- End smartphone / tablet look -->
    </div>
    <!-- mobile-menu-area end -->

</header>

<body>
    <div class="cookie-overlay p-4 d-none">
        <div class="d-flex">
            <div class="text-info" style="margin-bottom: 5px;">We use cookie to improve your experience on our site. By using our site you consent cookies. </div>
        </div>
        <button class="btn btn-primary mt-3 accept-cookies">Allow</button>
        <button class="btn btn-primary mt-3 close-cookies">Decline</button>
    </div>
    <div id="loader"></div>
    <div id="content">

        <!-- header end -->
        <!-- Start Slider Area -->
        <section id="top">


            <div id="slider">
                <!-- <div class="slides">
                    <img class="img-slide" src="<?= base_url('assets/front/image/festival.jpg?v=' . rand()) ?>" width="100%" />
                </div> -->
                <!-- <div class="slides">
                    <img class="img-slide" src="<?= base_url('assets/front/image/festival_winner.jpg?v=' . rand()) ?>" width="100%" />
                </div> -->
                <!-- <div class="slides">
                    <img class="img-slide" src="<?= base_url('assets/front/image/postar/web-announcement.jpg') ?>" width="100%" />
                </div> -->
                <div class="slides">
                    <img class="img-slide" src="<?= base_url('assets/front/image/web-layout.jpg') ?>" width="100%" />
                </div>

                <div id="dot">
                    <span class="dot"></span>
                    <span class="dot"></span>
                    <!-- <span class="dot"></span> -->
                </div>
            </div>
        </section>


        <section id="news">
            <div class="blog-area bg-color2 area-padding-2">

                <div class="container">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="section-headline text-center">

                                <h3>Latest news</h3>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <marquee scrollamount="30" onmouseover="stop()" onmouseout="start()" vspace="80%"> -->
                        <div class="blog-grid home-blog">
                            <!-- Start single blog -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single-blog">
                                    <div class="blog-image">
                                        <a class="image-scale">
                                            <img src="<?= base_url('assets/front/image/New Launch.png') ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-content slide">
                                        <div class="blog-category">
                                            <span>New Launch</span>
                                        </div>
                                        <div class="blog-meta">
                                            <span class="admin-type">
                                                <i class="fa fa-user"></i>
                                                Admin
                                            </span>
                                            <span class="date-type">
                                                <i class="fa fa-calendar"></i>
                                                05 October, 2020
                                            </span>
                                            <span class="comments-type">
                                                <i class="fa fa-comment-o"></i>
                                                232
                                            </span>
                                        </div>
                                        <div class="blog-title">
                                            <a>
                                                <h4>New Site launched with old morals.</h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End single blog -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single-blog">
                                    <div class="blog-image">
                                        <a class="image-scale">
                                            <img src="<?= base_url('assets/front/image/Application.png') ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-content">
                                        <div class="blog-category">
                                            <span>Application</span>
                                        </div>
                                        <div class="blog-meta">
                                            <span class="admin-type">
                                                <i class="fa fa-user"></i>
                                                Admin
                                            </span>
                                            <span class="date-type">
                                                <i class="fa fa-calendar"></i>
                                                01 October, 2020
                                            </span>
                                            <span class="comments-type">
                                                <i class="fa fa-comment-o"></i>
                                                322
                                            </span>
                                        </div>
                                        <div class="blog-title">
                                            <a>
                                                <h4>Android App (Beta) launhed, soon more updates</h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End single blog -->
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single-blog">
                                    <div class="blog-image">
                                        <a class="image-scale">
                                            <img src="<?= base_url('assets/front/image/Rewards.png') ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-content">
                                        <div class="blog-category">
                                            <span>Rewards</span>
                                        </div>
                                        <div class="blog-meta">
                                            <span class="admin-type">
                                                <i class="fa fa-user"></i>
                                                Admin
                                            </span>
                                            <span class="date-type">
                                                <i class="fa fa-calendar"></i>
                                                14 September, 2020
                                            </span>
                                            <span class="comments-type">
                                                <i class="fa fa-comment-o"></i>
                                                432
                                            </span>
                                        </div>
                                        <div class="blog-title">
                                            <a>
                                                <h4>More rewards & more benifit for community.</h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- </marquee> -->

                    </div>
                    <!-- End row -->
                </div>
            </div>
        </section>
        <!-- End Slider Area -->
        <!-- Start Count area -->
        <div class="counter-area fix bg-color area-padding-2">
            <div class="container">
                <!-- Start counter Area -->
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="single-fun">
                            <span class="counter-icon"><img src="<?= base_url('assets/front/image/icon/All member.svg') ?>" height="50" width="50" style="margin-top: 15px; margin-left: 10px;"></span>
                            <div class="fun-text">
                                <span class="counter">People's</span>
                                <h4>Platform</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="single-fun">
                            <span class="counter-icon"><img src="<?= base_url('assets/front/image/icon/Total Deposit.svg') ?>" height="50" width="50" style="margin-top: 15px; margin-left: 3px;"></span>
                            <div class="fun-text">
                                <span class="counter">Minimum</span>
                                <h4>Deposit</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="single-fun">
                            <span class="counter-icon"><img src="<?= base_url('assets/front/image/icon/World company.svg') ?>" height="50" width="50" style="margin-top: 15px; margin-left: 0px;"></i></span>
                            <div class="fun-text">
                                <span class="counter">General</span>
                                <h4>Presence</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Count area -->
        <!-- about-area start -->
        <div class="about-area bg-color2 area-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="about-image">
                            <img src="<?= base_url('assets/front/image/ab.jpg') ?>" alt="">
                            <div class="video-content">


                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- column end -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="about-content">
                            <h3>The most prestigious Investments company in worldwide.</h3>
                            <p class="hidden-sm" style="text-align: justify;-moz-text-align-last: center;text-align-last: left;">
                                Point Wish Group is committed to helping its team to reach their goals, to personalizing their event experiences, to providing an innovative environment, and to make a difference.<br>Our strong sense of identification with the project means that we are constantly striving to provide solutions, even for issues they aren’t yet aware of. To this end, we adopt a progressive approach towards technology and marketing techniques.
                            </p>
                            <div class="about-details">
                                <div class="single-about">
                                    <a><img src="<?= base_url('assets/front/image/icon/Secure Investment.svg') ?>" height="40" width="40" style="margin-top: 12px;"></i></a>
                                    <div class="icon-text">
                                        <h5>Highly Secured</h5>
                                        <p>We are using redundant, mirrored servers kept in a climate-controlled, fire-resistant room.</p>
                                    </div>
                                </div>
                                <div class="single-about">
                                    <a><img src="<?= base_url('assets/front/image/icon/Online Transfer.svg') ?>" height="40" width="40" style="margin-top: 17px; margin-left: 7px;"></i></a>
                                    <div class="icon-text">
                                        <h5>Point Wish App</h5>
                                        <p>We are coming soon with our app with advance and new technology features. Our expected launching date is Nov 2020.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- about-area end -->
        <!-- Start Invest area -->
        <section id="pool">
            <div class="invest-area bg-color area-padding-2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="section-headline text-center">
                                <h3>Pool System</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="pricing-content">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="pri_table_list">
                                    <div class="top-price-inner">
                                        <h3 style="text-align: center;">Opal</h3>
                                        <div class="rates">
                                            <center><img src="<?= base_url('assets/front/image/Opal.png?v=' . rand()) ?>" height="200px" width="200px"></center><br>
                                            <p style="text-align: center; margin-bottom: -40px; font-size: 30px;">$40</p>
                                        </div>
                                    </div>
                                    <!-- <ol class="pricing-text">
                                    <li class="check">Minimam Invest : $1000</li>
                                    
                                </ol> -->

                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="pri_table_list">
                                    <span class="base">Best seller</span>
                                    <div class="top-price-inner">
                                        <h3 style="text-align: center;">Jade</h3>
                                        <div class="rates">
                                            <center><img src="<?= base_url('assets/front/image/Jade.png?v=' . rand()) ?>" height="200px" width="200px"></center><br>
                                            <p style="text-align: center; margin-bottom: -40px; font-size: 30px;">$40</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="pri_table_list">
                                    <div class="top-price-inner">
                                        <h3 style="text-align: center;">Red Beryl</h3>
                                        <span class="base">Popular</span>
                                        <div class="rates">
                                            <center><img src="<?= base_url('assets/front/image/Red-Beryl.png?v=' . rand()) ?>" height="200px" width="200px"></center><br>
                                            <p style="text-align: center; margin-bottom: -40px; font-size: 30px;">$100</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="pri_table_list">

                                    <div class="top-price-inner">
                                        <h3 style="text-align: center;">Blue Nile</h3>
                                        <div class="rates">
                                            <center><img src="<?= base_url('assets/front/image/BLUE_NILE_01.png?v=' . rand()) ?>" height="200px" width="200px"></center><br>
                                            <p style="text-align: center; margin-bottom: -40px; font-size: 30px;">$1,000</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="pri_table_list">

                                    <div class="top-price-inner">
                                        <h3 style="text-align: center;">Eternity</h3>
                                        <div class="rates">
                                            <center><img src="<?= base_url('assets/front/image/Eternity.png?v=' . rand()) ?>" height="200px" width="200px"></center><br>
                                            <p style="text-align: center; margin-bottom: -40px; font-size: 30px;">$5,000</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="pri_table_list">

                                    <div class="top-price-inner">
                                        <h3 style="text-align: center;">Koh-i-Noor</h3>
                                        <div class="rates">
                                            <center><img src="<?= base_url('assets/front/image/Koh-i-Noor.png?v=' . rand()) ?>" height="200px;" width="200px;"></center><br>
                                            <p style="text-align: center; margin-bottom: -40px; font-size: 30px;">$10,000</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Invest area -->
        <!-- Start Support-service Area -->
        <div class="support-service-area bg-color2 area-padding-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="section-headline text-center">
                            <h3>Why choose us</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="support-all">
                        <!-- Start About -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="support-services ">
                                <span class="top-icon"><img src="<?= base_url('assets/front/image/icon/Expert management.svg') ?>" height="110" width="110"></span>
                                <a class="support-images"><img src="<?= base_url('assets/front/image/icon/Expert management.svg') ?>" height="50" width="50" style="margin-top: 23px;"></i></a>
                                <div class="support-content">
                                    <h4>Multi Language</h4>
                                    <p>Our team consists of advanced features and supports all languages. Using the Multilanguage option you can select the preferred language.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Start About -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="support-services ">
                                <span class="top-icon"><img src="<?= base_url('assets/front/image/icon/Instant withdrawl.svg') ?>" height="110" width="110"></i></span>
                                <a class="support-images"><img src="<?= base_url('assets/front/image/icon/Instant withdrawl.svg') ?>" height="50" width="50" style="margin-top: 20px; margin-left: -3px;"></i></a>
                                <div class="support-content">
                                    <h4>Multi Currency</h4>
                                    <p>With the multi-currency feature, you can see the rates in your currency and perform the transaction using the preferred currencies like ETH, BTC</p>
                                </div>
                            </div>
                        </div>
                        <!-- Start services -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="support-services ">
                                <span class="top-icon"><img src="<?= base_url('assets/front/image/icon/Registered Company.svg') ?>" height="110" width="110"></i></span>
                                <a class="support-images"><img src="<?= base_url('assets/front/image/icon/Registered Company.svg') ?>" height="50" width="50" style="margin-top: 25px;"></i></a>
                                <div class="support-content">
                                    <h4>Refer And Earn</h4>
                                    <p>Every time you refer POINTWISH to a person you will get money into your existing wallet amount. The more you refer, the more you earn.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Start services -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="support-services">
                                <span class="top-icon"><img src="<?= base_url('assets/front/image/icon/Secure.svg') ?>" height="100" width="100"></i></span>
                                <a class="support-images"><img src="<?= base_url('assets/front/image/icon/Secure.svg') ?>" height="50" width="50" style="margin-top: 20px;"></i></a>
                                <div class="support-content">
                                    <h4>24/7 Support</h4>
                                    <p>Our support team is available 24/7. Connect with our team anytime for any required assistance. Your queries will be resolved instantly.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Start services -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="support-services ">
                                <span class="top-icon"><img src="<?= base_url('assets/front/image/icon/verified security.svg') ?>" src="image/icon/verified security.svg" height="110" width="110"></i></span>
                                <a class="support-images"><img src="<?= base_url('assets/front/image/icon/verified security.svg') ?>" height="50" width="50" style="margin-top: 25px;"></i></a>
                                <div class="support-content">
                                    <h4>Incorporate</h4>
                                    <p>Incorporate the basic standards of riches building, for example, upper hand and substantially more into a customized riches plan.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Start services -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="support-services">
                                <span class="top-icon"><img src="<?= base_url('assets/front/image/icon/Live support customer.svg') ?>" src="image/icon/Live support customer.svg" height="110" width="110"></i></span>
                                <a class="support-images"><img src="<?= base_url('assets/front/image/icon/Live support customer.svg') ?>" height="50" width="50" style="margin-top: 18px;"></i></a>
                                <div class="support-content">
                                    <h4>Change</h4>
                                    <p>Change themselves to conquer the individual snags that keep them away from the achievement and have the right to accomplish.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Start Franchise-->
        
        <!--End Franchise-->

        <!-- Start Footer Area -->
        <?php $this->load->view('common/footer'); ?>
        <!-- End Footer area -->
    </div>

    <!-- all js here -->

    <script src="https://kenwheeler.github.io/slick/slick/slick.js"></script>
    <!-- bootstrap js -->
    <script src="<?= base_url('assets/front/js/bootstrap.min.js') ?>"></script>
    <!-- owl.carousel js -->
    <script src="<?= base_url('assets/front/js/owl.carousel.min.js') ?>"></script>
    <!-- magnific js -->
    <script src="<?= base_url('assets/front/js/magnific.min.js') ?>"></script>
    <!-- wow js -->
    <script src="<?= base_url('assets/front/js/wow.min.js') ?>"></script>
    <!-- meanmenu js -->
    <script src="<?= base_url('assets/front/js/jquery.meanmenu.js') ?>"></script>
    <!-- Form validator js -->
    <script src="<?= base_url('assets/front/js/form-validator.min.js') ?>"></script>
    <!-- plugins js -->
    <script src="<?= base_url('assets/front/js/plugins.js') ?>"></script>
    <!-- main js -->

    <script src="<?= base_url('assets/front/js/multislider.min.js') ?>"></script>
    <script src="<?= base_url('assets/front/js/main.js') ?>"></script><a id="scrollUp" href="#top" style="position: fixed; z-index: 2147483647;"><img src="<?= base_url('assets/front/image/icon/upload.png') ?>" height="20" width="20" style="margin-top: 10px;"></i></a>

    <script>
        function myFunction() {
            var x = document.getElementById("myLinks");
            if (x.style.display === "block") {
                x.style.display = "none";
            } else {
                x.style.display = "block";
            }
        }
    </script>

    <script>
        $('#exampleSlider').multislider({
            interval: 4000,
            continuous: true,
            duration: 5000
        });
    </script>

    <script type="text/javascript">
        var loader;

        function loadNow(opacity) {
            if (opacity <= 0) {
                displayContent();
            } else {
                loader.style.opacity = opacity;
                window.setTimeout(function() {
                    loadNow(opacity - 0.1)
                }, 100);
            }
        }

        function displayContent() {
            loader.style.display = 'none';
        }
        document.addEventListener("DOMContentLoaded", function() {
            loader = document.getElementById('loader');
            loadNow(2.5);
        });
    </script>

    <script>
        // cookie policy
        $(document).on('ready', function() {
            if (document.cookie.indexOf("accepted_cookies=") < 0) {
                $('.cookie-overlay').removeClass('d-none').addClass('d-block');
            }

            $('.accept-cookies').on('click', function() {
                document.cookie = "accepted_cookies=yes;"
                $('.cookie-overlay').removeClass('d-block').addClass('d-none');
            })

            // expand depending on your needs
            $('.close-cookies').on('click', function() {
                $('.cookie-overlay').removeClass('d-block').addClass('d-none');
            })

            $(".Modern-Slider").slick({
                autoplay: true,
                autoplaySpeed: 10000,
                speed: 600,
                slidesToShow: 1,
                slidesToScroll: 1,
                pauseOnHover: false,
                dots: true,
                pauseOnDotsHover: true,
                cssEase: 'linear',
                // fade:true,
                draggable: false,
                prevArrow: '<button class="PrevArrow"></button>',
                nextArrow: '<button class="NextArrow"></button>',
            });
        })
    </script>

    <script>
        var index = 0;
        var slides = document.querySelectorAll(".slides");
        var dot = document.querySelectorAll(".dot");

        function changeSlide() {
            if (index < 0) {
                index = slides.length - 1;
            }
            if (index > slides.length - 1) {
                index = 0;
            }
            for (let i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
                dot[i].classList.remove("active");
            }
            slides[index].style.display = "block";
            dot[index].classList.add("active");
            index++;
            setTimeout(changeSlide, 5000);
        }
        changeSlide();
    </script>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body post-img">
                    <img class="front-img" src="<?= base_url() ?>assets/front/image/postar/postar.jpg" style="height: 70%; width:100%" height="70%" width="100%">
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        // $(window).on('load', function() {
        //     $('#myModal').modal('show');
        // });
    </script>
</body>

</html>