<?php $this->load->view('common/header'); ?>

<head>
    !-- all css here -->
	<!-- bootstrap v3.3.6 css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/bootstrap.min.css') ?>">
	<!-- owl.carousel css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/owl.carousel.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/front/css/owl.transitions.css') ?>">
	<!-- Animate css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/animate.css') ?>">
	<!-- meanmenu css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/meanmenu.min.css') ?>">
	<!-- font-awesome css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/font-awesome.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/front/css/themify-icons.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/front/css/flaticon.css') ?>" >
	<!-- magnific css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/magnific.min.css') ?>">
	<!-- style css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/style.css') ?>">
	<!-- responsive css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/responsive.css') ?>">
    <link href="<?= base_url('assets/front/css/custom.css') ?>" rel="stylesheet">

    <!-- modernizr css -->
    


    <link rel="stylesheet" href="myProjects/webProject/icofont/css/icofont.min.css">


    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <style>
        body {
            
            line-height: 1.25;
}

table {
border: 1px solid #ccc;
border-collapse: collapse;
margin: 0;
padding: 0;
width: 100%;
table-layout: fixed;
}

table caption {
font-size: 1.5em;
margin: .5em 0 .75em;
}

table tr {
background-color: #f8f8f8;
border: 1px solid #ddd;
padding: .35em;
}

table th,
table td {
padding: .625em;
text-align: center;
}

table th {
font-size: .85em;
letter-spacing: .1em;
text-transform: uppercase;
}

@media screen and (max-width: 600px) {
table {
border: 0;
}

.topnav{
    display:inline-block !important;
    width: 100%;
    position: fixed;
}

table caption {
font-size: 1.3em;
}

table thead {
border: none;
clip: rect(0 0 0 0);
height: 1px;
margin: -1px;
overflow: hidden;
padding: 0;
position: absolute;
width: 1px;
}

table tr {
border-bottom: 3px solid #ddd;
display: block;
margin-bottom: .625em;
}

table td {
border-bottom: 1px solid #ddd;
display: block;
font-size: .8em;
text-align: right;
}

table td::before {
/*
* aria-label has no advantage, it won't be read inside a table
content: attr(aria-label);
*/
content: attr(data-label);
float: left;
font-weight: bold;
text-transform: uppercase;
}

table td:last-child {
border-bottom: 0;
}
}

@media screen and (max-width: 600px) {
.about{
    display: none;
}
.counter-area{
    display: block !important;
}
}






    </style>

</head>

<body>
    

    <!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->


    <?php
    include 'header.php';
    ?>
    <!-- header end -->
    <!-- Start Slider Area -->
  
    <!-- End Slider Area -->
    <!-- Start Count area --><section id="top">
    <div class="about">
        <!-- <h2 style="margin-top: 100px; color: white; text-align: center;">About Us</h2> -->
       <center> <img src="<?= base_url('assets/front/image/t&c.jpg') ?>" style="margin-top: 120px;"style="margin-left: 20px"; style="margin-right: 20px;" ></center>
    </div>
    <div class="counter-area fix bg-color area-padding-2" style="display: none;">
        <h2 style="margin-top: 100px; color: white; text-align: center;">Terms And Condition</h2>
    </div>
    <div style="margin-left: 20%; margin-right: 20%; margin-top: 40px; text-align: justify;
    -moz-text-align-last: center;
    text-align-last: left;" >
        

    
    <h2 style="color: #383838;" ><b>Welcome to Point Wish Ltd.!</b> </h2>
    <p style="color: black;">These terms and conditions outline the rules and regulations for the use of Point Wish
        Ltd.'s Website, located at https://www.pointwish.com </p>
        <p style="color: black;">By accessing this website, we assume you accept these terms and conditions. Do not
continue to use Point Wish Ltd. if you do not agree to take all of the terms and conditions
stated on this page. 
</p>
<p style="color: black;">The following terminology applies to these Terms and Conditions, Privacy Statement and
    Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person
    log on this website and compliant to the Company’s terms and conditions. "The Company",
    "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers
    to both the Client and ourselves. All terms refer to the offer, acceptance and consideration
    of payment necessary to undertake the process of our assistance to the Client in the most
    appropriate manner for the express purpose of meeting the Client’s needs in respect of
    provision of the Company’s stated services, in accordance with and subject to, prevailing
    law of Netherlands. Any use of the above terminology or other words in the singular, plural,
    capitalization and/or he/she or they, are taken as interchangeable and therefore as
    referring to same.  
    </p>
    <h2 style="color: #383838;">Cookies</h2>
    <p style="color: black;">We employ the use of cookies. By accessing Point Wish Ltd., you agreed to use cookies
        in agreement with the Point Wish Ltd.'s Privacy Policy.  
        </p>
        <p style="color: black;">Most interactive websites use cookies to let us retrieve the user’s details for each visit.
            Cookies are used by our website to enable the functionality of certain areas to make it
            easier for people visiting our website. Some of our affiliate/advertising partners may also
            use cookies. 
            
            </p>
            <h2 style="color: #383838;">License</h2>
            <p style="color: black;">Unless otherwise stated, Point Wish Ltd. and/or its licensors own the intellectual property
                rights for all material on Point Wish Ltd. All intellectual property rights are reserved. You
                may access this from Point Wish Ltd. for your own personal use subjected to restrictions
                set in these terms and conditions. 
                
                </p>
                <p style="color: black;">You must not: 
                    <br> • Republish material from Point Wish Ltd.
                    <br> • Sell, rent or sub-license material from Point Wish Ltd.
                    <br> • Reproduce, duplicate or copy material from Point Wish Ltd.
                    <br> • Redistribute content from Point Wish Ltd. 
                    
                    </p>
                    <p style="color: black;">Parts of this website offer an opportunity for users to post and exchange opinions and
                        information in certain areas of the website. Point Wish Ltd. does not filter, edit, publish or
                        review Comments prior to their presence on the website. Comments do not reflect the views
                        and opinions of Point Wish Ltd, Its agents and/or affiliates. Comments reflect the views
                        and opinions of the person who post their views and opinions. To the extent permitted by
                        applicable laws, Point Wish Ltd. shall not be liable for the Comments or for any liability,
                        damages or expenses caused and/or suffered as a result of any use of and/or posting of
                        and/or appearance of the Comments on this website. 
                        
                        </p>
                        <p style="color: black;">Point Wish Ltd. reserves the right to monitor all Comments and to remove any Comments
                            which can be considered inappropriate, offensive or causes breach of these Terms and
                            Conditions. 
                        </p>
                        <p style="color: black;">You warrant and represent that: 

                        
                        </p>
                        <p style="color: black;"> • You are entitled to post the Comments on our website and have all necessary licenses and
                            consents to do so;
                            <br> • The Comments do not invade any intellectual property right, including without limitation
                            copyright, patent or trademark of any third party;
                            <br> • The Comments do not contain any defamatory, libellous, offensive, indecent or otherwise
                            unlawful material which is an invasion of privacy
                            <br> • The Comments will not be used to solicit or promote business or custom or present
                            commercial activities or unlawful activity. 
                            
                        
                        </p>
                        <p style="color: black;">You hereby grant Point Wish Ltd. a non-exclusive license to use, reproduce, edit and
                            authorize others to use, reproduce and edit any of your Comments in any and all forms,
                            formats or media. 
                        
                        </p>
                        <h2 style="color: #383838;">Hyperlinking to our Content
                        </h2>
                        <p style="color: black;">The following organizations may link to our Website without prior written approval:
                            <br> • Government agencies;
                            <br> • Search engines;
                            <br> • News organizations;
                            <br> • Online directory distributors may link to our Website in the same manner as they hyperlink
                            to the Websites of other listed businesses; and
                            <br> • System wide Accredited Businesses except soliciting non-profit organizations, charity
                            shopping malls, and charity fundraising groups which may not hyperlink to our Web site. 
                        
                        </p>
                        <p style="color: black;">These organizations may link to our home page, to publications or to other Website
                            information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply
                            sponsorship, endorsement or approval of the linking party and its products and/or services;
                            and (c) fits within the context of the linking party’s site. 
                            
                        
                        </p>
                        <p style="color: #383838;">We may consider and approve other link requests from the following types of organizations:
                            <br>• commonly-known consumer and/or business information sources;
                            <br>• dot.com community sites;
                            <br>• associations or other groups representing charities;
                            <br>• online directory distributors;
                            <br>• internet portals;
                            <br>• accounting, law and consulting firms; and
                            <br>• educational institutions and trade associations.
                        
                        </p>
                        <p style="color: black;">We will approve link requests from these organizations if we decide that: (a) the link would
                            not make us look unfavourably to ourselves or to our accredited businesses; (b) the
                            organization does not have any negative records with us; (c) the benefit to us from the
                            visibility of the hyperlink compensates the absence of Point Wish Ltd.; and (d) the link is in
                            the context of general resource information. 
                            
                        
                        </p>
                        <p style="color: black;">TThese organizations may link to our home page so long as the link: (a) is not in any way
                            deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking
                            party and its products or services; and (c) fits within the context of the linking party’s site. 
                            
                        
                        </p>
                        <p style="color: black;">If you are one of the organizations listed in paragraph 2 above and are interested in linking
                            to our website, you must inform us by sending an e-mail to Point Wish Ltd., Please include
                            your name, your organization name, contact information as well as the URL of your site, a
                            list of any URLs from which you intend to link to our Website, and a list of the URLs on our
                            site to which you would like to link. Wait 2-3 weeks for a response. 
                             
                            
                        
                        </p>
                        <p style="color: black;">Approved organizations may hyperlink to our Website as follows:
                            <br> • By use of our corporate name; or
                            <br> • By use of the uniform resource locator being linked to; or
                            <br> • By use of any other description of our Website being linked to that makes sense within the
                            context and format of content on the linking party’s site. 
                            
                            
                        
                        </p>
                        <p style="color: black;">No use of Point Wish Ltd.'s logo or other artwork will be allowed for linking absent a
                            trademark license agreement. 
                            
                        
                        </p>
                        <h2 style="color: #383838;">iFrames
                        </h2>
                        <p style="color: black;">Without prior approval and written permission, you may not create frames around our
                            Webpages that alter in any way the visual presentation or appearance of our Website. 
                            
                        </p>
                        <h2 style="color: #383838;">Content Liability

                        </h2>
                        <p style="color: black;">We shall not be hold responsible for any content that appears on your Website. You agree
                            to protect and defend us against all claims that is rising on your Website. No link(s) should
                            appear on any Website that may be interpreted as libellous, obscene or criminal, or which
                            infringes, otherwise violates, or advocates the infringement or other violation of, any third
                            party rights. 
                        </p>
                        <h2 style="color: #383838;">Reservation of Rights


                        </h2>
                        <p style="color: black;">We reserve the right to request that you remove all links or any particular link to our
                            Website. You approve to immediately remove all links to our Website upon request. We
                            also reserve the right to amen these terms and conditions and it’s linking policy at any time.
                            By continuously linking to our Website, you agree to be bound to and follow these linking
                            terms and conditions. 
                            
                        </p>
                        <h2 style="color: #383838;">Removal of links from our website
                        </h2>
                        <p style="color: black;">If you find any link on our Website that is offensive for any reason, you are free to contact
                            and inform us any moment. We will consider requests to remove links but we are not
                            obligated to or so or to respond to you directly. 
                            
                            
                        </p>
                        <p style="color: black;">We do not ensure that the information on this website is correct, we do not warrant its
                            completeness or accuracy; nor do we promise to ensure that the website remains available
                            or that the material on the website is kept up to date. 
                            
                            
                        </p>
                        <h2 style="color: #383838;">Disclaimer
                        </h2>
                        <p style="color: black;">To the maximum extent permitted by applicable law, we exclude all representations,
                            warranties and conditions relating to our website and the use of this website. Nothing in
                            this disclaimer will:
                            <br> • limit or exclude our or your liability for death or personal injury;
                            <br> • limit or exclude our or your liability for fraud or fraudulent misrepresentation;
                            <br> • limit any of our or your liabilities in any way that is not permitted under applicable law; or
                            <br> • exclude any of our or your liabilities that may not be excluded under applicable law. 
                            
                            
                        </p>
                        <p style="color: black;">The limitations and prohibitions of liability set in this Section and elsewhere in this
                            disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising
                            under the disclaimer, including liabilities arising in contract, in tort and for breach of
                            statutory duty. 
                            
                            
                        </p>
                        <p style="color: black;">As long as the website and the information and services on the website are provided free
                            of charge, we will not be liable for any loss or damage of any nature. 
                            
                            
                        </p><br><br><br>
                        
</div>
</section>

    <!-- End Count area -->
    <!-- about-area start -->
    
    <!-- about-area end -->
    <!-- Start Invest area -->
   
    <!-- End Invest area -->
    <!-- Start Support-service Area -->
    
    <!-- End Support-service Area -->
    <!--Start payment-history area -->
    
    <!-- End payment-history area -->
   
    <!--End Widrawl-->
    <!--Start Franchise-->
    
    <!--End Franchise-->
    <!-- Start Affiliate Area -->
  
    <!-- End Affiliate Area -->
    <!-- Start Overview Area -->
   
  
    <!-- End Overview Area -->
    <!-- Start Blog area -->
   
    <!-- End Blog area -->
    <!-- Start Subscribe area -->
    
    <!-- End Subscribe area -->
    <!-- Start Footer Area -->
    
    <?php $this->load->view('common/footer'); ?>
   
    <!-- End Footer area -->

    <!-- all js here -->

     <!-- jquery latest version -->
		<!-- <script src="js/vendor/jquery-1.12.4.min.js"></script> -->
		<!-- bootstrap js -->
		<script src="<?= base_url('assets/front/js/bootstrap.min.js') ?>"></script>
		<!-- owl.carousel js -->
		<script src="<?= base_url('assets/front/js/owl.carousel.min.js') ?>"></script>
		<!-- magnific js -->
		<script src="<?= base_url('assets/front/js/magnific.min.js') ?>"></script>
		<!-- wow js -->
		<script src="<?= base_url('assets/front/js/wow.min.js') ?>"></script>
		<!-- meanmenu js -->
		<script src="<?= base_url('assets/front/js/jquery.meanmenu.js') ?>"></script>
		<!-- Form validator js -->
		<script src="<?= base_url('assets/front/js/form-validator.min.js') ?>"></script>
		<!-- plugins js -->
		<script src="<?= base_url('assets/front/js/plugins.js') ?>"></script>
		<!-- main js -->
		<script src="<?= base_url('assets/front/js/jquery-2.2.4.min.js') ?>"></script>
		<script src="<?= base_url('assets/front/js/multislider.min.js') ?>"></script>
		<script src="<?= base_url('assets/front/js/main.js') ?>"></script><a id="scrollUp" href="#top" style="position: fixed; z-index: 2147483647;"><img src="<?= base_url('assets/front/image/icon/upload.png') ?>" height="20" width="20" style="margin-top: 10px;"></i></a>
 
    <script>
        function myFunction() {
          var x = document.getElementById("myLinks");
          if (x.style.display === "block") {
            x.style.display = "none";
          } else {
            x.style.display = "block";
          }
        }
        </script>

    <script>
        $('#exampleSlider').multislider({
            interval: 4000,
            continuous: true,
            duration: 5000
        });
    </script>



</body>

</html>