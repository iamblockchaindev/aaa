<?php $this->load->view('common/header'); ?>
    
    <!--header section start-->
			<div class="breadcrumb-section contact-bg section-padding">
			<div class="container">
			    <div class="row">
			        <div class="col-md-6 col-md-offset-3 text-center">
			            <h1>DIGITAL currency</h1>
			             <p>DIGITAL currency is a payment method which exists only in electronic form and is not tangible. </p>

			        </div>
			    </div>
			</div>
			</div><!--Header section end-->

     <!--service section start-->
     <section class="section-padding service-section service-bg">
         <div class="container">

             <div class="row">
                 <div class="col-md-12">
                     <div class="section-title section-padding padding-bottom-0">
                         <h2 style="text-align: center;">DIGITAL currency</h2>
                         <p style="padding-bottom: 13px;">DIGITAL currency is a payment method which exists only in electronic form and is not tangible. Digital currency can be transferred between entities or users with the help of technology like computers, smartphones and the internet.</p>
                     </div>
                 </div>
             </div>

             <div class="row">
                 <div class="col-md-12">
                     <div class="section-title section-padding padding-bottom-0" style="padding-top: 13px;">
                         <h2 style="text-align: center;">Launching Own Digital Currency</h2>
                         <p style="padding-bottom: 13px;"><strong>Point Wish</strong> has finally revealed the details of its cryptocurrency, <strong>PT WISH</strong>, which will let you buy things or send money to people with nearly zero fees.</p>
                          <p style="padding-bottom: 13px;">You’ll pseudonymously buy or cash out your <strong>PT WISH</strong> online or at local exchange points like grocery stores, and spend it using inter-operable third-party wallet apps or Point Wish own PT wallet.</p>
                           <p style="padding-bottom: 13px;"><strong>Point Wish</strong> released its white paper explaining <strong>PT WISH</strong> and its testnet for working out the kinks of its block-chain system before a public launch in the first half of 2020. </p>
                     </div>
                 </div>
             </div>

             <div class="row">
                 <div class="col-md-12">
                     <div class="section-title section-padding padding-bottom-0" style="padding-top: 13px;">
                         <h2 style="text-align: center;">Point Wish Mission</h2>
                         <p style="padding-bottom: 0px;"><strong>Point Wish’s</strong> mission is to enable a simple global currency and financial infrastructure that empowers billion of people. </p>
                     </div>
                 </div>
             </div>

         </div>
     </section><!--service section end-->
    <!--Our Plan section start-->

    <?php $this->load->view('common/footer'); ?>