<?php $this->load->view('common/header'); ?>

<head>
    !-- all css here -->
	<!-- bootstrap v3.3.6 css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/bootstrap.min.css') ?>">
	<!-- owl.carousel css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/owl.carousel.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/front/css/owl.transitions.css') ?>">
	<!-- Animate css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/animate.css') ?>">
	<!-- meanmenu css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/meanmenu.min.css') ?>">
	<!-- font-awesome css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/font-awesome.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/front/css/themify-icons.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/front/css/flaticon.css') ?>" >
	<!-- magnific css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/magnific.min.css') ?>">
	<!-- style css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/style.css') ?>">
	<!-- responsive css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/responsive.css') ?>">
    <link href="<?= base_url('assets/front/css/custom.css') ?>" rel="stylesheet">

    <!-- modernizr css -->
    


    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<style>
    body {
        line-height: 1.25;
    }

    table {
        border: 1px solid #ccc;
        border-collapse: collapse;
        margin: 0;
        padding: 0;
        width: 100%;
        table-layout: fixed;
    }

    table caption {
        font-size: 1.5em;
        margin: .5em 0 .75em;
    }

    table tr {
        background-color: #f8f8f8;
        border: 1px solid #ddd;
        padding: .35em;
    }

    table th,
    table td {
        padding: .625em;
        text-align: center;
    }

    table th {
        font-size: .85em;
        letter-spacing: .1em;
        text-transform: uppercase;
    }

    @media screen and (max-width: 600px) {
        table {
            border: 0;
        }

        .topnav {
            display: inline-block !important;
            width: 100%;
            position: fixed;
        }

        table caption {
            font-size: 1.3em;
        }

        table thead {
            border: none;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }

        table tr {
            border-bottom: 3px solid #ddd;
            display: block;
            margin-bottom: .625em;
        }

        table td {
            border-bottom: 1px solid #ddd;
            display: block;
            font-size: .8em;
            text-align: right;
        }

        table td::before {
            /*
* aria-label has no advantage, it won't be read inside a table
content: attr(aria-label);
*/
            content: attr(data-label);
            float: left;
            font-weight: bold;
            text-transform: uppercase;
        }

        table td:last-child {
            border-bottom: 0;
        }
    }

    @media screen and (max-width: 600px) {
        .about {
            display: none;
        }

        .counter-area {
            display: block !important;
        }
    }
</style>

</head>

<body>

   
        <!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->



        <!-- header end -->
        <!-- Start Slider Area -->

        <!-- End Slider Area -->
        <!-- Start Count area -->
        <section id="top">
            <div class="about">
                <!-- <h2 style="margin-top: 100px; color: white; text-align: center;">About Us</h2> -->
                <img src="<?= base_url('assets/front/image/about us.jpg') ?>">

            </div>
            <div class="counter-area fix bg-color area-padding-2" style="display: none;">
                <h2 style="margin-top: 100px; color: white; text-align: center;">About Us</h2>
            </div>
            <div style="margin-left: 20%; margin-right: 20%; margin-top: 40px; text-align: justify;
    -moz-text-align-last: center;
    text-align-last: left;">


                <h2 style="color: #383838;">Awsome Community</h2>
                <p style="color: black;">Digital inventiveness continues to rewire and rewrite the way business gets done. Social and mobile platforms today add value to a rich and never-ending emotional connection between people and the brands they love.</p>
                <p style="color: black;">Experiences that connect on a human "difference-making" level matter make a true impression. So at PointWish, real client value begins with discovering compelling insights into human nature.</p>
                <h2 style="color: #383838;">Tappimg Into The Value Of Real Relationships.</h2>
                <p style="color: black;">Competition is fierce in the digital world as it is across virtually every business segment. Simply put, brands and businesses that anchor their digital experiences with insightful analysis and smart integrated planning, emotional understanding and artful design create sustainable engagement.</p>
                <p style="color: black;">We bring proven processes, innovative analytics and groundbreaking digital tools to virtually everything we do. But even more so, we bring it emotionally. Because brands and companies (all of us) today live and thrive by getting close to those that matter. So, in three words what’s our approach: Passionate, curious, relentless!</p>
                <h2 style="color:#383838;">We Share The Emotional Experience Of Being Human</h2>
                <p style="color: black;">The real value is in real connections, and consumers always have the final say in what’s genuine. Creating emotional currency is a richly affirmative way to show your consumers you get them.</p>
                <p style="color: black;">We create campaigns that forge a connection through a range of compelling visual language, artful messaging, and charm. It isn’t rocket science, but it is emotional sensitivity.</p>
                <h2 style="color: #383838;">We Covet</h2>
                <p style="color: black;">1. Meaningful, sustainable solutions<br>2. Our process<br>3. Overall business success</p><br><br>
            </div>
        </section>
  
    <!-- End Count area -->
    <!-- about-area start -->

    <!-- about-area end -->
    <!-- Start Invest area -->

    <!-- End Invest area -->
    <!-- Start Support-service Area -->

    <!-- End Support-service Area -->
    <!--Start payment-history area -->

    <!-- End payment-history area -->

    <!--End Widrawl-->
    <!--Start Franchise-->

    <!--End Franchise-->
    <!-- Start Affiliate Area -->

    <!-- End Affiliate Area -->
    <!-- Start Overview Area -->

    </div>
    <!-- End Overview Area -->
    <!-- Start Blog area -->

    <!-- End Blog area -->
    <!-- Start Subscribe area -->

    <!-- End Subscribe area -->
    <!-- Start Footer Area -->
    <?php $this->load->view('common/footer'); ?>
    <!-- End Footer area -->

    <!-- all js here -->

    <!-- jquery latest version -->
    <script src="<?= base_url('assets/front/js/jquery-2.2.4.min.js') ?>"></script>
    <!-- bootstrap js -->
    <script src="<?= base_url('assets/front/js/bootstrap.min.js') ?>"></script>
    <!-- owl.carousel js -->
    <script src="<?= base_url('assets/front/js/owl.carousel.min.js') ?>"></script>
    <!-- magnific js -->
    <script src="<?= base_url('assets/front/js/magnific.min.js') ?>"></script>
    <!-- wow js -->
    <script src="<?= base_url('assets/front/js/wow.min.js') ?>"></script>
    <!-- meanmenu js -->
    <script src="<?= base_url('assets/front/js/jquery.meanmenu.js') ?>"></script>
    <!-- Form validator js -->
    <script src="<?= base_url('assets/front/js/form-validator.min.js') ?>"></script>
    <!-- plugins js -->
    <script src="<?= base_url('assets/front/js/plugins.js') ?>"></script>
    <!-- main js -->
    
    <script src="<?= base_url('assets/front/js/multislider.min.js') ?>"></script>
    <script src="<?= base_url('assets/front/js/main.js') ?>"></script><a id="scrollUp" href="#top" style="position: fixed; z-index: 2147483647;"><img src="<?= base_url('assets/front/image/icon/upload.png') ?>" height="20" width="20" style="margin-top: 10px;"></i></a>

    <script>
        function myFunction() {
            var x = document.getElementById("myLinks");
            if (x.style.display === "block") {
                x.style.display = "none";
            } else {
                x.style.display = "block";
            }
        }
    </script>

    <script>
        $('#exampleSlider').multislider({
            interval: 4000,
            continuous: true,
            duration: 5000
        });
    </script>
   

</body>

</html>