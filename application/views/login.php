<html lang="en" class="mdl-js">

<head>
	<meta charset="UTF-8">
	<title>Login And Registration Form</title>
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<link rel="icon" href="<?php echo base_url(); ?>/assets/front/image/favicon.png" type="image/png" sizes="32x32">
	<!-- LIBRARY FONT-->
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/font-awesome.css?v=' . rand()) ?>" media="all" onload="if(media!=='all')media='all'">
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/bootstrap.css?v=' . rand()) ?>">
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/color-9.css?v=' . rand()) ?>" media="all" onload="if(media!=='all')media='all'">
	<!-- LIBRARY CSS-->
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/setresponsive.css?v=' . rand()) ?>" media="all" onload="if(media!=='all')media='all'">
	<!-- <script type="text/javascript" async="" src="<?= base_url('assets/user_panel/login/analytics.js?v=' . rand()) ?>"></script> -->
	<script src="<?= base_url('assets/user_panel/login/jquery.min.js?v=' . rand()) ?>"></script>

	<script type="text/javascript">
		function ValidateAlpha(evt) {
			var keyCode = (evt.which) ? evt.which : evt.keyCode;
			if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
				return false;
			return true;
		}
	</script>
	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/demo-signin.css?v=' . rand()) ?>">
	<link href="<?= base_url('assets/user_panel/login/select2.min.css?v=' . rand()) ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= base_url('assets/user_panel/login/bootstrap.min.css?v=' . rand()) ?>">


	<link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/firebaseui.css?v=' . rand()) ?>">
	<link href="<?= base_url('assets/user_panel/login/firebase.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/user_panel/login/jquery-ui.css?v=' . rand()) ?>">
	<link rel="stylesheet" href="<?= base_url('assets/user_panel/login/login.css?v=' . rand()) ?>" />

	<script>
		window.console = window.console || function(t) {};
		if (document.location.search.match(/type=embed/gi)) {
			window.parent.postMessage("resize", "*");
		}
	</script>


</head>

<body translate="no" id="log_regs">
	<div id="loading"></div>
	<div class="container container_d <?php if (!empty($_GET['pws_id'])) echo 'right-panel-active';  ?>" id="container" style="z-index:0;">
		<div class="row">
			<div class="col-md-12 top_h">
				<div class="logo">
					<a href="/">
						<div class="logo_main_top">


						</div>
					</a>
				</div>
				<div class="tab_btn">
					<button id="mobLogShow" style="top: 7px;" class="btn btn-green-3" onclick="mobLogShow()"><span>Login</span></button>

					<!--<a href="/partners">
                    <button class="btn btn-green-3">
                        <span>For partners</span>
                    </button>
                </a>-->


					<button id="mobRegShow" style="top: 7px;" class="btn btn-green-3" onclick="mobRegShow()"><span>Register</span></button>
				</div>
			</div>
		</div>
		<div class="form-container sign-up-container">
			<form id="signUpForm" name="signUpForm" autocomplete="off" method="POST">
				<div class="heading_tag">Register</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="row" style="margin-top: 30px;">
							<input type="text" name="sponsor_id" id="sponsor_id" placeholder="Sponser ID" value="<?php if (!empty($_GET['pws_id'])) echo $_GET['pws_id']; ?>" class="required" required>
							<span id="member_name_section" class="errorSpan"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<input type="text" name="fname" id="fname" placeholder="First Name" class="onlyAlphabet required" required>
							<span id="error_first_name" class="errorSpan"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<input type="text" name="lname" id="lname" placeholder="Last Name" class="onlyAlphabet required" required>
							<span id="error_last_name" class="errorSpan"></span>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row">
							<input type="text" name="email" id="email" placeholder="Email" class="" required>
							<span id="error_email" class="errorSpan"></span>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row">
							<input type="password" name="password" id="password" placeholder="Password" class="required" minlength="6" required>
							<span id="error_passwordUp" class="errorSpan"></span>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row passwd">
							<input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" equalTo="#password" class="">
							<span id="view_pass" onclick="viewPassFunc()">
								<div class="eye"></div>
							</span>
							<span id="error_cpassword" class="errorSpan"></span>
						</div>
					</div>
				</div>
				<div class="col-md-12">
				</div>
				<div class="col-md-12">
					<div _ngcontent-c3="" class="terms__wrapper">
						<input _ngcontent-c3="" formcontrolname="terms" id="terms" name="terms" required title="Terms And Conditions" checked type="checkbox" class="ng-untouched ng-pristine ng-valid">
						<label _ngcontent-c3="" class="terms" for="terms"></label>

						<div _ngcontent-c3="" class="term_cond">
							I agree to the
							<span _ngcontent-c3="" class="terms__link" routerlink="/privacy" tabindex="0"> <a href="#" onclick="$('#privacy_modal2 ').modal('show')">Terms &amp; Conditions.</a></span>
							<!--<br><span id="error_terms" class="errorSpan" ></span>-->
						</div>
					</div>
				</div>
				<button type="submit" id="signUpSubmit" class="btn btn-green button-design submit_button_class" style="background-color: #1C3FAA; color: white;"><span><b>Register</b></span></button>
			</form>
		</div>

		<div class="form-container sign-in-container ">
			<?php
			if ($this->session->flashdata('error_session')) {
			?>
				<div class="alert alert-danger alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<?php echo $this->session->flashdata('error_session'); ?>
				</div>
			<?php } ?>

			<?php
			if ($this->session->flashdata('success_message')) {
			?>
				<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<?php echo $this->session->flashdata('success_message'); ?>
				</div>
			<?php } ?>
			<form id="data_form" class="sp_t_log" autocomplete="off" method="POST">
				<h1>Login</h1>
				<div class="col-md-12">
					<div class="row" style="margin-top: 30px;">
						<span style="color:red;font-weight: bold" id="errorMessageOnline"></span>
						<input class="login-error" type="username" name="username" id="username" placeholder="Username" value="<?= $this->session->flashdata("username") ?>" autocomplete="off">
						<span id="error_user" class="errorSpan"></span>
					</div>
				</div>

				<div class="col-md-12">
					<div class="row passwd">
						<input type="password" name="password" id="lpassword" autocomplete="new-password" value="<?= $this->session->flashdata("password") ?>" placeholder="Password">
						<span id="view_password" onclick="viewPassFuncIn()">
							<div class="eye"></div>
						</span>
						<span id="error_pass" class="errorSpan"></span>
					</div>
				</div>
				<br>
				<div id="forgot_password" class="forgot_password"> <span>Forgot your password? </span></div>

				<button type="submit" id="login_btn_Online" class="btn btn-green  button-design" style="background-color: #1C3FAA; color: white;"><span><b>Login</b></span></button>

			</form>
		</div>

		<div class="overlay-container">
			<div class="overlay">
				<div class="overlay-panel overlay-left">
					<img onclick="redirect()" alt="Midone Tailwind HTML Admin Template" style="top: 25px;position: absolute;margin-left: -1em;cursor: pointer;" src="<?= base_url('assets/user_panel/dist/images/logo.svg?v=' . rand()) ?>">
					<div class="heading_tag">One Of Us?</div>
					<p>If you already have an account, just Login. We've missed you! </p>
					<button class="ghost btn btn-green button-design" id="signIn"><span><b>Login</b></span></button>
				</div>
				<div class="overlay-panel overlay-right">
					<img onclick="redirect()" alt="Midone Tailwind HTML Admin Template" style="top: 25px;position: absolute;margin-left: -1em;cursor: pointer;" src="<?= base_url('assets/user_panel/dist/images/logo.svg?v=' . rand()) ?>">
					<div class="heading_tag">New Here?</div>
					<p>Register and discover a whole new world of learning.</p>

					<button class="ghost btn btn-green button-design" id="signUp"><span><b>Register</b></span></button>
				</div>
				<script>
					function redirect() {
						window.location = "<?php echo base_url('home'); ?>";
					}
				</script>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal" id="privacy_modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

		<div class="modal-dialog" role="document">
			<div class="modal-content policy_content">
				<div class="modal-header">
					<div class="modal-title pl_head heading_tag" id="terms_modal">Terms and Conditions</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body text-dark">
					<div class="policy_para">
						<p>Welcome to Point Wish Ltd.!</p>
						<p>These terms and conditions outline the rules and regulations for the use of Point Wish Ltd.'s Website, located at https://www.PointWish.com/.</p>
						<p>By accessing this website we assume you accept these terms and conditions. Do not continue to use Point Wish Ltd. if you do not agree to take all of the terms and conditions stated on this page. </p>
						<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company’s
							terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration
							of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in
							accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring
							to same.</p>
						<p class="heading heading1"><strong>Cookies</strong></p>
						<p>We employ the use of cookies. By accessing Point Wish Ltd., you agreed to use cookies in agreement with the Point Wish Ltd.'s Privacy Policy.</p>
						<p>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of
							our affiliate/advertising partners may also use cookies.</p>
						<p class="heading heading1"><strong>License</strong></p>
						<p>Unless otherwise stated, Point Wish Ltd. and/or its licensors own the intellectual property rights for all material on Point Wish Ltd.. All intellectual property rights are reserved. You may access this from Point Wish Ltd. for
							your own personal use subjected to restrictions set in these terms and conditions.</p>
						<p>You must not:</p>
						<ul style="list-style-position: inside;">
							<li>Republish material from Point Wish Ltd.</li>
							<li>Sell, rent or sub-license material from Point Wish Ltd.</li>
							<li>Reproduce, duplicate or copy material from Point Wish Ltd.</li>
							<li>Redistribute content from Point Wish Ltd.</li>
						</ul>
						<p>This Agreement shall begin on the date hereof.</p>
						<p>Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. Point Wish Ltd. does not filter, edit, publish or review Comments prior to their presence on the
							website. Comments do not reflect the views and opinions of Point Wish Ltd.,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable
							laws, Point Wish Ltd. shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</p>
						<p>Point Wish Ltd. reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</p>
						<p>You warrant and represent that:</p>
						<ul style="list-style-position: inside;">
							<li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li>
							<li>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</li>
							<li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</li>
							<li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li>
						</ul>
						<p>You hereby grant Point Wish Ltd. a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</p>
						<p class="heading heading1"><strong>Hyperlinking to our Content</strong></p>
						<p>The following organizations may link to our Website without prior written approval:</p>
						<ul style="list-style-position: inside;">
							<li>Government agencies;</li>
							<li>Search engines;</li>
							<li>News organizations;</li>
							<li>Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and</li>
							<li>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</li>
						</ul>
						<p>These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking
							party and its products and/or services; and (c) fits within the context of the linking party’s site.</p>
						<p>We may consider and approve other link requests from the following types of organizations:</p>
						<ul style="list-style-position: inside;">
							<li>commonly-known consumer and/or business information sources;</li>
							<li>dot.com community sites;</li>
							<li>associations or other groups representing charities;</li>
							<li>online directory distributors;</li>
							<li>internet portals;</li>
							<li>accounting, law and consulting firms; and</li>
							<li>educational institutions and trade associations.</li>
						</ul>
						<p>We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with
							us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of Point Wish Ltd.; and (d) the link is in the context of general resource information.</p>
						<p>These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits
							within the context of the linking party’s site.</p>
						<p>If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to Point Wish Ltd. Please include your name, your organization name, contact
							information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.</p>
						<p>Approved organizations may hyperlink to our Website as follows:</p>
						<ul style="list-style-position: inside;">
							<li>By use of our corporate name; or</li>
							<li>By use of the uniform resource locator being linked to; or</li>
							<li>By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party’s site.</li>
						</ul>
						<p>No use of Point Wish Ltd..'s logo or other artwork will be allowed for linking absent a trademark license agreement.</p>
						<p class="heading heading1"><strong>iFrames</strong></p>
						<p>Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</p>
						<p class="heading heading1"><strong>Content Liability</strong></p>
						<p>We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted
							as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>
						<p class="heading heading1"><strong>Your Privacy</strong></p>
						<p>Please read <a href="https://privacypolicygenerator.info/" style="color: #008AD1">our Privacy Policy</a>.</p>
						<p class="heading heading1"><strong>Reservation of Rights</strong></p>
						<p>We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions
							and it’s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</p>
						<p class="heading heading1"><strong>Removal of links from our website</strong></p>
						<p>If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.</p>
						<p>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to
							date.</p>
						<p class="heading heading1"><strong>Disclaimer</strong></p>
						<p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</p>
						<ul style="list-style-position: inside;">
							<li>limit or exclude our or your liability for death or personal injury;</li>
							<li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
							<li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
							<li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>
						</ul>
						<p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities
							arising in contract, in tort and for breach of statutory duty.</p>
						<p>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>
					</div>
					<!--  <div class="modal-footer">
                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                   <button type="button" class="btn btn-primary">Save changes</button>
                 </div> -->
				</div>
			</div>
		</div>


	</div>

	<div id="forgotPassModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<span style="color:red;font-weight: bold" id="errorMessage"></span>
				<div class="modal-header">
					<h4 class="modal-title text-white pb-0">Forgot Password?</h4>
					<button type="button" class="close text-white" data-dismiss="modal">×</button>
				</div>
				<div class="modal-body">
					<form action="" method="post" id="form_forgot" style="height: auto;">
						<input type="text" value="" name="username" id="userNameFor" autocomplete="off" placeholder="User Name">
						<span id="error_forgotU" class="errorSpan"></span>
						<span id="successForget" style="color:green"></span>
						<button type="submit" class="btn btn-green button-design ">
							<span id="forgot_btn_Online">Submit</span>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script id="rendered-js">
		window.onload = function() {
			<?php if (!empty($_GET['pws_id'])) : ?>
				mobRegShow();
			<?php endif; ?>
		}
		$("#forgot_password").click(function() {
			$('#forgotPassModal').modal('show');
		});

		var x = window.matchMedia("(max-width: 700px)")

		function mobLogShow() {
			$('.sign-up-container').css('display', 'none');
			if (x.matches) {
				$('.sign-in-container').css('display', 'contents');
			}
			$('#mobLogShow').css('display', 'none');
			$('#mobRegShow').css('display', 'inline-block');
		}

		function mobRegShow() {
			if (x.matches) {
				$('.sign-up-container').css('display', 'contents');
			} else {
				$('.sign-up-container').css('display', 'inline-block');
			}

			$('#mobRegShow').css('display', 'none');
			$('#mobLogShow').css('display', 'inline-block');
		}

		$('#signUp').on('click', function() {
			var img = 'https://www.fourmodules.in/image_fly.jpg?width=200&height=77&image=https://www.fourmodules.com/SARK/assets/sub_domain/122.png&time=';
			$('.logo_main_top').animate({
				//opacity: 0
			}, 1000, function() {
				// Callback
				$('.logo_main_top').css("background-image", "url(" + img + ")").promise().done(function() {
					// Callback of the callback :)
					$('.logo_main_top').animate({
						opacity: 1
					}, 1000)
				});
			});
		});

		$('#signIn').on('click', function() {
			var img = 'https://www.fourmodules.com/portal/assets/img/logo_main.png';
			$('.logo_main_top').animate({
				//opacity: 0
			}, 1000, function() {
				// Callback
				$('.logo_main_top').css("background-image", "url(" + img + ")").promise().done(function() {
					// Callback of the callback :)
					$('.logo_main_top').animate({
						opacity: 1
					}, 1000)
				});
			});
		});
		const signUpButton = document.getElementById('signUp');
		const signInButton = document.getElementById('signIn');
		const container = document.getElementById('container');

		signUpButton.addEventListener('click', () => {
			container.classList.add("right-panel-active");
		});

		signInButton.addEventListener('click', () => {
			container.classList.remove("right-panel-active");
		});
		//# sourceURL=pen.js
	</script>

	<link href="<?= base_url('assets/user_panel/login/popup.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" media="all" onload="if(media!=='all')media='all'">	

	<script src="<?= base_url('assets/user_panel/login/select2.min.js?v=' . rand()) ?>"></script>
	<script src="<?= base_url('assets/user_panel/login/bootstrap.min.js?v=' . rand()) ?>"></script>
	<script src="<?= base_url('assets/user_panel/login/common.js?v=' . rand()) ?>"></script>
	<script src="<?= base_url('assets/user_panel/login/jquery-ui.js?v=' . rand()) ?>"></script>


	<!-- firebase end -->
	<script src="<?= base_url('assets/user_panel/login/firebase-app.js?v=' . rand()) ?>"></script>
	<script src="<?= base_url('assets/user_panel/login/firebase-auth.js?v=' . rand()) ?>"></script>
	<script src="<?= base_url('assets/user_panel/login/firebaseui.js?v=' . rand()) ?>"></script>

	<script src="<?php echo base_url(); ?>assets/user_panel/js/jquery.validate.js"></script>
	<script src="<?php echo base_url(); ?>assets/user_panel/js/bootstrap-dialog.js"></script>

	<div id="firebase_appTestAppend">

	</div>
	<script>
		$("#data_form").validate({
			errorPlacement: function(error, element) {
				element.parents('div.login-error').after(error);
			}
		});

		var em = 0;
		var con = 0;

		$(document).ready(function() {
			$('#signInBtnId').click(function(e) {
				$('.signupelement').removeClass('hidden-element');
				$('.signinelement').addClass('hidden-element');
				$('.image-container').addClass('width-change');
			});
			$('#signUpBtnId').click(function(e) {
				$('.signinelement').removeClass('hidden-element');
				$('.signupelement').addClass('hidden-element');
				$('.image-container').removeClass('width-change');
				$(".image-container").animate({
					maxwidth: "67%",
					right: "651px",
					left: "0px",
				}, 1500);
				$('.signinelement-login').addClass('transistionEffects');
			});
		});

		// HEADER FIXED WHEN SCROLL
		if ($('.header-main').hasClass('homepage-01')) {
			if ($(window).width() > 767) {
				var topmenu = $(".header-topbar").height();
				$(window).scroll(function() {
					if ($(window).scrollTop() > topmenu) {
						$(".header-main.homepage-01").addClass('header-fixed');
					} else {
						$(".header-main.homepage-01").removeClass('header-fixed');
					};
				});
			} else {
				var offset = 117;
				$(window).scroll(function() {
					if ($(window).scrollTop() > offset) {
						$(".header-main.homepage-01").addClass('header-fixed');
					} else {
						$(".header-main.homepage-01").removeClass('header-fixed');
					}
				});
			}
		}


		jQuery.validator.addMethod("lettersonly", function(value, element) {
			return this.optional(element) || /^[a-z\s]+$/i.test(value);
		}, "Only alphabetical characters");

		var errorMsg = '',
			$valid = false;
		$.validator.addMethod("is_sponser_exist", function(val, elem) {
			var len = $("#sponsor_id").val();
			if (len.length >= 10) {
				$.ajax({
					async: false,
					url: '<?php echo base_url(); ?>login/is_sponser_exist',
					type: "get",
					dataType: "json",
					data: {
						sponsor_id: $("#sponsor_id").val()
					},
					success: function(response) {
						if (response.status == 1) {
							errorMsg = 'Sponsor Available !';
							$valid = true;
							$("#member_name_section").html('Sponsor Name: ' + response.member_name);
							$("#member_name_section").addClass("successSpan");
                        	$("#member_name_section").removeClass("errorSpan");

						} else if (response.status == 2) {
							errorMsg = 'Sponsor not exist !';
							$valid = false;
							$("#member_name_section").html('');
						} else if (response.status == 3) {
							errorMsg = 'This Sponsor is not upgraded !';
							$valid = false;

							$("#member_name_section").html('');
						}
					}
				});
			}

			$.validator.messages["is_sponser_exist"] = errorMsg;
			return $valid;
		}, '');

		$("#signUpForm").validate({
			rules: {
				sponsor_id: {
					required: true,
					is_sponser_exist: true,
				},
			},
			errorPlacement: function(error, element) {
				//element.before(error);

				element.parents('div.input-group').after(error);
			}
		});
	</script>

	<div class="modal fade successful-form-sn" id="login_detail_modal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<div style="text-align: center;">
						<img style="width:79%" src="<?= base_url('assets/user_panel/mail/Pointwish_lOGO.jpg')  ?>">
					</div>
					<h2>Success!</h2>
					<p>
						Your registration is successful. Login details are emailed to your email ID. If you did not receive the details,it is possible that you entered worng email ID. Please enter new correct email ID and register again.
					</p>
					<h4 class="modal-title">Following is your login details:</h4>
					<ul class="list-inline list-unstyled">
						<li><strong>User name:</strong> <span id="username_span">PW10576</span></li>
						<li> <strong>Password:</strong> <span id="password_span">123456</span></li>
					</ul>

					<a href="<?php echo base_url(); ?>login"><button type="button" class="btn btn-default">OK</button></a>
				</div>
			</div>
		</div>
	</div>
	<ul id="ui-id-1" tabindex="0" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="display: none;"></ul>
	<div role="status" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
	<ul id="ui-id-2" tabindex="0" class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front" style="display: none;"></ul>
	<div role="status" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></div>
</body>

</html>