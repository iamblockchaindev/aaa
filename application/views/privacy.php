<?php $this->load->view('common/header'); ?>
<head>
    !-- all css here -->
	<!-- bootstrap v3.3.6 css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/bootstrap.min.css') ?>">
	<!-- owl.carousel css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/owl.carousel.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/front/css/owl.transitions.css') ?>">
	<!-- Animate css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/animate.css') ?>">
	<!-- meanmenu css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/meanmenu.min.css') ?>">
	<!-- font-awesome css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/font-awesome.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/front/css/themify-icons.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/front/css/flaticon.css') ?>" >
	<!-- magnific css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/magnific.min.css') ?>">
	<!-- style css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/style.css') ?>">
	<!-- responsive css -->
	<link rel="stylesheet" href="<?= base_url('assets/front/css/responsive.css') ?>">
    <link href="<?= base_url('assets/front/css/custom.css') ?>" rel="stylesheet">

    <!-- modernizr css -->
    


    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    

    <style>
        body {
            
line-height: 1.25;
}

table {
border: 1px solid #ccc;
border-collapse: collapse;
margin: 0;
padding: 0;
width: 100%;
table-layout: fixed;
}

table caption {
font-size: 1.5em;
margin: .5em 0 .75em;
}

table tr {
background-color: #f8f8f8;
border: 1px solid #ddd;
padding: .35em;
}

table th,
table td {
padding: .625em;
text-align: center;
}

table th {
font-size: .85em;
letter-spacing: .1em;
text-transform: uppercase;
}

@media screen and (max-width: 600px) {
table {
border: 0;
}

.topnav{
    display:inline-block !important;
    width: 100%;
    position: fixed;
}

table caption {
font-size: 1.3em;
}

table thead {
border: none;
clip: rect(0 0 0 0);
height: 1px;
margin: -1px;
overflow: hidden;
padding: 0;
position: absolute;
width: 1px;
}

table tr {
border-bottom: 3px solid #ddd;
display: block;
margin-bottom: .625em;
}

table td {
border-bottom: 1px solid #ddd;
display: block;
font-size: .8em;
text-align: right;
}

table td::before {
/*
* aria-label has no advantage, it won't be read inside a table
content: attr(aria-label);
*/
content: attr(data-label);
float: left;
font-weight: bold;
text-transform: uppercase;
}

table td:last-child {
border-bottom: 0;
}
}

@media screen and (max-width: 600px) {
.about{
    display: none;
}
.counter-area{
    display: block !important;
}
}






    </style>





</head>

<body>
 
    <!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->


  <?php
  include 'header.php';
  ?>
    <!-- header end -->
    <!-- Start Slider Area -->
  
    <!-- End Slider Area -->
    <!-- Start Count area --><section id="top">
    <div class="about">
        <!-- <h2 style="margin-top: 100px; color: white; text-align: center;">About Us</h2> -->
        <center><img src="<?= base_url('assets/front/image/privacy.jpg') ?>" style="margin-top: 10px;"></center>
    </div>
    <div class="counter-area fix bg-color area-padding-2" style="display: none;">
        <h2 style="margin-top: 100px; color: white; text-align: center;">Privacy Policy</h2>
    </div>
<div style="margin-left: 20%; margin-right: 20%; margin-top: 40px; text-align: justify;
-moz-text-align-last: center;
text-align-last: left;" >
        

    <h2 style="color:#383838">General rules</h2>
    
    <p style="color: black;">Point Wish Ltd. values customers’ personal information highly and complies with laws,
        including the Act on Promotion of Information and Communications Network Utilization
        and Information Protection, and other relative laws regarding the Personal Information
        Protection Act, and take such action to protect customer’s personal information.</p>
    <p style="color: black;">“Processing Personal Information” means a guideline that Company needs to comply in
        order to let the customers to use the services with confidence by protecting customer’s
        personal information, which the company highly values. 
        </p>
        <p style="color: black;">This processing personal information is applied to company’s
            website (https://www.pointwish.com) service. Separate privacy policies may apply to
            services provided on other websites.
            
        </p>
        <p style="color: black;">The company will company will notify you through the website announcement (or
            individual notice) when revising the personal information processing policy. Users are
            urged to check back frequently when visiting our site.             
        </p>
        <h2 style="color: #383838;">Purpose of collecting and using of personal information </h2>
        <p style="color: black;">Company may process personal information for the following purposes. The processed
            personal information will not be used for any purpose other than the following purposes
            and will be subject to prior agreement if the purpose of use is changed.                    
        </p>
        <p style="color: black;"> • Verification of a user’s identity, his/her willingness to obtain membership, his/her
            willingness to withdrawal membership and managing members.
            <br> •&nbsp; Preventing and sanctioning acts that interfere with the smooth operation of the
            service including restrictions on use and illegal use of members in violation<br>&nbsp;&nbsp;&nbsp;&nbsp;of
            laws and regulations and Company’s Terms of Use, preventing theft of accounts
            and fraudulent transactions.
            <br> • Provide notices such as amendment of the terms of use, preserve records for
            dispute settlement, process complaint, protect user and service operation.
            <br> • Authentication for payment and withdrawal.
            <br> • Providing services, providing content, and providing personalized services.
            <br> • Establishing a service utilization environment that users can use with confidence
            in terms of security, privacy, and safety.
            <br> • Data to provide personalized services such as service usage record and access
            frequency analysis, statistics on service utilization, customized service &nbsp;&nbsp;&nbsp;according
            to service analysis and statistics, and advertisement.
            <br> • Inform any upcoming events, marketing, advertisement, and etc.
            <br> • Providing information upon a requested made by proper and legal investigation
            on hacking/fraud and other performance of duty by  law .
                                 
        </p>
        <h2 style="color: #383838;">List of collecting personal information and method of collecting </h2>
        <p style="color: black;">List of collecting personal information and method of collecting The Company collects
            minimum personal information that customer need to use the services<br>of the Company.
            Customer may refuse to consent to collection or use of personal information if one does
            not wish to do so. However, if you do not agree to<br>the collection and use of essential
            items in the collected personal information items, there may be restrictions on the use of
            some functions.                      
        </p>
        <p style="color: black;">The personal information that the company collects from the user is as follows. 
        </p>
        <h2 style="color: #383838;">Registration </h2>
        <p style="color: black;">• E-mail, Password<br>• Providing exchange/wallet service <br>• Duplicated registration information(DI), and identification information of the same
            person(CI)  
        </p>

        <p style="color: black;">Information that is automatically generated during the process of using the service or
            processing the business: service usage history, access logs, cookies, access IP
            information, payment history.
        </p>
        <h2 style="color: #383838;">Confirmation identity </h2>
        <p style="color: black;">Copy of identification (make blank all other information except date of birth).   
        </p>
        <h2 style="color: #383838;">Others </h2>
        <p style="color: black;">Company will not collect sensitive personal information that may infringe human rights
            (such as race, ethnicity, ideology, creed, place of birth, homeland,<br>political orientation
            and criminal record, health status and sex life), and Company will obtain prior consent
            when the Company have to collect above<br>information. 
               
        </p>
        <p style="color: black;">In any case, the company will not use the information that the customer input for any
            other purpose other than the purpose stated above, or leak. 
               
        </p>
        <p style="color: black;">The Company collects personal information through the following methods:   
        </p>
        <p style="color: black;">• The company collect personal information which the customer, themselves, input and
            consent when signing up and using services. (Consent of legal &nbsp;&nbsp;&nbsp;representative is
            necessary when collecting personal information of children under 14 years old)
            <br>• Personal information of users can be collected through web pages, e-mail, fax, and telephone during the consultation process through the Customer Centre.  
            <br>• Personal information can be collected in written form at events, seminars, etc. held
            offline.  
            <br>
            • Generation information such as device information can be automatically generated and
            collected during the process of using PC web or mobile web (access IP &nbsp;&nbsp;&nbsp;information,
            cookie, service usage log, access log).  
        </p>
    
        <p style="color: black;">
        </p>
        <h2 style="color: #383838;">Retention and usage period of the collected personal information</h2>
        <p style="color: black;">If the Company collects personal information of the user, the retention period is until the
            cancellation (including withdrawal application, withdrawal of directorship) after sign up.
            Also, at the time of termination, the Company will destroy the personal information of the 
            user and instruct the third party to destroy the personal information that is provided to
            the third party. 
        </p>
        <p style="color: black;">However, if there is a need to preserve by the laws and regulations such as commercial
            law, Company may retain transaction details and minimum basic information during the
            preservation period prescribed by laws and regulations, and notify customers regarding
            this period in advance, and the company may preserve the customer’s personal
            information if the period of retention has not elapsed or if the company receive
            customer’s consent individually. 
        </p>
        <p style="color: black;">However, if there is a need to preserve by the laws and regulations such as commercial
            law, Company may retain transaction details and minimum basic information during the
            preservation period prescribed by laws and regulations, and notify customers regarding
            this period in advance, and the company may preserve the customer’s personal
            information if the period of retention has not elapsed or if the company receive
            customer’s consent individually.  
        </p>
        <h2 style="color: #383838;">Records of consumer complaints or disputes </h2>
        <p style="color: black;"> • Retention reason: consumer protection act in electronic commerce
            <br> • Retention period: 3 years  
        </p>
        <h2 style="color: #383838;">Records on the collection, processing and use of credit information 
        </h2>
        <p style="color: black;"> • Retention reason: Act on the Use and Protection of Credit Information
            <br> • Retention period: 3 years 
        </p>
        <h2 style="color: #383838;">Record of payment and goods supply  </h2>
        <p style="color: black;"> • Retention reason: Consumer Protection Act in Electronic Commerce
            <br> • Retention period: 5 years 
            
        </p>
        <h2 style="color: #383838;">Record of Log In 
        </h2>
        <p style="color: black;"> • Retention reason: Protection of communications secrets Act
            <br> • Retention period: 3 months 
            
        </p>
        <h2 style="color: #383838;">Company internal policy to prevent illegal use 
        </h2>
        <p style="color: black;"> • Record period of illegal use: 5 years  
            <br>• Procedures and methods of personal information destruction 
            <br>• In principle, the company destroys customer’s personal information without any delay
            when the purpose of collecting personal information if achieved. The &nbsp;&nbsp;&nbsp;procedure and
            method of destroying personal information of the company are as follows.
        </p>
        
        <h2 style="color: #383838;">Destruction procedure   </h2>
        <p style="color: black;">The information entered by the user for registration is transferred to a separate DB
            (retained in separate cabinet in case of written form) after the purpose is achieved and is 
            stored for a certain period of time and destroyed after the period according to internal
            policy and other relevant laws and regulations (refer to retention and usage period).        
        </p>
        <h2 style="color: #383838;">Destruction period  </h2>
        <p style="color: black; ">In the case where the personal information of the user has elapsed, destruction
            should be taken place within 5 days from the end of the period of holding the personal
            information. If the personal information such as accomplishing the purpose of processing
            personal information, abolishing the service, the destruction should be taken place within
            5 days from the day when it is deemed unnecessary to process the personal information.              
        </p>
        <h2 style="color: #383838;">Destruction method
        </h2>
        <p style="color: black;"> • Personal information printed on paper shall be destroyed by being shredded
            through shredders or burnt
            <br> • Personal information stored in an electronic file is deleted using a technical
            method that can’t play the record              
        </p>
        <h2 style="color: #383838;">Transfer of personal information abroad 
        </h2>
        <p style="color: black;">The company uses overseas could services to provide smooth services and enhance
            user convenience. In this regard, we inform you about the transfer of personal
            information abroad as follows. 
                        
        </p>
        <p style="color: black;">Accepting the transfer of personal information according to use of overseas server
            (Microsoft Azure).
                         
        </p>
        <h2 style="color: #383838;">Transferring items  
        </h2>
        <p style="color: black;">E-mail, password, name, cell phone number, account number, date of birth, sex,
            domestic/foreign, duplicated registration information(DI), and identification information of
            the same person(CI), access IP, cookies, service usage record, access logs, credit
            rating information, address, career, copy of identification (masking unique identification
            information), video information. 
                         
        </p>
        <h2 style="color: #383838;">Transfer country - date - method 
        </h2>
        <p style="color: black;"> • County: Microsoft Azure service provided area
            <br> • Date: Registration and revision of each customer
           <br> • Method: Transfer data through highly secured private network server that are in
            Global cloud area          
        </p>
        <h2 style="color: #383838;">Transfer recipient 
        </h2>
        <p style="color: black;"> • Name of company: Microsoft Corporation
            <br>• Address: 15010 NE 36th St Redmond, WA 98052
            <br>• Phone number: (800) 642-7676     
        </p>
        <h2 style="color: #383838;">Transfer Purpose 
        </h2>
        <p style="color: black;"> • Provide Global Cloud service in order to operate this Web site.     
        </p>
        <h2 style="color: #383838;">Retention ∙ usage period 

        </h2>
        <p style="color: black;"> • Until personal information is retained and purpose is fulfilled.     
        </p>
        <h2 style="color: #383838;">Consignment of personal information procedure 
        </h2>
        <p style="color: black;">The company entrusts the following personal information processing tasks for the
            smooth handling of personal information business, and both regulates and supervises
            the entrusted company to process personal information safely in accordance with the
            Information and Communications Network Act.     
        </p>
        <p style="color: black;">Customers have the right to refuse consent to the consignment (provision) of personal
            information. However, customer may be restricted from using the services when refused
            to consent.   
        </p>
        <h2 style="color: #383838;">Customer’s rights and how to exercise the rights 
        </h2>
        <p style="color: black;">1. The subject of the information can exercise the right of personal information
            protection at any time with respect to the company
           <br> • Personal information request
            <br> • Request revision when error is found
            <br> • Request destruction
            <br> • Request stop the procedure
        </p>
        <p style="color: black;">2. The exercise of rights under 1) may be done in writing, by telephone, e-mail, or
            fax (FAX) to the person in charge of personal information protection and the
            department in charge. And the Company take action without any delay.    
        </p>
        <p style="color: black;">3. If the subject requests correction or deletion of personal information, the
            company will not use or provide the personal information until the correction or
            deletion is completed.
        </p>
         
        <p style="color: black;">4. Users can view or modify their registered personal information at any time and
            may request withdrawal of consent and termination of membership.             
        </p>
        <p style="color: black;">5. The company does not accept membership of children under the age of 14 who
            need the consent of legal representative.  
        </p>
        <h2 style="color: #383838;">Issues regarding Cookie operation 
        </h2>
        <p style="color: black;">Cookie means “contact information file” and is used by the company to provide members
            with faster and more convenient use of the website, and to provide specialized
            customized services. The Company identifies your computer with regard to cookie
            management but does not identify you individually. 
              
        </p>
        <p style="color: black;">Customers have the option of installing cookies. By setting options in the web browser,
            members can accept all cookies, check each time a cookie is saved, or refuse to save all
            cookies. However, if you refuse to install cookies, you will not be able to use the web
            conveniently, and you may have difficulty using some services that require login. 
             
        </p>
        <p style="color: black;">Measures to ensure the safety of personal information 
 
        </p>
        <p style="color: black;">The Company, pursuant to Article 29 of the Personal Information Protection Act, has the
            following technical, administrative and physical measures to ensure safety. 
        </p>
        <h2 style="color: #383838;">Technical measures against hacking 
        </h2>
        <p style="color: black;">The Company installs security programs to prevent leakage and damage of personal
            information caused by hacking and computer viruses, and the Company periodically
            updates and checks, installs the system in an area where the place is separated from
            outside and technically and physically monitors and blocks them. 
        </p>
        <h2 style="color: #383838;">Minimize the staff and educate them
        </h2>
        <p style="color: black;">The Company limits the personal information processing staff to the person in charge
            and implements measures to manage personal information. In addition, we constantly
            emphasize compliance with the company's personal information processing policy
            through on-the-job training to the person in charge.  
        </p>
        <h2 style="color: #383838;">Encryption of personal information 
        </h2>
        <p style="color: black;">The personal information of the user is encrypted, stored and managed so that only the
            user can know it, and the important data is using the separate security function such as
            encrypting the file and transmission data or using the file lock function. 
        </p>
        <h2 style="color: #383838;">Restricted access to personal information  
        </h2>
        <p style="color: black;">The company takes necessary measures to control access to personal information
            through granting, modifying, and deleting access rights to the database system that
            processes personal information. We also control unauthorized access from outside by
            using an intrusion prevention system. 
        </p>
        <h2 style="color: #383838;">Using locks for document security  
        </h2>
        <p style="color: black;">We keep documents with personal information and auxiliary storage media in safe place
            with lock. 
             
        </p>
        <h2 style="color: #383838;">Access control to unauthorized persons. 
        </h2>
        <p style="color: black;">The company has set up a separate physical storage place for storing personal
            information and both set up and operates access control procedures on this place. 
             
        </p>
        <h2 style="color: #383838;">Duty of Notification 
        </h2>
        <p style="color: black;">In the case of addition, deletion or modification of this Privacy Policy, we will announce
            amendment and contents through announcement at least 7 days prior to the
            enforcement date.              
        </p>
        <h2 style="color: #383838;">Effective date: June 10th 2020 
        </h2><br><br>

</div>
</section>

    <!-- End Count area -->
    <!-- about-area start -->
    
    <!-- about-area end -->
    <!-- Start Invest area -->
   
    <!-- End Invest area -->
    <!-- Start Support-service Area -->
    
    <!-- End Support-service Area -->
    <!--Start payment-history area -->
    
    <!-- End payment-history area -->
   
    <!--End Widrawl-->
    <!--Start Franchise-->
    
    <!--End Franchise-->
    <!-- Start Affiliate Area -->
  
    <!-- End Affiliate Area -->
    <!-- Start Overview Area -->
   
    </div>
    <!-- End Overview Area -->
    <!-- Start Blog area -->
   
    <!-- End Blog area -->
    <!-- Start Subscribe area -->
    
    <!-- End Subscribe area -->
    <!-- Start Footer Area -->
    <?php $this->load->view('common/footer'); ?>
    <!-- End Footer area -->

    <!-- all js here -->

    <!-- jquery latest version -->
		<script src="<?= base_url('assets/front/js/jquery-2.2.4.min.js') ?>"></script>
		<!-- bootstrap js -->
		<script src="<?= base_url('assets/front/js/bootstrap.min.js') ?>"></script>
		<!-- owl.carousel js -->
		<script src="<?= base_url('assets/front/js/owl.carousel.min.js') ?>"></script>
		<!-- magnific js -->
		<script src="<?= base_url('assets/front/js/magnific.min.js') ?>"></script>
		<!-- wow js -->
		<script src="<?= base_url('assets/front/js/wow.min.js') ?>"></script>
		<!-- meanmenu js -->
		<script src="<?= base_url('assets/front/js/jquery.meanmenu.js') ?>"></script>
		<!-- Form validator js -->
		<script src="<?= base_url('assets/front/js/form-validator.min.js') ?>"></script>
		<!-- plugins js -->
		<script src="<?= base_url('assets/front/js/plugins.js') ?>"></script>
		<!-- main js -->
		
		<script src="<?= base_url('assets/front/js/multislider.min.js') ?>"></script>
		<script src="<?= base_url('assets/front/js/main.js') ?>"></script><a id="scrollUp" href="#top" style="position: fixed; z-index: 2147483647;"><img src="<?= base_url('assets/front/image/icon/upload.png') ?>" height="20" width="20" style="margin-top: 10px;"></i></a>
 
    <script>
        function myFunction() {
          var x = document.getElementById("myLinks");
          if (x.style.display === "block") {
            x.style.display = "none";
          } else {
            x.style.display = "block";
          }
        }
        </script>

    <script>
        $('#exampleSlider').multislider({
            interval: 4000,
            continuous: true,
            duration: 5000
        });
    </script>






</body>

</html>