<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Forget Password</title>
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="icon" href="<?php echo base_url(); ?>assets/front/image/favicon.png?v=<?= rand() ?>" type="image/png" sizes="32x32">
    <!-- LIBRARY FONT-->
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/font-awesome.css?v=' . rand()) ?>" media="all" onload="if(media!=='all')media='all'">
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/bootstrap.css?v=' . rand()) ?>">
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/color-9.css?v=' . rand()) ?>" media="all" onload="if(media!=='all')media='all'">
    <!-- LIBRARY CSS-->
    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/setresponsive.css?v=' . rand()) ?>" media="all" onload="if(media!=='all')media='all'">
    <!-- <script type="text/javascript" async="" src="<?= base_url('assets/user_panel/login/analytics.js?v=' . rand()) ?>"></script> -->
    <script src="<?= base_url('assets/user_panel/login/jquery.min.js?v=' . rand()) ?>"></script>

    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/demo-signin.css?v=' . rand()) ?>">
    <link href="<?= base_url('assets/user_panel/login/select2.min.css?v=' . rand()) ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/login/bootstrap.min.css?v=' . rand()) ?>">


    <link type="text/css" rel="stylesheet" href="<?= base_url('assets/user_panel/login/firebaseui.css?v=' . rand()) ?>">
    <link href="<?= base_url('assets/user_panel/login/firebase.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/user_panel/login/jquery-ui.css?v=' . rand()) ?>">
    <style type="text/css">
        @media screen and (max-width: 600px) {
            .inpt {
                width: 100%;
                /* margin-left: -10% !important; */
            }

            .frgt {
                margin-left: -1% !important;
            }

            .tg {
                margin-left: -10% !important;
            }

            .pclass {
                margin-left: 5% !important;
            }
        }

        @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');

        * {
            box-sizing: border-box;
        }

        body {
            background: #f6f5f7;
            /* background-image: url(11_bg.png); */
            display: block;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            font-family: 'Montserrat', sans-serif;
            margin: 0px 0px;
        }

        header {
            font-family: lato, Helvetica, Arial, sans-serif;
        }

        .desktop_log_menu .btn {
            border-radius: 0px;
        }

        h1 {
            font-weight: bold;
            margin: 0;
            font-size: 42px !important;
        }

        h2 {
            text-align: center;
        }

        p {
            font-size: 14px;
            font-weight: 100;
            line-height: 20px;
            letter-spacing: 0.5px;
            margin: 20px 0 30px;
        }

        .heading_tag {
            font-weight: bold;
            margin: 0;
            font-size: 42px !important;
            text-align: center;
        }

        span {
            font-size: 12px;
        }

        a {
            color: #333;
            font-size: 14px;
            text-decoration: none;
            margin: 0px 0;
        }

        .button-design {
            border-radius: 0px !important;
            border: none !important;
            background-color: #ff9700;
            color: #FFFFFF;
            padding: 12px 45px !important;
            letter-spacing: 1px;
            text-transform: uppercase;
            transition: transform 80ms ease-in;
        }

        button:active {
            transform: scale(0.95);
        }

        button:focus {
            outline: none;
        }

        button.ghost {
            border: 1px solid #fff !important;
            background-color: transparent;
            border-color: #FFFFFF;
        }

        form {
            background-color: #FFFFFF;
            display: block;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 35px 29px 37px;
            height: 100%;
            text-align: center;
        }

        input,
        select {
            border-radius: 0px;
            background-color: #eef5f3;
            border: none;
            padding: 12px 15px;
            margin: 8px 0;
            width: 100%;
        }

        input,
        select,
        textarea {
            margin: 4px 4px;
        }

        #programSelect {
            -moz-appearance: none;
            -webkit-appearance: none;
            appearance: none;
            background-repeat: no-repeat;
            background-size: 0.5em auto;
            background-position: right 0.90em center;
            padding-right: 1em;
            background-image: url("data:image/svg+xml;charset=utf-8, \<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 60 40'> \<polygon points='0,0 60,0 30,40' style='fill:black;'/> \</svg>");
        }

        .container {
            margin-top: 9% !important;
            margin-bottom: 9% !important;
            z-index: 999;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
            position: relative;
            overflow: hidden;
            max-width: 500px !important;
            min-height: 455px;
        }

        .btn.btn-transition-3:hover {
            color: #fff;
        }

        .form-container {
            position: absolute;
            top: 0;
            height: 100%;
            transition: all 0.6s ease-in-out;
        }

        header .header-main.header-fixed .logo .gifimg_flag {
            max-width: 67px;
            width: 70%;
            transition: all .3s ease-in-out;
        }

        .sign-in-container {
            left: 0;
            width: 50%;
            /*                z-index: 2;*/
            position: absolute;
            opacity: 1
        }

        .container.right-panel-active .sign-in-container {
            transform: translateX(100%);
            -webkit-transform: translateX(100%);
            opacity: 0;
            z-index: 2;
        }

        .sign-up-container {
            left: 0;
            width: 50%;
            opacity: 0;
            /*z-index: 1;*/
            position: absolute
        }

        .container.right-panel-active .sign-up-container {
            transform: translateX(100%);
            -webkit-transform: translateX(100%);
            opacity: 1;
            z-index: 5;
            animation: show 0.6s;
            position: absolute
        }

        @keyframes show {

            0%,
            49.99% {
                opacity: 0;
            }

            50%,
            100% {
                opacity: 1;
            }
        }

        @-webkit-keyframes show {

            0%,
            49.99% {
                opacity: 0;
            }

            50%,
            100% {
                opacity: 1;
            }
        }

        .overlay-container {
            position: absolute;
            top: 0;
            left: 50%;
            width: 50%;
            height: 100%;
            overflow: hidden;
            transition: transform 0.6s ease-in-out;
            z-index: 100;
        }

        .container.right-panel-active .overlay-container {
            transform: translateX(-100%);
            -webkit-transform: translateX(-100%);
        }

        .overlay {
            background-size: cover;
            background-image: url("Bg_01.png");
            background-repeat: repeat;
            background-position: center;
            color: #FFFFFF;
            position: relative;
            left: -100%;
            height: 100%;
            width: 200%;
            transform: translateX(0);
            -webkit-transform: translateX(0);
            transition: transform 0.6s ease-in-out;
        }

        .container.right-panel-active .overlay {
            transform: translateX(50%);
            -webkit-transform: translateX(50%);
        }

        .overlay-panel {
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 0 40px;
            text-align: center;
            top: 0;
            height: 100%;
            width: 50%;
            transform: translateX(0);
            -webkit-transform: translateX(0);
            transition: transform 0.6s ease-in-out;
        }

        .overlay-left {
            transform: translateX(-20%);
            -webkit-transform: translateX(-20%);
        }

        .container.right-panel-active .overlay-left {
            transform: translateX(0);
            -webkit-transform: translateX(0);
        }

        .overlay-right {
            right: 0;
            transform: translateX(0);
            -webkit-transform: translateX(0);
        }

        .container.right-panel-active .overlay-right {
            transform: translateX(20%);
            -webkit-transform: translateX(20%);
        }

        .social-container {
            margin: 14px 0 7px;
        }

        .social-container a {
            border: 1px solid #DDDDDD;
            border-radius: 50%;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            margin: 0 5px;
            height: 40px;
            width: 40px;
        }

        footer {
            z-index: 222;
            background-color: #242c42;
            color: #fff;
            margin-top: 29px;
            padding: 0 10px 0 10px;
            font-size: 14px;
            bottom: 0;
            text-align: center;
        }

        footer p {
            color: #4F5B6B;
            margin: 10px 0;
            float: right;
            letter-spacing: 0;
        }

        footer i {
            color: red;
        }

        footer a {
            padding: 0px 12px;
            font-size: 13px;
            color: #ef5734;
            text-decoration: none;
        }

        footer a:hover {
            color: #ef5734;
        }

        .foot_link {
            float: left;
            margin: 10px 0;
        }

        /*.fa-facebook{background:#074bb3; color: #fff; padding: 10px 15px; border-radius: 50%; font-size: 26px;}*/

        .fa-google-plus {
            background: #f00;
            color: #fff;
            padding: 10px 15px;
            border-radius: 50%;
            font-size: 26px;
        }

        .login__form-title-wrapper[_ngcontent-c3] {
            width: 100%;
            height: auto;
            margin: auto;
        }

        #program_id label {
            margin: 0px 26px;
        }

        #program_id input[type="radio"] {
            opacity: 0;
            margin-top: 2px !important;
            width: 100px;
            z-index: 20;
        }

        .checkround {
            position: absolute;
            top: 0px;
            left: 2px;
            height: 17px;
            width: 17px;
            background-color: #fff;
            border-color: #908787;
            border-style: solid;
            border-width: 2px;
            border-radius: 50%;
            z-index: 1;
        }

        #program_id {
            float: left;
            width: 100%;
            margin: 4px 0px;
        }

        .forgot_password a {
            width: 100%;
            float: left;
        }

        .radio {
            display: inline-grid;
            position: relative;
            padding-left: 30px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 12px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            color: #818181;
            font-weight: 400;
        }

        .radio input[type="radio"],
        .radio-inline input[type="radio"],
        .checkbox input[type="checkbox"],
        .checkbox-inline input[type="checkbox"] {
            position: absolute;
        }

        .terms[_ngcontent-c3] {
            padding-left: 2.75rem;
            position: relative;
            border-radius: 1px;
            min-height: 20px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            margin-left: -14px;
        }

        /* #terms[_ngcontent-c3]:checked+.terms[_ngcontent-c3]::before {
            background: url(/assets/img/icon_checked.png) center center/16px 12px no-repeat #ef5734 !important;
        } */

        .redBack::before {
            background: #ffcccc !important
        }

        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: 700;
        }

        .terms__link[_ngcontent-c3] {
            color: #ff9700;
            cursor: pointer;
        }

        .terms__link[_ngcontent-c3] a:hover {
            color: #ef5734;
        }

        .redClass {
            color: red;
        }

        .redClass a:hover {
            color: red !important;
        }

        .errorSpan {
            clear: both;
            color: red;
            font-weight: 500;
            font-size: 9px;
            float: left;
            margin-bottom: 10px;
            text-align: left;
            width: 100%;
            height: 9px;
            padding: 0px 4px;
        }

        #program_id .errorSpan {
            text-align: center;
            height: 9px;
        }

        #sub_cat_name {
            text-align: center;
            height: 3px;
            font-size: 11px;
        }

        .terms__wrapper[_ngcontent-c3] {
            display: -webkit-box;
            display: -ms-flexbox;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: start;
            -ms-flex-pack: start;
            justify-content: flex-start;
            font-size: .8125rem;
            color: #141716;
            text-align: center;
            margin: 9px auto;
            margin-left: 21%;
            margin-top: 0;
        }

        .terms__wrapper input {
            width: 0%;
            opacity: 0
        }

        .radio .checkround::after {
            left: 2px;
            top: 2px;
            width: 9px;
            height: 9px;
            border-radius: 50%;
            background: #223335;
        }

        .radio .orange::after {
            background: #ef5734 !important;
        }

        .radio .green::after {
            background: #ef5734 !important;
        }

        .radio input:checked~.checkround::after {
            display: block;
        }

        .checkbox input[type="checkbox"],
        .checkbox-inline input[type="checkbox"],
        .radio input[type="radio"],
        .radio-inline input[type="radio"] {
            margin-left: 5px;
        }

        .checkround::after {
            content: "";
            position: absolute;
            display: none;
        }

        #ielts_modal .modal-header {
            color: #fff;
            border-bottom: none !important;
            background: #ef5734;
            padding: 11px 16px;
            border-top-left-radius: .3rem;
            border-top-right-radius: .3rem;
        }

        #ielts_modal .modal-header .close {
            padding: 11px !important;
            margin: -1rem -1rem -1rem auto;
        }

        #ielts_modal .modal-content {
            padding: 0 !important;
        }

        #ielts_modal .font-weight-bold {
            font-weight: normal !important;
            font-size: 20px;
            float: left;
            margin-top: 3px;
            margin-bottom: 3px;
        }

        .font-weight-bold {
            font-weight: 700 !important;
            font-size: 17px;
            float: left;
            margin-top: 3px;
            margin-bottom: 3px;
        }

        .modal-header .close {
            padding: 1rem;
            margin: -1rem -1rem -1rem auto;
        }

        #ielts_modal label {
            margin-left: 10px;
        }

        #ielts_modal .checkbox input[type="checkbox"],
        .checkbox-inline input[type="checkbox"],
        .radio input[type="radio"],
        .radio-inline input[type="radio"] {
            margin-left: 0px;
            width: 20px;
            opacity: 0
        }

        .close span {
            float: right;
            font-size: 2.5rem;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-shadow: 0 1px 0 #000;
        }

        .countryCodeSelect,
        .countryCodeSpan .select2.select2-container {
            margin: 3px 2px 0px 0px;
            width: 35% !important;
            float: left;
            padding: 0px 0px;
        }

        .wdt .select2 {
            width: 29% !important;
            float: left;
            margin: 8px 3px;
        }

        .wdt {
            width: 100%;
            border-radius: 0px;
            background: #eef5f3;
            margin: 4px 4px;
            float: left;
        }

        .wdt .select2-container--default .select2-selection--single {
            background-color: unset;
            border: none;
            border-radius: 4px;
        }

        .wdt input {
            width: 67%;
            border-radius: 0px 25px 25px 0px;
            border-left: #ccc 1px solid;
            margin: 0;
        }

        .wdt #contact {
            background-color: unset;
        }

        .countryCodeSpan,
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            font-size: 14px !important;
        }

        .form-container .row {
            margin-right: -10px;
            margin-left: -8px;
        }

        .logo_main_top {
            background-image: url("<?= base_url('assets/user_panel/login/logo.png') ?>") !important;
            width: 74px !important;
            display: block;
            height: 70px !important;
            cursor: pointer;
            background-size: cover;
            position: absolute;
            z-index: 999;
            top: 10px;
        }

        .page-logo_auth {
            background-image: url("https://www.fourmodules.com/portal/assets/img/logo_main_white.png");
            display: none;
        }

        .signUpFunClass {
            cursor: pointer;
        }

        #signUpSubmit:disabled {
            background-color: #ccc !important;
        }

        .passwd .eye {
            background-image: url("https://www.fourmodules.com/portal/assets/img/eye.png");
            width: 26px;
            height: 16px;
            position: absolute;
            top: 17px;
            right: 17px;
            background-repeat: no-repeat;
        }

        .passwd .eye:hover {
            background-image: url("https://www.fourmodules.com/portal/assets/img/eye_hover.png");
            cursor: pointer;
        }

        .term_cond {
            padding: 7px 0px 0px;
            height: 25px;
            margin-bottom: 10px;
        }

        #otp_modal .modal-header {
            color: #fff;
            border-bottom: none !important;
            background: #ef5734;
            padding: 11px 16px;
            border-top-left-radius: .3rem;
            border-top-right-radius: .3rem;
        }

        #otp_modal .modal-title {
            font-size: 20px;
            float: left;
            margin-top: 3px;
            margin-bottom: 3px;
        }

        #otp_modal .modal-header .close {
            padding: 11px !important;
            margin: -1rem -1rem -1rem auto;
            font-size: 28px;
        }

        #otp_modal .close span {
            float: right;
            font-size: 2.5rem;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-shadow: 0 1px 0 #000;
        }

        #otp_modal .modal-content {
            padding: 0px 0px 0px 0px !important;
        }

        #otp_Error {
            clear: both;
            color: red !important;
            font-weight: 500 !important;
            font-size: 12px;
            float: left;
            margin-bottom: 10px;
            text-align: center;
            width: 100%;
        }

        #otp_modal .mt-2 button {
            border-radius: 20px;
            border: none;
            color: #FFFFFF;
            font-size: 11px;
            padding: 13px 45px !important;
            letter-spacing: 1px;
            text-transform: uppercase;
            margin: 15px 2px 4px;
        }

        #otp_modal .mt-2 {
            text-align: center;
        }

        #verifyOTP .modal-dialog {
            width: 355px;
            margin: 160px auto;
        }

        #verifyOTP .modal-header {
            color: #fff;
            border-bottom: none !important;
            background: #ef5734;
            padding: 11px 16px;
            border-top-left-radius: .3rem;
            border-top-right-radius: .3rem;
        }

        #verifyOTP .modal-title {
            font-size: 20px;
            float: left;
            margin-top: 3px;
            margin-bottom: 3px;
        }

        #verifyOTP .modal-header .close {
            padding: 10px 10px;
            margin: -1rem -1rem -1rem auto;
            font-size: 28px;
        }

        #verifyOTP .close span {
            float: right;
            font-size: 2.5rem;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-shadow: 0 1px 0 #000;
        }

        #verifyOTP .modal-content {
            padding: 0px 0px 0px 0px !important;
        }

        #OTPfooter button {
            border-radius: 20px;
            border: none;
            color: #FFFFFF;
            font-size: 12px;
            padding: 12px 45px;
            letter-spacing: 1px;
            text-transform: uppercase;
        }

        #OTPfooter {
            text-align: center;
        }

        #verifyOTPBody {
            text-align: center;
        }

        #verifyOTPBody {
            text-align: center;
        }

        #signUpSubmit:disabled {
            background-color: #ccc !important;
            background-image: unset !important;
        }

        .error1,
        .error1 a {
            background-color: #ffcccc !important;
            color: #000 !important;
        }

        #signUpSubmit:disabled:hover {
            background-color: #ccc !important;
            background-image: unset !important;
        }

        .sp_t_log {
            padding: 92px 29px 37px;
        }

        .top_h {
            background-image: none;
            border: none;
            float: left;
            width: 100%;
            padding: 7px 15px 0px;
        }

        #privacy_modal2 .modal-content {
            padding: 0 !important;
            border: none;
        }

        #privacy_modal2 .modal-header {
            color: #fff;
            border-bottom: none !important;
            background: #ef5734;
            padding: 11px 16px;
            border-top-left-radius: .3rem;
            border-top-right-radius: .3rem;
            width: 100%;
        }

        #privacy_modal2 .modal-title {
            font-weight: normal !important;
            font-size: 20px !important;
            float: left;
            margin-top: 3px;
            margin-bottom: 3px;
        }

        #privacy_modal2 .modal-header .close {
            padding: 11px !important;
            margin: -1rem -1rem -1rem auto;
        }

        .close span {
            float: right;
            font-size: 2.5rem;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-shadow: 0 1px 0 #000;
        }

        #privacy_modal2 .modal-body {
            overflow-y: scroll;
            height: 400px;
        }

        #privacy_modal2 .modal-dialog {
            margin-top: 5%;
        }

        #log_regs .fa-whatsapp {
            margin-right: 5px !important;
        }

        .top_h .logo {
            display: none;
        }

        .containern {
            width: 1170px;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
            display: table;
        }

        header .header-main {
            margin-bottom: 29px;
        }

        header .header-main.header-fixed {
            z-index: 976;
            position: fixed;
            padding: 6px 0px;
            top: 0;
        }

        .userExists {
            color: red;
            font-weight: bold;
            text-align: center;
            font-size: 13px;
        }

        header .header-main .navigation .nav-links li .main-menu {
            margin: 0 20px;
        }

        header .header-main .logo .gifimg_flag img {
            max-width: 77px;
            width: 73%;
            transition: all .3s ease-in-out;
        }

        header .header-main.header-fixed .logo .gifimg_flag img {
            max-width: 77px;
            width: 70%;
            transition: all .3s ease-in-out;
        }

        header .header-main .logo {
            width: 30%;
        }

        @media screen and (max-width:1024px) {
            .gifimg_flag {
                position: absolute;
                left: 15px;
            }
        }
    </style>

    <script>
        var BASE = "<?= base_url() ?>";
    </script>
   
</head>

<body translate="no" id="log_regs">
    <div id="loading"></div>
    <div class="container container_d" id="container" style="z-index:0;">
        <div class="row">
            <div class="col-md-12 top_h">
                <div class="logo">
                    <a href="/">
                        <div class="logo_main_top" style="background-image: <?= base_url('assets/user_panel/login/logo.png') ?>;">
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="form-container sign-in-container " style="width: auto;">
            <form action="" method="post" id="form_reset" style="height: auto;">
                <h1 class="tg">&nbsp;&nbsp;&nbsp;Forget&nbsp;Password</h1>

                <div class="col-md-12">
                    <div class="row">
                        <span style="color:red;font-weight: bold" id="errorMessageOnline"></span>
                        <input class="inpt" type="password" name="new_password" id="userNameRe" placeholder="New Password" autocomplete="off" minlength="6" style="margin-top: 40px;">
                        <span id="error_ResetU" class="errorSpan"></span>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row passwd">
                        <input class="inpt" type="password" name="confirm_password" id="userNameReConfirm" autocomplete="new-password" placeholder="Confirm Password"  equalTo="#userNameRe">
                        </span>
                        <span id="error_ResetC" class="errorSpan"></span>
                    </div>
                </div>
                <br>


                <button type="submit" class="btn btn-green  button-design frgt" style="background-color: #1C3FAA; color: white;">
                    <span id="reset_btn_Online">Submit</span>
                </button>
                <br>                

                <span id="successmsg" style="color:green; font-weight:bold;"></span>

                <p class="pclass" style="text-align: center;">You&nbsp;already&nbsp;have&nbsp;an&nbsp;account&nbsp;<a href="<?= base_url('login') ?>" style="color: blue;">Login</a></p>


            </form>
        </div>
    </div>
</body>
</html>

<link href="<?= base_url('assets/user_panel/login/popup.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" media="all" onload="if(media!=='all')media='all'">
<script src="<?= base_url('assets/user_panel/login/select2.min.js?v=' . rand()) ?>"></script>
<script src="<?= base_url('assets/user_panel/login/bootstrap.min.js?v=' . rand()) ?>"></script>
<script src="<?= base_url('assets/user_panel/login/common.js?v=' . rand()) ?>"></script>
<script src="<?= base_url('assets/user_panel/login/jquery-ui.js?v=' . rand()) ?>"></script>