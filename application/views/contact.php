<?php $this->load->view('common/header'); ?>


<head>
    <!-- all css here -->
    <!-- bootstrap v3.3.6 css -->
    <link rel="stylesheet" href="<?= base_url('assets/front/css/bootstrap.min.css') ?>">
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="<?= base_url('assets/front/css/owl.carousel.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/front/css/owl.transitions.css') ?>">
    <!-- Animate css -->
    <link rel="stylesheet" href="<?= base_url('assets/front/css/animate.css') ?>">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="<?= base_url('assets/front/css/meanmenu.min.css') ?>">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="<?= base_url('assets/front/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/front/css/themify-icons.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/front/css/flaticon.css') ?>">
    <!-- magnific css -->
    <link rel="stylesheet" href="<?= base_url('assets/front/css/magnific.min.css') ?>">
    <!-- style css -->
    <link rel="stylesheet" href="<?= base_url('assets/front/css/style.css') ?>">
    <!-- responsive css -->
    <link rel="stylesheet" href="<?= base_url('assets/front/css/responsive.css') ?>">
    <link href="<?= base_url('assets/front/css/custom.css') ?>" rel="stylesheet">

    <!-- modernizr css -->


    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


    <style>
        .thank-you-pop {
            width: 100%;
            padding: 20px;
            text-align: center;
        }

        .thank-you-pop h1 {
            font-size: 42px;
            margin-bottom: 25px;
            color: #5C5C5C;
        }

        .thank-you-pop p {
            font-size: 20px;
            margin-bottom: 27px;
            color: #5C5C5C;
        }

        body {
            line-height: 1.25;
        }

        label {
            color: black;
        }

        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }

        table caption {
            font-size: 1.5em;
            margin: .5em 0 .75em;
        }

        table tr {
            background-color: #f8f8f8;
            border: 1px solid #ddd;
            padding: .35em;
        }

        table th,
        table td {
            padding: .625em;
            text-align: center;
        }

        table th {
            font-size: .85em;
            letter-spacing: .1em;
            text-transform: uppercase;
        }

        @media screen and (max-width: 600px) {
            table {
                border: 0;
            }

            .topnav {
                display: inline-block !important;
                width: 100%;
                position: fixed;
            }

            table caption {
                font-size: 1.3em;
            }

            table thead {
                border: none;
                clip: rect(0 0 0 0);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 1px;
            }

            table tr {
                border-bottom: 3px solid #ddd;
                display: block;
                margin-bottom: .625em;
            }

            table td {
                border-bottom: 1px solid #ddd;
                display: block;
                font-size: .8em;
                text-align: right;
            }

            table td::before {
                content: attr(data-label);
                float: left;
                font-weight: bold;
                text-transform: uppercase;
            }

            table td:last-child {
                border-bottom: 0;
            }
        }

        @media screen and (max-width: 600px) {
            .about {
                display: none;
            }

            .counter-area {
                display: block !important;
            }



        }

        @media screen and (max-width: 600px) {
            .mp {
                width: 100%;
            }
        }

        @media screen and (min-width: 600px) {
            .mp {
                width: 100%;
            }
        }

        @media screen and (min-width: 768px) {
            .mp {
                width: 100%;
            }
        }

        @media screen and (min-width: 992px) {
            .mp {
                width: 100%;
            }
        }

        @media screen and (min-width: 1200px) {
            .mp {
                width: 100%;
            }
        }

        @media screen and (max-width: 600px) {
            .fm {
                width: 100%;
            }
        }

        @media screen and (min-width: 600px) {
            .fm {
                width: 100%;
            }
        }

        @media screen and (min-width: 768px) {
            .fm {
                width: 100%;
            }
        }

        @media screen and (min-width: 992px) {
            .fm {
                width: 100%;
            }
        }

        @media screen and (min-width: 1200px) {
            .fm {
                width: 100%;
            }
        }


        input[type=text],
        input[type=email],
        select,
        textarea {
            color: black;
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            margin-top: 6px;
            margin-bottom: 16px;
            resize: vertical;
        }

        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        .container1 {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }
    </style>



</head>

<body>

    <div id="loader"></div>
    <div id="content1">

        <!-- Start Count area -->
        <section id="top">
            <div class="about">
                <!-- <h2 style="margin-top: 100px; color: white; text-align: center;">About Us</h2> -->
                <img src="<?= base_url('assets/front/image/contact_us.jpg') ?>">

            </div>
            <div class="counter-area fix bg-color area-padding-2" style="display: none;">
                <h2 style="margin-top: 100px; color: white; text-align: center;">Contact Us</h2>
            </div>
            <div class="grid grid-cols-12 gap-7 mt-7">
                <div class="support-service-area area-padding-2">
                    <div class="container">
                        <!--<div class="row">-->
                        <!--    <div class="support-all">           -->
                        <!--        <div class="grid grid-cols-12 gap-7 mt-7">-->
                        <!--            <div class="col-span-12 sm:col-span-4 xl:col-span-4 intro-y">-->
                        <!--                <div class="col-md-4 col-sm-6 col-xs-12">-->
                        <!--                    <div class="support-services ">-->
                        <!--                        <span class="top-icon"><img src="<?= base_url('assets/front/image/Thailand.png') ?>" height="110" width="110"></i></span>-->
                        <!--                        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/Thailand.png') ?>"></i></a>-->
                        <!--                        <div class="support-content">-->
                        <!--                            <h4>Thiland</h4>-->
                        <!--                            <p><span>Head Office :</span> Asia</p>-->
                        <!--                            <p><span>Tel :</span> +00 0000000000</p>-->
                        <!--                            <p><span>Email :</span> support@pointwish.com</p>-->
                        <!--                        </div>-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--            <div class="col-span-12 sm:col-span-4 xl:col-span-4 intro-y">-->
                        <!--                <div class="col-md-8 col-sm-6 col-xs-12">-->
                        <!--                    <p><iframe class="mp" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7961366.751750858!2d96.99460933379245!3d13.011066597376619!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304d8df747424db1%3A0x9ed72c880757e802!2sThailand!5e0!3m2!1sen!2sin!4v1602305954661!5m2!1sen!2sin" width="755" height="320" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></p>-->
                        <!--                </div>-->
                        <!--            </div>-->

                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <div class="row">
                            <div class="support-all">
                                
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services ">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/China.png') ?>" height="110" width="110"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/China.png') ?>"></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>China</h4>-->
                                <!--            <p><span>Name :</span> Mr.Sanu Kaji Dangol</p>-->
                                <!--            <p><span>Tel :</span> +86 15381351205</p>-->
                                <!--            <p><span>Email :</span> sanukaji014@gmail.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                                
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services ">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/france.png') ?>" height="110" width="110"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/france.png') ?>" style=""></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>France</h4>-->
                                <!--            <p><span>Name :</span> Mrs.Dhan Kumari Limbu</p>-->
                                <!--            <p><span>Tel :</span> +33 758891590</p>-->
                                <!--            <p><span>Email :</span> kangsore1970@gmail.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                                
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services ">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/india.png') ?>" height="110" width="110"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/india.png') ?>"></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>India</h4>-->
                                <!--            <p><span>Name :</span> Mr.Rahul Jain</p>-->
                                <!--            <p><span>Tel :</span> +91 7011100628</p>-->
                                <!--            <p><span>Email :</span> rahul.anya@gmail.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->

                               
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/portugal.png') ?>" height="110" width="110"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/portugal.png') ?>"></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>Portugal</h4>-->
                                <!--            <p><span>Name :</span> Mr.Tehang Ijam Limbu</p>-->
                                <!--            <p><span>Tel :</span> +351 920502675</p>-->
                                <!--            <p><span>Email :</span> ijambblimbu@yahoo.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                               
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services ">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/Quatar.png') ?>" src="image/icon/verified security.svg" height="110" width="110"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/Quatar.png') ?>"></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>Qatar</h4>-->
                                <!--            <p><span>Name :</span> Mr.Tirtha Bahadur Thapa Magar</p>-->
                                <!--            <p><span>Tel :</span> +974 50256754</p>-->
                                <!--            <p><span>Email :</span> tirtham13@gmail.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                               
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services ">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/south Korea.png') ?>" height="110" width="110"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/south Korea.png') ?>"></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>South Korea</h4>-->
                                <!--            <p><span>Name :</span> Mr.Rajkumar Limbu</p>-->
                                <!--            <p><span>Tel :</span> +82 1063863427</p>-->
                                <!--            <p><span>Email :</span> romitakhati@gmail.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                                
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/japan.png') ?>" height="100" width="100"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/japan.png') ?>"></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>Japan</h4>-->
                                <!--            <p><span>Name :</span> Mr.Daulat Singh</p>-->
                                <!--            <p><span>Tel :</span> +81 9068375456</p>-->
                                <!--            <p><span>Email :</span> daulatqtr@gmail.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                                
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services ">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/malaysia.png') ?>" height="110" width="110"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/malaysia.png') ?>"></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>Malaysia</h4>-->
                                <!--            <p><span>Name :</span> Mr.Khapadha Man Thapa</p>-->
                                <!--            <p><span>Tel :</span> +60 197734974</p>-->
                                <!--            <p><span>Email :</span> nicedream638@gmail.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                               
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services ">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/usa.png') ?>" height="110" width="110"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/usa.png') ?>"></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>USA</h4>-->
                                <!--            <p><span>Name :</span> Mr.Bharat Bahdur Ale</p>-->
                                <!--            <p><span>Tel :</span> +1 (626)8664893</p>-->
                                <!--            <p><span>Email :</span> bharatale84657@gmail.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                                
                                </div>
                                
                                <!--<div class="col-md-4 col-sm-6 col-xs-12">-->
                                <!--    <div class="support-services ">-->
                                <!--        <span class="top-icon"><img src="<?= base_url('assets/front/image/iraq.png') ?>" height="110" width="110"></i></span>-->
                                <!--        <a style="border: none;" class="support-images"><img src="<?= base_url('assets/front/image/iraq.png') ?>"></i></a>-->
                                <!--        <div class="support-content">-->
                                <!--            <h4>Iraq</h4>-->
                                <!--            <p><span>Name :</span> Mr.Dibash Tamang</p>-->
                                <!--            <p><span>Tel :</span> +964 7723976199</p>-->
                                <!--            <p><span>Email :</span> dibaslama0@gmail.com</p>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->

                                <div class="col-md-8 col-sm-6 col-xs-12 fm">
                                    <div class="support-services " style="background-color:white; border:none;">
                                        <form action="<?= base_url('page/create_enquiry') ?>" method="POST">
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <label for="fname">Full Name</label>
                                                <input required type="text" id="fname" name="fullname" placeholder="Full name..">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <label for="mobile">Mobile Number</label>
                                                <input required type="text" id="mobileno" name="mobileno" placeholder="Mobile Number">
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <label for="email">Email Id</label>
                                                <input required type="email" id="Email" name="email" placeholder="Your Email Address..">
                                                <label for="country">Country</label>
                                                <select required id="country" name="country">
                                                    <option value="">Seelct Country</option>
                                                    <?php
                                                    $country_list = getCountryList();

                                                    if (!empty($country_list)) {
                                                        foreach ($country_list as $row) {
                                                    ?>
                                                            <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <label for="subject" style="color: black;">Subject</label>
                                                <textarea required id="subject" name="subject" placeholder="Write something.." rows="6"></textarea>
                                                <input type="submit" value="Submit" style="background-color: #1C3FAA; float:right;">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    </div>
    <!-- Start Footer Area -->
    <?php $this->load->view('common/footer'); ?>
    <!-- End Footer area -->

    <!-- all js here -->

    <!-- jquery latest version -->
    <script src="<?= base_url('assets/front/js/jquery-2.2.4.min.js') ?>"></script>
    <!-- bootstrap js -->
    <script src="<?= base_url('assets/front/js/bootstrap.min.js') ?>"></script>
    <!-- owl.carousel js -->
    <script src="<?= base_url('assets/front/js/owl.carousel.min.js') ?>"></script>
    <!-- magnific js -->
    <script src="<?= base_url('assets/front/js/magnific.min.js') ?>"></script>
    <!-- wow js -->
    <script src="<?= base_url('assets/front/js/wow.min.js') ?>"></script>
    <!-- meanmenu js -->
    <script src="<?= base_url('assets/front/js/jquery.meanmenu.js') ?>"></script>
    <!-- Form validator js -->
    <script src="<?= base_url('assets/front/js/form-validator.min.js') ?>"></script>
    <!-- plugins js -->
    <script src="<?= base_url('assets/front/js/plugins.js') ?>"></script>

    <!-- main js -->

    <script src="<?= base_url('assets/front/js/multislider.min.js') ?>"></script>
    <script src="<?= base_url('assets/front/js/main.js') ?>"></script><a id="scrollUp" href="#top" style="position: fixed; z-index: 2147483647;"><img src="<?= base_url('assets/front/image/icon/upload.png') ?>" height="20" width="20" style="margin-top: 10px;"></i></a>

    <?php
    if ($this->session->flashdata("error_message")) {
    ?>
        <script type="text/javascript">
            setTimeout(function() {

                $('#login_detail_modal').modal({
                    backdrop: 'static',
                    keyboard: false
                });

            }, 100);
        </script>
    <?php
    }
    ?>

    <div class="modal fade" id="login_detail_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                </div>
                <div class="modal-body">
                    <div class="thank-you-pop">
                        <h1>Thank You!</h1>
                        <p>Your submission is received and we will contact you soon</p>
                        <h3 class="cupon-pop">Your Id: <span>12345</span></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function myFunction() {
            var x = document.getElementById("myLinks");
            if (x.style.display === "block") {
                x.style.display = "none";
            } else {
                x.style.display = "block";
            }
        }
    </script>

    <script>
        $('#exampleSlider').multislider({
            interval: 4000,
            continuous: true,
            duration: 5000
        });
    </script>

</body>

</html>