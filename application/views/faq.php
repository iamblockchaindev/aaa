<?php $this->load->view('common/header'); ?>

    <!--header section start-->
			<section class="breadcrumb-section contact-bg section-padding">
			<div class="container">
			    <div class="row">
			        <div class="col-md-6 col-md-offset-3 text-center">
			            <h1>Faq</h1>
			             <p>For general query, please read following FAQ </p>
			        </div>
			    </div>
			</div>
			</section><!--Header section end-->

			<!--faq page content start-->
    <section class="section-padding padding-top-0 faq-page">
         <div class="container">
             <div class="row">
              <div class="col-md-12">
              <div class="clearfix"></div>
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#generalOne" aria-expanded="true" >
                      What is Point Wish ?
                    </a>
                  </h4>
                  </div>
                  <div id="generalOne" class="panel-collapse collapse in" role="tabpanel" >
                    <div class="panel-body">
                      Point Wish is UK's leading marketing with active users from 67 countries & so many investment opportunities.
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#generalTwo" aria-expanded="true" >
                      How does <strong>PT WISH</strong> currency work ?
                    </a>
                  </h4>
                  </div>
                  <div id="generalTwo" class="panel-collapse collapse " role="tabpanel" >
                    <div class="panel-body">
                      By now you know the basics of <strong>PT WISH</strong>. Cash in a local currency, get <strong>PT WISH</strong>, spend them like dollars without big transaction fees or your real name attached, cash them out whenever you want.
                    </div>
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#generalTwo3" aria-expanded="true" >
                      The <strong>PT WISH</strong> currency — a stable currency ?
                    </a>
                  </h4>
                  </div>
                  <div id="generalTwo3" class="panel-collapse collapse " role="tabpanel" >
                    <div class="panel-body">
                      A <strong>PT WISH</strong> is a unit of the <strong>PT WISH</strong> Digital-currency that’s represented by a three wavy horizontal line unicode character ≋ like the dollar is represented by $. 
                    </div>
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#generalTwo4" aria-expanded="true" >
                      The <strong>PT WISH</strong> Reserve — one for one ?
                    </a>
                  </h4>
                  </div>
                  <div id="generalTwo4" class="panel-collapse collapse " role="tabpanel" >
                    <div class="panel-body">
                      Each time someone cashes in a dollar or their respective local currency, that money goes into the <strong>PT WISH</strong>  Reserve and an equivalent value of <strong>PT WISH</strong> is minted and doled out to that person. 
                    </div>
                  </div>
                </div>                

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#generalTwo5" aria-expanded="true" >
                      The PT WISSH Block-chain — built for speed ?
                    </a>
                  </h4>
                  </div>
                  <div id="generalTwo5" class="panel-collapse collapse " role="tabpanel" >
                    <div class="panel-body">
                      Every <strong>PT WISH</strong> payment is permanently written into the <strong>PT WISH</strong> Blockchain — a cryptographically authenticated database that acts as a public online ledger designed to handle more then 1,000 transactions per second.  
                    </div>
                  </div>
                </div> 

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#generalTwo6" aria-expanded="true" >
                      <strong>PT WISH</strong> incentives — rewarding early businesses ?
                    </a>
                  </h4>
                  </div>
                  <div id="generalTwo6" class="panel-collapse collapse " role="tabpanel" >
                    <div class="panel-body">
                      The <strong>PT WISH</strong> Association wants to encourage more developers and merchants to work with its Digital. That’s why it plans to issue incentives, possibly <strong>PT WISH</strong> coins, to validator node operators who can get people signed up for and using <strong>PT WISH</strong>.
                    </div>
                  </div>
                </div> 

                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#generalTwo7" aria-expanded="true" >
                      Using <strong>PT WISH</strong> ?
                    </a>
                  </h4>
                  </div>
                  <div id="generalTwo7" class="panel-collapse collapse " role="tabpanel" >
                    <div class="panel-body">
                      So how do you actually own and spend <strong>PT WISH</strong>? Through <strong>PT WISH</strong> wallets for others that will be built by third-parties, potentially including <strong>PT WISH</strong> Association members. The idea is to make sending money to a friend or paying for something as easy.
                    </div>
                  </div>
                </div> 

              </div>
              </div>
             </div>
        </div>
</section>

<?php $this->load->view('common/footer'); ?>