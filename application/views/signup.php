<?php $this->load->view('common/header'); ?>

<style type="text/css">
  label.error {
    color: red;
    padding: 0 !important;
  }

  .success_message {
    color: green;
  }
</style>

<!--header section start-->
<section class="breadcrumb-section contact-bg section-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
        <h1>Register</h1>
        <p>Join us and complete your dream with us ! </p>
      </div>
    </div>
  </div>
</section>
<!--Header section end-->

<!--login section start-->
<div class="login-section section-padding login-bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="bottom_login_head">
          <h2>Register</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="main-login main-center">

          <?php echo validation_errors(); ?>

          <form id="signUpForm" name="signUpForm" class="form-horizontal" method="post">

            <div id="section1" class="section_class">
              <div class="form-group">
                <label for="name" class="cols-sm-2 control-label">Sponser ID</label>
                <div class="cols-sm-10">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fas fa-key"></i></span>
                    <input type="text" class="form-control required" name="sponsor_id" id="sponsor_id" placeholder="Sponser ID" value="<?php if (!empty($_GET['pws_id'])) echo $_GET['pws_id']; ?>" />
                  </div>

                  <span id="member_name_section" class="success_message"></span>

                </div>
              </div>
              <div class="form-group">
                <label for="name" class="cols-sm-2 control-label">Name</label>
                <div class="cols-sm-10">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control required" name="name" id="name" placeholder="Your Name" />
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="email" class="cols-sm-2 control-label">Email</label>
                <div class="cols-sm-10">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                    <input type="email" class="form-control required" name="email" id="email" placeholder="Your Email" />
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="password" class="cols-sm-2 control-label">Password</label>
                <div class="cols-sm-10">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                    <input type="password" class="form-control required" name="password" id="password" placeholder="Your Password" />
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="password" class="cols-sm-2 control-label">Confirm Password</label>
                <div class="cols-sm-10">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                    <input type="password" class="form-control required" name="confirm_password" id="confirm_password" placeholder="Your Password" equalto="#password" />
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="username" class="cols-sm-2 control-label">Country</label>
                <div class="cols-sm-10">

                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fas fa-globe-asia"></i>
                    </span>

                    <select name="country" id="country" class="form-control myinput required" onchange="change_country();">

                      <option value="">Select</option>
                      <?php
                      $country_list = getCountryList();

                      if (!empty($country_list)) {
                        foreach ($country_list as $row) {
                      ?>
                          <option value="<?php echo $row->id; ?>" data-countrycode="<?php echo $row->phonecode; ?>" <?php if (!empty($country_code) && ($country_code == $row->sortname)) echo "selected"; ?>><?php echo $row->name; ?></option>
                      <?php
                        }
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="username" class="cols-sm-2 control-label">State</label>
                <div class="cols-sm-10">

                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fas fa-globe-asia"></i>
                    </span>

                    <select name="state" id="state" class="form-control myinput required" onchange="change_state();">

                      <option value="">Select</option>

                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="username" class="cols-sm-2 control-label">City</label>
                <div class="cols-sm-10">

                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fas fa-globe-asia"></i>
                    </span>

                    <select name="city" id="city" class="form-control myinput required">

                      <option value="">Select</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="password" class="cols-sm-2 control-label">Mobile Number</label>
                <div class="cols-sm-10">
                  <div class="input-group">
                    <span class="input-group-addon" id="country_code_text"><?php if (!empty($phone_code)) echo '+' . $phone_code; ?></span>
                    <input type="hidden" id="country_code" name="country_code" value="<?php if (!empty($phone_code)) echo $phone_code; ?>">

                    <input type="text" class="form-control required" name="mobile_number" id="mobile_number" placeholder="Your Mobile Number" onkeypress="return isNumber(event);" />
                  </div>
                </div>
              </div>

              <div class="form-group ">
                <button type="submit" class="submit-btn btn btn-lg btn-block login-button submit_button_class" >Submit</button>
              </div>

            </div>

            <a href="<?php echo base_url(); ?>login"><span class="dont_have">
                You have already an account</span> Login</a>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!--login section end-->

<?php $this->load->view('common/footer'); ?>

<script src="<?php echo base_url(); ?>assets/user_panel/js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/user_panel/js/bootstrap-dialog.js"></script>

<script type="text/javascript">
  jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
  }, "Only alphabetical characters");

  var errorMsg = '',
    $valid = false;
  $.validator.addMethod("is_sponser_exist", function(val, elem) {
    var len = $("#sponsor_id").val();
    if (len.length >= 11) {
      $.ajax({
        async: false,
        url: '<?php echo base_url(); ?>login/is_sponser_exist',
        type: "get",
        dataType: "json",
        data: {
          sponsor_id: $("#sponsor_id").val()
        },
        success: function(response) {
          if (response.status == 1) {
            errorMsg = 'Sponsor Available !';
            $valid = true;

            $("#member_name_section").html('Sponsor Name: ' + response.member_name);

          } else if (response.status == 2) {
            errorMsg = 'Sponsor not exist !';
            $valid = false;

            $("#member_name_section").html('');
          } else if (response.status == 3) {
            errorMsg = 'This Sponsor is not upgraded !';
            $valid = false;

            $("#member_name_section").html('');
          }
        }
      });
    }

    $.validator.messages["is_sponser_exist"] = errorMsg;
    return $valid;
  }, '');


  $("#signUpForm").validate({
    rules: {
      sponsor_id: {
        required: true,
        is_sponser_exist: true,
      },
      name: {
        required: true,
        lettersonly: true,
      },
      // email: {  
      //     required: true,
      //     remote: "<?php echo base_url(); ?>login/is_email_exist"
      // },
      // mobile_number: {  
      //     required: true,
      //     remote: "<?php echo base_url(); ?>login/is_mobile_exist"
      // },        
      password: {
        minlength: 6,
      },
      mobile_otp: {
        required: true,
        is_otp_verify: true,
      },
    },
    errorPlacement: function(error, element) {
      //element.before(error);

      element.parents('div.input-group').after(error);
    }
  });

  function submit_data() {
    var form = $("#data_form");
    if (form.valid() == false) {
      return false;
    } else {
      $(".submit_button_class").html('<i class="fa fa-spinner" aria-hidden="true"></i>');
      $(".submit_button_class").attr('disabled', true);

      $("#data_form").submit();
    }
  }

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
</script>

<?php
if ($this->session->flashdata("error_message")) {
?>
  <script type="text/javascript">
    setTimeout(function() {

      $("#username_span").html('<?php echo $this->session->flashdata("username"); ?>');
      $("#password_span").html('<?php echo $this->session->flashdata("password"); ?>');

      $('#login_detail_modal').modal({
        backdrop: 'static',
        keyboard: false
      });

    }, 100);
  </script>
<?php
}
?>

<div class="modal fade successful-form-sn" id="login_detail_modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <img src="https://www.ugp100.com/assets/images/check-mark.png">
        <h2>Success!</h2>
        <p>
          Your registration is successful. Login details are emailed to your email ID. If you did not receive the details,it is possible that you entered worng email ID. Please enter new correct email ID and register again.
        </p>
        <h4 class="modal-title">Following is your login details:</h4>
        <ul class="list-inline list-unstyled">
          <li><strong>User name:</strong> <span id="username_span">PW10576</span></li>
          <li> <strong>Password:</strong> <span id="password_span">123456</span></li>
        </ul>
        <a href="<?php echo base_url(); ?>login"><button type="button" class="btn btn-default">OK</button></a>
      </div>
    </div>
  </div>
</div>