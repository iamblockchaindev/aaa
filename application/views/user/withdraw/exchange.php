<?php $this->load->view('user/common/header'); ?>

<div class="content-wrapper">
	<div class="container-full">
		<section class="content">
			<div class="box">
				<div class="box-body">
					<h4 class="box-title text-bdd1f8">1 XYZ = <?= number_format(get_pwt()['current_price'], 2) ?> USDT</h4>
					<div class="result-crypto"></div>
					<form id="convertForm" name="convertForm" class="" method="post">
						<div class="row">
							<div class="col-12">
								<div class="exchange-calculator">
									<div class="mt-35 row">
										<div class="col-xl-5 col-lg-5 col-md-5">
											<div class="mt-n3 mb-2">
												<span class="badge badge-primary active" id="fromCurrecy">XYZ</span>
											</div>
											<input type="text" class="form-control w-100p required" placeholder="0.00" id='value' name="name" type="number" min="10" oninput="calc_usdt_price()" required>
										</div>
										<div class="col-xl-2 col-lg-2 col-md-2">
											<i style="margin: auto;top: 14px; margin-top: 30px; height: 30px;" data-feather="repeat" aria-hidden="true" id="go" onclick="swapValues()"></i>
										</div>
										<div class="col-xl-5 col-lg-5 col-md-5">
											<div class="mt-n3 mb-2">
												<span class="badge badge-primary active" id="toCurrecy">USDT</span>
											</div>
											<input class="form-control required w-100p" id='value1' placeholder="0.00" name="name" type="number" readonly="">
										</div>
									</div>
								</div>
								<div class="text-center mt-15 mb-25">
									<button type="submit" class="btn btn-primary mx-auto">
										Exchange Now
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title text-bdd1f8">Exchange History</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
						<table id="exchange_history" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Position</th>
									<th>Office</th>
									<th>Age</th>
									<th>Start date</th>
									<th>Salary</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Tiger Nixon</td>
									<td>System Architect</td>
									<td>Edinburgh</td>
									<td>61</td>
									<td>2011/04/25</td>
									<td>$320,800</td>
								</tr>
								<tr>
									<td>Garrett Winters</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>63</td>
									<td>2011/07/25</td>
									<td>$170,750</td>
								</tr>
								<tr>
									<td>Ashton Cox</td>
									<td>Junior Technical Author</td>
									<td>San Francisco</td>
									<td>66</td>
									<td>2009/01/12</td>
									<td>$86,000</td>
								</tr>
								<tr>
									<td>Cedric Kelly</td>
									<td>Senior Javascript Developer</td>
									<td>Edinburgh</td>
									<td>22</td>
									<td>2012/03/29</td>
									<td>$433,060</td>
								</tr>
								<tr>
									<td>Airi Satou</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>33</td>
									<td>2008/11/28</td>
									<td>$162,700</td>
								</tr>
								<tr>
									<td>Ashton Cox</td>
									<td>Junior Technical Author</td>
									<td>San Francisco</td>
									<td>66</td>
									<td>2009/01/12</td>
									<td>$86,000</td>
								</tr>
								<tr>
									<td>Cedric Kelly</td>
									<td>Senior Javascript Developer</td>
									<td>Edinburgh</td>
									<td>22</td>
									<td>2012/03/29</td>
									<td>$433,060</td>
								</tr>
								<tr>
									<td>Airi Satou</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>33</td>
									<td>2008/11/28</td>
									<td>$162,700</td>
								</tr>
								<tr>
									<td>Ashton Cox</td>
									<td>Junior Technical Author</td>
									<td>San Francisco</td>
									<td>66</td>
									<td>2009/01/12</td>
									<td>$86,000</td>
								</tr>
								<tr>
									<td>Cedric Kelly</td>
									<td>Senior Javascript Developer</td>
									<td>Edinburgh</td>
									<td>22</td>
									<td>2012/03/29</td>
									<td>$433,060</td>
								</tr>
								<tr>
									<td>Airi Satou</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>33</td>
									<td>2008/11/28</td>
									<td>$162,700</td>
								</tr>
								<tr>
									<td>Ashton Cox</td>
									<td>Junior Technical Author</td>
									<td>San Francisco</td>
									<td>66</td>
									<td>2009/01/12</td>
									<td>$86,000</td>
								</tr>
								<tr>
									<td>Cedric Kelly</td>
									<td>Senior Javascript Developer</td>
									<td>Edinburgh</td>
									<td>22</td>
									<td>2012/03/29</td>
									<td>$433,060</td>
								</tr>
								<tr>
									<td>Airi Satou</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>33</td>
									<td>2008/11/28</td>
									<td>$162,700</td>
								</tr>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</section>
	</div>
</div>

<?php $this->load->view('user/common/footer'); ?>


<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.1/toastr.min.css" rel="stylesheet" media="all">
<script type="text/javascript">
	function swapValues() {
		var val = $('#value').val();
		var val1 = $('#value1').val();
		var lbl = $('#fromCurrecy').text();
		var lbl1 = $('#toCurrecy').text();
		var wallet = $('#fromWallet').text();
		var wallet1 = $('#toWallet').text();
		$('#value').val(val1);
		$('#value1').val(val);
		$('#fromCurrecy').text(lbl1);
		$('#toCurrecy').text(lbl);
		$('#fromWallet').text(wallet1);
		$('#toWallet').text(wallet);
		calc_usdt_price();
	}

	function calc_usdt_price() {
		let pwtPrice = "<?= get_pwt()['current_price'] ?>";
		let agency = "<?= $session_data->is_agency ?>";
		let amount = $('#value').val();
		let convertPrice = 0;
		var calc = 0,
			getAmount = 0;
		if ($('#fromCurrecy').text() == "PWT") {
			calc = +amount * +pwtPrice;
		} else {
			calc = +amount / +pwtPrice;
		}
		convertPrice = calc;
		getAmount = agency == 1 ? calc : calc - calc * 0.001; //Others 0.1% charge;
		let p = +convertPrice;
		$('#value1').val(p.toFixed(2));
		$('#getAmount').text('You will get ' + getAmount.toLocaleString());
	}


	$("#convertForm").submit(function(e) {
		e.preventDefault();
		form = $("#convertForm");
		if (form.valid() == false) {
			return false;
		} else {
			BootstrapDialog.show({
				title: "Exchange",
				message: "<p>Do you really want to Exchange amount ?</p>",
				buttons: [{
						label: 'Yes',
						cssClass: 'bg-theme-1 button mr-1 text-white yes_class',

						action: function(dialogItself) {
							$("#submit_button_id").prop('disabled', 'disabled');
							$(".yes_class").prop('disabled', 'disabled');
							$(".no_class").prop('disabled', 'disabled');
							var postData = new FormData($("#data_form")[0]);
							var url = '<?php echo base_url(); ?>user/withdraw/convert_price';
							$.ajax({
								type: 'POST',
								url: url,
								data: {
									amount: $('#value').val(),
									convert: $('#value1').val(),
									fromCurrecy: $('#fromCurrecy').text(),
									toCurrecy: $('#toCurrecy').text(),
								},
								dataType: 'json',
								beforeSend: function() {
									$(".yes_class").html("Wait...");
								},
								success: function(response) {
									if (response.success == true) {
										toastr.success(response.message);
										window.location.reload(true);
									} else {
										toastr.error(response.message);
									}
								}
							});
						}
					},
					{
						label: 'No',
						cssClass: 'bg-theme-6 button text-white no_class',
						action: function(dialogItself) {
							dialogItself.close();
						}
					}
				]
			});
		}
	});

	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}

	$('#amount').bind("paste", function(e) {
		e.preventDefault();
	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.1/toastr.min.js"></script>

<script>
	$(document).ready(function() {
		$("#exchange_history").DataTable();
	})
</script>