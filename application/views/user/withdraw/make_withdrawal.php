<?php $this->load->view('user/common/header'); ?>

<div class="content-wrapper">
	<div class="container-full">
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h4 class="box-title text-bdd1f8"><i class="ti-wallet me-15"></i> Deposite Fund</h4>
				</div>
				<!-- /.box-header -->
				<form class="form">
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label">Withdraw in</label>
									<select class="form-control">
										<option>PWT Address</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="form-label">Amount</label>
									<input type="number" set="any" class="form-control" placeholder="Amount">
								</div>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer text-right">
						<button type="reset" class="btn btn-warning me-1">Reset</button>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title text-bdd1f8">Deposite Fund History (26)</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
						<table id="withdraw_fund_tbl" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Position</th>
									<th>Office</th>
									<th>Age</th>
									<th>Start date</th>
									<th>Salary</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Tiger Nixon</td>
									<td>System Architect</td>
									<td>Edinburgh</td>
									<td>61</td>
									<td>2011/04/25</td>
									<td>$320,800</td>
								</tr>
								<tr>
									<td>Garrett Winters</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>63</td>
									<td>2011/07/25</td>
									<td>$170,750</td>
								</tr>
								<tr>
									<td>Ashton Cox</td>
									<td>Junior Technical Author</td>
									<td>San Francisco</td>
									<td>66</td>
									<td>2009/01/12</td>
									<td>$86,000</td>
								</tr>
								<tr>
									<td>Cedric Kelly</td>
									<td>Senior Javascript Developer</td>
									<td>Edinburgh</td>
									<td>22</td>
									<td>2012/03/29</td>
									<td>$433,060</td>
								</tr>
								<tr>
									<td>Airi Satou</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>33</td>
									<td>2008/11/28</td>
									<td>$162,700</td>
								</tr>
								<tr>
									<td>Ashton Cox</td>
									<td>Junior Technical Author</td>
									<td>San Francisco</td>
									<td>66</td>
									<td>2009/01/12</td>
									<td>$86,000</td>
								</tr>
								<tr>
									<td>Cedric Kelly</td>
									<td>Senior Javascript Developer</td>
									<td>Edinburgh</td>
									<td>22</td>
									<td>2012/03/29</td>
									<td>$433,060</td>
								</tr>
								<tr>
									<td>Airi Satou</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>33</td>
									<td>2008/11/28</td>
									<td>$162,700</td>
								</tr>
								<tr>
									<td>Ashton Cox</td>
									<td>Junior Technical Author</td>
									<td>San Francisco</td>
									<td>66</td>
									<td>2009/01/12</td>
									<td>$86,000</td>
								</tr>
								<tr>
									<td>Cedric Kelly</td>
									<td>Senior Javascript Developer</td>
									<td>Edinburgh</td>
									<td>22</td>
									<td>2012/03/29</td>
									<td>$433,060</td>
								</tr>
								<tr>
									<td>Airi Satou</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>33</td>
									<td>2008/11/28</td>
									<td>$162,700</td>
								</tr>
								<tr>
									<td>Ashton Cox</td>
									<td>Junior Technical Author</td>
									<td>San Francisco</td>
									<td>66</td>
									<td>2009/01/12</td>
									<td>$86,000</td>
								</tr>
								<tr>
									<td>Cedric Kelly</td>
									<td>Senior Javascript Developer</td>
									<td>Edinburgh</td>
									<td>22</td>
									<td>2012/03/29</td>
									<td>$433,060</td>
								</tr>
								<tr>
									<td>Airi Satou</td>
									<td>Accountant</td>
									<td>Tokyo</td>
									<td>33</td>
									<td>2008/11/28</td>
									<td>$162,700</td>
								</tr>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</section>
	</div>
</div>


<?php $this->load->view('user/common/footer'); ?>



<script type="text/javascript">
	$("#data_form").validate();

	$("#data_form").submit(function(e) {
		e.preventDefault();
		form = $("#data_form");
		if (form.valid() == false) {
			return false;
		} else {
			BootstrapDialog.show({
				title: "Withdrawal",
				message: "<p>Do you really want to withdraw amount ?</p>",
				buttons: [{
						label: 'Yes',
						cssClass: 'bg-theme-1 button mr-1 text-white yes_class',

						action: function(dialogItself) {
							$("#submit_button_id").prop('disabled', 'disabled');
							$(".yes_class").prop('disabled', 'disabled');
							$(".no_class").prop('disabled', 'disabled');
							var postData = new FormData($("#data_form")[0]);
							var url = '<?php echo base_url(); ?>user/withdraw/make_withdrawal';
							$.ajax({
								type: 'POST',
								url: url,
								processData: false,
								contentType: false,
								data: postData,
								dataType: 'json',
								beforeSend: function() {
									$(".yes_class").html("Wait...");
								},
								success: function(data) {
									dialogItself.close();
									$("#submit_button_id").prop('disabled', false);
									$(".yes_class").prop('disabled', false);
									$(".no_class").prop('disabled', false);
									if (data.status == false) {
										BootstrapDialog.show({
											title: "Message",
											message: data.message,
										});
									} else {
										location.reload(true);
									}
								}
							});
						}
					},
					{
						label: 'No',
						cssClass: 'bg-theme-6 button text-white no_class',
						action: function(dialogItself) {
							dialogItself.close();
						}
					}
				]
			});
		}
	});

	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}

	$('#amount').bind("paste", function(e) {
		e.preventDefault();
	});
</script>

<script>
	$(document).ready(function() {
		$("#withdraw_fund_tbl")
	});
</script>