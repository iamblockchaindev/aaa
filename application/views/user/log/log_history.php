<?php $this->load->view('user/common/header'); ?>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2 class="head-pending-set"><?php if(!empty($title)) echo $title; ?></h2>
            </div>

            <!-- Widgets -->
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <?php if(!empty($title)) echo $title; ?>
                            </h2>

                        </div>
                        <div class="body table-responsive">

<?php
if($this->session->flashdata('error_message'))
{
?>
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata('error_message'); ?>
    </div>
<?php
}
?>

            <table id="myTable" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No.</th>  
                        <th>Message</th>                            
                        <th>Browser Info</th>                                        
                        <th>IP</th>                                   
                        <th>Date</th>                        
                    </tr>
                </thead>
                <tbody>
                <?php
                if(!empty($data_result))
                {
                    if(!empty($_GET['offset']))
                    {
                      $i = ($_GET['offset'] * 10) - 9;
                    }
                    else
                    {
                      $i = 1;
                    }
                    
                    foreach ($data_result as $row) 
                    {
                
                ?>
                    <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>

                        <td>
                        <?php 
                        if($row->status == 1)
                        {
                        ?>
                            <p style="color: green;"><?php echo $row->message; ?></p>
                        <?php
                        }
                        else
                        {
                        ?>
                            <p style="color: red;"><?php echo $row->message; ?></p>
                        <?php
                        }
                        ?>
                            
                        </td>
                        <td>
                        <?php
                        if(!empty($row->browser_image))
                        {
                        ?>
                        <img src="<?php echo base_url(); ?>assets/user_panel/images/browser_icons/<?php echo $row->browser_image; ?>" width="32px">
                        <?php
                        }
                        ?>
                        <?php echo $row->browser_name; ?>
                            
                        </td>
                        <td><?php echo $row->ip_address; ?></td>
                        <td><?php echo date('d-m-Y H:i A', strtotime($row->created_datetime)); ?></td>

                    </tr>
                <?php
                $i++;
                    }
                }
                else
                {
                ?>
                    <tr><td colspan="8" align="center">No Log History Found !</td></tr>
                <?php
                }
                ?>                    
                </tbody>
            </table>

<?php
if(!empty($links))
{
?>
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php echo $links; ?>
  </ul>
</nav>
<?php
}
?>


                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
        </div>
    </section>

</body>

</html>

<?php $this->load->view('user/common/footer'); ?>