<?php $this->load->view('user/common/header'); ?>
<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-3">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="media align-items-center p-0">
                            <div class="text-center">
                                <a href="#"><i class="cc BTC me-5" title="BTC"></i></a>
                            </div>
                            <div>
                                <h2 class="no-margin">Bitcoin BTC</h2>
                            </div>
                        </div>
                        <div class="flexbox align-items-center mt-25">
                            <div>
                                <p class="no-margin">2.340000434 <span class="text-gray">BTC</span> <span class="text-info">$0.04</span></p>
                            </div>
                            <div class="text-end">
                                <p class="no-margin"><span class="text-success">+5.35%</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('user/common/footer'); ?>