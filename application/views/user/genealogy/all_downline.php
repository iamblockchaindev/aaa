<?php $this->load->view('user/common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/dist/css/style.css?v=' . rand()) ?>" />
<div ng-controller="User_Controller" ng-init="get_level_user(<?= $_GET['level'] ?>)" ng-cloak>
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">
            <a href="<?php echo base_url(); ?>user/genealogy/level" class="button w-20 bg-theme-1 text-white ml-auto">Back</a>
        </h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-12">
            <div class="intro-y box mt-5">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200">
                    <h2 class="font-medium text-base mr-auto">
                        Leavel <?= $this->input->get('level') ?>({{levelusers.length}})
                    </h2>
                </div>
                <div class="p-5" ng-show="levelLoader">
                    <i data-loading-icon="three-dots" class="h-5 w-full"></i>
                </div>
                <div class="p-5" id="striped-rows-table" ng-show="!levelLoader">
                    <div class="preview">
                        <div>
                            <table datatable="ng">
                                <thead>
                                    <tr>
                                        <th scope="col">Sr.No</th>
                                        <th scope="col">Member Id</th>
                                        <th scope="col">Contact</th>
                                        <th scope="col">Rank</th>
                                        <th scope="col">Joining Date</th>
                                        <th scope="col">Pool Upgrade Date</th>
                                        <th scope="col">Downline Users</th>
                                        <th scope="col">Reffer By</th>
                                        <th scope="col">Satus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="bg-gray-200 dark:bg-dark-1" ng-repeat="row in levelusers">
                                        <td data-label="Sr.No">{{$index+1}}.</td>
                                        <td data-label="Member Id">{{row.username}}<br />({{row.full_name}})</td>
                                        <td data-label="Contact">{{row.mobile ? row.mobile : '---'}}</td>
                                        <td data-label="Rank">
                                            <center>

                                                <img ng-if="row.package == 30" src="<?php echo base_url('assets/user_panel/images/shield/OPAL.png?v='.rand()); ?>" alt="OPAL Image" height="70" width="70">

                                                <img ng-if="row.package == 40" src="<?php echo base_url('assets/user_panel/images/shield/JADE.png?v='.rand()); ?>" alt="JADE Image" height="70" width="70">

                                                <img ng-if="row.package == 100" src="<?php echo base_url('assets/user_panel/images/shield/RED_BERYL.png?v='.rand()); ?>" alt="RED BERYL Image" height="70" width="70">

                                                <img ng-if="row.package == 1000" src="<?php echo base_url('assets/user_panel/images/shield/BLUE_NILE.png?v='.rand()); ?>" alt="BLUE NILE Image" height="70" width="70">

                                                <img ng-if="row.package == 5000" src="<?php echo base_url('assets/user_panel/images/shield/ETERNITY.png?v='.rand()); ?>" alt="ETERNITY Image" height="70" width="70">

                                                <img ng-if="row.package == 10000" src="<?php echo base_url('assets/user_panel/images/shield/KOH_I_NOOR.png?v='.rand()); ?>" alt="KOH-I-NOOR Image" height="70" width="70">

                                                <img ng-if="row.package == ''" src="<?php echo base_url('assets/user_panel/images/shield/qq.png?v='.rand()); ?>" alt="Opening Image">


                                            </center>
                                        </td>
                                        <td data-label="Joining Date">{{row.created_time}}</td>
                                        <td data-label="Pool Update Date">{{row.upgrade_time}}</td>
                                        <td data-label="Downline Users">{{row.downline}}</td>
                                        <td data-label="Reffer By">
                                            <?=
                                            $_GET['level'] == 1 ? 'You' : '{{row.sponsor}}' . '<br>' . '({{row.name}})'
                                            ?>
                                        </td>
                                        <td class="table-border" data-label="Status"><b class="text-success">Active</b></td>
                                    </tr>
                                    <?php
                                    //  $i++;
                                    // }

                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Striped Rows -->
        </div>
    </div>
</div>
<?php $this->load->view('user/common/footer'); ?>