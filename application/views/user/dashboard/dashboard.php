<?php $this->load->view('user/common/header.php'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-4">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="dashboard-card">
                            <div>
                                <h2 class="amount-dash-card">$ 00.00</h2>
                                <h6 class="name-dash-card">Total Trade Return</h6>
                            </div>
                            <div>
                                <img height="60px" src="<?= base_url('assets/user_panel/images/icons/trade_return.svg'); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="dashboard-card">
                            <div>
                                <h2 class="amount-dash-card">$ 00.00</h2>
                                <h6 class="name-dash-card">Total Referral Income</h6>
                            </div>
                            <div>
                                <img height="60px" src="<?= base_url('assets/user_panel/images/icons/referral_income.svg'); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="dashboard-card">
                            <div>
                                <h2 class="amount-dash-card">$ 00.00</h2>
                                <h6 class="name-dash-card">Total Matching Income</h6>
                            </div>
                            <div>
                                <img height="60px" src="<?= base_url('assets/user_panel/images/icons/matching_income.svg'); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="dashboard-card">
                            <div>
                                <h2 class="amount-dash-card">$ 00.00</h2>
                                <h6 class="name-dash-card">Last Trade Package</h6>
                            </div>
                            <div>
                                <img height="60px" src="<?= base_url('assets/user_panel/images/icons/last_package.svg'); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="dashboard-card">
                            <div>
                                <h2 class="amount-dash-card">$ 00.00</h2>
                                <h6 class="name-dash-card">Total Trade Package</h6>
                            </div>
                            <div>
                                <img height="60px" src="<?= base_url('assets/user_panel/images/icons/total_package.svg'); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="dashboard-card">
                            <div>
                                <h2 class="amount-dash-card">$ 00.00</h2>
                                <h6 class="name-dash-card">Total Fund Withdraw</h6>
                            </div>
                            <div>
                                <img height="60px" src="<?= base_url('assets/user_panel/images/icons/fund_withdraw.svg'); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>

<?php $this->load->view('user/common/footer.php'); ?>