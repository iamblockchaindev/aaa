<?php $this->load->view('user/common/header'); ?>


<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title text-bdd1f8"><i class="fa fa-address-book-o me-15"></i> XYZ Address</h4>
                </div>
                <!-- /.box-header -->
                <form class="form">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="form-label">XYZ Address</label>
                                <input type="text" class="form-control" placeholder="Enter XYZ Address">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <button type="reset" class="btn btn-warning me-1">Reset</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>


<?php $this->load->view('user/common/footer'); ?>
<script>
    $("#form_eth").validate({
        rules: {
            eth_address: {
                required: true,
            },
        },
    });
    $("#form_btc").validate({
        rules: {
            btc_address: {
                required: true,
            },
        },
    });
</script>