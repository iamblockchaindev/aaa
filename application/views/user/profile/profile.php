<?php $this->load->view('user/common/header'); ?>

<div class="content-wrapper" ng-init="get_auth_status()">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title text-bdd1f8"><i class="ti-user me-15"></i> Personal Information</h4>
                </div>
                <!-- /.box-header -->
                <form id="data_form" name="data_form" class="form" action="<?= base_url('user/account/edit_profile') ?>" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Member ID</label>
                                    <input type="text" class="form-control" id="username" name="username" value="<?= $this->session->user_name ?>" readonly="" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php
                                    $package = getLatestRankByUserID($this->session->user_id);
                                    if ($package == '30') {
                                        $pkg = "OPAL";
                                    } else if ($package == 40) {
                                        $pkg = "JADE";
                                    } else if ($package == 100) {
                                        $pkg = "RED BERYL";
                                    } else if ($package == 1000) {
                                        $pkg = "BLUE NILE";
                                    } else if ($package == 5000) {
                                        $pkg = "ETERNITY";
                                    } else if ($package == 10000) {
                                        $pkg = "KOH-I-NOOR";
                                    } else {
                                        $pkg = "Upgrade Your Account";
                                    }
                                    ?>
                                    <label class="form-label">Package</label>
                                    <input type="text" class="form-control" id="package" name="package" value="<?= $pkg ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Full Name</label>
                                    <input type="text" class="form-control" placeholder="Fullname" id="full_name" name="full_name" value="<?php if (!empty($user_data->full_name)) echo $user_data->full_name; ?>" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">E-mail</label>
                                    <input type="email" class="form-control" placeholder="E-mail" id="email" name="email" value="<?php if (!empty($user_data->email)) echo $user_data->email; ?>" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Country</label>
                                    <select class="form-control required" id="country" name="country" onchange="change_country();" required>
                                        <option value="">Select Country</option>
                                        <?php
                                        $country_list =  getCountryList();

                                        if (!empty($country_list)) {
                                            foreach ($country_list as $row) {
                                        ?>
                                                <option value="<?php echo $row->id; ?>" data-phonecode="<?php echo $row->phonecode; ?>" <?php if (!empty($user_data->country) && ($row->id == $user_data->country)) echo "selected"; ?>><?php echo $row->name; ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Mobile Number</label>
                                    <div class="input-group mb-3">
                                        <select id="country_code" name="country_code" class="input-group-text" required>
                                            <option value="">+</option>
                                            <?php
                                            $country_list = getCountryList();

                                            if (!empty($country_list)) {
                                                foreach ($country_list as $row) {
                                            ?>
                                                    <option value="<?php echo $row->phonecode; ?>" <?php if (!empty($user_data->country_code) && ($user_data->country_code == $row->phonecode)) echo "selected"; ?>> +<?php echo $row->phonecode; ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input required type="text" class="form-control" placeholder="Mobile Number" id="mobile" name="mobile" value="<?php if (!empty($user_data->mobile)) echo $user_data->mobile; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <button type="reset" class="btn btn-warning me-1">Reset</button>
                        <button type="submit" class="btn btn-primary" ng-disabled="!authsuccess">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('user/common/footer'); ?>

<script type="text/javascript">
    $("#data_form").validate({
        rules: {
            mobile: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
            },
        },
        messages: {
            mobile: {
                required: 'Please enter mobile number !',
            }
        }
    });
</script>

<script type="text/javascript">
    function set_country_code() {
        var phonecode = $("#country").find(':selected').data('phonecode');

        // alert(phonecode);

        $("#country_code").val(phonecode);

        // $('.selectpicker').selectpicker('refresh');
    }


    function change_country() {
        set_country_code();



        // $('.selectpicker').selectpicker('refresh');

        var country_code = $("#country").find(':selected').data('countrycode');

        var country_id = $("#country").val();

    }
</script>