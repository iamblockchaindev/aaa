<?php $this->load->view('user/common/header'); ?>

<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title text-bdd1f8"><i class="ti-lock me-15"></i> Set Transaction Password</h4>
                </div>
                <!-- /.box-header -->
                <form class="form" id="data_form" name="data_form" action="" method="POST">
                    <div class="box-body">
                        <div class="row">
                            <?php
                            if ($user_data) {
                            ?>
                                <div class="form-group">
                                    <label class="form-label">Old Transaction Password</label>
                                    <input type="text" name="old_password" class="form-control" placeholder="Old Transaction Password">
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label class="form-label">New Transaction Password</label>
                                <input type="password" class="form-control required" placeholder="New Transaction Password" id="new_password" name="new_password" type="password">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Confirm Transaction Password</label>
                                <input type="password" class="form-control" placeholder="Confirm Transaction Password" id="confirm_password" name="confirm_password">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <?php
                        if ($user_data) {
                        ?>
                            <button class="btn btn-warning me-1" type="submit" id="reset_button_id" onclick="reset_request();">Reset</button>
                        <?php } ?>
                        <button type="submit" class="btn btn-primary" id="submit_button_id" onclick="submit_data();">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>

<?php $this->load->view('user/common/footer'); ?>

<script type="text/javascript">
    $("#data_form").validate({
        rules: {
            new_password: {
                required: true,
                minlength: 6,
                maxlength: 10
            },
            confirm_password: {
                required: true,
                minlength: 6,
                maxlength: 10
            },
        },

    });

    function submit_data() {
        var form = $("#data_form");

        if (form.valid() == false) {
            return false;
        } else {
            // $("#submit_button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $("#submit_button_id").attr('disable', true);

            $("#data_form").submit();
        }
    }

    function reset_request() {
        BootstrapDialog.show({
            title: "Reset Password",
            // message: "<p>Website is under-maintenance for new upgrades and cryptocurrency integration from 15/12/2020 withdrawal will be started.Thank You</p>",
            message: "<p>Do you really want to reset password ?</p>",
            buttons: [{
                    label: 'Yes',
                    cssClass: 'bg-theme-1 button mr-1 text-white yes_class',
                    action: function(dialogItself) {

                        $(".yes_class").attr('disable', true);
                        $("#reset_button_id").attr('disable', true);

                        // $("#data_form").submit();
                        var postData = new FormData($("#data_form")[0]);

                        var url = '<?php echo base_url(); ?>user/account/reset_transaction_password';

                        $.ajax({
                            type: 'POST',
                            url: url,
                            processData: false,
                            contentType: false,
                            data: postData,
                            dataType: 'json',
                            success: function(response) {
                                $("#reset_button_id").attr('disabled', false);
                                if (response.status == 1) {
                                    window.location.reload(true);
                                }

                            }
                        });
                    }
                },
                {
                    label: 'No',
                    cssClass: 'bg-theme-6 button text-white',
                    action: function(dialogItself) {
                        dialogItself.close();
                    }
                }
            ]
        });

    }


    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    $('#amount').bind("paste", function(e) {
        e.preventDefault();
    });
</script>