<?php $this->load->view('user/common/header'); ?>
<div class="content-wrapper" ng-init="get_auth_status()">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title text-bdd1f8"><i class="ti-lock me-15"></i> Change Password</h4>
                </div>
                <!-- /.box-header -->
                <form class="form" id="data_form" name="data_form" action="" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="form-label">Old Password</label>
                                <input type="password" class="form-control required" placeholder="Old Password" id="old_password" name="old_password" value="" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">New Password</label>
                                <input type="password" class="form-control required" placeholder="New Password" minlength="6" id="new_password" name="new_password" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Confirm Password</label>
                                <input type="password" class="form-control" placeholder="Confirm Password" minlength="6" id="confirm_password" name="confirm_password" equalTo="#new_password" required>
                                <!-- <span id="view_pass" onclick="viewPassFunc()">
                                    <div class="eye">
                                        <i class="fa fa-eye"></i>
                                    </div>
                                </span> -->
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <button type="reset" class="btn btn-warning me-1">Reset</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('user/common/footer'); ?>

<script type="text/javascript">
    $("#data_form").validate({
        new_password: {
            minlength: 6
        },
        confirm_password: {
            minlength: 6
        }
    });
</script>