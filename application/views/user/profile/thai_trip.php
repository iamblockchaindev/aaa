<?php $this->load->view('user/common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/thai.css') ?>">
<div class="row">
    <div class="col-12">
        <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
            <!-- BEGIN: Daily Sales -->
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">
                        Bluenile Trip
                    </h2>
                </div>
                <div class="p-5">
                    <?php if (empty($user_document) || $user_document->verfied_ornot == 2) { ?>
                        <form id="data_form" name="data_form" action="<?= base_url('user/account/edit_tripdata') ?>" method="POST" enctype="multipart/form-data">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="mt-2">
                                        <label><b>First Name</b></label>
                                        <input type="text" class="input w-full border mt-2 required" placeholder="Enter Your First Name" id="username" name="username" required>
                                    </div>
                                    <div class="mt-2">
                                        <label><b>Passport Number</b></label>
                                        <input type="text" class="input w-full border mt-2 required" placeholder="Enter Your Passport Number" id="passport_num" name="passport_num" required>
                                    </div>
                                    <div class="mt-2">
                                        <label><b>Passport Photo <small class="text-danger">(*Mobile photos are not allowed)</small></b></label>
                                        <a style="float: right; font-weight: 500; font-size: 13px;" href="<?= base_url('assets/user_panel/images/sample.png') ?>" download>Download sample image</a>
                                        <input type="file" class="input w-full border mt-2 required" id="pas_photo" name="pas_photo" required>
                                    </div>
                                </div>

                                <div class="col-span-12 xl:col-span-6">
                                    <div class="mt-2">
                                        <label><b>Last Name</b></label>
                                        <input type="text" class="input w-full border mt-2 required" placeholder="Enter Your Last Name" id="last_name" name="last_name" required>
                                    </div>
                                    <div class="mt-2">
                                        <label><b>Passport Front<small class="text-danger"> (*only scan copy allowed)</small></b></label>
                                        <input type="file" class="input w-full border mt-2 required" id="pas_front" name="pas_front" required>
                                    </div>
                                    <div class="mt-2">
                                        <label><b>Passport Back<small class="text-danger"> (*only scan copy allowed)</small></b></label>
                                        <input type="file" class="input w-full border mt-2 required" id="pas_back" name="pas_back" required>
                                    </div>
                                    <div class="mt-4">
                                        <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto btn-save float-right">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    <?php } elseif ($user_document->verfied_ornot == 1) { ?>
                        <h2 class="font-medium text-base mr-auto">
                            Your document is approved
                        </h2>
                    <?php } else { ?>
                        <h2 class="font-medium text-base mr-auto">
                            Your document verfication is pending
                        </h2>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('user/common/footer'); ?>