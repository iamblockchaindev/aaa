<?php
$session_data = getSessionData();
?>
<?php $this->load->view('user/common/header'); ?>
<style>
    #amount_royalty .error {
        margin-bottom: 0.25rem !important;
    }
</style>
<div class="grid grid-cols-9 gap-6">
    <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
        <div class="col-span-12 mt-8">
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    <b>Wallets</b>
                </h2>
            </div>
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5" style="margin-top: 2%">
                    TEAM BONUS
                </h2>

            </div>
            <div class="grid grid-cols-12 gap-6 mt-5">
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php echo round(getMyWorkingLevelIncome(), 2); ?></div>
                            <div class="text-base text-gray-600 mt-1">Wallet</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWorkingLevelWithdrawal(); ?></div>
                            <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php
                                                                            $balance = getMyWorkingLevelIncomeBalance();
                                                                            echo $balance;
                                                                            ?>
                                <?php
                                if (!empty((float)$balance)) {
                                ?>
                                    <button type="button" class="btn btn-primary" onclick="open_amount_other_form('level');"> WITHDRAW</button>
                                <?php
                                }
                                ?>
                            </div>

                            <div class="text-base text-gray-600 mt-1">Balance</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
<div class="grid grid-cols-9 gap-6">
    <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
        <!-- BEGIN: General Report -->
        <div class="col-span-12 mt-8">

            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    CRYSTAL BONUS
                </h2>

            </div>
            <div class="grid grid-cols-12 gap-6 mt-5">
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyDirectWalletIncome(); ?></div>
                            <div class="text-base text-gray-600 mt-1">Wallet</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyDirectWithdrawal(); ?></div>
                            <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php $balance = getMyDirectWalletbalance();
                                                                            echo $balance; ?><?php if (!empty((float)$balance)) { ?>
                                <button type="button" class="btn btn-primary" onclick="open_amount_other_form('direct');"> WITHDRAW</button>
                            <?php } ?>
                            </div>
                            <div class="text-base text-gray-600 mt-1">Balance</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
<div class="grid grid-cols-9 gap-6">
    <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
        <!-- BEGIN: General Report -->
        <div class="col-span-12 mt-8">

            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    TEAM INCOME
                </h2>

            </div>
            <div class="grid grid-cols-12 gap-6 mt-5">
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?= round($session_data->royalty_income_wallet, 2)  ?></div>
                            <div class="text-base text-gray-600 mt-1">Wallet</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?= round($session_data->royalty_income_withdrawal, 2)  ?></div>
                            <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php
                                                                            $balance = round($session_data->royalty_income_balance, 2);
                                                                            echo $balance;
                                                                            ?><?php
                                                                                if (!empty((float)$balance)) {
                                                                                ?>
                                <button type="button" class="btn btn-primary" onclick="open_amount_other_form('royalty');"> WITHDRAW</button>
                            <?php
                                                                                }
                            ?>
                            </div>
                            <div class="text-base text-gray-600 mt-1">Balance</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
<div class="grid grid-cols-9 gap-6">
    <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
        <!-- BEGIN: General Report -->
        <div class="col-span-12 mt-8">

            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    CRYPTO BONUS
                </h2>

            </div>
            <div class="grid grid-cols-12 gap-6 mt-5">
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php $wallet = number_format(getCryptoBonus()['crypto_bonus'], 2);
                                                                            echo $wallet; ?></div>
                            <div class="text-base text-gray-600 mt-1">Wallet</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php $withdraw = number_format(getCryptoBonus()['income_bonus'], 2);
                                                                            echo $withdraw;  ?></div>
                            <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6"><?php
                                                                            echo $wallet - $withdraw;
                                                                            ?><?php
                                                                                if ($wallet != $withdraw) {
                                                                                ?>
                                <button type="button" class="btn btn-primary" onclick="open_amount_other_form('bonus');"> WITHDRAW</button>
                            <?php
                                                                                }
                            ?>
                            </div>
                            <div class="text-base text-gray-600 mt-1">Balance</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
<?php
$current_package = getMyCurrentMembershipPackage();

$package_arr = array('30', '40', '100', '1000', '5000', '10000');

if ($current_package >= 30) {
?>
    <div class="grid grid-cols-9 gap-6">
        <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
            <!-- BEGIN: General Report -->
            <div class="col-span-12 mt-8">

                <div class="intro-y flex items-center h-10">
                    <h2 class="text-lg font-medium truncate mr-5">
                        OPAL

                    </h2>

                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawableAmountByPackage('30'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Wallet</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawlByPackage('30'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php
                                                                                $balance = getMyBalanceByPackage('30');
                                                                                echo $balance;
                                                                                ?><?php
                                                                                    if (!empty((float)$balance)) {
                                                                                    ?>
                                    <button type="button" class="btn btn-primary" onclick="open_amount_form('30');"> WITHDRAW</button>
                                <?php
                                                                                    }
                                ?>
                                </div>
                                <div class="text-base text-gray-600 mt-1">Balance</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
<?php } ?>


<?php
if ($current_package >= 40) {
?>
    <div class="grid grid-cols-9 gap-6">
        <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
            <!-- BEGIN: General Report -->
            <div class="col-span-12 mt-8">

                <div class="intro-y flex items-center h-10">
                    <h2 class="text-lg font-medium truncate mr-5">
                        JADE

                    </h2>

                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawableAmountByPackage('40'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Wallet</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawlByPackage('40'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php
                                                                                $balance = getMyBalanceByPackage('40');
                                                                                echo $balance;
                                                                                ?><?php
                                                                                    if (!empty((float)$balance)) {
                                                                                    ?>
                                    <button type="button" class="btn btn-primary" onclick="open_amount_form('40');"> WITHDRAW</button>
                                <?php
                                                                                    }
                                ?>
                                </div>
                                <div class="text-base text-gray-600 mt-1">Balance</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
<?php
}
?>

<?php
if ($current_package >= 100) {
?>
    <div class="grid grid-cols-9 gap-6">
        <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
            <!-- BEGIN: General Report -->
            <div class="col-span-12 mt-8">

                <div class="intro-y flex items-center h-10">
                    <h2 class="text-lg font-medium truncate mr-5">
                        RED BERYL
                    </h2>

                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawableAmountByPackage('100'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Wallet</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawlByPackage('100'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php
                                                                                $balance = getMyBalanceByPackage('100');
                                                                                echo $balance;
                                                                                ?><?php
                                                                                    if (!empty((float)$balance)) {
                                                                                    ?>
                                    <button type="button" class="btn btn-primary" onclick="open_amount_form('100');"> WITHDRAW</button>
                                <?php
                                                                                    }
                                ?>
                                </div>
                                <div class="text-base text-gray-600 mt-1">Balance</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
}
?>

<?php
if ($current_package >= 1000) {
?>
    <div class="grid grid-cols-9 gap-6">
        <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
            <!-- BEGIN: General Report -->
            <div class="col-span-12 mt-8">

                <div class="intro-y flex items-center h-10">
                    <h2 class="text-lg font-medium truncate mr-5">
                        BLUE NILE
                    </h2>

                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawableAmountByPackage('1000'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Wallet</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawlByPackage('1000'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php
                                                                                $balance = getMyBalanceByPackage('1000');
                                                                                echo $balance;
                                                                                ?><?php
                                                                                    if (!empty((float)$balance)) {
                                                                                    ?>
                                    <button type="button" class="btn btn-primary" onclick="open_amount_form('1000');"> WITHDRAW</button>
                                <?php
                                                                                    }
                                ?>
                                </div>
                                <div class="text-base text-gray-600 mt-1">Balance</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
}
?>

<?php
if ($current_package >= 5000) {
?>
    <div class="grid grid-cols-9 gap-6">
        <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
            <!-- BEGIN: General Report -->
            <div class="col-span-12 mt-8">

                <div class="intro-y flex items-center h-10">
                    <h2 class="text-lg font-medium truncate mr-5">
                        ETERNITY
                    </h2>

                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawableAmountByPackage('5000'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Wallet</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawlByPackage('5000'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php
                                                                                $balance = getMyBalanceByPackage('5000');
                                                                                echo $balance;
                                                                                ?><?php
                                                                                    if (!empty((float)$balance)) {
                                                                                    ?>
                                    <button type="button" class="btn btn-primary" onclick="open_amount_form('5000');"> WITHDRAW</button>
                                <?php
                                                                                    }
                                ?>
                                </div>
                                <div class="text-base text-gray-600 mt-1">Balance</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
}
?>

<?php
if ($current_package >= 10000) {
?>
    <div class="grid grid-cols-9 gap-6">
        <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
            <!-- BEGIN: General Report -->
            <div class="col-span-12 mt-8">

                <div class="intro-y flex items-center h-10">
                    <h2 class="text-lg font-medium truncate mr-5">
                        KOH-I-NOOR
                    </h2>

                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-10"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawableAmountByPackage('10000'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Wallet</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-11"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php echo getMyWithdrawlByPackage('10000'); ?></div>
                                <div class="text-base text-gray-600 mt-1">Withdrwal</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="dollar-sign" class="report-box__icon text-theme-12"></i>

                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6"><?php
                                                                                $balance = getMyBalanceByPackage('10000');
                                                                                echo $balance;
                                                                                ?><?php
                                                                                    if (!empty((float)$balance)) {
                                                                                    ?>
                                    <button type="button" class="btn btn-primary" onclick="open_amount_form('5000');"> WITHDRAW</button>
                                <?php
                                                                                    }
                                ?>
                                </div>
                                <div class="text-base text-gray-600 mt-1">Balance</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
}
?>
<?php $this->load->view('user/common/footer'); ?>

<script type="text/javascript">
    $("#data_form").validate();
</script>

<script type="text/javascript">
    function isNumberKey(evt) {
        // var charCode = (evt.which) ? evt.which : event.keyCode
        // var inputValue = $(evt.target).val();

        // if (charCode == 46) {
        //     if (inputValue.indexOf('.') < 1) {
        //         return true;
        //     }
        //     return false;
        // }
        // if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        //     return false;
        // }

        // return true;
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function open_amount_form(package) {
        $("#package").val(package);

        /******Reset Data******/
        $("#amount").val('');
        $("#withdraw_button_id").css('opacity', 0.5);
        $("#withdraw_button_id").attr('disabled', true);
        /******Reset Data******/

        $('#transfer_amount_popup').modal({
            backdrop: 'static',
            keyboard: false
        })
    }

    function open_amount_other_form(type) {
        $("#type").val(type);

        /******Reset Data******/
        $("#amount_other").val('');
        $("#withdraw_other_button_id").css('opacity', 0.5);
        $("#withdraw_other_button_id").attr('disabled', true);
        /******Reset Data******/

        if (type == 'royalty') {
            $('#transfer_royalty_amount_popup').modal({
                backdrop: 'static',
                keyboard: false
            })
        } else {
            $('#transfer_other_amount_popup').modal({
                backdrop: 'static',
                keyboard: false
            })
        }

    }

    function transfer_amount() {
        var form = $("#data_form");

        if (form.valid() == false) {
            return false;
        } else {
            $(".error_p").remove();
            $('#transfer_amount_popup').modal('hide');

            // $("#withdraw_button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $("#withdraw_button_id").attr('disabled', true);

            var package = $("#package").val();
            var amount = $("#amount").val();
            var url = '<?php echo base_url(); ?>user/membership_wallet/transfer_amount';
            var dataString = 'package=' + package + '&amount=' + amount;

            BootstrapDialog.show({
                title: "Confirm",
                message: "<p>Do you really want to transfer amount ?</p>",
                buttons: [{
                        label: 'Yes',
                        cssClass: 'bg-theme-1 button mr-1 text-white yes_class',
                        action: function(dialogItself) {
                            $.ajax({
                                type: "POST",
                                data: dataString,
                                url: url,
                                dataType: "json",
                                success: function(response) {
                                    $("#withdraw_button_id").html('WITHDRAW');
                                    $("#withdraw_button_id").attr('disabled', false);

                                    if (response.status == 1) {
                                        window.location.reload(true);
                                    } else {
                                        $('#transfer_amount_popup').modal('show');
                                        dialogItself.close();
                                        $("#amount").after('<div><span class="text-danger">' + response.message + '</span></div>');
                                        setTimeout(function() {
                                            $(".text-danger").remove();
                                        }, 3000);
                                    }
                                }
                            });
                        }
                    },
                    {
                        label: 'No',
                        cssClass: 'bg-theme-6 button text-white',
                        action: function(dialogItself) {
                            dialogItself.close();
                        }
                    }
                ]
            });
        }
    }

    function transfer_amount_other() {
        var type = $("#type").val();
        if (type == 'royalty') {
            var form = $("#data_royalty_form");
        } else {
            var form = $("#data_form_other");
        }

        if (form.valid() == false) {
            return false;
        } else {
            $(".error_p").remove();
            if (type == 'royalty') {
                $('#transfer_royalty_amount_popup').modal('hide');
                var amount = $("#amount_royalty").val();
            } else {
                $('#transfer_other_amount_popup').modal('hide');
                var amount = $("#amount_other").val();
            }
            // $("#withdraw_other_button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            // $("#withdraw_other_button_id").attr('disabled', true);



            var url = '<?php echo base_url(); ?>user/membership_wallet/transfer_amount_other';
            var dataString = 'type=' + type + '&amount=' + amount;

            BootstrapDialog.show({
                title: "Confirm",
                message: "<p>Do you really want to transfer amount ?</p>",
                buttons: [{
                        label: 'Yes',
                        cssClass: 'bg-theme-1 button mr-1 text-white yes_class',
                        action: function(dialogItself) {
                            $.ajax({
                                type: "POST",
                                data: dataString,
                                url: url,
                                dataType: "json",
                                success: function(response) {
                                    if (type == 'royalty') {
                                        $("#withdraw_royalty_button_id").html('WITHDRAW');
                                        $("#withdraw_royalty_button_id").attr('disabled', false);
                                    } else {
                                        $("#withdraw_other_button_id").html('WITHDRAW');
                                        $("#withdraw_other_button_id").attr('disabled', false);
                                    }

                                    if (response.status == 1) {
                                        window.location.reload(true);
                                    } else {
                                        $('#transfer_other_amount_popup').modal('show');
                                        dialogItself.close();
                                        $("#amount_other").after('<div><span class="text-danger">' + response.message + '</span></div>');
                                        setTimeout(function() {
                                            $(".text-danger").remove();
                                        }, 3000);
                                    }
                                }
                            });
                        }
                    },
                    {
                        label: 'No',
                        cssClass: 'bg-theme-6 button text-white',
                        action: function(dialogItself) {
                            dialogItself.close();
                        }
                    }
                ]
            });
        }
    }

    $(document).ready(function() {
        $("#amount").on('keyup', function() {

            var amount = $(this).val();

            if (amount != '') {
                if (parseFloat(amount) == 0) {
                    $("#withdraw_button_id").css('opacity', 0.5);
                    $("#withdraw_button_id").attr('disabled', true);
                } else {
                    $("#withdraw_button_id").css('opacity', 1);
                    $("#withdraw_button_id").attr('disabled', false);
                }
            } else {
                $("#withdraw_button_id").css('opacity', 0.5);
                $("#withdraw_button_id").attr('disabled', true);
            }

            if (amount == 0) {
                $("#withdraw_button_id").css('opacity', 0.5);
                $("#withdraw_button_id").attr('disabled', true);
            } else {
                $("#withdraw_button_id").css('opacity', 1);
                $("#withdraw_button_id").attr('disabled', false);
            }

        });

        $("#amount_other").on('keyup', function() {

            var amount = $(this).val();

            if (amount != '') {
                if (parseFloat(amount) == 0) {
                    $("#withdraw_other_button_id").css('opacity', 0.5);
                    $("#withdraw_other_button_id").attr('disabled', true);
                } else {
                    $("#withdraw_other_button_id").css('opacity', 1);
                    $("#withdraw_other_button_id").attr('disabled', false);
                }
            } else {
                $("#withdraw_other_button_id").css('opacity', 0.5);
                $("#withdraw_other_button_id").attr('disabled', true);
            }

            if (amount == 0) {
                $("#withdraw_other_button_id").css('opacity', 0.5);
                $("#withdraw_other_button_id").attr('disabled', true);
            } else {
                $("#withdraw_other_button_id").css('opacity', 1);
                $("#withdraw_other_button_id").attr('disabled', false);
            }

        });

        $("#amount_royalty").on('keyup', function() {

            var amount = $(this).val();

            if (amount != '') {
                if (parseFloat(amount) == 0) {
                    $("#withdraw_royalty_button_id").css('opacity', 0.5);
                    $("#withdraw_royalty_button_id").attr('disabled', true);
                } else {
                    $("#withdraw_royalty_button_id").css('opacity', 1);
                    $("#withdraw_royalty_button_id").attr('disabled', false);
                }
            } else {
                $("#withdraw_royalty_button_id").css('opacity', 0.5);
                $("#withdraw_royalty_button_id").attr('disabled', true);
            }

            if (amount == 0) {
                $("#withdraw_royalty_button_id").css('opacity', 0.5);
                $("#withdraw_royalty_button_id").attr('disabled', true);
            } else {
                $("#withdraw_royalty_button_id").css('opacity', 1);
                $("#withdraw_royalty_button_id").attr('disabled', false);
            }

        });

        /*****paste `disable`*****/
        $('#amount, #amount_other, #amount_royalty').bind("paste", function(e) {
            e.preventDefault();
        });
        /*****paste `disable`*****/

    });
</script>


<!-- Modal -->
<div class="modal fade" id="transfer_amount_popup" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="float:right" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Withdraw Amount </h4>
            </div>
            <div class="modal-body">

                <form id="data_form" name="data_form" action="" method="POST">

                    <input type="hidden" id="package" name="package">

                    <div>
                        <label class="m-1"><b>Amount</b></label>
                        <input type="text" class="input border bg-gray-100 mt-2 required" placeholder="Input Amount" id="amount" name="amount" autocomplete="off" onkeypress="return isNumberKey(event);" required>
                        <button type="button" style="margin-top: 25px;opacity: 0.5;" class="button m-5 bg-theme-1 text-white ml-auto" disabled="" onclick="transfer_amount();" id="withdraw_button_id">Withdraw</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="transfer_other_amount_popup" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="float:right" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Withdraw Amount </h4>
            </div>
            <div class="modal-body">

                <form id="data_form_other" name="data_form_other" action="" method="POST">

                    <input type="hidden" id="type" name="type">


                    <div>
                        <label class="m-1"><b>Amount</b></label>
                        <input type="text" class="input border bg-gray-100 mt-2 required" placeholder="Input Amount" id="amount_other" name="amount_other" autocomplete="off" onkeypress="return isNumberKey(event);" required>
                        <button type="button" style="margin-top: 25px;opacity: 0.5;" class="button m-5 bg-theme-1 text-white ml-auto" disabled="" onclick="transfer_amount_other();" id="withdraw_other_button_id">Withdraw</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="transfer_royalty_amount_popup" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="float:right" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Withdraw Amount </h4>
            </div>
            <div class="modal-body">

                <form id="data_royalty_form" name="data_royalty_form" action="" method="POST">

                    <input type="hidden" id="type" name="type">
                    <?php
                    $limit = getWithdrawalLimit($this->session->user_id);
                    $income = round($session_data->royalty_income_balance, 2);
                    $total_withdrawal = round($session_data->royalty_income_withdrawal, 2);
                    ?>


                    <div>
                        <span><small style="color: red;" ng-show="<?= $limit ?>"> (Your available withdrwal limit is <?= !empty($limit) ? $limit - $total_withdrawal : $income ?>$)</small></span>
                        <br />
                        <label class="m-1"><b>Amount</b></label>
                        <!-- <input title="" min="40" type="number" max="<?= !empty($limit) ? $limit - $total_withdrawal : ''  ?>" class="input border bg-gray-100 mt-2 w-1/4 required" placeholder="Amount" id="amount_royalty" name="amount_other" autocomplete="off" onkeypress="return isNumberKey(event);" required> -->
                        <input title="" required type="number" max="<?= !empty($limit) ? $limit - $total_withdrawal : $income  ?>" class="input border bg-gray-100 mt-2 w-1/4 required" id="amount_royalty" name="amount_royalty" autocomplete="off" onkeypress="return isNumberKey(event);" placeholder="Amount">
                        <button type="button" style="opacity: 0.5;" class="button mb-6 bg-theme-1 text-white ml-auto" disabled="" onclick="transfer_amount_other();" id="withdraw_royalty_button_id">Withdraw</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
</div>

<script>
    $("#data_royalty_form").validate({
        // amount: {
        //     min: 'Please enter 40 digits',
        //     max: 'Please upgrade to the next pool to increase the withdrawal limit'
        // }

        rules: {
            amount_royalty: {
                required: true,
                max: "<?= !empty($limit) ? $limit - $total_withdrawal : $income  ?>"
            }
        },
        messages: {
            amount_royalty: {
                max: "<?= !empty($limit) ? 'Please upgrade to the next pool to increase the withdrawal limit' : 'Amound should be less than your balance'  ?>"
            }
        }
    });
</script>