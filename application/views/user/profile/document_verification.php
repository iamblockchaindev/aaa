<?php $this->load->view('user/common/header'); ?>

<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title text-bdd1f8"><i class="ti-file-o me-15"></i> XYZ</h4>
                </div>
                <div class="box-body">
                    <form action="#" class="dropzone">
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>


<?php $this->load->view('user/common/footer'); ?>
<?php
if ($this->session->flashdata("success_message")) {
?>
    <script type="text/javascript">
        BootstrapDialog.show({
            title: "Message",
            message: "<?php echo $this->session->flashdata("success_message"); ?>",
        });
    </script>
<?php
}
?>

<script type="text/javascript">
    $("#form2").validate();
    $("#form-address").validate();
</script>