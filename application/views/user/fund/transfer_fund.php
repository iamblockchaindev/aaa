<?php
$session_data = getSessionData();
?>
<?php $this->load->view('user/common/header'); ?>

<div class="content-wrapper">
    <div class="container-full">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title text-bdd1f8"><i class="ti-wallet me-15"></i> Transfer Fund</h4>
                </div>
                <!-- /.box-header -->
                <form class="form">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Member Id</label>
                                    <input type="text" set="any" class="form-control" placeholder="Member Id">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Amount ( $2,627.97 )</label>
                                    <input type="text" set="any" class="form-control" placeholder="Amount">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Transaction Password</label>
                                    <input type="password" set="any" class="form-control" placeholder="Transaction Password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <button type="reset" class="btn btn-warning me-1">Reset</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title text-bdd1f8">Transfer History (26)</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="transfer_fund_tbl" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tiger Nixon</td>
                                    <td>System Architect</td>
                                    <td>Edinburgh</td>
                                    <td>61</td>
                                    <td>2011/04/25</td>
                                    <td>$320,800</td>
                                </tr>
                                <tr>
                                    <td>Garrett Winters</td>
                                    <td>Accountant</td>
                                    <td>Tokyo</td>
                                    <td>63</td>
                                    <td>2011/07/25</td>
                                    <td>$170,750</td>
                                </tr>
                                <tr>
                                    <td>Ashton Cox</td>
                                    <td>Junior Technical Author</td>
                                    <td>San Francisco</td>
                                    <td>66</td>
                                    <td>2009/01/12</td>
                                    <td>$86,000</td>
                                </tr>
                                <tr>
                                    <td>Cedric Kelly</td>
                                    <td>Senior Javascript Developer</td>
                                    <td>Edinburgh</td>
                                    <td>22</td>
                                    <td>2012/03/29</td>
                                    <td>$433,060</td>
                                </tr>
                                <tr>
                                    <td>Airi Satou</td>
                                    <td>Accountant</td>
                                    <td>Tokyo</td>
                                    <td>33</td>
                                    <td>2008/11/28</td>
                                    <td>$162,700</td>
                                </tr>
                                <tr>
                                    <td>Ashton Cox</td>
                                    <td>Junior Technical Author</td>
                                    <td>San Francisco</td>
                                    <td>66</td>
                                    <td>2009/01/12</td>
                                    <td>$86,000</td>
                                </tr>
                                <tr>
                                    <td>Cedric Kelly</td>
                                    <td>Senior Javascript Developer</td>
                                    <td>Edinburgh</td>
                                    <td>22</td>
                                    <td>2012/03/29</td>
                                    <td>$433,060</td>
                                </tr>
                                <tr>
                                    <td>Airi Satou</td>
                                    <td>Accountant</td>
                                    <td>Tokyo</td>
                                    <td>33</td>
                                    <td>2008/11/28</td>
                                    <td>$162,700</td>
                                </tr>
                                <tr>
                                    <td>Ashton Cox</td>
                                    <td>Junior Technical Author</td>
                                    <td>San Francisco</td>
                                    <td>66</td>
                                    <td>2009/01/12</td>
                                    <td>$86,000</td>
                                </tr>
                                <tr>
                                    <td>Cedric Kelly</td>
                                    <td>Senior Javascript Developer</td>
                                    <td>Edinburgh</td>
                                    <td>22</td>
                                    <td>2012/03/29</td>
                                    <td>$433,060</td>
                                </tr>
                                <tr>
                                    <td>Airi Satou</td>
                                    <td>Accountant</td>
                                    <td>Tokyo</td>
                                    <td>33</td>
                                    <td>2008/11/28</td>
                                    <td>$162,700</td>
                                </tr>
                                <tr>
                                    <td>Ashton Cox</td>
                                    <td>Junior Technical Author</td>
                                    <td>San Francisco</td>
                                    <td>66</td>
                                    <td>2009/01/12</td>
                                    <td>$86,000</td>
                                </tr>
                                <tr>
                                    <td>Cedric Kelly</td>
                                    <td>Senior Javascript Developer</td>
                                    <td>Edinburgh</td>
                                    <td>22</td>
                                    <td>2012/03/29</td>
                                    <td>$433,060</td>
                                </tr>
                                <tr>
                                    <td>Airi Satou</td>
                                    <td>Accountant</td>
                                    <td>Tokyo</td>
                                    <td>33</td>
                                    <td>2008/11/28</td>
                                    <td>$162,700</td>
                                </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
</div>

<?php $this->load->view('user/common/footer'); ?>

<script type="text/javascript">
    var errorMsg1 = '',
        $valid1 = false;
    $.validator.addMethod("isCorrectPassword", function(val, elem) {
        $.ajax({
            async: false,
            url: '<?php echo base_url(); ?>user/fund/isCorrectPassword',
            type: "POST",
            dataType: "json",
            data: {
                transaction_password: $("#transaction_password").val()
            },
            success: function(response) {
                if (response.status == 1) {
                    errorMsg1 = '';
                    $valid1 = true;

                    $("#password_section").html('');
                } else if (response.status == 2) {
                    errorMsg1 = 'Password is not correct !';
                    $valid1 = false;

                    $("#password_section").html('');
                }
            }
        });

        $.validator.messages["isCorrectPassword"] = errorMsg1;
        return $valid1;
    }, '');


    var errorMsg2 = '',
        $valid2 = false;
    $.validator.addMethod("isMemberAvailabe", function(val, elem) {
        var len = $("#member_id").val();
        if (len.length >= 7) {
            $.ajax({
                async: false,
                url: '<?php echo base_url(); ?>user/fund/isMemberAvailabe',
                type: "POST",
                dataType: "json",
                data: {
                    member_id: $("#member_id").val()
                },
                success: function(response) {
                    if (response.status == 1) {
                        errorMsg2 = 'Member Available !';
                        $valid2 = true;

                        $("#member_name_section").html('Member Name: ' + response.member_name);
                    } else if (response.status == 2) {
                        errorMsg2 = 'Member Not Exist !';
                        $valid2 = false;

                        $("#member_name_section").html('');
                    } else if (response.status == 3) {
                        errorMsg2 = "You can't transfer funds to yourself !";
                        $valid2 = false;

                        $("#member_name_section").html('');
                    }
                }

            });
        }

        $.validator.messages["isMemberAvailabe"] = errorMsg2;
        return $valid2;
    }, '');


    $("#data_form").validate({
        onkeyup: function(element, event) {
            if (event.which === 9 && this.elementValue(element) === "") {
                return;
            } else {
                this.element(element);
            }
        },

        rules: {
            member_id: {
                required: true,
                isMemberAvailabe: true,
            },
            transaction_password: {
                required: true,
                // isCorrectPassword: true,
            },
        },
    });

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function submit_data() {
        var form = $("#data_form");

        if (form.valid() == false) {
            return false;
        } else {
            accept_submit();
        }
    }


    function accept_submit() {
        BootstrapDialog.show({
            title: "",
            message: "<p>Do you really want to transfer amount ?</p>",
            buttons: [{
                    label: 'Yes',
                    cssClass: 'bg-theme-1 button mr-1 text-white yes_class',
                    action: function(dialogItself) {

                        $(".yes_class").attr('disable', true);
                        $("#submit_button_id").attr('disable', true);

                        $("#data_form").submit();
                    }
                },
                {
                    label: 'No',
                    cssClass: 'bg-theme-6 button text-white',
                    action: function(dialogItself) {
                        dialogItself.close();
                    }
                }
            ]
        });

    }
</script>

<script>
    $(document).ready(function() {
        $("#transfer_fund_tbl").DataTable();
    });
</script>