<?php $this->load->view('user/common/header'); ?>

<div class="content-wrapper">
    <div class="container-full">
        <section class="content">
            <div class="row">
                <div class="col-lg-3">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="media align-items-center p-0">
                            <div class="text-center">
                                <a href="#"><i class="cc BTC me-5" title="BTC"></i></a>
                            </div>
                            <div>
                                <h2 class="no-margin">Bitcoin BTC</h2>
                            </div>
                        </div>
                        <div class="flexbox align-items-center mt-25">
                            <div>
                                <p class="no-margin">2.340000434 <span class="text-gray">BTC</span> <span class="text-info">$0.04</span></p>
                            </div>
                            <div class="text-end">
                                <p class="no-margin"><span class="text-success">+5.35%</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="media align-items-center p-0">
                            <div class="text-center">
                                <a href="#"><i class="cc BTC me-5" title="BTC"></i></a>
                            </div>
                            <div>
                                <h2 class="no-margin">Bitcoin BTC</h2>
                            </div>
                        </div>
                        <div class="flexbox align-items-center mt-25">
                            <div>
                                <p class="no-margin">2.340000434 <span class="text-gray">BTC</span> <span class="text-info">$0.04</span></p>
                            </div>
                            <div class="text-end">
                                <p class="no-margin"><span class="text-success">+5.35%</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="media align-items-center p-0">
                            <div class="text-center">
                                <a href="#"><i class="cc BTC me-5" title="BTC"></i></a>
                            </div>
                            <div>
                                <h2 class="no-margin">Bitcoin BTC</h2>
                            </div>
                        </div>
                        <div class="flexbox align-items-center mt-25">
                            <div>
                                <p class="no-margin">2.340000434 <span class="text-gray">BTC</span> <span class="text-info">$0.04</span></p>
                            </div>
                            <div class="text-end">
                                <p class="no-margin"><span class="text-success">+5.35%</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="box box-body pull-up bg-hexagons-white">
                        <div class="media align-items-center p-0">
                            <div class="text-center">
                                <a href="#"><i class="cc BTC me-5" title="BTC"></i></a>
                            </div>
                            <div>
                                <h2 class="no-margin">Bitcoin BTC</h2>
                            </div>
                        </div>
                        <div class="flexbox align-items-center mt-25">
                            <div>
                                <p class="no-margin">2.340000434 <span class="text-gray">BTC</span> <span class="text-info">$0.04</span></p>
                            </div>
                            <div class="text-end">
                                <p class="no-margin"><span class="text-success">+5.35%</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title text-bdd1f8"><i class="fa fa-ticket me-15"></i> Create Ticket</h4>
                </div>
                <!-- /.box-header -->
                <form class="form">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Member Id</label>
                                    <input type="text" class="form-control" value="PW28080961" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Full Name</label>
                                    <input type="text" class="form-control" placeholder="Full Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Department</label>
                                    <select class="form-control">
                                        <option>Deposite</option>
                                        <option>Withdrawal</option>
                                        <option>Transfer</option>
                                        <option>Account</option>
                                        <option>Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Attachment <small>*(only jpg & png files are allowed)</small></label>
                                    <input type="file" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Subject</label>
                                    <textarea rows="1" class="form-control" placeholder="Subject"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Description</label>
                                    <textarea rows="1" class="form-control" placeholder="Description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <button type="reset" class="btn btn-warning me-1">Reset</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>

<?php $this->load->view('user/common/footer'); ?>

<script type="text/javascript">
    $("#data_form").validate();
</script>

<script type="text/javascript">
    $(function() {
        $("#attachment").bind("change", function() {

            var file = this.files[0];
            var fileType = file["type"];
            var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
            if ($.inArray(fileType, ValidImageTypes) < 0) {

                alert('Please select correct image !');
                $("#attachment").val('');
            }

            // if (typeof($("#attachment")[0].files) != "undefined") {
            //     var size = parseFloat($("#attachment")[0].files[0].size / 1024).toFixed(2);

            //     var size_mb = (parseFloat(size) / 1024).toFixed(2);

            //     if (size_mb > 2) {
            //         $("#attachment").val('');

            //         alert('Please upload photo having size less than 2 MB !');
            //     }

            // } else {
            //     alert("This browser does not support HTML5.");
            // }
        });
    });

    function show_image(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                var src = e.target.result;
                $('#show_image_div_area img').attr('src', src);

                $("#show_image_div_area").show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>