<?php $this->load->view('user/common/header'); ?>
<style>
    button:focus {
        outline: none;
    }
</style>
<!-- BEGIN: Chat Content -->
<div class="intro-y col-span-12 lg:col-span-8 xxl:col-span-9">
    <div class="chat__box box">
        <!-- BEGIN: Chat Active -->
        <div class="h-full flex flex-col">
            <div class="flex flex-col sm:flex-row border-b border-gray-200 dark:border-dark-5 px-5 py-4">
                <div class="flex items-center">
                    <div class="w-10 h-10 sm:w-12 sm:h-12 flex-none image-fit relative">
                        <img class="rounded-full" src="<?= base_url('assets/user_panel/images/user.png') ?>">
                    </div>
                    <div class="ml-3 mr-auto">
                        <div class="font-medium text-base">TICKET ID : <?php echo $this->input->get('ticket_id'); ?></div>
                        <!-- <div class="text-gray-600 text-xs sm:text-sm">Hey, I am using chat <span class="mx-1">•</span> Online</div> -->
                    </div>
                </div>
                <div class="flex items-center sm:ml-auto mt-5 sm:mt-0 border-t sm:border-0 border-gray-200 pt-3 sm:pt-0 -mx-5 sm:mx-0 px-5 sm:px-0">
                    <a href="<?php echo base_url(); ?>user/ticket/tickets_list?type=all" class="button text-white bg-theme-1">Back </a>
                </div>
            </div>
            <div class="overflow-y-scroll scrollbar-hidden px-5 pt-5 flex-1" id="message_chat_body">
                <!--  -->
            </div>
            <!-- <form action="<?php echo base_url(); ?>user/ticket/submit_ticket_reply" method="POST"> -->
            <form method="POST">
                <div class="pt-4 pb-10 sm:py-4 flex items-center border-t border-gray-200 dark:border-dark-5">
                    <input type="hidden" id="ticket_id" name="ticket_id" value="<?= $this->input->get('ticket_id') ?>">
                    <textarea required id="description" name="description" class="chat__box__input input dark:bg-dark-3 w-full h-16 resize-none border-transparent px-5 py-3 focus:shadow-none" rows="1" placeholder="Type your message..."></textarea>
                    <button type="submit" onclick="submit_reply();" class="w-8 h-8 sm:w-10 sm:h-10 block bg-theme-1 text-white rounded-full flex-none flex items-center justify-center mr-5"> <i data-feather="send" class="w-4 h-4"></i> </a>
                </div>
            </form>
        </div>
        <!-- END: Chat Active -->
    </div>
</div>
<!-- END: Chat Content -->
<?php $this->load->view('user/common/footer'); ?>
<script>
    // setTimeout(function() {
        getMessageByTicketID();
    // }, 600);

    function submit_reply() {
        var description = $("#description").val();

        if (description.trim() == '') {
            return;
        } else {
            var ticket_id = $("#ticket_id").val();

            var postData = "ticket_id=" + ticket_id + "&description=" + description;

            var url = '<?php echo base_url(); ?>user/ticket/submit_ticket_reply';

            $.ajax({
                type: 'POST',
                url: url,
                data: postData,
                dataType: 'json',
                success: function(data) {
                    var message_html = '';

                    var base_url_n = '<?php echo base_url(); ?>';
                    message_html += '<div class="chat__box__text-box flex items-end float-right mb-4">';
                    message_html += '<div class="bg-theme-1 px-4 py-3 text-white rounded-l-md rounded-t-md">';
                    message_html += data.message;
                    message_html += '<div class="mt-1 text-xs text-theme-25">';
                    message_html += data.time;
                    message_html += '</div>';
                    message_html += '</div>';
                    message_html += '<div class="w-10 h-10 hidden sm:block flex-none image-fit relative ml-5">';
                    message_html += '<img src="' + base_url_n + 'assets/user_panel/images/user.png" class="rounded-full" alt="avatar"> <span class="avatar-status bg-teal"></span></div>';
                    message_html += '</div>';
                    message_html += '</div>';
                    message_html += '<div class="clear-both"></div>';

                    $("#message_chat_body").append(message_html);

                    $("#description").val('');
                }
            });
        }
    }

    function getMessageByTicketID() {
        var ticket_id = $("#ticket_id").val();

        var postData = "ticket_id=" + ticket_id;

        var url = '<?php echo base_url(); ?>user/ticket/getMessageByTicketID';

        $.ajax({
            async: false,
            type: 'POST',
            url: url,
            data: postData,
            dataType: 'json',
            success: function(data) {
                $("#message_chat_body").html(data.message_html);
            }
        });

    }
</script>