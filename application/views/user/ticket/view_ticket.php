<?php $this->load->view('user/common/header'); ?>
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2><?php if (!empty($title)) echo $title; ?></h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php if (!empty($title)) echo $title; ?>

                            <a href="<?php echo base_url(); ?>user/ticket/tickets_list?type=all" class="btn btn-primary pull-right">Back</a>
                        </h2>

                    </div>
                    <div class="body table-responsive">



                        <table id="myTable" class="table table-bordered table-hover" cellspacing="0" width="100%">

                            <tr>
                                <td><strong>Ticket ID:</strong></td>
                                <td>
                                    <?php echo $data_result->ticket_id; ?>
                                </td>
                            </tr>

                            <tr>
                                <td><strong>First Name:</strong></td>
                                <td><?php echo $data_result->first_name; ?></td>
                            </tr>

                            <tr>
                                <td><strong>Member Id:</strong></td>
                                <td><?php echo $data_result->last_name; ?></td>
                            </tr>

                            <tr>
                                <td><strong>Email:</strong></td>
                                <td><?php echo $data_result->email; ?></td>
                            </tr>

                            <tr>
                                <td><strong>Attachment:</strong></td>
                                <td>
                                    <?php
                                    if (!empty($data_result->attachment)) {
                                    ?>
                                        <a href="<?php echo base_url(); ?>uploads/ticket_attachment/<?php echo $data_result->attachment; ?>" class="btn btn-primary" download="">Download</a>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <td><strong>Subject:</strong></td>
                                <td><?php echo $data_result->subject; ?></td>
                            </tr>

                            <tr>
                                <td><strong>Description:</strong></td>
                                <td><?php echo nl2br($data_result->description); ?></td>
                            </tr>

                            <tr>
                                <td><strong>status:</strong></td>
                                <td>
                                    <?php
                                    if ($data_result->status == 'open') {
                                    ?>
                                        <a href="javascript:void(0);" class="label label-default">Open</a>
                                    <?php
                                    } else if ($data_result->status == 'process') {
                                    ?>
                                        <a href="javascript:void(0);" class="label label-danger">In Process</a>
                                    <?php
                                    } else if ($data_result->status == 'close') {
                                    ?>
                                        <a href="javascript:void(0);" class="label label-success">Closed</a>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>


                            <tr>
                                <td><strong>Created Date:</strong></td>
                                <td><?php echo date('d-m-Y H:i A', strtotime($data_result->modified_datetime)); ?></td>
                            </tr>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
    </div>
</section>

</body>

</html>

<?php $this->load->view('user/common/footer'); ?>