<?php $this->load->view('user/common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/dist/css/style.css?v=' . rand()) ?>" />
<button style=" margin-top: 20px; width: auto;" class="button w-20 bg-theme-1 text-white ml-auto"><a href="<?= base_url('user/ticket/create_ticket') ?>">Back</a></button>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 lg:col-span-12">
        <div class="intro-y box mt-5">
            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200">
                <h2 class="font-medium text-base mr-auto">
                    <?php if (!empty($title)) echo $title; ?>(<?= $total_rec ?>)
                </h2>
            </div>
            <div class="p-5" id="striped-rows-table">
                <div class="preview">
                    <div>
                        <table id="table">

                            <thead>
                                <tr>
                                    <th scope="col">Sr.No</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Ticket Id</th>
                                    <th scope="col">Subject</th>
                                    <th scope="col">Satus</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($data_result as $row) {
                                ?>
                                    <tr class="tblsrch" id="tr_<?php echo $row->id; ?>" class="bg-gray-200 dark:bg-dark-1">
                                        <td data-label="sr.No"><?= $count++; ?>.</td>
                                        <td data-label="Date"><?php echo date('d M, Y H:i', strtotime($row->created_datetime)); ?></td>
                                        <td data-label="Ticket Id"><?php echo $row->ticket_id; ?></td>
                                        <td data-label="Subject"><?php echo $row->subject; ?></td>
                                        <td data-label="Status"><?php
                                                                if ($row->status == 'open') {
                                                                ?>
                                                <a href="javascript:void(0);" class="text-info"><b>Open</b></a>
                                            <?php
                                                                } else if ($row->status == 'process') {
                                            ?>
                                                <a href="javascript:void(0);" class="text-warning"><b>In Process</a>
                                            <?php
                                                                } else if ($row->status == 'close') {
                                            ?>
                                                <a href="javascript:void(0);" class="text-success"><b>Closed</b></a>
                                            <?php
                                                                }
                                            ?>
                                        </td>
                                        <td class="table-border">
                                            <?php
                                            if ($row->status == 'open' || $row->status == 'process') { ?>
                                                <a href="<?php echo base_url(); ?>user/ticket/view_chat?ticket_id=<?php echo $row->ticket_id; ?>" class="btn btn-success"> Conversation</a>
                                            <?php } ?>
                                        </td>

                                    </tr>
                                <?php
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Striped Rows -->
    </div>
</div>
<?php $this->load->view('user/common/footer'); ?>