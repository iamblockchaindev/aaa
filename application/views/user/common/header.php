<?php
$session_data = getSessionData();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://crypto-admin-templates.multipurposethemes.com/sass/bs5/images/favicon.ico">

    <title>Crypto Admin - Responsive Bootstrap Admin HTML Templates + Bitcoin Dashboards + ICO </title>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <!-- Vendors Style-->
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/css/vendors_css.css') ?>">

    <!-- Style-->
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/css/style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/css/skin_color.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/css/app.css') ?>">

    <script>
        // Add the following into your HEAD section
        var timer = 0;

        function set_interval() {
            // the interval 'timer' is set as soon as the page loads
            timer = setInterval("auto_logout()", 300000);
            // the figure '10000' above indicates how many milliseconds the timer be set to.
            // Eg: to set it to 5 mins, calculate 5min = 5x60 = 300 sec = 300,000 millisec.
            // So set it to 300000
        }

        function reset_interval() {
            //resets the timer. The timer is reset on each of the below events:
            // 1. mousemove   2. mouseclick   3. key press 4. scroliing
            //first step: clear the existing timer

            if (timer != 0) {
                clearInterval(timer);
                timer = 0;
                // second step: implement the timer again
                timer = setInterval("auto_logout()", 300000);
                // completed the reset of the timer
            }
        }

        function auto_logout() {
            // this function will redirect the user to the logout script
            // window.location = "<?php echo base_url('logout'); ?>";
        }
    </script>

    <script src="<?= base_url('assets/user_panel/plugins/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/user_panel/js/angular.min.js') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-datatables/0.6.4/angular-datatables.min.js"></script>

    <script>
        var BASE_URL = "<?php echo base_url(); ?>";
    </script>

</head>

<body class="hold-transition dark-skin sidebar-mini theme-warning fixed">
    <div class="wrapper">
        <div id="loader"></div>

        <header class="main-header">
            <div class="d-flex align-items-center logo-box justify-content-start">
                <!-- Logo -->
                <a href="index.html" class="logo">
                    <!-- logo-->
                    <div class="logo-mini w-30">
                        <span class="light-logo"><img src="<?= base_url('assets/user_panel/images/logo-letter.png') ?>" alt="logo"></span>
                        <span class="dark-logo"><img src="<?= base_url('assets/user_panel/images/logo-letter.png') ?>" alt="logo"></span>
                    </div>
                    <div class="logo-lg">
                        <span class="light-logo"><img src="<?= base_url('assets/user_panel/images/logo-dark-text.png') ?>" alt="logo"></span>
                        <span class="dark-logo"><img src="<?= base_url('assets/user_panel/images/logo-light-text.png') ?>" alt="logo"></span>
                    </div>
                </a>
            </div>
            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <div class="app-menu">
                    <!-- <ul class="header-megamenu nav">
                        <li class="btn-group nav-item">
                            <a href="#" class="waves-effect waves-light nav-link push-btn btn-primary-light" data-toggle="push-menu" role="button">
                                <i data-feather="align-left"></i>
                            </a>
                        </li>
                        <li class="btn-group nav-item d-none d-xl-inline-block">
                            <a href="contact_app_chat.html" class="waves-effect waves-light nav-link svg-bt-icon btn-primary-light" title="Chat">
                                <i data-feather="message-circle"></i>
                            </a>
                        </li>
                        <li class="btn-group nav-item d-none d-xl-inline-block">
                            <a href="mailbox.html" class="waves-effect waves-light nav-link svg-bt-icon btn-primary-light" title="Mailbox">
                                <i data-feather="at-sign"></i>
                            </a>
                        </li>
                        <li class="btn-group nav-item d-none d-xl-inline-block">
                            <a href="extra_taskboard.html" class="waves-effect waves-light nav-link svg-bt-icon btn-primary-light" title="Taskboard">
                                <i data-feather="clipboard"></i>
                            </a>
                        </li>
                    </ul> -->
                </div>

                <div class="navbar-custom-menu r-side">
                    <ul class="nav navbar-nav">
                        <li class="btn-group nav-item d-lg-inline-flex d-none">
                            <a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link full-screen btn-primary-light" title="Full Screen">
                                <i data-feather="maximize"></i>
                            </a>
                        </li>
                        <!-- Notifications -->
                        <li class="dropdown notifications-menu">
                            <a href="#" class="waves-effect waves-light dropdown-toggle btn-primary-light" data-bs-toggle="dropdown" title="Notifications">
                                <i data-feather="bell"></i>
                            </a>
                            <ul class="dropdown-menu animated bounceIn">

                                <li class="header">
                                    <div class="p-20">
                                        <div class="flexbox">
                                            <div>
                                                <h4 class="mb-0 mt-0">Notifications</h4>
                                            </div>
                                            <div>
                                                <a href="#" class="text-danger">Clear All</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu sm-scrol">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users text-info"></i> Curabitur id eros quis nunc suscipit blandit.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-warning text-warning"></i> Duis malesuada justo eu sapien elementum, in semper diam posuere.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users text-danger"></i> Donec at nisi sit amet tortor commodo porttitor pretium a erat.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-shopping-cart text-success"></i> In gravida mauris et nisi
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user text-danger"></i> Praesent eu lacus in libero dictum fermentum.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user text-primary"></i> Nunc fringilla lorem
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user text-success"></i> Nullam euismod dolor ut quam interdum, at scelerisque ipsum imperdiet.
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#">View all</a>
                                </li>
                            </ul>
                        </li>

                        <!-- User Account-->
                        <li class="dropdown user user-menu">
                            <a href="#" class="waves-effect waves-light dropdown-toggle btn-primary-light" data-bs-toggle="dropdown" title="User">
                                <i data-feather="user"></i>
                            </a>
                            <ul class="dropdown-menu animated flipInX">
                                <li class="user-body">
                                    <a class="dropdown-item" href="#"><i class="ti-user text-muted me-2"></i> Profile</a>
                                    <a class="dropdown-item" href="#"><i class="ti-wallet text-muted me-2"></i> My Wallet</a>
                                    <a class="dropdown-item" href="#"><i class="ti-settings text-muted me-2"></i> Settings</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#"><i class="ti-lock text-muted me-2"></i> Logout</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>

        <aside class="main-sidebar">
            <!-- sidebar-->
            <section class="sidebar position-relative">
                <div class="multinav">
                    <div class="multinav-scroll" style="height: 100%;">
                        <!-- sidebar menu-->
                        <ul class="sidebar-menu" data-widget="tree">
                            <li>
                                <a href="<?= base_url('user/dashboard') ?>">
                                    <i class="fa fa-tachometer"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-cogs"></i>
                                    <span>User Settings</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-right pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li>
                                        <a href="<?= base_url('user/account/view_profile') ?>">
                                            <i class="fa fa-user-circle-o"></i>
                                            <span class="pl-1">Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('user/account/change_password') ?>">
                                            <i class="fa fa-lock"></i>
                                            <span class="pl-1">Change Password</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('user/account/set_transaction_password') ?>">
                                            <i class="fa fa-lock"></i>
                                            <span class="pl-1">Transaction Password</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('user/account/document_verification') ?>">
                                            <i class="fa fa-file-text-o"></i>
                                            <span class="pl-1">Document Vefification</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('user/account/eth_address') ?>">
                                            <i class="fa fa-address-book-o"></i>
                                            <span class="pl-1">XYZ Address</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('user/account/verification') ?>">
                                            <i class="fa fa-shield"></i>
                                            <span class="pl-1">2FA Verification</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-users"></i>
                                    <span>Users</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-right pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li>
                                        <a href="<?= base_url('user/genealogy/level') ?>">
                                            <i class="fa fa-level-up"></i>
                                            <span class="pl-1">Level Tree</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('user/genealogy/users') ?>">
                                            <i class="fa fa-users"></i>
                                            <span class="pl-1">Users</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="<?= base_url('user/fund/add_fund') ?>">
                                    <i class="ti-wallet"></i>
                                    <span>Deposite Fund</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('user/fund/transfer_fund') ?>">
                                    <i class="ti-exchange-vertical"></i>
                                    <span>Transfer Fund</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('user/withdraw/make_withdrawal') ?>">
                                    <i class="ti-money"></i>
                                    <span>Withdraw Fund</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('exchange') ?>">
                                    <i class="ti-reload"></i>
                                    <span>Exchange</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('user/notification/notification_list') ?>">
                                    <i class="fa fa-history"></i>
                                    <span>Transaction History</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('user/ticket/create_ticket') ?>">
                                    <i class="fa fa-ticket"></i>
                                    <span>Ticket</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
        </aside>