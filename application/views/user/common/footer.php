<!-- /.content-wrapper -->
<footer class="main-footer">
    &copy; 2022 <a>ABC</a>. All Rights Reserved.
</footer>



<!-- Vendor JS -->

<script src="<?= base_url('assets/user_panel/js/vendors.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/js/pages/chat-popup.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/icons/feather-icons/feather.min.js') ?>"></script>

<script src="<?= base_url('assets/user_panel/vendor_components/apexcharts-bundle/dist/apexcharts.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/lib/4/core.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/lib/4/charts.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/lib/4/themes/animated.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/vendor_components/Web-Ticker-master/jquery.webticker.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/js/pages/web-ticker.js') ?>"></script>

<script src="<?php echo base_url(); ?>assets/user_panel/js/admin.js"></script>
<script src="<?php echo base_url(); ?>assets/user_panel/js/demo.js"></script>
<script src="<?php echo base_url('assets/user_panel/js/app.js'); ?>"></script>
<script src="<?php echo base_url(); ?>assets/user_panel/js/jquery.validate.js"></script>


<!-- DataTables Js -->
<script src="<?= base_url('assets/user_panel/plugins/datatable/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/plugins/datatable/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/plugins/datatable/js/dataTables.bootstrap.min.js'); ?>"></script>

<script src="<?= base_url('assets/user_panel/plugins/toastr/toastr.min.js?v=' . rand()) ?>"></script>

<script src="<?= base_url('assets/user_panel/js/myapp.js') ?>"></script>

<script src="<?= base_url('assets/user_panel/vendor_components/chart.js-master/Chart.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/js/pages/chartjs-int.js') ?>"></script>


<!-- Crypto Admin App -->
<script src="<?= base_url('assets/user_panel/js/template.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/js/pages/dashboard5.js') ?>"></script>

</body>

<!-- Mirrored from crypto-admin-templates.multipurposethemes.com/sass/bs5/main/index5.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Jun 2022 05:23:17 GMT -->

</html>