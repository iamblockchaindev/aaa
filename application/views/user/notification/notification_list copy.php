
<?php $this->load->view('user/common/header'); ?>
<style>
  table td {
    font-weight: bold;
  }
</style>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/dist/css/style.css?v=' . rand()) ?>" />
<div class="intro-y flex items-center mt-8">
  <h2 class="text-lg font-medium mr-auto">
    Transaction History
  </h2>
</div>
<div class="grid grid-cols-12 gap-6 mt-5">
  <div class="intro-y col-span-12 lg:col-span-12">
    <!-- BEGIN: Striped Rows -->
    <div class="intro-y box mt-5">
      <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200">
        <h2 class="font-medium text-base mr-auto">
          <!-- Transaction History (<?= $total_records ?>) -->
        </h2>
      </div>
      <div class="p-5" id="striped-rows-table">
        <div class="preview">
          <div>
            <table id="server-table">

              <thead>
                <tr>
                  <th scope="col">Sr.No</th>
                  <th scope="col">Date</th>
                  <th scope="col">Credit/Debit</th>
                  <th scope="col">Type</th>
                  <th scope="col">Amount</th>
                  <th scope="col">Balance</th>
                  <th scope="col">Status</th>
                  <th scope="col">Transactions Id</th>
                </tr>
              </thead>
              <tbody>
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Striped Rows -->
  </div>
</div>
<?php $this->load->view('user/common/footer'); ?>
<script>
  //datatables
  table = $('#server-table').DataTable({

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": "<?php echo site_url('user/notification/ajax_list') ?>",
      "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [{
      "targets": [0], //first column / numbering column
      "orderable": false, //set not orderable
    }, ],

  });
</script>
<!-- <script>
    $(document).ready(function() {
        $('#server-table').DataTable({
            "processing": true,
            "serverside": true,
            "order": [],
            "ajax": {
                "url": "<?php echo base_url('user/Notification/getLists/'); ?>";
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
    });
</script> -->