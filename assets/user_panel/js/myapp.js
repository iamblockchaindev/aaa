var PW = angular.module('pw', ['datatables']);
var config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
};

const App_Controller = ($scope, $http) => {

    $scope.message = "Hello";

    $scope.get_auth_status = () => {
        $http.get(BASE_URL + 'user/account/get_auth_status').then((res) => {
            $scope.auths = res.data[0];
            if ($scope.auths.google_auth_status == 'true') {
                $scope.authsuccess = true;
                $scope.google_auth_status = true;
            } else {
                $scope.authsuccess = false;
                $scope.google_auth_status = false;
            }
        })
    }

    $scope.set_auth = () => {

        if ($scope.google_auth_status == true) {
            $http.get(BASE_URL + 'user/account/set_auth_key').then((res) => {
                $scope.auth = res.data;
                $scope.authsuccess = false;
            })
        }
    }

    $scope.check_auth = () => {

        if (!$scope) {
            var data = $.param({
                code: '',
                code1: '',
                code2: '',
                code3: '',
                code4: '',
                code5: '',
                status: 'true'
            })
        } else {
            var data = $.param({
                code: $scope.code,
                code1: $scope.code1,
                code2: $scope.code2,
                code3: $scope.code3,
                code4: $scope.code4,
                code5: $scope.code5,
                status: 'true'
            });
        }
        $http.post(BASE_URL + 'user/account/chcek_auth', data, config).then((res) => {
            if (res.data.success) {
                $scope.code = '';
                $scope.code1 = ''
                $scope.code2 = ''
                $scope.code3 = ''
                $scope.code4 = ''
                $scope.code5 = ''
                $scope.authsuccess = true;
                console.log('success');
            } else {
                console.log('failed');
            }
        })
    }

    $scope.off_auth = () => {

        if (!$scope) {
            var data = $.param({
                code: '',
                code1: '',
                code2: '',
                code3: '',
                code4: '',
                code5: '',
                status: 'false'
            })
        } else {
            var data = $.param({
                code: $scope.code,
                code1: $scope.code1,
                code2: $scope.code2,
                code3: $scope.code3,
                code4: $scope.code4,
                code5: $scope.code5,
                status: 'false'
            });
        }
        $http.post(BASE_URL + 'user/account/chcek_auth', data, config).then((res) => {
            if (res.data.success) {
                $scope.code = '';
                $scope.code1 = ''
                $scope.code2 = ''
                $scope.code3 = ''
                $scope.code4 = ''
                $scope.code5 = ''
                $scope.authsuccess = false;
                $scope.google_auth_status = false;
                console.log('success');
            } else {
                console.log('failed');
            }
        })
    }

}

const Transaction_Controller = ($scope, $http) => {
    $scope.get_history = () => {
        $scope.historyLoader = true;
        $http.get(BASE_URL + 'user/notification/get_history/').then((res) => {
            $scope.transactions = res.data;
            $scope.historyLoader = false;
        })
    }
    $scope.get_history();
}

const Transfer_Controller = ($scope, $http) => {
    $scope.fund_transfer = () => {
        BootstrapDialog.show({
            title: "",
            message: "<p>Do you really want to transfer amount ?</p>",
            buttons: [{
                    label: 'Yes',
                    cssClass: 'bg-theme-1 button mr-1 text-white yes_class',
                    action: function(dialogItself) {

                        // $(".yes_class").attr('disable', true);
                        // $("#submit_button_id").attr('disable', true);
                        $scope.transfer_fund();
                    }
                },
                {
                    label: 'No',
                    cssClass: 'bg-theme-6 button text-white',
                    action: function(dialogItself) {
                        dialogItself.close();
                    }
                }
            ]
        });
    }
    $scope.transfer_fund = () => {
        if (!$scope) {
            var data = $.param({
                amount: '',
                member_id: '',
                transaction_password: ''
            })
        } else {
            var data = $.param({
                amount: $scope.amount,
                member_id: $scope.member_id,
                transaction_password: $scope.transaction_password
            });
        }
        $http.post(BASE_URL + 'user/fund/fund_transfer/', data, config).then((res) => {
            if (res.data.success == true) {
                $scope.get_history();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        })
    }
    $scope.transferLoader = true;
    $http.get(BASE_URL + 'user/fund/get_history/').then((res) => {
        $scope.funds = res.data;
        $scope.transferLoader = false;
        funds = res.data;
    })
}

const User_Controller = ($scope, $http, $filter) => {
    $scope.levels = 0;
    $scope.get_all_active_users = () => {
        $http.get(BASE_URL + 'user/genealogy/get_all_active_users/').then((res) => {
            $scope.levels = res.data;
        })
    }

    $scope.levelLoader = true;
    $scope.get_level_user = (level) => {
        $http.get(BASE_URL + 'user/genealogy/get_level_user/' + level).then((res) => {
            $scope.levelusers = res.data;
            $scope.levelLoader = false;
        })
    }

    $scope.dataLoader = true;
    $scope.get_all_users = () => {
        $http.get(BASE_URL + 'user/genealogy/get_all_users').then((res) => {
            $scope.users = res.data;
            $scope.dataLoader = false;
        })
    }

    $scope.get_level_matrix = () => {
        $scope.matrixLoader = true;
        if (!$scope) {
            var data = $.param({
                package: '',
                level: '',
                start: '',
                end: '',
            })
        } else {
            var data = $.param({
                package: $scope.package,
                level: $scope.level,
                start: $filter('date')($scope.start, "yyyy-MM-dd"),
                end: $filter('date')($scope.end, "yyyy-MM-dd"),
            });
        }
        $http.post(BASE_URL + 'admin/user/get_level_matrix', data, config).then((res) => {

            $scope.trees = res.data;
            $scope.matrixLoader = false;

        })
    }

    $scope.submit_data = (form) => {
        $scope.matrixLoader = true;
        if (!form.$invalid) {
            if (!$scope) {
                var data = $.param({
                    level: '',
                    package: '',
                    poollevel: '',
                    start: '',
                    end: ''

                })
            } else {
                var data = $.param({
                    level: $scope.level,
                    package: $scope.package,
                    poollevel: $scope.pool_level,
                    start: $filter('date')($scope.start, "yyyy-MM-dd"),
                    end: $filter('date')($scope.end, "yyyy-MM-dd"),
                    // users: $scope.trees
                });
            }
            $http.post(BASE_URL + 'admin/user/set_pool_income/', data, config).then((res) => {
                $scope.matrixLoader = false;;
                if (res.data.success == true) {
                    toastr.success(res.data.message);
                    $scope.get_level_matrix()
                } else {
                    toastr.error(res.data.message);
                }

            })
        } else {
            toastr.error('Field is required');
        }
    }

    $scope.generate_jade_income = (form) => {
        $scope.matrixLoader = true;
        if (!form.$invalid) {
            if (!$scope) {
                var data = $.param({
                    level: '',
                    package: '',
                    poollevel: '',
                    start: '',
                    end: ''

                })
            } else {
                var data = $.param({
                    level: $scope.level,
                    package: $scope.package,
                    poollevel: $scope.pool_level,
                    start: $filter('date')($scope.start, "yyyy-MM-dd"),
                    end: $filter('date')($scope.end, "yyyy-MM-dd"),
                    // users: $scope.trees
                });
            }
            $http.post(BASE_URL + 'admin/user/generate_jade_income/', data, config).then((res) => {
                $scope.matrixLoader = false;;
                if (res.data.success == true) {
                    toastr.success(res.data.message);
                    $scope.get_level_matrix()
                } else {
                    toastr.error(res.data.message);
                }

            })
        } else {
            toastr.error('Field is required');
        }
    }

    $scope.get_distribution_history = () => {
        $scope.dataLoader = true;
        $http.get(BASE_URL + 'admin/income_distribution/get_distribution_history').then((res) => {
            $scope.history = res.data;
            $scope.dataLoader = false;
        })
    }

    $scope.get_total_users = (level, package) => {
        $scope.dataLoader = true;
        $http.get(BASE_URL + 'admin/user/get_total_users/' + level + '/' + package).then((res) => {
            $scope.trees_count = res.data.count;
            $scope.dataLoader = false;
        })
    }

    $scope.get_user_matrix = () => {
        $scope.matrixLoader = true;
        if (!$scope) {
            var data = $.param({
                user: '',
                package: '',
                // level: '',
                start: '',
                end: '',
            })
        } else {
            var data = $.param({
                user: $scope.user,
                package: $scope.package,
                // level: $scope.level,
                start: $filter('date')($scope.start, "yyyy-MM-dd"),
                end: $filter('date')($scope.end, "yyyy-MM-dd"),
            });
        }
        $http.post(BASE_URL + 'admin/user/get_user_matrix', data, config).then((res) => {

            $scope.trees = res.data;
            $scope.matrixLoader = false;

        })
    }

    $scope.generate_user_income = (form) => {
        $scope.matrixLoader = true;
        if (!form.$invalid) {
            if (!$scope) {
                var data = $.param({
                    user: '',
                    package: '',
                    poollevel: '',
                    start: '',
                    end: ''

                })
            } else {
                var data = $.param({
                    user: $scope.user,
                    package: $scope.package,
                    poollevel: $scope.pool_level,
                    start: $filter('date')($scope.start, "yyyy-MM-dd"),
                    end: $filter('date')($scope.end, "yyyy-MM-dd"),
                });
            }
            $http.post(BASE_URL + 'admin/user/generate_user_income/', data, config).then((res) => {
                $scope.matrixLoader = false;;
                if (res.data.success == true) {
                    toastr.success(res.data.message);
                    $scope.get_user_matrix()
                } else {
                    toastr.error(res.data.message);
                }

            })
        } else {
            toastr.error('Field is required');
        }
    }

    $scope.get_user_data = () => {
        $scope.matrixLoader = true;
        if (!$scope) {
            var data = $.param({
                user: '',
                package: '',
            })
        } else {
            var data = $.param({
                user: $scope.user,
                package: $scope.package,
            });
        }
        $http.post(BASE_URL + 'admin/user/get_user_data', data, config).then((res) => {
            $scope.trees = res.data;
            $scope.matrixLoader = false;

        })
    }

    $scope.generate_pool_income = (form) => {
        $scope.matrixLoader = true;
        if (!form.$invalid) {
            if (!$scope) {
                var data = $.param({
                    user: '',
                    package: '',
                    poollevel: '',

                })
            } else {
                var data = $.param({
                    user: $scope.user,
                    package: $scope.package,
                    poollevel: $scope.pool_level,
                });
            }
            $http.post(BASE_URL + 'admin/user/generate_pool_income/', data, config).then((res) => {
                $scope.matrixLoader = false;;
                if (res.data.success == true) {
                    toastr.success(res.data.message);
                    $scope.get_user_data()
                } else {
                    toastr.error(res.data.message);
                }

            })
        } else {
            toastr.error('Field is required');
        }
    }

}
PW.controller('App_Controller', App_Controller);
PW.controller('Transaction_Controller', Transaction_Controller);
PW.controller('Transfer_Controller', Transfer_Controller);
PW.controller('User_Controller', User_Controller);

// PW.filter("myfilter", function($filter) {
//     return function(items, from, to) {
//         return $filter('filter')(items, "name", function(v) {
//             var date = moment(v);
//             return date >= moment(from) && date <= moment(to);
//         });
//     };
// });