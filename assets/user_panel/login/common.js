var pageNo = 1;

function pagingAjax(e, t, n) {
    showTdiv();
    $.ajax({
        url: t,
        cache: false,
        success: function(data) {
            hideTdiv();
            $("#" + n).html(data);
            $('html, body').animate({ scrollTop: $('#' + n).offset().top - 200 }, '500', 'swing', function() {});
        }
    });
}

function showTdiv() {
    $('.overlay').show();
}
$(".removescroll").click(function() {
    $('body').css('overflow', 'scroll');
});


function hideTdiv() {
    $('.overlay').hide();
}

function keyupOtp(e) {
    if (e.which === 13) {
        checkOtp();
    }
}

function keyupOtpCheck(e) {
    if (e.which == 13) {
        checkOtpLogin();
    }
}

function keyupOtpSignCheck(e) {
    if (e.which === 13) {
        signupCheckOtp();
    }
}
$(document).ready(function(e) {
    $("#sponsor_id").focusin(function() {
        $(this).removeClass('error1');
        $("#member_name_section").text("");
    });
    $("#fname").focusin(function() {
        $(this).removeClass('error1');
        $("#error_first_name").text("");
    });
    $("#lname").focusin(function() {
        $(this).removeClass('error1');
        $("#error_last_name").text("");
    });
    $("#email").focusin(function() {
        $(this).removeClass('error1');
        $("#error_email").text("");
    });
    $("#password").focusin(function() {
        $(this).removeClass('error1');
        $("#error_passwordUp").text("");
    });
    $("#confirm_password").focusin(function() {
        $(this).removeClass('error1');
        $("#error_cpassword").text("");
    });

    function focusOtp() {
        $("#otp_Error").html("");
    }

    $("#signUpForm").submit(function(e) {
        e.preventDefault();
        var retVal = checkValidate();
        if (retVal > 0) {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: "login/signup",
            data: $("#signUpForm").serialize(),
            beforeSend: function() {
                // $("#signUpSubmit span").html("Please Wait...");
                $('#signUpSubmit').prop('disabled', 'disabled');
            },
            success: function(result) {
                $("#signUpSubmit span").html("Please Wait...")
                $('#signUpSubmit').prop('disabled', false);
                var result = JSON.parse(result);
                if (result['success'] == '1') {
                    // location.href = 'login';
                    $("#username_span").html(result['username']);
                    $("#password_span").html(result['password']);
                    $('#login_detail_modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                } else if (result['success'] == '2') {
                    $("#signUpSubmit span").html("Register")
                    $("#member_name_section").html('Sponsor not exist !');
                } else if (result['success'] == '3') {
                    $("#signUpSubmit span").html("Register")
                    $("#member_name_section").html('This Sponsor is not upgraded !');
                }
            }
        });
    });
});

$("#terms").change(function() {
    if ($('#terms').prop('checked')) {
        //        $("#signUpSubmit").attr("disabled", false);
        $(".term_cond").removeClass("redClass");
        $(".terms").removeClass("redBack");
        $(".terms__link a").removeClass("redClass");
    } else {
        //        $("#signUpSubmit").attr("disabled", true);
        $(".term_cond").addClass("redClass");
        $(".terms").addClass("redBack");
        $(".terms__link a").addClass("redClass");
    }
});
$('.onlyAlphabet').keydown(function(e) {
    if (e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
            e.preventDefault();
        }
    }
});
$('.onlyNumber').keydown(function(e) {
    var charCode = e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 95 || charCode > 105)) {
        e.preventDefault();
    }
});

function isNumberKey() {
    var charCode = (event.which) ? event.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$(document).ready(function() {
    $("#data_form").submit(function(event) {
        event.preventDefault();
        var user = $("#username").val();
        var pass = $("#lpassword").val();
        var retVal = 0;
        $("#errorMessageOnline").html("");
        if (user == "") {
            $("#username").addClass("error1");
            $("#error_user").html("*Required Field.");
            retVal++
        } else {
            $("#username").removeClass("error1");
            $("#error_user").html("");
        }
        if (pass == "") {
            $("#lpassword").addClass("error1");
            $("#error_pass").html("*Required Field.");
            retVal++
        } else {
            $("#lpassword").removeClass("error1");
            $("#error_pass").html("");
        }
        if (retVal > 0) {
            return false;
        }
        $.ajax({
            url: "login",
            type: 'POST',
            data: $("#data_form").serialize(),
            beforeSend: function() {
                $("#login_btn_Online span").html("Please Wait...");
            },
            success: function(result) {
                var result = JSON.parse(result);
                if (result['success'] == '1') {
                    location.href = 'user/dashboard';
                } else if (result['success'] == '2') {
                    $("#login_btn_Online span").html("Login")
                    $("#errorMessageOnline").html('Invalid Username or Password !');
                } else if (result['success'] == '5') {
                    functionOverseasVarification(result['contact'], result['alpha2Code']);
                } else if (result['success'] == '3') {
                    $("#login_btn_Online span").html("Login")
                    location.href = 'user/account/view_profile';

                } else {
                    $("#login_btn_Online span").html("Login");
                    $("#errorMessageOnline").html("Invalid Username/Password.");
                }
            }
        });

    });

    $("#close_category").click(function() {
        $('input:radio[name=program][value=1]').prop("checked", false);
        $('input:radio[name=adProgram][value=1]').prop("checked", false);
    });

    $("#form_forgot").submit(function(event) {
        event.preventDefault();
        var user = $("#userNameFor").val();
        var retVal = 0;
        if (user == "") {
            $("#error_forgotU").html("*Required Field.");
            retVal++
        } else {
            $("#error_forgotU").html("");
        }
        if (retVal > 0) {
            return false;
        }
        $.ajax({
            url: "forgot_password",
            type: 'POST',
            data: 'action=forgotPassOnline&username=' + user,
            beforeSend: function(e) {
                $('#forgot_btn_Online').attr('disabled', true);
                $('#forgot_btn_Online span').html('Please wait..');
            },
            success: function(e) {
                $('#forgot_btn_Online').attr('disabled', false);
                $('#forgot_btn_Online span').html('SUBMIT');
                var result = JSON.parse(e);
                if (result['success'] == 1) {
                    $("#successForget").html("Please check your email for the password !");
                    // $('#forgotPassModal').modal('hide');

                    // $('#otp_modal').modal('show');
                    $('body').css('overflow', 'hidden');
                    // $("#otp_Error").html("OTP sent");                   

                } else if (result['success'] == 2) {
                    $("#error_forgotU").html("Please enter your registered username !");
                }
            }
        });

    });

    $("#form_reset").submit(function(event) {
        event.preventDefault();
        var user = $("#userNameRe").val();
        var userc = $("#userNameReConfirm").val();
        var retVal = 0;
        if (user == "") {
            $("#error_ResetU").html("*Required Field.");
            retVal++
        } else {
            $("#error_ResetU").html("");
        }
        if (userc == "") {
            $("#error_ResetC").html("*Required Field.");
            retVal++
        } else {
            if (user == userc) {
                $("#error_ResetC").html("");
            } else {
                $("#error_ResetC").html("*Password not match.");
                retVal++;
            }
        }
        if (retVal > 0) {
            return false;
        }
        $.ajax({
            url: "",
            type: 'POST',
            data: $("#form_reset").serialize(),
            beforeSend: function(e) {
                $('#reset_btn_Online').attr('disabled', true);
                $('#reset_btn_Online span').html('Please wait..');
            },
            success: function(result) {
                console.log(result);
                $('#reset_btn_Online').attr('disabled', false);
                $('#reset_btn_Online span').html('SUBMIT');
                var result = JSON.parse(result);
                if (result['success'] == 1) {
                    $("#successmsg").html("Password has been reset successfully login now.");
                    location.href = BASE + 'login'
                } else if (result['success'] == 0) {
                    $("#error_ResetU").html("User Not Exist");
                }
            }
        });

    });

});

function signUpFunction() {
    $("#signUp").trigger("click");
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function checkValidate() {

    var sponsor_id = $.trim($("#sponsor_id").val());
    var name = $.trim($("#fname").val());
    var lname = $.trim($("#lname").val());
    var email = $.trim($("#email").val());
    var passwordUp = $("#password").val();
    var confirm_password = $('#confirm_password').val();
    var chk_err = 0;
    // if ($('#terms').prop('checked')) {} else {
    //     $(".terms").addClass("redBack");
    //     $(".term_cond").addClass("redClass");
    //     $(".terms__link a").addClass("redClass");
    //     chk_err++;
    // }

    if (sponsor_id == "") {
        $("#sponsor_id").addClass("error1");
        $('#member_name_section').addClass('text-danger');
        $("#member_name_section").text("*Please enter Sponcer ID.");
        chk_err++;
    } else {
        $("#member_name_section").text("");
        $('#member_name_section').removeClass('text-danger');
        $("#sponsor_id").removeClass("error1");
    }
    if (name == "") {
        $("#fname").addClass("error1");
        $("#error_first_name").text("*Please enter your first name.");
        chk_err++;
    } else {
        $("#error_first_name").text("");
        $("#fname").removeClass("error1");
    }
    if (lname == "") {
        $("#lname").addClass("error1");
        $("#error_last_name").text("*Please enter your last name.");
        chk_err++;
    } else {
        $("#error_last_name").text("");
        $("#lname").removeClass("error1");
    }
    if (email == "") {
        $("#email").addClass("error1");
        $("#error_email").text("*Please enter your email.");
        chk_err++;
    } else if (!validateEmail(email) && email != '') {
        $("#email").addClass("error1");
        $("#error_email").text("*Please enter valid email.");
        chk_err++;
    } else {
        $("#error_email").text("");
        $("#email").removeClass("error1");
    }
    if (passwordUp == "") {
        $("#passwordUp").addClass("error1");
        $("#error_passwordUp").text("*Please enter your password.");
        chk_err++;
    } else if (passwordUp.length < 6 && passwordUp != "") {
        $("#passwordUp").addClass("error1");
        $("#error_passwordUp").text("*Password should be of minimum 6 digits.");
        chk_err++;
    } else {
        $("#error_passwordUp").text("");
        $("#passwordUp").removeClass("error1");
    }

    if (passwordUp != confirm_password) {
        $("#cpasswordUp").addClass("error1");
        $("#error_cpassword").text("*Password not match.");
        chk_err++;
    } else {
        $("#error_cpassword").text("");
        $("#cpasswordUp").removeClass("");
    }

    return chk_err;
}

function viewPassFunc() {
    var getAtt = $("#confirm_password").attr("type");
    if (getAtt == 'password') {
        $("#confirm_password").attr("type", "text");
    } else if (getAtt == 'text') {
        $("#confirm_password").attr("type", "password");
    }
}

function viewPassFuncIn() {
    var getAtt = $("#lpassword").attr("type");
    if (getAtt == 'password') {
        $("#lpassword").attr("type", "text");
    } else if (getAtt == 'text') {
        $("#lpassword").attr("type", "password");
    }
}

$('#programSelect').change(function() {
    $("#error_program").text("");
    $("#sub_cat_name").text("");
    //$('#sub_cat_id').val('');
    var val = $('#programSelect').val();
    //    console.log(val);
    //    if (val == 1) {
    //        $('#ielts_modal').modal('show');
    //    }
});